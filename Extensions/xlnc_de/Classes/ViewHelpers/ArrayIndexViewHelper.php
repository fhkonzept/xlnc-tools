<?php
namespace Fh\XlncDe\ViewHelpers;

/**
 * Class ArrayIndexViewHelper
 *
 * @author Max Busch, Carsten Walther
 * @package Fh\XlncDe\ViewHelpers
 */
class ArrayIndexViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @param $object  Object|array Objekt oder Array in dem der Index ist
     * @param $index string Index auf den zugegriffen werden soll
     * @param $prop string Index auf den zugegriffen werden soll
     * @return mixed
     */
    public function render($object, $index = '', $prop = '')
    {
        if(is_object($object)) {
            return $object->$prop;
        }
        elseif(is_array($object)) {
            if(array_key_exists($index, $object)) {
                return $object[$index];
            }
        }
        return NULL;
    }
}