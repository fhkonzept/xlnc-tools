<?php
namespace Fh\XlncDe\ViewHelpers;

/**
 * renders a child (given by uid) of a grid (given by data)
 *
 * @author Max Busch, Christian Rath
 * @package Fh\XlncDe\ViewHelpers
 */
class GridChildViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * @param $data     array GridElements data array containing tx_gridlements_view_child_XXX
     * @param $childUid int uid of child to be returned
     * @return          string
     */
    public function render($data, $childUid)
    {
        return $data["tx_gridelements_view_child_" . $childUid];
    }

}