<?php
namespace Fh\XlncDe\ViewHelpers;

/**
 * renders a child (given by uid) of a grid (given by data)
 *
 * @author Max Busch, Christian Rath
 * @package Fh\XlncDe\ViewHelpers
 */
class InGroupsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    
    /**
     * @param $elements array mit Elementen
     * @param $size int Gruppengröße
     * @param $fillLastChunk bool wenn TRUE, Auffüllen von arrays < $size
     * @return array
     */
    public function render($elements, $size = 3, $fillLastChunk = FALSE)
    {
        $chunks = array_chunk($elements, $size, true);
        
        if ($fillLastChunk) {
        	$lastChunkIndex = count($chunks) - 1;
        	$lastChunk = $chunks[$lastChunkIndex];

	    	$num = count($lastChunk);

	    	for ( $i = $num; $i < $size; $i++ ) { 
	    		$lastChunk[] = NULL;
	    	}

	    	$chunks[$lastChunkIndex] = $lastChunk;
        }
    	
    	return $chunks;
        
    }

}