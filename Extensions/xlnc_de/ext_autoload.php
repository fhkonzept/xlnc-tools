<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$extensionPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('xlnc_de');

return array(
    'Fh\XlncDe\ViewHelpers\ArrayIndexViewHelper' => $extensionPath . 'Classes/ViewHelpers/ArrayIndexViewHelper.php'
);