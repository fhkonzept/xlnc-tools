config {
	sys_language_uid = 0
	language = de
	sys_language_isocode = de
	sys_language_isocode_default = de
	htmltag_LangKey = de
}
page.config {
	sys_language_uid = 0
	language = de
	sys_language_isocode = de
	sys_language_isocode_default = de
	htmltag_LangKey = de
}

[globalVar = GP:L = 1]
config {
	sys_language_uid = 1
	language = en
	sys_language_isocode = en
	htmltag_LangKey = en
}
page.config {
	sys_language_uid = 1
	language = en
	sys_language_isocode = en
	htmltag_LangKey = en
}
[end]
