<?php

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'Pfeil mit Link',
        'arrowLink',
        'content-header'
    ],
    'textmedia',
    'after'
);

$GLOBALS['TCA']['tt_content']['types']['arrowLink'] = [
    'showitem'         => '
        --palette--;;general,header;Title,header_link,tx_gridelements_container,tx_gridelements_columns
    '
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'Referenz Bild',
        'referenceImg',
        'content-header'
    ],
    'textmedia',
    'after'
);

$GLOBALS['TCA']['tt_content']['types']['referenceImg']['textmedia'] = [
    'showitem'         => 'assets'

];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'Text auf weißem Kasten',
        'whiteBox',
        'content-header'
    ],
    'textmedia',
    'after'
);

$GLOBALS['TCA']['tt_content']['types']['whiteBox'] = $GLOBALS['TCA']['tt_content']['types']['textmedia'];
