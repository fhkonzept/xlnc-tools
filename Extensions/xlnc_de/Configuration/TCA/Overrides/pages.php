<?php
defined('TYPO3_MODE') or die();

// Add pageTSconfig
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
	'xlnc_de',
	'Configuration/PageTSconfig/PageLayouts.txt',
	'XLNC Page Layouts'
);
