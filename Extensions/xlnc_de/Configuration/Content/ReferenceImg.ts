tt_content {
    referenceImg < lib.fluidContent
    referenceImg {
        templateName = ReferenceImg
        dataProcessing {
            10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
            10 {
                    references.fieldName = assets
            }
            20 = TYPO3\CMS\Frontend\DataProcessing\GalleryProcessor
            20 {
                    maxGalleryWidth = {$styles.content.textmedia.maxW}
                    maxGalleryWidthInText = {$styles.content.textmedia.maxWInText}
                    columnSpacing = {$styles.content.textmedia.columnSpacing}
                    borderWidth = {$styles.content.textmedia.borderWidth}
                    borderPadding = {$styles.content.textmedia.borderPadding}
            }
        }

    }
}
