mod.wizards.newContentElement.wizardItems.common {
    elements {
        arrowLink {
            iconIdentifier = content-header
            title = Link mit Pfeil
            description = Link mit Pfeil für die Startseite
            tt_content_defValues {
                CType = arrowLink
            }
        }
        referenceImg {
            iconIdentifier = content-header
            title = Referenz Bild
            description = Bild für die Referenz Gallarie
            tt_content_defValues {
                CType = referenceImg
            }
        }
        whiteBox {
            iconIdentifier = content-header
            title = Text auf weißem Kasten
            description = Text auf weißem Kasten für Header-Bilder
            tt_content_defValues {
                CType = whiteBox
            }
        }
    }
    show := addToList(arrowLink,whiteBox,referenceImg)
}

RTE.default.contentCSS = EXT:xlnc_de/Resources/Public/Css/RteContentCss.css
RTE.classes {
    arrow-list {
        name = Liste mit Pfeilen
        value = color: red;
    }
}
RTE.default.proc.allowedClasses := addToList(arrowList)

TCEMAIN.table.pages.disablePrependAtCopy = 1
TCEMAIN.table.tt_content.disablePrependAtCopy = 1


TCEMAIN.table.pages.disableHideAtCopy = 1
TCEMAIN.table.tt_content.disableHideAtCopy = 1