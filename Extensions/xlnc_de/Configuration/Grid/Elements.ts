# we don't need the headline
# tt_content.gridelements_pi1.10 =< lib.stdheader

tt_content.gridelements_pi1.20.10.setup {

    carousel < lib.gridelements.defaultGridSetup
    carousel {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/Carousel.html
        }
    }

    site_picture < lib.gridelements.defaultGridSetup
    site_picture {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

        templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
        partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
        layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            file = EXT:xlnc_de/Resources/Private/Templates/Grid/SitePicture.html
        }
    }

    site_picture_center < lib.gridelements.defaultGridSetup
    site_picture_center {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

        templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
        partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
        layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            file = EXT:xlnc_de/Resources/Private/Templates/Grid/SitePictureCenter.html
        }
    }

    header_slider < lib.gridelements.defaultGridSetup
    header_slider {


        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

        templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
        partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
        layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            file = EXT:xlnc_de/Resources/Private/Templates/Grid/HeaderSlider.html
        }
    }

    header_slider_item < lib.gridelements.defaultGridSetup
    header_slider_item {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

        templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
        partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
        layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            file = EXT:xlnc_de/Resources/Private/Templates/Grid/HeaderSliderItem.html
        }
    }

    header_slider_item_centre < lib.gridelements.defaultGridSetup
    header_slider_item_centre {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

        templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
        partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
        layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            file = EXT:xlnc_de/Resources/Private/Templates/Grid/HeaderSliderItemCenter.html
        }
    }

    column_one < lib.gridelements.defaultGridSetup
    column_one {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/ColumnOne.html
        }
    }

    profile < lib.gridelements.defaultGridSetup
    profile {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/Profile.html
        }
    }

    roof_line < lib.gridelements.defaultGridSetup
    roof_line {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/RoofLine.html
        }
    }

    referenceGallery < lib.gridelements.defaultGridSetup
    referenceGallery {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/ReferenceGallery.html
        }
    }

    reference < lib.gridelements.defaultGridSetup
    reference {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/Reference.html
        }
    }

    seperator_line < lib.gridelements.defaultGridSetup
    seperator_line {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/SeperatorLine.html
        }
    }

    column_one_facelift < lib.gridelements.defaultGridSetup
    column_one_facelift {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/ColumnOneFacelift.html
        }
    }

    column_one_ten_facelift < lib.gridelements.defaultGridSetup
    column_one_ten_facelift {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/ColumnOneTenFacelift.html
        }
    }

    column_one_block_facelift < lib.gridelements.defaultGridSetup
    column_one_block_facelift {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/ColumnOneBlockFacelift.html
        }
    }

    column_two < lib.gridelements.defaultGridSetup
    column_two {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/ColumnTwo.html
        }
    }
    column_two_facelift < lib.gridelements.defaultGridSetup
    column_two_facelift {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/ColumnTwoFacelift.html
        }
    }

    column_three_facelift < lib.gridelements.defaultGridSetup
    column_three_facelift {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/ColumnThreeFacelift.html
        }
    }

    column_three < lib.gridelements.defaultGridSetup
    column_three {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/ColumnThree.html
        }
    }

    column_five_three < lib.gridelements.defaultGridSetup
    column_five_three {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/ColumnFiveThreeFacelift.html
        }
    }

    column_three_without_container < lib.gridelements.defaultGridSetup
    column_three_without_container {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/ColumnThreeWithoutContainer.html
        }
    }

    green_content < lib.gridelements.defaultGridSetup
    green_content {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/GreenContent.html
        }
    }

    gray_background < lib.gridelements.defaultGridSetup
    gray_background {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/GrayBackground.html
        }
    }

    no_background < lib.gridelements.defaultGridSetup
    no_background {

        # fluidtemplate rendering
        cObject = FLUIDTEMPLATE
        cObject {

            templateRootPath = EXT:xlnc_de/Resources/Private/Templates/
            partialRootPath = EXT:xlnc_de/Resources/Private/Partials/
            layoutRootPath = EXT:xlnc_de/Resources/Private/Layouts/Grid/

            # template file
            file = EXT:xlnc_de/Resources/Private/Templates/Grid/NoBackground.html
        }
    }

}
