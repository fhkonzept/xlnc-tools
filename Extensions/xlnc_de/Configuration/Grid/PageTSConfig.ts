tx_gridelements {

    # boolean; Usually if the ID of the TSconfig is the same like the record ID of grid elements,
    # the configuration of the TSconfig overrides the record configuration recursively (!).
    # If this option is set the record configuration overrides the TSconfig.
    overruleRecords = 0

    # string; With this option you can disable single layouts. Use the record or TSconfig ID.
    excludeLayoutIds =

    setup {

        #Header_slider
        header_slider {
                    # string; This string will be parsed by the translation function, so "LLL:" can be used.
                    title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow

                    # string; This string will be parsed by the translation function, so "LLL:" can be used.
                    description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.desc

                    # string; "EXT:" can be used here.
                    icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

                    # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
                    frame = 1

                    # boolean; If this option is set the grid element can be set as top level only.
                    topLevelLayout = 0

                    # Here comes the normal configuration which can be made by the wizard of the grid layout record.
                    config {
                        colCount = 1
                        rowCount = 1

                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:site.picture.info
                                        colPos = 1
                                    }
                                }
                            }
                        }
                    }
                }

        #Header_slider
        header_slider_item {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.content

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.desc.content

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 4
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.text
                                colPos = 1
                                allowed = textmedia,whiteBox
                            }
                            2 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.desktop
                                colPos = 2
                                allowed = textmedia
                            }
                            3 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.smartphone
                                colPos = 3
                                allowed = textmedia
                            }
                            4 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.smartphone.landscape
                                colPos = 4
                                allowed = textmedia
                            }
                        }
                    }
                }
            }

            flexformDS = FILE:EXT:xlnc_de/Configuration/Grid/WhiteText.xml
        }

        #Header_slider_item_centre
        header_slider_item_centre {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.content.centre

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.desc.content

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 4
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.text.centre
                                colPos = 1
                                allowed = textmedia,whiteBox
                            }
                            2 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.desktop
                                colPos = 2
                                allowed = textmedia
                            }
                            3 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.smartphone
                                colPos = 3
                                allowed = textmedia
                            }
                            4 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.smartphone.landscape
                                colPos = 4
                                allowed = textmedia
                            }
                        }
                    }
                }
            }

            flexformDS = FILE:EXT:xlnc_de/Configuration/Grid/WhiteText.xml
        }

        # 5-3 Spalten
        column_five_three {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:five.three.title

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:five.three.discription

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = Links
                                rowspan = 2
                                colPos = 1
                            }
                            2 {
                                name = Rechts
                                colPos = 2
                            }
                        }
                    }
                }
            }
        }

        #Header Picture
        site_picture {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:site.picture

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:site.picture.description

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 4
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.text
                                colPos = 1
                                allowed = textmedia,whiteBox
                            }
                            2 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.desktop
                                colPos = 2
                                allowed = textmedia
                            }
                            3 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.smartphone
                                colPos = 3
                                allowed = textmedia
                            }
                            4 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.smartphone.landscape
                                colPos = 4
                                allowed = textmedia
                            }
                        }
                    }
                }
            }

            flexformDS = FILE:EXT:xlnc_de/Configuration/Grid/WhiteText.xml
        }

        site_picture_center {
            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:site.picture_center

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:site.picture_center.description

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 4
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.text
                                colPos = 1
                                allowed = textmedia,whiteBox
                            }
                            2 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.desktop
                                colPos = 2
                                allowed = textmedia
                            }
                            3 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.smartphone
                                colPos = 3
                                allowed = textmedia
                            }
                            4 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:facelift.slideshow.smartphone.landscape
                                colPos = 4
                                allowed = textmedia
                            }
                        }
                    }
                }
            }

            flexformDS = FILE:EXT:xlnc_de/Configuration/Grid/WhiteText.xml
        }

        # Dachzeile
        roof_line {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:roofline.content

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:roofline.content.description

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:roofline.content.description.col
                                colPos = 1
                                allowed = header
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }


        # reference Gallery
        referenceGallery {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:referenceGallery.content

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:referenceGallery.content.description

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:referenceGallery.content.description.col
                                colPos = 1

                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }

        # reference
        reference {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:reference.content

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:reference.content.description

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:reference.content.description.col1
                                colPos = 1

                            }
                            2 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:reference.content.description.col2
                                colPos = 2

                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }


        # Trennlinie
        seperator_line {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:seperatorline.content

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:seperatorline.content.description

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 0
                rowCount = 0

            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }

        # 1 Spalte Facelift
        column_one_facelift {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.facelift

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.description.facelift

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.col.1.facelift
                                colPos = 1
                                allowed =
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }
        # 1 Spalte Facelift
        column_one_ten_facelift {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.facelift.ten

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.description.facelift

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.col.1.facelift
                                colPos = 1

                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }
        # 1 Spalte blocksatz Facelift
        column_one_block_facelift {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.block.facelift

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.block.description.facelift

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.block.col.1.facelift
                                colPos = 1

                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }


        # 2 Spalten Facelift
        column_two_facelift {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.two.facelift

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.two.description.facelift

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = col 1
                                colPos = 0

                            }
                            2 {
                                name = col 2
                                colPos = 1

                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }
        # 3 Spalten Facelift
        column_three_facelift {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.three.facelift

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.three.description.facelift

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 3
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = col 1
                                colPos = 0

                            }
                            2 {
                                name = col 2
                                colPos = 1

                            }
                            3 {
                                name = col 3
                                colPos = 2
                                
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }

        # Profile - Über Uns
        profile {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:profile

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:profile.description

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:profile.links
                                colPos = 1
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }

        # 1 Spalte - Kein Hintergrund
        no_background {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:no.content

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:no.content.description

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 2

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:no.content.description.col
                                colPos = 1

                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )

        }

        # 1 Spalte - Grauer Hintergrund
        gray_background {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:gray.content

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:gray.content.description

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 2

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:gray.content.description.col
                                colPos = 1
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )

        }

        # Carousel
        carousel {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:container

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:container.description

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 1

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:container.col.1
                                colPos = 1
                                allowed = textmedia,text,textpic,image
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<T3DataStructure>
    <meta>
        <langDisable>1</langDisable>
    </meta>
    <ROOT>
        <el>
            <interval>
                <TCEforms>
                    <label>Interval (Seconds):</label>
                    <config>
                        <type>input</type>
                        <size>12</size>
                        <default>5</default>
                    </config>
                </TCEforms>
            </interval>
            <pause>
                <TCEforms>
                    <label>Pause on:</label>
                    <config>
                        <type>select</type>
                        <items type="array">
                            <numIndex index="0" type="array">
                                <numIndex index="0"></numIndex>
                                <numIndex index="1"></numIndex>
                            </numIndex>
                            <numIndex index="1" type="array">
                                <numIndex index="0">hover</numIndex>
                                <numIndex index="1">hover</numIndex>
                            </numIndex>
                        </items>
                    </config>
                </TCEforms>
            </pause>
            <effect>
                <TCEforms>
                    <label>Effect:</label>
                    <config>
                        <type>select</type>
                        <items type="array">
                            <numIndex index="0" type="array">
                                <numIndex index="0">slide</numIndex>
                                <numIndex index="1"></numIndex>
                            </numIndex>
                            <numIndex index="1" type="array">
                                <numIndex index="0">fade</numIndex>
                                <numIndex index="1">carousel-fade</numIndex>
                            </numIndex>
                        </items>
                    </config>
                </TCEforms>
            </effect>
        </el>
    </ROOT>
</T3DataStructure>
            )
        }

        # 1 Spalte
        column_one {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.description

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.col.1
                                colPos = 1
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }

        # 2 Spalten
        column_two {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.two

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.description.two

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.col.2
                                colPos = 0
                                allowed = textmedia,text,textpic,image
                            }
                            2 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.col.2
                                colPos = 1
                                allowed = textmedia,text,textpic,image
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }

        # 3 Spalten
        column_three {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.three

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.description.three

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 3
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = Content Links
                                colPos = 1
                            }
                            2 {
                                name = Content Mitte
                                colPos = 2
                            }
                            3 {
                                name = Content Rechts
                                colPos = 3
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }


        column_three_without_container {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.three.ohneContainer

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:spalte.description.three.ohneContainer

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 3

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 3
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = Content Links
                                colPos = 1
                            }
                            2 {
                                name = Content Mitte
                                colPos = 2
                            }
                            3 {
                                name = Content Rechts
                                colPos = 3
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )
        }

        # 1 Spalte - Grüner Hintergrund
        green_content {

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            title = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:green.content

            # string; This string will be parsed by the translation function, so "LLL:" can be used.
            description = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:green.content.description

            # string; "EXT:" can be used here.
            icon = EXT:xlnc_de/Resources/Public/Icons/content-textpic.svg

            # integer; Use a colored frame as in the record form. 0, 1 = red, 2 = green, 3 = blue
            frame = 2

            # boolean; If this option is set the grid element can be set as top level only.
            topLevelLayout = 0

            # Here comes the normal configuration which can be made by the wizard of the grid layout record.
            config {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = LLL:EXT:xlnc_de/Resources/Private/Language/Grid.xlf:green.content.col.1
                                colPos = 1
                                allowed = textmedia,text,textpic,image
                            }
                        }
                    }
                }
            }

            # Set own FlexForm fields in the grid element. This is the default value.
            flexformDS (
            )

    }
}
