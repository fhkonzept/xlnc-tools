(function($) {
    $(function () {
        var slickOpts = {
            dots: true,
            infinite: true,
            speed: 500,
            arrows: true,
            touchMove: true,
            prevArrow: '<button type="button" class="arrow-prev slick-prev"></button>',
            nextArrow: '<button type="button" class="arrow-next slick-next"></button>',
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    arrows: true
                  }
                }
              ]
        };
        // Init the slick
        $('.header_slider').slick(slickOpts);
        var slickEnabled = true;
    });
})(jQuery);
