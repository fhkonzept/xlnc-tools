<?php
$EM_CONF [$_EXTKEY] = array (
		'title' => 'XLNC Tools Site Setup',
		'description' => 'Site core setup incl. TS, CSS, Fluid',
		'category' => 'XLNC Tools',
		'author' => 'fh-konzept GmbH',
		'author_email' => 'info@fh-konzept.de',
		'shy' => 0,
		'dependencies' => 'extbase,fluid',
		'conflicts' => '',
		'priority' => '',
		'module' => '',
		'state' => 'alpha',
		'internal' => '',
		'uploadfolder' => 0,
		'createDirs' => '',
		'modify_tables' => '',
		'clearCacheOnLoad' => 0,
		'lockType' => '',
		'author_company' => 'fh-konzept GmbH',
		'version' => '0.0.1',
		'constraints' => array (
				'depends' => array (
						'php' => '5.4.0-0.0.0',
						'typo3' => '7.5.0-7.6.0'
				),
				'conflicts' => array (),
				'suggests' => array ()
		)
);
?>