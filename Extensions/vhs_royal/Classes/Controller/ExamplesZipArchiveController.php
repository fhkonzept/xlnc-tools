<?php
namespace Fhkonzept\VhsRoyal\Controller;

class ExamplesZipArchiveController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	public function indexAction() {

		$this->settings['dest'] = 'D';

		$this->view->assign('settings', $this->settings);

		$absExportedFilePath = $this->view->render();

		echo $absExportedFilePath;

	}

}