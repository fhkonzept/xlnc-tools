<?php
namespace Fhkonzept\VhsRoyal\Controller;

define("DIRECTION_VERTICAL"		, 690001);
define("DIRECTION_HORIZONTAL"		, 690002);

define("SCALE_POS_LEFTRIGHT"		, 690101);
define("SCALE_POS_TOPBOTTOM"		, 690102);

define("SCALE_MODE_FLOATING"		, 690201);
define("SCALE_MODE_START0"		, 690202);
define("SCALE_MODE_ADDALL"		, 690203);
define("SCALE_MODE_ADDALL_START0"	, 690204);
define("SCALE_MODE_MANUAL"		, 690205);

define("SCALE_SKIP_NONE"		, 690301);
define("SCALE_SKIP_SAME"		, 690302);
define("SCALE_SKIP_NUMBERS"		, 690303);

define("TEXT_ALIGN_TOPLEFT"		, 690401);
define("TEXT_ALIGN_TOPMIDDLE"		, 690402);
define("TEXT_ALIGN_TOPRIGHT"		, 690403);
define("TEXT_ALIGN_MIDDLELEFT"		, 690404);
define("TEXT_ALIGN_MIDDLEMIDDLE"	, 690405);
define("TEXT_ALIGN_MIDDLERIGHT"	, 690406);
define("TEXT_ALIGN_BOTTOMLEFT"		, 690407);
define("TEXT_ALIGN_BOTTOMMIDDLE"	, 690408);
define("TEXT_ALIGN_BOTTOMRIGHT"	, 690409);

define("POSITION_TOP"                  , 690501);
define("POSITION_BOTTOM"               , 690502);

define("LABEL_POS_LEFT"		, 690601);
define("LABEL_POS_CENTER"		, 690602);
define("LABEL_POS_RIGHT"		, 690603);
define("LABEL_POS_TOP"			, 690604);
define("LABEL_POS_BOTTOM"		, 690605);
define("LABEL_POS_INSIDE"		, 690606);
define("LABEL_POS_OUTSIDE"		, 690607);

define("ORIENTATION_HORIZONTAL"	, 690701);
define("ORIENTATION_VERTICAL"		, 690702);
define("ORIENTATION_AUTO"		, 690703);

define("LEGEND_NOBORDER"		, 690800);
define("LEGEND_BOX"			, 690801);
define("LEGEND_ROUND"			, 690802);

define("LEGEND_VERTICAL"		, 690901);
define("LEGEND_HORIZONTAL"		, 690902);

define("LEGEND_FAMILY_BOX"		, 691051);
define("LEGEND_FAMILY_CIRCLE"		, 691052);
define("LEGEND_FAMILY_LINE"		, 691053);

define("DISPLAY_AUTO"			, 691001);
define("DISPLAY_MANUAL"		, 691002);

define("LABELING_ALL"			, 691011);
define("LABELING_DIFFERENT"		, 691012);

define("BOUND_MIN"			, 691021);
define("BOUND_MAX"			, 691022); 
define("BOUND_BOTH"			, 691023);

define("BOUND_LABEL_POS_TOP"		, 691031);
define("BOUND_LABEL_POS_BOTTOM"	, 691032);
define("BOUND_LABEL_POS_AUTO"		, 691033);

define("CAPTION_LEFT_TOP"		, 691041);
define("CAPTION_RIGHT_BOTTOM"		, 691042);

define("GRADIENT_SIMPLE"		, 691051);
define("GRADIENT_EFFECT_CAN"		, 691052);

define("LABEL_TITLE_NOBACKGROUND"	, 691061);
define("LABEL_TITLE_BACKGROUND"	, 691062);

define("LABEL_POINT_NONE"		, 691071);
define("LABEL_POINT_CIRCLE"		, 691072);
define("LABEL_POINT_BOX"		, 691073);

define("ZONE_NAME_ANGLE_AUTO"		, 691081);

define("PI"		, 3.14159265);
define("ALL"		, 69);
define("NONE"		, 31);
define("AUTO"		, 690000);
define("OUT_OF_SIGHT"	, -10000000000000);

define("AXIS_FORMAT_DEFAULT"		, 680001);
define("AXIS_FORMAT_TIME"		, 680002);
define("AXIS_FORMAT_DATE"		, 680003);
define("AXIS_FORMAT_METRIC"		, 680004);
define("AXIS_FORMAT_CURRENCY"		, 680005);
define("AXIS_FORMAT_TRAFFIC"		, 680006);
define("AXIS_FORMAT_CUSTOM"		, 680007);

/* Axis position */
define("AXIS_POSITION_LEFT"		, 681001);
define("AXIS_POSITION_RIGHT"		, 681002);
define("AXIS_POSITION_TOP"		, 681001);
define("AXIS_POSITION_BOTTOM"		, 681002);

/* Families of data points */
define("SERIE_SHAPE_FILLEDCIRCLE"	, 681011);
define("SERIE_SHAPE_FILLEDTRIANGLE"	, 681012);
define("SERIE_SHAPE_FILLEDSQUARE"	, 681013);
define("SERIE_SHAPE_FILLEDDIAMOND"	, 681017);
define("SERIE_SHAPE_CIRCLE"		, 681014);
define("SERIE_SHAPE_TRIANGLE"		, 681015);
define("SERIE_SHAPE_SQUARE"		, 681016);
define("SERIE_SHAPE_DIAMOND"		, 681018);

/* Axis position */
define("AXIS_X"			, 682001);
define("AXIS_Y"			, 682002);

class ExamplesChartController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	public function indexAction() {

		$scaleFactor = 1.2;

		$fontFamily = 'itcavantgardestd-bk-webfont'; // nice, thin
		$fontFamily = 'itcavantgardepro-md-webfont'; // very nice, normal
		#$fontFamily = 'MyriadPro-Regular';
		#$fontFamily = 'fontawesome-webfont';
		#$fontFamily = 'glyphicons-halflings-regular'; // Symbols (Fontawesome/Glyphicons) work with HTML-entities like &#9993; (envelope, see http://www.fileformat.info/info/unicode/char/2709/index.htm), not with Unicode
		$fontR = 100;
		$fontG = 100;
		$fontB = 100;
		$smallFontSize = 9 * $scaleFactor;
		$largeFontSize = 10 * $scaleFactor;
 
		$this->view->assignMultiple(array(
			'settings', $this->settings,

			// Data
			// Serie 1
			'serie1Values'			=> array(10,40,70,45,15,90),
			'serie1Name'			=> 'Serie1',
			'serie1Description'		=> ' Fremdbild - IST ',
			// Serie 2
			'serie2Values'			=> array(31,15,80,56,23,72),
			'serie2Name'			=> 'Serie2',
			'serie2Description'		=> ' Selbstbild - IST',
			// Serie 3
			'serie3Values'			=> array("COACHIV ","NORMATIV ","PARTIZIPATIV ","INTEGRATIV ","INSPIRATIV ","DIREKTIV "),
			'serie3Name'			=> 'Absissa',
			
			// Axis
			'axisPosition'			=> AXIS_POSITION_LEFT,

			// Dimensions and coordinates
			'imageWidth'			=> 720 * $scaleFactor,
			'imageHeight'			=> 510 * $scaleFactor,

			'graphareaX1'			=> 100 * $scaleFactor,
			'graphareaY1'			=> 70 * $scaleFactor,
			'graphareaX2'			=> 650 * $scaleFactor,
			'graphareaY2'			=> 475 * $scaleFactor,

			'gradientarea3X1'		=> 100 * $scaleFactor,
			'gradientarea3Y1'		=> 70 * $scaleFactor,
			'gradientarea3X2'		=> 320 * $scaleFactor,
			'gradientarea3Y2'		=> 475 * $scaleFactor,

			'gradientarea4X1'		=> 320 * $scaleFactor,
			'gradientarea4Y1'		=> 70 * $scaleFactor,
			'gradientarea4X2'		=> 485 * $scaleFactor,
			'gradientarea4Y2'		=> 475 * $scaleFactor,

			'gradientarea5X1'		=> 485 * $scaleFactor,
			'gradientarea5Y1'		=> 70 * $scaleFactor,
			'gradientarea5X2'		=> 650 * $scaleFactor,
			'gradientarea5Y2'		=> 475 * $scaleFactor,

			'threshold1X'			=> 144 * $scaleFactor,
			'threshold1Y'			=> 40 * $scaleFactor,
			'threshold2X'			=> 350 * $scaleFactor,
			'threshold2Y'			=> 36 * $scaleFactor,
			'threshold3X'			=> 514 * $scaleFactor,
			'threshold3Y'			=> 40 * $scaleFactor,

			// Rect
			'rectFormat'			=> array("R"=>0, "G"=>0, "B"=>0, "Dash"=>1, "DashR"=>20, "DashG"=>20, "DashB"=>20),

			// Gradient Backgrounds
			'gradient1Direction'	=> DIRECTION_HORIZONTAL,
			'gradient2Direction'	=> DIRECTION_VERTICAL,
			'gradient1Format'		=> array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>238, "EndG"=>238, "EndB"=>238, "Alpha"=>95),
			'gradient2Format'		=> array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>238, "EndG"=>238, "EndB"=>238, "Alpha"=>95),
			'gradient3Format'		=> array("StartR"=>90, "StartG"=>156, "StartB"=>213, "EndR"=>90, "EndG"=>156, "EndB"=>213, "Alpha"=>30),
			'gradient4Format'		=> array("StartR"=>238, "StartG"=>125, "StartB"=>50, "EndR"=>238, "EndG"=>125, "EndB"=>50, "Alpha"=>30),
			'gradient5Format'		=> array("StartR"=>165, "StartG"=>165, "StartB"=>165, "EndR"=>165, "EndG"=>165, "EndB"=>165, "Alpha"=>30),
			
			// Shadow
			'shadowEnabled'			=> 1,
			'shadowFormat'			=> array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>10),
			
			// Fonts
			'mainFontFormat'		=> array("R"=>$fontR,"G"=>$fontG,"B"=>$fontB,"FontName"=>"typo3conf/ext/vhs_royal/Lib/pChart/fonts/" . $fontFamily . ".ttf","FontSize"=>$smallFontSize),
			
			// Scale Format
			'scaleFormat'			=> array(
										"Pos"=>SCALE_POS_TOPBOTTOM,
										"Mode"=>SCALE_MODE_MANUAL,
										"ManualScale"=>array(0=>array("Min"=>0,"Max"=>100)),
										"LabelingMethod"=>LABELING_ALL,
										"LabelSkip"=>0,
										"GridR"=>180, "GridG"=>180, "GridB"=>180, "GridAlpha"=>50,
										"TickR"=>0, "TickG"=>0, "TickB"=>0, "TickAlpha"=>50, 
										"LabelRotation"=>0, 
										"DrawXLines"=>0, 
										"DrawSubTicks"=>0, 
										"SubTickR"=>255,
										"SubTickG"=>0, 
										"SubTickB"=>0, 
										"SubTickAlpha"=>50, 
										"DrawYLines"=>ALL),
			'axisDisplayType'		=> AXIS_FORMAT_CUSTOM,
			'axisDisplayFunc'		=> 'renderAxisLabel',
			
			// Bar chart format
			'chartFormat'			=> array(
										"DisplayValues"=>1, 
										"Rounded"=>1, 
										"AroundZero"=>1,
										"DisplayPos"=>LABEL_POS_INSIDE,
										"DisplayR"=>255, 
										"DisplayG"=>255, 
										"DisplayB"=>255,
										"DisplayShadow"=>0,
										"Gradient"=>1,
										"Surrounding"=>30
										),
			
			// Legend
			'legendX'				=> 100 * $scaleFactor,
			'legendY'				=> 490 * $scaleFactor,
			'legendFormat'			=> array(
										"FontR"=>$fontR, "FontG"=>$fontG, "FontB"=>$fontB, "FontName"=>"typo3conf/ext/vhs_royal/Lib/pChart/fonts/" . $fontFamily . ".ttf", "FontSize"=>$largeFontSize, 
										"Margin"=>10, 
										"Alpha"=>90, 
										"BoxSize"=>15, 
										"Style"=>LEGEND_NOBORDER,
										"Mode"=>LEGEND_HORIZONTAL,
										"Family"=>LEGEND_FAMILY_BOX
										),
			// Color
			'serie1Palette'			=> array("R"=>214,"G"=>130,"B"=>129,"Alpha"=>90),
			'serie2Palette'			=> array("R"=>184,"G"=>75,"B"=>73,"Alpha"=>90),
			// Threshholds
			'threshold1'			=> 40,
			'threshold2'			=> 70,
			'threshold3'			=> 100,
			'threshold1Format'		=> array("R"=>0, "G"=>0, "B"=>0, "Alpha"=>50, "AxisID"=>0, "Ticks"=>8, "WriteCaption"=>0, "Caption"=>"Entwicklungsbereich", "CaptionOffset"=>-80),
			'threshold2Format'		=> array("R"=>0, "G"=>0, "B"=>0, "Alpha"=>50, "AxisID"=>0, "Ticks"=>8, "WriteCaption"=>0, "Caption"=>"Potenzialbereich", "CaptionOffset"=>-80),
			'threshold3Format'		=> array("R"=>0, "G"=>0, "B"=>0, "Alpha"=>50, "AxisID"=>0, "Ticks"=>8, "WriteCaption"=>0, "Caption"=>"Signaturbereich", "CaptionOffset"=>-80),
			));

	}

	private function test() {

		$myData = new pData();
		$myData->addPoints(array(17,30,20,40,50,15),"Serie1");
		$myData->setSerieDescription("Serie1","Fremdbild - IST");
		$myData->setSerieOnAxis("Serie1",0);

		$myData->addPoints(array(50,13,20,62,70,80,95),"Serie2");
		$myData->setSerieDescription("Serie2","Selbstbild - IST");
		$myData->setSerieOnAxis("Serie2",0);

		$myData->addPoints(array("COACHIV","NORMATIV","PARTIZIPATIV","INTEGRATIV","INSPIRATIV","DIREKTIV"),"Absissa");
		$myData->setAbscissa("Absissa");

		$myData->setAxisPosition(0,AXIS_POSITION_LEFT);
		$myData->setAxisName(0,"");
		$myData->setAxisUnit(0,"%");

		$myPicture = new pImage(720,500,$myData);
		$Settings = array("R"=>0, "G"=>0, "B"=>0, "Dash"=>1, "DashR"=>20, "DashG"=>20, "DashB"=>20);
		$myPicture->drawFilledRectangle(0,0,720,500,$Settings);

		$Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>238, "EndG"=>238, "EndB"=>238, "Alpha"=>95);
		$myPicture->drawGradientArea(0,0,720,500,DIRECTION_HORIZONTAL,$Settings);

		$myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>20));
		$myPicture->setShadow(FALSE);

		$myPicture->setGraphArea(70,70,650,475);
		$myPicture->setFontProperties(array("R"=>0,"G"=>0,"B"=>0,"FontName"=>"fonts/GeosansLight.ttf","FontSize"=>9));

		$Settings = array("Pos"=>SCALE_POS_TOPBOTTOM
		, "Mode"=>SCALE_MODE_FLOATING
		, "LabelingMethod"=>LABELING_ALL
		, "GridR"=>255, "GridG"=>255, "GridB"=>255, "GridAlpha"=>50, "TickR"=>0, "TickG"=>0, "TickB"=>0, "TickAlpha"=>50, "LabelRotation"=>0, "DrawXLines"=>1, "DrawSubTicks"=>1, "SubTickR"=>255, "SubTickG"=>0, "SubTickB"=>0, "SubTickAlpha"=>50, "DrawYLines"=>ALL);
		$myPicture->drawScale($Settings);

		$myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>10));

		$Config = array("DisplayValues"=>1, "Rounded"=>1, "AroundZero"=>1);
		$myPicture->drawBarChart($Config);

		$Config = array("FontR"=>0, "FontG"=>0, "FontB"=>0, "FontName"=>"fonts/GeosansLight.ttf", "FontSize"=>12, "Margin"=>10, "Alpha"=>30, "BoxSize"=>15, "Style"=>LEGEND_NOBORDER
		, "Mode"=>LEGEND_HORIZONTAL
		, "Family"=>LEGEND_FAMILY_CIRCLE
		);
		$myPicture->drawLegend(494,20,$Config);

	}

}