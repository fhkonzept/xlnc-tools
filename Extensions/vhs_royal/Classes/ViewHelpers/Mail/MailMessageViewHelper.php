<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Mail;

class MailMessageViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	private $extConf;

	public function initialize() {

		$this->extConf = @unserialize($GLOBALS['TYPO3_CONF_VARS'] ['EXT'] ['extConf'] ['vhs_royal']);

	}

	/**
	 * @return boolean
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:mail.mailmessage>
	 *		<children /> ...
	 *	</vhsroyal:mail.mailmessage>
	 * </code>
	 */
	public function render() {

		$mailMessage = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Mail\MailMessage');

		if($this->viewHelperVariableContainer->exists(get_class($this), 'mailmessage')) {
			$this->viewHelperVariableContainer->remove(get_class($this), 'mailmessage');
		}

		$this->viewHelperVariableContainer->add(get_class($this), 'mailmessage', $mailMessage);

		$this->renderChildren();

		return $mailMessage->send();

	}
}
