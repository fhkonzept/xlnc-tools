<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessage\Element;

class BodyViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param string $contenttype
	 * @param string $mailtemplate
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:mail.element.body mailmessage="{mailmessage}" contenttype="text/html" mailtemplate="/absolute/path/to/my/mailtemplate">
	 *		<children /> ...
	 *	</vhsroyal:mail.element.body>
	 * </code>
	 */
	public function render($contenttype = 'text/html', $mailtemplate = '') {

		$mailBody = $this->renderChildren();
		
		// Use template
		$mailtemplate = ($mailtemplate && file_exists($mailtemplate)) ? $mailtemplate : \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('vhs_royal') . 'Resources/Private/Templates/Mailmessage/Index.html';
		if($mailtemplate && file_exists($mailtemplate)) {

			$emailView = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Fluid\View\StandaloneView');
			$emailView->setTemplatePathAndFilename($mailtemplate);
			$emailView->assignMultiple(
				array(
					'logo' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('vhs_royal') . 'Resources/Public/Images/logo.png',
					'now' => time(),
					'mailBody' => $mailBody
					)
				);
			$mailBody = $emailView->render();
			$mailBody = $this->parseMailBody($mailBody);

		}

		$this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessageViewHelper', 'mailmessage')->setBody($mailBody, $contenttype);

	}

	private function parseMailBody($mailBody) {

		preg_match_all ( "/<img (.*[^>]+)>/U", $mailBody, $result, PREG_PATTERN_ORDER );

		if (count ( $result [0] )) {
			for($i = 0; $i < count ( $result [0] ); $i ++) {
				$txtPart = $result [0] [$i];

				$attrArr = array ();

				$attr = $result [1] [$i];
				preg_match_all ( '/(\w+)\s*(?:=\s*(?:"([^"]*)"|\'([^\']*)\'|(\w+)))?/usix', $attr, $attrArr, PREG_SET_ORDER );

				$newAttributes = array ();

				foreach ( $attrArr as $attribute ) {
					$k = $attribute [1];
					$v2 = $attribute [2];

					switch (strtoupper ( $k )) {
						case 'ALT' :
							$newAttributes [] = 'alt="' . $v2 . '"';
							break;

						case 'SRC' :
							if(file_exists(PATH_site . $v2))
								$v2 = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessageViewHelper', 'mailmessage')->embed(\Swift_Image::fromPath(PATH_site . $v2));
							$newAttributes [] = 'src="' . $v2 . '"';
							break;

						default :
							$newAttributes [] = $k . '="' . $v2 . '"';
							break;
					}
				}
				$imgTag = '<img ' . implode ( ' ', $newAttributes ) . ' />';
				$mailBody = preg_replace ( '|' . $txtPart . '|', $imgTag, $mailBody );
			}
		}

		return $mailBody;
	}
}
