<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessage\Element;

class AttachmentViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param string $name
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:mail.element.attachment name="myAttachment.pdf">
	 *		<children /> ...
	 *	</vhsroyal:mail.element.attachment>
	 * </code>
	 */
	public function render($name = '') {

		$attachmentFile = $this->renderChildren();

		if(file_exists($attachmentFile)) {

			$attachment = \Swift_Attachment::fromPath($attachmentFile, $this->_getFileMimeType($attachmentFile));
			if($name != '')
				$attachment->setFilename($name);
			$this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessageViewHelper', 'mailmessage')->attach($attachment);

		}

	}

	protected function _getFileMimeType($file) {

		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mimeType = finfo_file($finfo, $file);
		finfo_close($finfo);

		return $mimeType;

	}
}
