<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessage\Setter;

class PriorityViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:mail.setter.priority>
	 *		<children /> ...
	 *	</vhsroyal:mail.setter.priority>
	 * </code>
	 */
	public function render() {

		$this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessageViewHelper', 'mailmessage')->setPriority($this->renderChildren());

	}
}
