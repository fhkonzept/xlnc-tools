<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessage\Setter;

class FromViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:mail.setter.from>
	 *		<children /> ...
	 *	</vhsroyal:mail.setter.from>
	 * </code>
	 */
	public function render() {

		$this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessageViewHelper', 'mailmessage')->setFrom($this->renderChildren());

	}
}
