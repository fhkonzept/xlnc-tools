<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessage\Setter;

class SenderViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:mail.setter.sender>
	 *		<children /> ...
	 *	</vhsroyal:mail.setter.sender>
	 * </code>
	 */
	public function render() {

		$this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessageViewHelper', 'mailmessage')->setSender($this->renderChildren());

	}
}
