<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessage\Setter;

class BccViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:mail.setter.bcc>
	 *		<children /> ...
	 *	</vhsroyal:mail.setter.bcc>
	 * </code>
	 */
	public function render() {

		$this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessageViewHelper', 'mailmessage')->setBcc($this->renderChildren());

	}
}
