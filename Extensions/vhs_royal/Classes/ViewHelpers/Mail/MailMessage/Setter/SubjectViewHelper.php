<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessage\Setter;

class SubjectViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:mail.setter.subject>
	 *		<children /> ...
	 *	</vhsroyal:mail.setter.subject>
	 * </code>
	 */
	public function render() {

		$this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Mail\MailMessageViewHelper', 'mailmessage')->setSubject($this->renderChildren());

	}
}
