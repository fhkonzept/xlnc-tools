<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Zip\ZipArchive\Setter;

class FileViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param \ZipArchive $zip
	 * @param $file (string) Relative file path.
	 * @return void
	 */
	public function render($zip, $file) {

		$zip->addFile(PATH_site . rtrim($file, '/'), basename($file));

	}
}
