<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Zip;

require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Lib/tcpdf/tcpdf.php');

class ZipArchiveViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	private $extConf;

	public function initialize() {

		$this->extConf = @unserialize($GLOBALS['TYPO3_CONF_VARS'] ['EXT'] ['extConf'] ['vhs_royal']);
		$this->extConf['tempPath'] = $this->extConf['tempPath'] ? rtrim($this->extConf['tempPath'], '/') . '/' : 'typo3temp/';

	}

	/**
	 * @param array $configuration
	 * @return void|string
	 */
	public function render(array $configuration = array('dest' => 'D', 'destname' => 'ziparchive.zip')) {

		$absExportedFilePath = PATH_site . $this->extConf['tempPath'] . $configuration['destname'];

		$zip = new \ZipArchive();
		$res = $zip->open($absExportedFilePath, \ZipArchive::CREATE);

		if($res === TRUE) {

			$templateVariableContainer = $this->renderingContext->getTemplateVariableContainer();
			$templateVariableContainer->add('zip', $zip);

			$this->renderChildren();

			$zip->close();

			if(file_exists($absExportedFilePath)) {

				if($configuration['dest'] == 'D') {

					// http headers for zip downloads
					header("Pragma: public");
					header("Expires: 0");
					header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
					header("Cache-Control: public");
					header("Content-Description: File Transfer");
					header("Content-type: application/octet-stream");
					header("Content-Disposition: attachment; filename=\"".$configuration['destname']."\"");
					header("Content-Transfer-Encoding: binary");
					header("Content-Length: ".filesize($absExportedFilePath));

					@readfile($absExportedFilePath);

					die();
					

				} else if($configuration['dest'] == 'F') {

					return $absExportedFilePath;
				
				}

			}
			
		}

	}

}
