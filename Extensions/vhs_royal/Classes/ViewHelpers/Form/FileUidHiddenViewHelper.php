<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Form;

use TYPO3\CMS\Fluid\ViewHelpers\Form\HiddenViewHelper;

class FileUidHiddenViewHelper extends HiddenViewHelper {

	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('fileObject', 'mixed', 'The file object to "hide" in here.', TRUE);
	}

	protected function getValue() {
		$file = $this->arguments['fileObject'];
		if(is_null($file)) return null;
		return $file->getUid();
	
	}
	
}
