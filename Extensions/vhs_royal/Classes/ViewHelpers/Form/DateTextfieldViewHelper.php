<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Form;

/**
 * this class is used to render an input type="text" field that handles a property of type \DateTime
 * @see https://forge.typo3.org/issues/34186
 * the standard viewhelper does not support DateTime property values for now, and setting them via 
 * attribute "value" will reset the field after failed validation
 *
 * = Examples =
 *
 * <code title="Example">
 * <vhs:form.dateTextfield property="myDate" dateFormat="d-m-y" />
 * </code>
 * <output>
 * <input type="text" [...] value="01-01-70" />
 * </output>
 */
class DateTextfieldViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\Form\TextfieldViewhelper {

	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerTagAttribute('dateFormat', 'string', 'The format the given DateTime object should have after conversion to string');
	}

	protected function getPropertyValue() {
		$propertyValue = parent::getPropertyValue();
		if(is_null($propertyValue)) return null;
		return $propertyValue->format($this->arguments['dateFormat']);
	}

}

