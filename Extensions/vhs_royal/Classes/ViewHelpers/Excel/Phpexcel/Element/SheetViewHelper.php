<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Element;

class SheetViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param  int|null $iSheetIndex Index where sheet should go (0,1,..., or null for last)
	 * @param string $title Title of the sheet
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.element.sheet iSheetIndex="0" title="Sheet 1">
	 *		<children />...
	 *	</vhsroyal:excel.phpexcel.element.sheet>
	 * </code>
	 */
	public function render($iSheetIndex = NULL, $title = 'Worksheet') {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		
		$sheet = $phpexcel->createSheet($iSheetIndex);
		$sheet->setTitle($title);

		$this->renderChildren();

	}
}
