<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Element;

class FromArrayViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param array $source Source array
     * @param mixed $nullValue Value in source array that stands for blank cell
     * @param string $startCell Insert array starting from this cell address as the top left coordinate
     * @param boolean $strictNullComparison Apply strict comparison when testing for null values in the array
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.element.fromarray
        source="{0:{0:'Mustermann',1:'Max',2:'max@mustermann.de',3:'male'},1:{0:'Mustermann
geb. Niemand',1:'Martina',2:'martina@mustermann.de',3:'female'}}"
        startCell="A2" />
	 * </code>
	 */
	public function render($source = null, $nullValue = null, $startCell = 'A1', $strictNullComparison = false) {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		$phpexcel->getActiveSheet()->fromArray($source, $nullValue, $startCell, $strictNullComparison);

	}
}
