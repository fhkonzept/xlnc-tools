<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Element;

class CellValueViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param string $pCoordinate Coordinate of the cell
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.element.cellvalue pCoordinate="A1">Name</vhsroyal:excel.phpexcel.element.cellvalue>
	 * </code>
	 */
	public function render($pCoordinate = 'A1') {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		$phpexcel->getActiveSheet()->setCellValue($pCoordinate, $this->renderChildren(), false);

	}
}
