<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Element;

class DrawingViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param string $path The relative image path
	 * @param string $coordinates The cell coordinates
	 * @param string $name The image name
	 * @param string $description The image description
	 * @param integer $height The image height
	 * @param integer $offsetX The image x-coordinates offset
	 * @param integer $rotation The image rotation
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.element.drawing coordinates="A4" height="40" path="typo3conf/ext/vhs_royal/Resources/Public/Images/logo.png" />
	 * </code>
	 */
	public function render($path, $coordinates = 'A1', $name = '', $description = '', $height = NULL, $offsetX = 0, $rotation = 0) {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');

		$path = PATH_site . ltrim($path, '/');

		$objDrawing = new \PHPExcel_Worksheet_Drawing();
		$objDrawing->setName($name);
		$objDrawing->setDescription($description);
		$objDrawing->setPath($path);
		$objDrawing->setHeight($height);
		$objDrawing->setCoordinates($coordinates);
		$objDrawing->setOffsetX($offsetX);
		$objDrawing->setRotation($rotation);

		$objDrawing->setWorksheet($phpexcel->getActiveSheet());

	}
}
