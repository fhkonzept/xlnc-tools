<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Setter;

class BoldViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $range (string) The range of columns to set bold ('A1' or 'A1:D1').
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.setter.bold range="A1:D1" />
	 * </code>
	 */
	public function render($range) {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		$phpexcel->getActiveSheet()->getStyle($range)->getFont()->setBold(true);

	}
}
