<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Setter;

class SubjectViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $subject (string) The subject.
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.setter.subject subject="PHPExcel via TYPO3 Fluid ViewHelper" />
	 * </code>
	 */
	public function render($subject) {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		$phpexcel->getProperties()->setDescription($subject);

	}
}
