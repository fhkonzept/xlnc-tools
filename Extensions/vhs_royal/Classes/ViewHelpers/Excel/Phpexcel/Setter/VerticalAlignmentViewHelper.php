<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Setter;

class VerticalAlignmentViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $range (string) The range of columns to set the vertical alignment ('A1' or 'A1:D1').
	 * @param string $align (default: top)
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.setter.verticalalignment range="A1:A4" align="top" />
	 * </code>
	 */
	public function render($range, $align = '') {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');

		$alignment = '';
		switch ($align) {
			case 'top':
				$alignment = \PHPExcel_Style_Alignment::VERTICAL_TOP;
				break;

			case 'bottom':
				$alignment = \PHPExcel_Style_Alignment::VERTICAL_BOTTOM;
				break;

			case 'center':
				$alignment = \PHPExcel_Style_Alignment::VERTICAL_CENTER;
				break;

			case 'justify':
				$alignment = \PHPExcel_Style_Alignment::VERTICAL_JUSTIFY;
				break;
			
			default:
				$alignment = \PHPExcel_Style_Alignment::VERTICAL_TOP;
				break;
		}

		$phpexcel->getActiveSheet()->getStyle($range)->getAlignment()->setVertical($alignment);

	}
}
