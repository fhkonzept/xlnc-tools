<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Setter;

class CreatorViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.setter.creator>TYPO3</vhsroyal:excel.phpexcel.setter.creator>
	 * </code>
	 */
	public function render() {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		$phpexcel->getProperties()->setCreator($this->renderChildren());

	}
}
