<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Setter;

class ColumnAutosizeViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $column (string) The column to set the width ('A' or 'B' etc).
	 * @param $autosize (boolean)
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.setter.columnautosize column="A" autosize="1" />
	 * </code>
	 */
	public function render($column, $autosize) {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		$phpexcel->getActiveSheet()->getColumnDimension($column)->setAutoSize($autosize);

	}
}
