<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Setter;

class ApplyStylesFromArrayViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $range (string) The range of columns to set the color ('A1' or 'A1:D1').
	 * @param array	$pStyles Array of style definitions
	 * @return void
	 *
	 * Example for pStyles['fill']:
	 * <code>
	 * $objPHPExcel->getActiveSheet()->getStyle('B2')->getFill()->applyFromArray(
	 *		array(
	 *			'type'	   => 'solid', // or linear etc., see Fill.php
	 *			'rotation'   => 0,
	 *			'startcolor' => array(
	 *				'rgb' => '000000'
	 *			),
	 *			'endcolor'   => array(
	 *				'argb' => 'FFFFFFFF'
	 *			)
	 *		)
	 * );
	 * </code>
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.setter.applystylesfromarray range="A1:D1" pStyles="{fill:{type:'linear',rotation:'90',startcolor:{argb:'FFCCCCCC'},endcolor:{argb:'FFFFFFFF'}}}" />
	 * </code>
	 */
	public function render($range, $pStyles) {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		$phpexcel->getActiveSheet()->getStyle($range)->applyFromArray($pStyles);

	}
}
