<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Setter;

class SecurityViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param string $password
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.setter.security password="password" />
	 * </code>
	 */
	public function render($password) {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');

		$security = new \PHPExcel_DocumentSecurity();
		$security->setLockWindows(true);
		$security->setLockStructure(true);
		$security->setWorkbookPassword($password);

		$phpexcel->setSecurity($security);
	}
}
