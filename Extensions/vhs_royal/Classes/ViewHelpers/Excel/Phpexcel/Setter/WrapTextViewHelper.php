<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Setter;

class WrapTextViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $range (string) The range of columns to set the color ('A1' or 'A1:D1').
	 * @param string $wrap
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.setter.wraptext range="A1:A4" wrap="1" />
	 * </code>
	 */
	public function render($range, $wrap) {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		$phpexcel->getActiveSheet()->getStyle($range)->getAlignment()->setWrapText(($wrap == '1'));

	}
}
