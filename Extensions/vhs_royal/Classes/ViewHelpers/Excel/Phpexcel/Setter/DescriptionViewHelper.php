<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Setter;

class DescriptionViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.setter.description>Renders Excel document via PHPExcel and TYPO3 Fluid ViewHelper</vhsroyal:excel.phpexcel.setter.description>
	 * </code>
	 */
	public function render() {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		$phpexcel->getProperties()->setDescription($this->renderChildren());

	}
}
