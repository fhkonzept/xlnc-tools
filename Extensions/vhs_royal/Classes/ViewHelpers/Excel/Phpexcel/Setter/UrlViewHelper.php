<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Setter;

class UrlViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param string $cell The column to set the link, like C3
	 * @param string $url 'http://www.phpexcel.net' or "sheet://'Sheetname'!A1"
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.setter.url cell="C3" url="mailto:martina@mustermann.de" />
	 * </code>
	 */
	public function render($cell, $url) {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		$phpexcel->getActiveSheet()->getCell($cell)->getHyperlink()->setUrl($url);

	}
}
