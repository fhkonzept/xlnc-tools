<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Setter;

class ColorViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $range (string) The range of columns to set the color ('A1' or 'A1:D1').
	 * @param	string	$pARGB	ARGB value for the colour, like FF000000 (black)
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.setter.color range="A1:D1" pARGB="FFFF0000" />
	 * </code>
	 */
	public function render($range, $pARGB) {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		$phpexcel->getActiveSheet()->getStyle($range)->getFont()->setColor(new \PHPExcel_Style_Color($pARGB));

	}
}
