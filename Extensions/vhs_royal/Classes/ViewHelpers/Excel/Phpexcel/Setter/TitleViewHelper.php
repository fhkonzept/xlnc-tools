<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel\Phpexcel\Setter;

class TitleViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel.setter.title>PHPExcel via TYPO3 Fluid ViewHelper</vhsroyal:excel.phpexcel.setter.title>
	 * </code>
	 */
	public function render() {

		$phpexcel = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Excel\PhpexcelViewHelper', 'phpexcel');
		$phpexcel->getProperties()->setTitle($this->renderChildren());

	}
}
