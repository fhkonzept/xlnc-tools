<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Excel;

require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Lib/phpexcel/Classes/PHPExcel.php');

class PhpexcelViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	private $extConf;

	public function initialize() {

		$this->extConf = @unserialize($GLOBALS['TYPO3_CONF_VARS'] ['EXT'] ['extConf'] ['vhs_royal']);
		$this->extConf['tempPath'] = $this->extConf['tempPath'] ? rtrim($this->extConf['tempPath'], '/') . '/' : 'typo3temp/';

	}

	/**
	 * @param array $configuration
	 * @param \ZipArchive $zip
	 * @return void|string
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:excel.phpexcel configuration="{dest:'D',destname:'test.xslx'}">
	 *		<children />...
	 *	</vhsroyal:excel.phpexcel>
	 * </code>
	 */
	public function render(array $configuration = array('dest' => 'D', 'destname' => 'spreadsheet.xlsx'), $zip = NULL) {

		$phpexcel = new \PHPExcel();
		$phpexcelWriter = new \PHPExcel_Writer_Excel2007($phpexcel);

		if($this->viewHelperVariableContainer->exists(get_class($this), 'phpexcel')) {
			$this->viewHelperVariableContainer->remove(get_class($this), 'phpexcel');
		}
		if($this->viewHelperVariableContainer->exists(get_class($this), 'phpexcelWriter')) {
			$this->viewHelperVariableContainer->remove(get_class($this), 'phpexcelWriter');
		}
		$this->viewHelperVariableContainer->add(get_class($this), 'phpexcel', $phpexcel);
		$this->viewHelperVariableContainer->add(get_class($this), 'phpexcelWriter', $phpexcelWriter);

		$this->renderChildren();

		$absExportedFilePath = PATH_site . $this->extConf['tempPath'] . $configuration['destname'];

		$phpexcelWriter->save($absExportedFilePath);
		$phpexcel->garbageCollect();

		if($configuration['dest'] == 'D') {

			// Generate and send to browser
			if (ob_get_contents()) {
				die('Some data has already been output, can\'t send Excel file');
			}
			header('Content-Description: File Transfer');
			if (headers_sent()) {
				die('Some data has already been output to browser, can\'t send Excel file');
			}
			header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');

			if (strpos(php_sapi_name(), 'cgi') === false) {
				header('Content-Type: application/force-download');
				header('Content-Type: application/octet-stream', false);
				header('Content-Type: application/download', false);
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', false);
			} else {
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			}

			header('Content-Disposition: attachment; filename="'.basename($configuration['destname']).'"');
			header('Content-Transfer-Encoding: binary');

			@readfile($absExportedFilePath);

			die();

		} else if($configuration['dest'] == 'F') {

			if( is_null($zip))
				return $absExportedFilePath;
			else
				$zip->addFile($absExportedFilePath, basename($absExportedFilePath));

			return;
		}
	}
}
