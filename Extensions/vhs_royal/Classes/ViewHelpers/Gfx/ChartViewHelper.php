<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx;

// pChart
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Lib/pChart/class/pData.class.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Lib/pChart/class/pDraw.class.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Lib/pChart/class/pImage.class.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Lib/pChart/class/pCache.class.php');
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/ChartFuncs.php');

/**
* UNUSED: PHPlot (http://sourceforge.net/projects/phplot)
* UNUSED: phpCHART (http://phpchart.com/)
* CURRENT: pChart (http://www.pchart.net/)
*/
class ChartViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	private $extConf;
	private $doBuildViewHelpers = FALSE;// Enables create ViewHelpers PHP-files: DO NOT use when viewhelpers has been modified, will be overwritten

	public function initialize() {

		$this->extConf = @unserialize($GLOBALS['TYPO3_CONF_VARS'] ['EXT'] ['extConf'] ['vhs_royal']);
		$this->extConf['tempPath'] = $this->extConf['tempPath'] ? rtrim($this->extConf['tempPath'], '/') . '/' : 'typo3temp/pics/';

		// Creates ViewHelpers PHP-files: DO NOT use when viewhelpers has been modified, will be overwritten
		if($this->doBuildViewHelpers) {
			$this->buildViewHelpers('\pImage', 'draw');
			$this->buildViewHelpers('\pImage', 'set');
			$this->buildViewHelpers('\pData', 'set');
			$this->buildViewHelpers('\pData', 'add');
			exit;
		}

	}

	/**
	 * @param integer $width
	 * @param integer $height
	 * @param string $dest
	 * @param string $preset
	 * @param array $data
	 * @return string
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:gfx.chart width="640" height="480" preset="barchart" />
	 * </code>
	 */
	public function render($width, $height, $dest = 'B', $preset = '', $data = array()) {

		$this->templateVariableContainer->add('data', $data);

		if($preset != '') {
			$presetConfig = $this->getConfigPreset($preset, (array_key_exists('overwriteChartConfig', $data) ? $data['overwriteChartConfig'] : array()));
			if(is_array($presetConfig)) {
				$this->templateVariableContainer->add('preset', $presetConfig);
			}
		}

		// Set temp file path
		$uniqueId = md5(microtime().rand(100000000, 123456789));

		$relChartFilePath = $this->extConf['tempPath'] . $uniqueId . '.png';
		$chartFilePath = PATH_site . $relChartFilePath;

		// Register chart image for rendering
		$pImage = new \pImage($width, $height);
		$pImage->Antialias = TRUE;
		if($this->viewHelperVariableContainer->exists(get_class($this), 'pImage')) {
			$this->viewHelperVariableContainer->remove(get_class($this), 'pImage');
		}
		$this->viewHelperVariableContainer->add(get_class($this), 'pImage', $pImage);

		// Register chart data for rendering
		$pData = new \pData();
		if($this->viewHelperVariableContainer->exists(get_class($this), 'pData')) {
			$this->viewHelperVariableContainer->remove(get_class($this), 'pData');
		}
		$this->viewHelperVariableContainer->add(get_class($this), 'pData', $pData);

		// Render chart children settings
		$this->renderChildren();

		// Render and write chart
		$pImage->Render($chartFilePath);

		if($dest == 'B') {
			return '<img src="' . $relChartFilePath . '" width="' . $width . '" height="' . $height . '" />';
		} else if($dest == 'A') {
			return $chartFilePath;
		} else if($dest == 'R') {
			return $relChartFilePath;
		}
	}

	// ViewHelpers Creator functions

	private function buildViewHelpers($className, $funcPrefix) {

		$class = new \ReflectionClass($className);
		$methods = $class->getMethods();

		foreach($methods as $method) {
			if($method->isPublic() && substr($method->name, 0, strlen($funcPrefix)) == $funcPrefix) {
				$this->buildViewHelper($funcPrefix, $method, $className);
			}
		}

	}

	private function buildViewHelper($funcPrefix, $method, $className) {

		$viewHelperName = ucfirst(strtolower(substr($method->name, strlen($funcPrefix))));

		$subfolder = str_replace('ViewHelper', '', end(explode('\\', get_class($this))));
		$subfolderPath = dirname(__FILE__) . '/' . $subfolder;
		if(!is_dir($subfolderPath)) {
			mkdir($subfolderPath);
		}

		$folderPath = $subfolderPath . '/' . ucfirst(strtolower($funcPrefix));
		if(!is_dir($folderPath)) {
			mkdir($folderPath);
		}

		$filePath = $folderPath .  '/' . $viewHelperName  . 'ViewHelper.php';

		$content = '<?php' . chr(10);
		$content .= 'namespace ' . __NAMESPACE__ . '\\' . $subfolder . '\\' . ucfirst(strtolower($funcPrefix)) . ';' . chr(10);
		$content .= chr(10);
		$content .= 'class ' . $viewHelperName . 'ViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {' . chr(10);
		$content .= chr(10);
		$content .= chr(9) . '/**';
		$i = 0;
		foreach($method->getParameters() as $param) {
			$paramObj = new \ReflectionParameter(array($method->class, $method->name), $i);
			$varName = '$' . $param->name;
			if($varName == '$Format' || $varName == '$Values' || $varName == '$DataSet') {
				$content .= chr(10) . chr(9) . '* @param array ' . $varName;
			} else {
				$content .= chr(10) . chr(9) . '* @param string ' . $varName;
			}
			$i++;
		}
		$content .= chr(10) . chr(9) . '* @return void';
		$content .= chr(10) . chr(9) . '*/';
		$content .= chr(10) . chr(9) . 'public function render(';
		$funcArgs = array();
		$funcArgs2 = array();
		$i = 0;
		foreach($method->getParameters() as $param) {
			$paramObj = new \ReflectionParameter(array($method->class, $method->name), $i);
			$funcArgs[$i] = $funcArgs2[$i] = '$' . $param->name;
			if($paramObj->isOptional()) {
				$funcArgs[$i] .= ' = ""';
			}
			$i++;
		}
		$content .= implode(', ', $funcArgs);
		$content .= ') {' . chr(10);
		$varName = end(explode('\\', $className));
		$content .= chr(9).chr(9).'$' . $varName . ' = $this->viewHelperVariableContainer->get("' . get_class($this) . '", "' . $varName . '");' .chr(10);
		$content .= chr(9).chr(9).'$' . $varName . '->' . $method->name . '(' . implode(', ', $funcArgs2) . ');' . chr(10);
		$content .= chr(9) . '}' . chr(10);
		$content .= '}' . chr(10);

		file_put_contents($filePath, $content);
	}

	protected function getConfigPreset($presetName, $overwriteConfig = array()) {
		$presetFile = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/Presets/' . $presetName . '.php';
		if(file_exists($presetFile)) {
			$presetContent = include($presetFile);
			if(!empty($overwriteConfig)) {
				$presetContent = array_replace_recursive($presetContent, $overwriteConfig);
			}
			return $presetContent;
		} else {
			throw new \Exception('Chart preset template cannot be included with path "' . $presetFile . '".', 20150712);
		}
	}

}
