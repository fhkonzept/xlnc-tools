<?php
function renderAxisLabel($Value) {
	if($Value%20 == 0 || is_integer($Value)) {
		return $Value . '%';
	} else {
		return '';
	}
}
function renderAxisLabelNone($Value) {
	return '';
}

function renderFourtySeventyOnehundred($Value) {

	if(in_array($Value, [0, 40, 70, 100])) {
		return $Value . '%';
	} else {
		return '';
	}

}
