<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Draw;

class CircleViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $Xc
	* @param string $Yc
	* @param string $Height
	* @param string $Width
	* @param array $Format
	* @return void
	*/
	public function render($Xc, $Yc, $Height, $Width, $Format = "") {
		$pImage = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pImage");
		$pImage->drawCircle($Xc, $Yc, $Height, $Width, $Format);
	}
}
