<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Draw;

class AlphapixelViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $X
	* @param string $Y
	* @param string $Alpha
	* @param string $R
	* @param string $G
	* @param string $B
	* @return void
	*/
	public function render($X, $Y, $Alpha, $R, $G, $B) {
		$pImage = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pImage");
		$pImage->drawAlphaPixel($X, $Y, $Alpha, $R, $G, $B);
	}
}
