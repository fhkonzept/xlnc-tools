<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Draw;

class FrompictureViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $PicType
	* @param string $FileName
	* @param string $X
	* @param string $Y
	* @return void
	*/
	public function render($PicType, $FileName, $X, $Y) {
		$pImage = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pImage");
		$pImage->drawFromPicture($PicType, $FileName, $X, $Y);
	}
}
