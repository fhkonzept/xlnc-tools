<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Draw;

class ThresholdareaViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $Value1
	* @param string $Value2
	* @param array $Format
	* @return void
	*/
	public function render($Value1, $Value2, $Format = "") {
		$pImage = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pImage");
		$pImage->drawThresholdArea($Value1, $Value2, $Format);
	}
}
