<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Draw;

class ZonechartViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $SerieA
	* @param string $SerieB
	* @param array $Format
	* @return void
	*/
	public function render($SerieA, $SerieB, $Format = "") {
		$pImage = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pImage");
		$pImage->drawZoneChart($SerieA, $SerieB, $Format);
	}
}
