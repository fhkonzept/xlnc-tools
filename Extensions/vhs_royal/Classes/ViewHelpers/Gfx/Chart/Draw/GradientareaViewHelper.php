<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Draw;

class GradientareaViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $X1
	* @param string $Y1
	* @param string $X2
	* @param string $Y2
	* @param string $Direction
	* @param array $Format
	* @return void
	*/
	public function render($X1, $Y1, $X2, $Y2, $Direction, $Format = "") {
		$pImage = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pImage");
		$pImage->drawGradientArea($X1, $Y1, $X2, $Y2, $Direction, $Format);
	}
}
