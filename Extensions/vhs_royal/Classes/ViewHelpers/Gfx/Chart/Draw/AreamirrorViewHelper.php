<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Draw;

class AreamirrorViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $X
	* @param string $Y
	* @param string $Width
	* @param string $Height
	* @param array $Format
	* @return void
	*/
	public function render($X, $Y, $Width, $Height, $Format = "") {
		$pImage = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pImage");
		$pImage->drawAreaMirror($X, $Y, $Width, $Height, $Format);
	}
}
