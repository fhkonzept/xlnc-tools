<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Draw;

class ArrowlabelViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $X1
	* @param string $Y1
	* @param string $Text
	* @param array $Format
	* @return void
	*/
	public function render($X1, $Y1, $Text, $Format = "") {
		$pImage = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pImage");
		$pImage->drawArrowLabel($X1, $Y1, $Text, $Format);
	}
}
