<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Draw;

class RoundedfilledrectangleViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $X1
	* @param string $Y1
	* @param string $X2
	* @param string $Y2
	* @param string $Radius
	* @param array $Format
	* @return void
	*/
	public function render($X1, $Y1, $X2, $Y2, $Radius, $Format = "") {
		$pImage = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pImage");
		$pImage->drawRoundedFilledRectangle($X1, $Y1, $X2, $Y2, $Radius, $Format);
	}
}
