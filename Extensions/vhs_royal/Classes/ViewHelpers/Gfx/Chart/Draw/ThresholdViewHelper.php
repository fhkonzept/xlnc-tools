<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Draw;

class ThresholdViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $Value
	* @param array $Format
	* @return void
	*/
	public function render($Value, $Format = "") {
		$pImage = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pImage");
		$pImage->drawThreshold($Value, $Format);
	}
}
