<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Add;

class RandomvaluesViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $SerieName
	* @param string $Options
	* @return void
	*/
	public function render($SerieName = "", $Options = "") {
		$pData = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pData");
		$pData->addRandomValues($SerieName, $Options);
	}
}
