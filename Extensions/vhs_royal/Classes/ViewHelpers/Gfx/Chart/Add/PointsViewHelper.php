<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Add;

class PointsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param array $Values
	* @param string $SerieName
	* @return void
	*/
	public function render($Values, $SerieName = "") {
		$pData = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pData");
		$pData->addPoints($Values, $SerieName);
	}
}
