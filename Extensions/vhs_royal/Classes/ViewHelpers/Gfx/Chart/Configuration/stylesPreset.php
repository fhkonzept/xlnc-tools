<?php
$preset = require(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/climatePreset.php');

$imageW = 720;
$imageH = 510;

$targetChartWidth = 1440;
$scaleFactor = $targetChartWidth/$imageW;

$graphOffsetX = 0;

$graphTopTitlesY = 70;
$graphLeftTitlesY = 140;
$graphLeftTitlesYsize = 90;
$graphLeftTitlesmargin = 10;
$graphLeftTitlesX = 40;
$graphLeftTitlesXsize = $graphLeftTitlesX + 40;
$graphLeftTitlesTextMargin = 24;

$graphAbscissaOffset = "     ";

$graphStartX = 202 + $graphOffsetX;
$graphMaxLength = 1100;

$diffBarsStartY1 = 150;
$diffBarsStartY2 = 184;
$diffBarsDistanceNextBar = 34;
$diffBarsDistanceNextCriteria = 90;

$climatePreset = $preset;

$climatePreset['serie3Values'] = array(
	\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.serie3Values.1", "xlnc_tools") . $graphAbscissaOffset,
	\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.serie3Values.2", "xlnc_tools") . $graphAbscissaOffset,
	\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.serie3Values.3", "xlnc_tools") . $graphAbscissaOffset,
	\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.serie3Values.4", "xlnc_tools") . $graphAbscissaOffset,
	\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.serie3Values.5", "xlnc_tools") . $graphAbscissaOffset,
	\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.serie3Values.6", "xlnc_tools") . $graphAbscissaOffset,
);

$climatePreset['serie1Values'] = array(10,40,70,45,15,90);
$climatePreset['serie2Values'] = array(20,30,95,25,85,40);

	// Dimensions and coordinates
$climatePreset['imageWidth']	= $imageW * $scaleFactor;
$climatePreset['imageHeight'] 	= $imageH * $scaleFactor;

$climatePreset['graphareaX1'] 	= $graphOffsetX + 100 * $scaleFactor;
$climatePreset['graphareaY1'] 	= 70 * $scaleFactor;

$climatePreset['graphareaX2'] 	= $graphOffsetX + ($imageW-70) * $scaleFactor;
$climatePreset['graphareaY2'] 	= 475 * $scaleFactor;

$climatePreset['thresholds']['items'][0]['X'] = 120 * $scaleFactor;
$climatePreset['thresholds']['items'][1]['X'] = 210 * $scaleFactor;
$climatePreset['thresholds']['items'][2]['X'] = 402 * $scaleFactor;
$climatePreset['thresholds']['items'][3]['X'] = 567 * $scaleFactor;

$climatePreset['thresholds']['items'][0]['label'] = '';
$climatePreset['thresholds']['items'][1]['label'] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.trasholds.item.1", "xlnc_tools");
$climatePreset['thresholds']['items'][2]['label'] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.trasholds.item.2", "xlnc_tools");
$climatePreset['thresholds']['items'][3]['label'] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.trasholds.item.3", "xlnc_tools");

$climatePreset['thresholds']['lines']['positions'][0]['X'] = 101.5 * $scaleFactor;
$climatePreset['thresholds']['lines']['positions'][1]['X'] = 320 * $scaleFactor;
$climatePreset['thresholds']['lines']['positions'][2]['X'] = 485 * $scaleFactor;
$climatePreset['thresholds']['lines']['positions'][3]['X'] = 650 * $scaleFactor;

$climatePreset['thresholds']['lines']['labelY'] += 20;
$climatePreset['legendHeaderY'] += 20;

$climatePreset['legendHeaderX'] = $graphOffsetX + 100 * $scaleFactor;
#$climatePreset['legendHeaderY'] = 320 * $scaleFactor;




  $climatePreset['legendBox1Y1']
= $climatePreset['legendBox2Y1']
= $climatePreset['legendBox3Y1']
= $climatePreset['legendBox4Y1']
= $climatePreset['legendBox5Y1']
= $climatePreset['legendBox1Y1'] + 16;

  $climatePreset['legendBox1Y2']
= $climatePreset['legendBox2Y2']
= $climatePreset['legendBox3Y2']
= $climatePreset['legendBox4Y2']
= $climatePreset['legendBox5Y2']
= $climatePreset['legendBox1Y1'] + 15 * $scaleFactor;

$climatePreset['legendBox1X1']	= $graphOffsetX + 100 * $scaleFactor;
$climatePreset['legendBox1X2']	= $climatePreset['legendBox1X1'] + 15 *$scaleFactor;

$climatePreset['legendBox2X1']	= $graphOffsetX + 200 * $scaleFactor;
$climatePreset['legendBox2X2']	= $climatePreset['legendBox2X1']+ 15 * $scaleFactor;

$climatePreset['legendBox3X1']	= $graphOffsetX + 300 * $scaleFactor;
$climatePreset['legendBox3X2']	= $climatePreset['legendBox3X1']+ 15 * $scaleFactor;

$climatePreset['legendBox4X1']	= $graphOffsetX + 400 * $scaleFactor;
$climatePreset['legendBox4X2']	= $climatePreset['legendBox4X1']+ 15 * $scaleFactor;

$climatePreset['legendBox5X1']	= $graphOffsetX + 500 * $scaleFactor;
$climatePreset['legendBox5X2']	= $climatePreset['legendBox5X1']+ 15 * $scaleFactor;

$climatePreset['legendTextY']	+= 16;
$climatePreset['legendTextY2']	+= 16;
#$climatePreset['legendTextYalt'] = $climatePreset['legendTextY'] - 7;
#$climatePreset['legendTextYalt2'] = $climatePreset['legendTextY'] + 1;

$climatePreset['legendText1X']	= $climatePreset['legendBox1X1'] + 25 * $scaleFactor;
$climatePreset['legendText2X']	= $climatePreset['legendBox2X1'] + 25 * $scaleFactor;
$climatePreset['legendText3X']	= $climatePreset['legendBox3X1'] + 25 * $scaleFactor;
$climatePreset['legendText4X']	= $climatePreset['legendBox4X1'] + 25 * $scaleFactor;
$climatePreset['legendText5X']	= $climatePreset['legendBox5X1'] + 25 * $scaleFactor;

  $climatePreset['legendBox3Format']
= $climatePreset['legendBox4Format']
= $climatePreset['legendBox5Format']
= $climatePreset['legendBox1Format'];

$climatePreset['legendFormat']["Align"] = TEXT_ALIGN_BOTTOMLEFT;
$climatePreset['legendFormat']["DrawBox"] = FALSE;
$climatePreset['legendFormat']["R"] = $colors["grey"]["R"];
$climatePreset['legendFormat']["G"] = $colors["grey"]["G"];
$climatePreset['legendFormat']["B"] = $colors["grey"]["B"];


return $climatePreset;
