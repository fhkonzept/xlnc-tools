<?php
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/constants.php');

$targetChartWidth = 1440;
$scaleFactor = $targetChartWidth/720;

$imageH = 510;
$graphEndY = $imageH - 45;
$thresholdLabelY = $graphEndY + 8;
$legendHeaderY = $thresholdLabelY + 34;
$legendBoxY1 = $legendHeaderY + 5;
$legendBoxY2 = $legendBoxY1 + 15;
$legendTextY = $legendBoxY2 - 3;

$graphOffsetX = 100;

$graphTopTitlesY = 70;
$graphLeftTitlesY = 140;
$graphLeftTitlesYsize = 90;
$graphLeftTitlesmargin = 10;
$graphLeftTitlesX = 40;
$graphLeftTitlesXsize = $graphLeftTitlesX + 40;
$graphLeftTitlesTextMargin = 24;

$graphAbscissaOffset = "     ";

$graphStartX = 202 + $graphOffsetX;
$graphMaxLength = 1100;

$diffBarsStartY1 = 149;
$diffBarsStartY2 = 182;
$diffBarsDistanceNextBar = 34.5;
$diffBarsDistanceNextCriteria = 88;

$fonts = [
	"light" => "typo3conf/ext/vhs_royal/Lib/pChart/fonts/SourceSansPro-Light.ttf",
	"regular" => 'typo3conf/ext/vhs_royal/Lib/pChart/fonts/SourceSansPro-Regular.ttf',
	"bold" => 'typo3conf/ext/vhs_royal/Lib/pChart/fonts/SourceSansPro-Bold.ttf',
];
$fontSize = [
	"small" => 9 * $scaleFactor,
	"large" => 10 * $scaleFactor,
];
$colors = [
	"grey" => [
		"R" => 125,
		"G" => 125,
		"B" => 125,
	],
    "fliederHighlight" => [
		"R" => 128,
		"G" => 132,
		"B" => 164,
	],
	"textBlue" => [
		"R" => 73,
		"G" => 75,
		"B" => 67,
	],
	"redHighlight" => [
		"R" => 185,
		"G" => 14,
		"B" => 12,
	]
];

$diffBoxFormat = [
	"DrawBox" => TRUE,
	"R" => 255,
	"G" => 255,
	"B" => 255,
	"Alpha" => 100,
	"FontName" => $fonts["bold"],
	"BoxR" => $colors["grey"]["R"],
	"BoxG" => $colors["grey"]["G"],
	"BoxB" => $colors["grey"]["B"],
	"BoxAlpha" => 100,
	"BoxRounded" => TRUE,
	"RoundedRadius" => 4,
];

// todo: remove
//
$fontR = 100;
$fontG = 100;
$fontB = 100;

$smallFontSize = 9 * $scaleFactor;
$largeFontSize = 10 * $scaleFactor;

$climatePreset = array(
	// Data
	// Serie 1
	'serie1Values'			=> array(10,40,70,45,15,90,10,20,85),
	'serie1Name'			=> 'Serie1',
	'serie1Description'		=> \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.selfIs", "xlnc_tools"),
	// Serie 2
	'serie2Values'			=> array(31,15,80,56,23,72,60,70,65),
	'serie2Name'			=> 'Serie2',
	'serie2Description'		=> \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.forIs", "xlnc_tools"),
	// Serie 3
	'serie3Values'			=> array(
		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.absissa.credibility", "xlnc_tools") . $graphAbscissaOffset,
		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.absissa.autonomy", "xlnc_tools") . $graphAbscissaOffset,
		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.absissa.reliability", "xlnc_tools") . $graphAbscissaOffset,
		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.absissa.transparency", "xlnc_tools") . $graphAbscissaOffset,
		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.absissa.flexibility", "xlnc_tools") . $graphAbscissaOffset,
		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.absissa.appreciation", "xlnc_tools") . $graphAbscissaOffset,
		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.absissa.development", "xlnc_tools") . $graphAbscissaOffset,
		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.absissa.identification", "xlnc_tools") . $graphAbscissaOffset,
		\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.absissa.team_spirit", "xlnc_tools") . $graphAbscissaOffset
	),
	'serie3Name'			=> 'Absissa',

	// Axis
	// this puts the serie axis at the bottom - no idea why
	'axisPosition'			=> AXIS_POSITION_RIGHT,

	// Dimensions and coordinates
	'imageWidth'			=> 720 * $scaleFactor,
	'imageHeight'			=> $imageH * $scaleFactor,

	'graphareaX1'			=> $graphOffsetX + 100 * $scaleFactor,
	'graphareaY1'			=> 70 * $scaleFactor,
	'graphareaX2'			=> $graphOffsetX + 650 * $scaleFactor,
	'graphareaY2'			=> $graphEndY * $scaleFactor,



	// Rect
	'rectFormat'			=> array("R"=>255, "G"=>255, "B"=>255),

	// Fonts
	'mainFontFormat'		=> array(
        "R"     => $colors["grey"]["R"],
        "G"     => $colors["grey"]["G"],
        "B"     => $colors["grey"]["B"],
        "Alpha" => 100,
        "FontName" => $fonts["regular"],
        "FontSize" => $smallFontSize
    ),
	'blueFontFormat'		=> array(
        "R"     => $colors["textBlue"]["R"],
        "G"     => $colors["textBlue"]["G"],
        "B"     => $colors["textBlue"]["B"],
        "FontName" => $fonts["bold"],
        "FontSize" => $smallFontSize
    ),

	// Scale Format
	'scaleFormat'			=> array(
								"Pos"				=> SCALE_POS_TOPBOTTOM,
								"Mode"				=> SCALE_MODE_MANUAL,
								"ManualScale"		=> array(0=>array("Min"=>0,"Max"=>100)),
								"TickR"				=> 255,
								"TickG"				=> 255,
								"TickB"				=> 255,
								"TickAlpha"			=> 50,
                                "AxisR"             => 255,
                                "AxisB"             => 255,
                                "AxisG"             => 255,
								),
	'axisDisplayType'		=> AXIS_FORMAT_CUSTOM,
	#'axisDisplayFunc'		=> 'renderFourtySeventyOnehundred',

	// Bar chart format
	'chartFormat'			=> array(
								"DisplayValues"=>1,
								"Rounded"=>1,
								"AroundZero"=>1,
								"DisplayPos"=>LABEL_POS_INSIDE,
								"DisplayR"=>255,
								"DisplayG"=>255,
								"DisplayB"=>255,
								"DisplayShadow"=>0,
								"Gradient"=>0,
								),

	// Legend
	'legendX'				=> 100 * $scaleFactor,
	'legendY'				=> 490 * $scaleFactor,

	'legendHeaderX'			=> $graphOffsetX + 100 * $scaleFactor,
	'legendHeaderY'			=> $legendHeaderY * $scaleFactor,

	'legendBox1X1'			=> $graphOffsetX + 100 * $scaleFactor,
	'legendBox1Y1'			=> $legendBoxY1 * $scaleFactor,
	'legendBox1X2'			=> $graphOffsetX + 115 * $scaleFactor,
	'legendBox1Y2'			=> $legendBoxY2 * $scaleFactor,

	'legendBox2X1'			=> $graphOffsetX +  100 * $scaleFactor,
	'legendBox2Y1'			=> ( $legendBoxY1 + 20 ) * $scaleFactor,
	'legendBox2X2'			=> $graphOffsetX +  115 * $scaleFactor,
	'legendBox2Y2'			=> ( $legendBoxY2 + 20 ) * $scaleFactor,

	'legendText1X'			=> $graphOffsetX +  120 * $scaleFactor,
	'legendText2X'			=> $graphOffsetX +  120 * $scaleFactor,
	'legendTextY'			=> $legendTextY * $scaleFactor + 5,
	'legendTextY2'			=> ($legendTextY + 20) * $scaleFactor + 5,

    'legendBox1Format'      => [
        "R" => 0,
        "G" => 0,
        "B" => 0,
    ],

    'legendBox2Format'      => [
        "R" => 255,
        "G" => 255,
        "B" => 255,
    ],

	'legendFormat'			=> array(
		"FontR"     => $colors["grey"]["R"],
        "FontG"     => $colors["grey"]["G"],
        "FontB"     => $colors["grey"]["B"],
        "Alpha"     => 100,
        "FontName"  => $fonts["light"],
        "FontSize"  => $largeFontSize,
		"Margin"    => 2,

		"BoxSize"   => 10,
	),
	// Color
	'serie1Palette'			=> array("R" => 0,"G" => 0,"B" => 0,"Alpha" => 100),
	'serie2Palette'			=> array("R" => 255,"G" => 255, "B" => 255, "Alpha" => 100),
	// Threshholds
	'thresholds' => [
        'items' => [
            [
                "value" => 0.3,
                "label" => "",
                "X"     => 120 * $scaleFactor,
            ],
            [
                "value" => 40,
                "label" => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.thresholds.low", "xlnc_tools"),
                "X"     => $graphOffsetX + 210 * $scaleFactor,
            ],
            [
                "value" => 70,
                "label" => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.thresholds.middle", "xlnc_tools"),
                "X"     => $graphOffsetX + 402 * $scaleFactor,
            ],
            [
                "value" => 100,
                "label" => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.thresholds.high", "xlnc_tools"),
                "X"     => $graphOffsetX + 567 * $scaleFactor,
            ],
        ],
        'upperTextY'    => 40 * $scaleFactor,

        'format' => [
            "R" => $colors["grey"]["R"],
            "G" => $colors["grey"]["G"],
            "B" => $colors["grey"]["B"],
            "Alpha" => 100,
            "AxisID" => 0,
            "Ticks" => 3,
            "WriteCaption" => 0,
            "FontName" => $fonts["light"],
            "CaptionOffset" => 820,
            "NoMargin" => TRUE,
            "DrawBox" => TRUE
        ],

        'textFormat' => [
			"R"     => $colors["grey"]["R"],
            "G"     => $colors["grey"]["G"],
            "B"     => $colors["grey"]["B"],
            "Alpha"     => 100,
            "FontName"  => $fonts["regular"],
            "FontSize"  => $smallFontSize,
			"Margin"    => 2,
            #"DrawBox"   => 1,
            "Align"     => TEXT_ALIGN_TOPMIDDLE,

			#"BorderOffset"   => 135,
        ],

        'additionalLabelFormat' => [
			"R"     => $colors["grey"]["R"],
            "G"     => $colors["grey"]["G"],
            "B"     => $colors["grey"]["B"],
            "Alpha"     => 100,
            "FontName"  => $fonts["regular"],
            "FontSize"  => $smallFontSize,
			"Margin"    => 2,
            #"DrawBox"   => 1,
            "Align"     => TEXT_ALIGN_TOPRIGHT,
            "X"         => 134 * $scaleFactor,
            "Y"         => 24 + 40 * $scaleFactor,
            "label"     => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.1", "xlnc_tools"),
            "secondLabel"     => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.2", "xlnc_tools"),
			#"BorderOffset"   => 135,
        ],

        'lines' => [
            "positions" => [
				["X" => $graphOffsetX + 101.5 * $scaleFactor, "label" => "0%"],
				["X" => $graphOffsetX + 320 * $scaleFactor, "label" => "40%"],
				["X" => $graphOffsetX + 485 * $scaleFactor, "label" => "70%"],
				["X" => $graphOffsetX + 650 * $scaleFactor, "label" => "100%"],

            ],
			"startY" 	=> 40 * $scaleFactor,
			"endY" 		=> 70 * $scaleFactor,
			"labelY" 	=> ($thresholdLabelY) * $scaleFactor,
			"format" 	=> [
				'R' => $colors["grey"]["R"],
				'G' => $colors["grey"]["G"],
				'B' => $colors["grey"]["B"],
				'FontName' => $fonts["light"],
				'FontSize' => $fontSize["large"],
				"Align"     => TEXT_ALIGN_TOPMIDDLE,
				'Weight' => 0,
				"Ticks" => 3
			],
        ]
    ],
	'threshold0Format'		=> array("R"=>0, "G"=>0, "B"=>0, "Alpha"=>50, "AxisID"=>0, "Ticks"=>8, "WriteCaption"=>0, "Caption"=>"TITEL", "CaptionOffset"=>500, "NoMargin"=>TRUE),
	'threshold1Format'		=> array("R"=>0, "G"=>0, "B"=>0, "Alpha"=>50, "AxisID"=>0, "Ticks"=>8, "WriteCaption"=>0, "Caption"=>"gering", "CaptionOffset"=>500, "NoMargin"=>TRUE),
	'threshold2Format'		=> array("R"=>0, "G"=>0, "B"=>0, "Alpha"=>50, "AxisID"=>0, "Ticks"=>8, "WriteCaption"=>0, "Caption"=>"mittel", "CaptionOffset"=>500, "NoMargin"=>TRUE),
	'threshold3Format'		=> array("R"=>0, "G"=>0, "B"=>0, "Alpha"=>50, "AxisID"=>0, "Ticks"=>8, "WriteCaption"=>0, "Caption"=>"hoch", "CaptionOffset"=>500, "NoMargin"=>TRUE),
	// Abscissa groupings
	'abscissaFormat'		=> array(
		'format' => array(
			'color' => array("R" => 242, "G" => 145, "B" => 23),
			'blank' => array("R" => 255, "G" => 255, "B" => 255),
		),
		'textFormat' => array(
			"R"		=> 242,
			"G"		=> 145,
			"B"		=> 23,
			"FontName"	=> $fonts["bold"],
			"FontSize"	=> $fontSize["small"],
			"Margin"	=> 20,
			"BoxSize"	=> 15,
			"Style"		=>	LEGEND_NOBORDER,
			"Mode"		=>	LEGEND_HORIZONTAL,
			"Family"	=>	LEGEND_FAMILY_BOX,
			"Angle" 	=> 90
		),
		'items' => array(
			0 => array(
				'position' => array(
					'X1' => $graphLeftTitlesX,
					'X2' => $graphLeftTitlesXsize,
					'Y1' => $graphLeftTitlesY + $graphLeftTitlesmargin,
					'Y2' => $graphLeftTitlesY - $graphLeftTitlesmargin + 3 * $graphLeftTitlesYsize,
				),
			),

			1 => array(
				'position' => array(
					'X1' => $graphLeftTitlesX,
					'X2' => $graphLeftTitlesXsize,
					'Y1' => $graphLeftTitlesY + $graphLeftTitlesmargin + 3 * $graphLeftTitlesYsize,
					'Y2' => $graphLeftTitlesY - $graphLeftTitlesmargin + 7 * $graphLeftTitlesYsize,
				),
			),

			2 => array(
				'position' => array(
					'X1' => $graphLeftTitlesX- 24,
					'X2' => $graphLeftTitlesXsize,
					'Y1' => $graphLeftTitlesY + $graphLeftTitlesmargin + 7 * $graphLeftTitlesYsize,
					'Y2' => $graphLeftTitlesY - $graphLeftTitlesmargin + 9 * $graphLeftTitlesYsize,
				),
			)
		),
		'textbox' => array(
			0 => array(
				'position' => array(
					'X1' => $graphLeftTitlesX,
					'X2' => $graphLeftTitlesXsize -2,
					'Y1' => $graphLeftTitlesY + $graphLeftTitlesmargin,
					'Y2' => $graphLeftTitlesY - $graphLeftTitlesmargin -2 + 3 * $graphLeftTitlesYsize,
				),
				'textPosition' => array(
					'X' => $graphLeftTitlesX + $graphLeftTitlesTextMargin,
					'Y' => 3 * $graphLeftTitlesYsize + $graphLeftTitlesY - $graphLeftTitlesmargin - $graphLeftTitlesTextMargin + 12 ,
				),
				'title' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("preset.abscissaFormat.textbox.0.title", "xlnc_tools"),
			),

			1 => array(
				'position' => array(
					'X1' => $graphLeftTitlesX - 24,
					'X2' => $graphLeftTitlesXsize - 2,
					'Y1' => $graphLeftTitlesY + $graphLeftTitlesmargin + 3 * $graphLeftTitlesYsize,
					'Y2' => $graphLeftTitlesY - $graphLeftTitlesmargin - 2 + 7 * $graphLeftTitlesYsize,
				),
				'textPosition' => array(
					'X' => $graphLeftTitlesX + $graphLeftTitlesTextMargin,
					'Y' => 7 * $graphLeftTitlesYsize + $graphLeftTitlesY - $graphLeftTitlesmargin - $graphLeftTitlesTextMargin + 12,
				),
				'title' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("preset.abscissaFormat.textbox.1.title", "xlnc_tools"),
			),

			2 => array(
				'position' => array(
					'X1' => $graphLeftTitlesX - 24,
					'X2' => $graphLeftTitlesXsize - 2,
					'Y1' => $graphLeftTitlesY + $graphLeftTitlesmargin + 7 * $graphLeftTitlesYsize,
					'Y2' => $graphLeftTitlesY - $graphLeftTitlesmargin - 2 + 9 * $graphLeftTitlesYsize,
				),
				'textPosition' => array(
					'X' => $graphLeftTitlesX + $graphLeftTitlesTextMargin - 24,
					'X2' => $graphLeftTitlesX + $graphLeftTitlesTextMargin,
					'Y' => 9 * $graphLeftTitlesYsize + $graphLeftTitlesY - $graphLeftTitlesmargin - $graphLeftTitlesTextMargin + 12,
				),
				'title' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("preset.abscissaFormat.textbox.2.title", "xlnc_tools"),
				'title2' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("preset.abscissaFormat.textbox.2.title2", "xlnc_tools"),
			)
		)


	),
	'diffBarFormat' => array(
		"R" => 255,
		"G" => 255,
		"B" => 255,
		// "EndR" => $colors["grey"]["R"],
		// "EndG" => $colors["grey"]["G"],
		// "EndB" => $colors["grey"]["B"],
		"Alpha" => 100,
		"Dash" => TRUE,
		"DashStep" => 13,
		"DashR" => $colors["grey"]["R"],
		"DashG" => $colors["grey"]["G"],
		"DashB" => $colors["grey"]["B"],
		// 'BorderR' => 0,
		// 'BorderG' => 0,
		// 'BorderB' => 0
	),
	'diffBars' => [
		[
			'X1' => $graphStartX + 1,
			'X2' => ($graphStartX+$graphMaxLength*(
				(max(10,
				31)/100)) ) - 2 - 2,
			'Y1' => $diffBarsStartY1 + (0*$diffBarsDistanceNextCriteria),
			'Y2' => $diffBarsStartY2 + (0*$diffBarsDistanceNextCriteria) - 2,
		],
		[
			'X1' => $graphStartX + 1,
			'X2' => ($graphStartX+$graphMaxLength*(
				(max(40,
				15)/100)) ) - 2 - 2,
			'Y1' => $diffBarsStartY1 + (1*$diffBarsDistanceNextCriteria) + $diffBarsDistanceNextBar + 1,
			'Y2' => $diffBarsStartY2 + (1*$diffBarsDistanceNextCriteria) + $diffBarsDistanceNextBar - 1,
		],
		[
			'X1' => $graphStartX + 1,
			'X2' => ($graphStartX+$graphMaxLength*(
				(max(70,
				80)/100)) ) - 2 - 2,
			'Y1' => $diffBarsStartY1 + (2*$diffBarsDistanceNextCriteria),
			'Y2' => $diffBarsStartY2 + (2*$diffBarsDistanceNextCriteria) - 2,
		],
		[
			'X1' => $graphStartX + 1,
			'X2' => ($graphStartX+$graphMaxLength*(
				(max(45,
				56)/100)) ) - 2 - 2,
			'Y1' => $diffBarsStartY1 + (3*$diffBarsDistanceNextCriteria),
			'Y2' => $diffBarsStartY2 + (3*$diffBarsDistanceNextCriteria) - 2,
		],
		[
			'X1' => $graphStartX + 1,
			'X2' => ($graphStartX+$graphMaxLength*(
				(max(15,
				23)/100)) ) - 2 - 2,
			'Y1' => $diffBarsStartY1 + (4*$diffBarsDistanceNextCriteria),
			'Y2' => $diffBarsStartY2 + (4*$diffBarsDistanceNextCriteria) - 2,
		],
		[
			'X1' => $graphStartX + 1,
			'X2' => ($graphStartX+$graphMaxLength*(
				(max(90,
				72)/100)) ) - 2 - 2,
			'Y1' => $diffBarsStartY1 + (5*$diffBarsDistanceNextCriteria) + $diffBarsDistanceNextBar + 1,
			'Y2' => $diffBarsStartY2 + (5*$diffBarsDistanceNextCriteria) + $diffBarsDistanceNextBar - 1,
		],
		[
			'X1' => $graphStartX + 1,
			'X2' => ($graphStartX+$graphMaxLength*(
				(max(10,
				60)/100)) ) - 2 - 2,
			'Y1' => $diffBarsStartY1 + (6*$diffBarsDistanceNextCriteria),
			'Y2' => $diffBarsStartY2 + (6*$diffBarsDistanceNextCriteria) - 2,
		],
		[
			'X1' => $graphStartX + 1,
			'X2' => ($graphStartX+$graphMaxLength*(
				(max(20,
				70)/100)) ) - 2 - 2,
			'Y1' => $diffBarsStartY1 + (7*$diffBarsDistanceNextCriteria),
			'Y2' => $diffBarsStartY2 + (7*$diffBarsDistanceNextCriteria) - 2,
		],
		[
			'X1' => $graphStartX + 1,
			'X2' => ($graphStartX+$graphMaxLength*(
				(max(85,
				65)/100)) ) - 2 - 2,
			'Y1' => $diffBarsStartY1 + (8*$diffBarsDistanceNextCriteria) + $diffBarsDistanceNextBar + 1,
			'Y2' => $diffBarsStartY2 + (8*$diffBarsDistanceNextCriteria) + $diffBarsDistanceNextBar - 1,
		]
	],
	'differenceTextX' => 1370 + $graphOffsetX,
	'diffTextsY' => [
		$diffBarsStartY2 + (0*$diffBarsDistanceNextCriteria) - 1 - 10 ,
		$diffBarsStartY2 + (1*$diffBarsDistanceNextCriteria) + $diffBarsDistanceNextBar + 1 - 10 ,
		$diffBarsStartY2 + (2*$diffBarsDistanceNextCriteria) - 10 ,
		$diffBarsStartY2 + (3*$diffBarsDistanceNextCriteria) - 10 ,
		$diffBarsStartY2 + (4*$diffBarsDistanceNextCriteria) - 10 ,
		$diffBarsStartY2 + (5*$diffBarsDistanceNextCriteria) + $diffBarsDistanceNextBar + 1 - 10 ,
		$diffBarsStartY2 + (6*$diffBarsDistanceNextCriteria) - 10 ,
		$diffBarsStartY2 + (7*$diffBarsDistanceNextCriteria) - 10 ,
		$diffBarsStartY2 + (8*$diffBarsDistanceNextCriteria) + $diffBarsDistanceNextBar + 1 - 10 ,
	],
	'diffs' => [
		"21",
		"25",
		"10",
		"11",
		"8",
		"18",
		"50",
		"50",
		"20",
	],
	'diffFormats' => [
		[
			"DrawBox" => TRUE,
			"R" => 255,
			"G" => 255,
			"B" => 255,
			"Alpha" => 100,
			"FontName" => $fonts["bold"],

			"BoxR" => $colors["redHighlight"]["R"],
			"BoxG" => $colors["redHighlight"]["G"],
			"BoxB" => $colors["redHighlight"]["B"],
			"BoxAlpha" => 100,
			"BoxRounded" => TRUE,
			"RoundedRadius" => 4,


		],
		[
			"DrawBox" => TRUE,
			"R" => 255,
			"G" => 255,
			"B" => 255,
			"Alpha" => 100,
			"FontName" => $fonts["bold"],

			"BoxR" => $colors["redHighlight"]["R"],
			"BoxG" => $colors["redHighlight"]["G"],
			"BoxB" => $colors["redHighlight"]["B"],
			"BoxAlpha" => 100,
			"BoxRounded" => TRUE,
			"RoundedRadius" => 4,


		],
		$diffBoxFormat,
		$diffBoxFormat,
		$diffBoxFormat,
        [
			"DrawBox" => TRUE,
			"R" => 255,
			"G" => 255,
			"B" => 255,
			"Alpha" => 100,
			"FontName" => $fonts["bold"],

			"BoxR" => $colors["redHighlight"]["R"],
			"BoxG" => $colors["redHighlight"]["G"],
			"BoxB" => $colors["redHighlight"]["B"],
			"BoxAlpha" => 100,
			"BoxRounded" => TRUE,
			"RoundedRadius" => 4,


		],
		[
			"DrawBox" => TRUE,
			"R" => 255,
			"G" => 255,
			"B" => 255,
			"Alpha" => 100,
			"FontName" => $fonts["bold"],

			"BoxR" => $colors["redHighlight"]["R"],
			"BoxG" => $colors["redHighlight"]["G"],
			"BoxB" => $colors["redHighlight"]["B"],
			"BoxAlpha" => 100,
			"BoxRounded" => TRUE,
			"RoundedRadius" => 4,


		],
		[
			"DrawBox" => TRUE,
			"R" => 255,
			"G" => 255,
			"B" => 255,
			"Alpha" => 100,
			"FontName" => $fonts["bold"],

			"BoxR" => $colors["redHighlight"]["R"],
			"BoxG" => $colors["redHighlight"]["G"],
			"BoxB" => $colors["redHighlight"]["B"],
			"BoxAlpha" => 100,
			"BoxRounded" => TRUE,
			"RoundedRadius" => 4,


		],
		[
			"DrawBox" => TRUE,
			"R" => 255,
			"G" => 255,
			"B" => 255,
			"Alpha" => 100,
			"FontName" => $fonts["bold"],

			"BoxR" => $colors["redHighlight"]["R"],
			"BoxG" => $colors["redHighlight"]["G"],
			"BoxB" => $colors["redHighlight"]["B"],
			"BoxAlpha" => 100,
			"BoxRounded" => TRUE,
			"RoundedRadius" => 4,


		],
		$diffBoxFormat
	]
);
return $climatePreset;
