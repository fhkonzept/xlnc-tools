<?php
$preset = require(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/stylesPreset.php');

$imageH = 360;
$graphEndY = $imageH - 85;
$thresholdLabelY = $graphEndY + 15;
$legendHeaderY = $thresholdLabelY + 44;
$legendBoxY1 = $legendHeaderY + 5;
$legendBoxY2 = $legendBoxY1 + 15;
$legendTextY = $legendBoxY2 - 1;

$preset['imageHeight'] = $imageH * $scaleFactor;
$preset['graphareaY2'] = $graphEndY * $scaleFactor;
$preset['thresholds']['lines']['labelY'] = $thresholdLabelY * $scaleFactor;
$preset['legendHeaderY'] = $legendHeaderY * $scaleFactor;

  $preset['legendBox1Y1']
= $preset['legendBox2Y1']
= $preset['legendBox3Y1']
= $preset['legendBox4Y1']
= $preset['legendBox5Y1']
= $legendBoxY1 * $scaleFactor;

  $preset['legendBox1Y2']
= $preset['legendBox2Y2']
= $preset['legendBox3Y2']
= $preset['legendBox4Y2']
= $preset['legendBox5Y2']
= $legendBoxY2 * $scaleFactor;

  $preset['legendTextY']
= $legendTextY * $scaleFactor;

$preset['legendTextYalt']
= $preset['legendTextY'] - 7 ;

// EAF6FE
$preset['legendBox1Format']["R"] = 234;
$preset['legendBox1Format']["G"] = 246;
$preset['legendBox1Format']["B"] = 254;

// A1DAF8
$preset['legendBox2Format']["R"] = 161;
$preset['legendBox2Format']["G"] = 218;
$preset['legendBox2Format']["B"] = 248;

// 0BBBEF
$preset['legendBox3Format']["R"] = 11;
$preset['legendBox3Format']["G"] = 187;
$preset['legendBox3Format']["B"] = 239;

// 0095DB
$preset['legendBox4Format']["R"] = 0;
$preset['legendBox4Format']["G"] = 149;
$preset['legendBox4Format']["B"] = 219;

// 0069B4
$preset['legendBox5Format']["R"] = 0;
$preset['legendBox5Format']["G"] = 105;
$preset['legendBox5Format']["B"] = 180;

$preset['chartFormat']['OverrideColors'] = [
    $preset['legendBox5Format'],
    $preset['legendBox1Format'],
    $preset['legendBox2Format'],
    $preset['legendBox3Format'],
    $preset['legendBox4Format'],
    $preset['legendBox5Format']
];

return $preset;
