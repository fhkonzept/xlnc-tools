<?php
$preset = require(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/climatePreset.php');

$preset['serie1Palette'] = $preset["legendBox1Format"] = [
	"R"         => 129,
	"G"         => 131,
	"B"         => 164,
	"Alpha"     => 100
];

$preset['serie2Palette'] = $preset["legendBox2Format"] = [
	"R"         => 188,
	"G"         => 193,
	"B"         => 212,
	"Alpha"     => 100
];

return $preset;
