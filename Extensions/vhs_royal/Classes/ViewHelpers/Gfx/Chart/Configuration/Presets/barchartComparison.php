<?php
$preset = require(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/stylesPreset.php');

$preset['serie1Palette'] = $preset['legendBox1Format'] = array(
	"R" => 131,
	"G" => 208,
	"B" => 245,
	"Alpha" => 100
);

$preset['serie2Palette'] = $preset['legendBox2Format'] = array(
	"R" => 73,
	"G" => 75,
	"B" => 97,
	"Alpha" => 100
);

return $preset;
