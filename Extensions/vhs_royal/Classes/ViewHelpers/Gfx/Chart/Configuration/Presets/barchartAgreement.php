<?php
$preset = require(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/stylesPreset.php');

$preset["chartFormat"]["DisplayValues"] = FALSE;

$preset['serie1Palette']['R'] = 125;
$preset['serie1Palette']['G'] = 125;
$preset['serie1Palette']['B'] = 125;

$preset["legendBox1Format"]["R"] = 125;
$preset["legendBox1Format"]["G"] = 125;
$preset["legendBox1Format"]["B"] = 125;

$preset['serie2Palette']['R'] = 238;
$preset['serie2Palette']['G'] = 239;
$preset['serie2Palette']['B'] = 239;

$preset['legendBox2Format']['R'] = 238;
$preset['legendBox2Format']['G'] = 239;
$preset['legendBox2Format']['B'] = 239;

$preset['legendBox2Format']['BorderR'] = 125;
$preset['legendBox2Format']['BorderG'] = 125;
$preset['legendBox2Format']['BorderB'] = 125;

$preset["chartFormat"]["BorderR"] = 125;
$preset["chartFormat"]["BorderG"] = 125;
$preset["chartFormat"]["BorderB"] = 125;

$preset["legendBox2X1"] = 455;
$preset["legendBox2X2"] = $preset["legendBox2X1"] + 15 * $scaleFactor;
$preset["legendText2X"] = $preset["legendBox2X2"] + 15;

$preset["legendBox2X1"] = $preset["legendBox1X1"];
$preset["legendBox2X2"] = $preset["legendBox1X2"];

$preset["legendBox2Y1"] = $preset["legendBox1Y1"] + 20  * $scaleFactor;
$preset["legendBox2Y2"] = $preset["legendBox1Y2"] + 20  * $scaleFactor;

$preset['thresholds']['items'] = [
    [
        "value" => 0.3,
        "label" => "",
        "X"     => 120 * $scaleFactor,
    ],
    [
        "value" => 33,
        "label" => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.thresholds.low", "xlnc_tools"),
        "X"     => $graphOffsetX + 191.5 * $scaleFactor,
    ],
    [
        "value" => 66,
        "label" => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.thresholds.middle", "xlnc_tools"),
        "X"     => $graphOffsetX + 372.25 * $scaleFactor,
    ],
    [
        "value" => 100,
        "label" => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.thresholds.high", "xlnc_tools"),
        "X"     => $graphOffsetX + 556.5 * $scaleFactor,
    ],
];

$preset['thresholds']['lines']["positions"] = [
    ["X" => $graphOffsetX + 101.5 * $scaleFactor, "label" => "0%"],
    ["X" => $graphOffsetX + 281.5 * $scaleFactor, "label" => "33%"],
    ["X" => $graphOffsetX + 463 * $scaleFactor, "label" => "66%"],
    ["X" => $graphOffsetX + 650 * $scaleFactor, "label" => "100%"],
];

return $preset;
