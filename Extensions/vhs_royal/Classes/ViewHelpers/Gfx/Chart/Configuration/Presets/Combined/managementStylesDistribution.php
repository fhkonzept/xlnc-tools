<?php
$preset = require(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/stylesPreset.php');
$imageH = 360;
$graphEndY = $imageH - 85;
$thresholdLabelY = $graphEndY + 15;
$legendHeaderY = $thresholdLabelY + 44;
$legendBoxY1 = $legendHeaderY + 5;
$legendBoxY2 = $legendBoxY1 + 15;
$legendTextY = $legendBoxY2 - 1;

$preset['imageHeight'] = $imageH * $scaleFactor;
$preset['graphareaY2'] = $graphEndY * $scaleFactor;
$preset['thresholds']['lines']['labelY'] = $thresholdLabelY * $scaleFactor;
$preset['legendHeaderY'] = $legendHeaderY * $scaleFactor;

$preset['legendBox2X1'] += 120;
$preset['legendBox2X2'] += 120;
$preset['legendText2X'] += 120;

$preset['legendBox3X1'] += 220;
$preset['legendBox3X2'] += 220;
$preset['legendText3X'] += 220;

$preset['chartFormat']['Rounded'] = 0;

$preset['values'] = [
    "development"   => [10,7,70,30,10,60],
    "potential"     => [30,13,25,40,10,30],
    "signature"     => [60,80,5,30,80,10]
];

$preset['names'] = [
    "development"   => "development",
    "potential"     => "potential",
    "signature"     => "signature"
];

$preset['palettes']['signature'] = [
    'R' => 73,
    'G' => 75,
    'B' => 97
];
$preset['palettes']['potential'] = [
    'R' => 128,
    'G' => 132,
    'B' => 164
];
$preset['palettes']['development'] = [
    'R' => 188,
    'G' => 193,
    'B' => 212
];

$preset['thresholds']['items'][0]['value'] = 0.1;

  $preset['legendBox1Y1']
= $preset['legendBox2Y1']
= $preset['legendBox3Y1']
= $legendBoxY1 * $scaleFactor;

  $preset['legendBox1Y2']
= $preset['legendBox2Y2']
= $preset['legendBox3Y2']
= $legendBoxY2 * $scaleFactor;

  $preset['legendTextY']
= $legendTextY * $scaleFactor;

  $preset['legendTextYalt']
= $legendTextY * $scaleFactor - 7 ;

return $preset;
