<?php
require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/constants.php');

$targetChartWidth = 1440;
$scaleFactor = $targetChartWidth/720;

$fontFamily = 'itcavantgardestd-bk-webfont'; // nice, thin
$fontFamily = 'itcavantgardepro-md-webfont'; // very nice, normal
#$fontFamily = 'MyriadPro-Regular';
#$fontFamily = 'fontawesome-webfont';
#$fontFamily = 'glyphicons-halflings-regular'; // Symbols (Fontawesome/Glyphicons) work with HTML-entities like &#9993; (envelope, see http://www.fileformat.info/info/unicode/char/2709/index.htm), not with Unicode
$fontR = 100;
$fontG = 100;
$fontB = 100;
$smallFontSize = 9 * $scaleFactor;
$largeFontSize = 10 * $scaleFactor;

return array(
	// Data
	// Serie 1
	'serie1Values'			=> array(10,40,70,45,15,90),
	'serie1Name'			=> 'Serie1',
	'serie1Description'		=> \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.forIs", "xlnc_tools"),
	// Serie 2
	'serie2Values'			=> array(31,15,80,56,23,72),
	'serie2Name'			=> 'Serie2',
	'serie2Description'		=> \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.climate.charts.selfIs", "xlnc_tools"),
	// Serie 3
	'serie3Values'			=> array(
                                    \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.serie3Values.1", "xlnc_tools"),
                                    \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.serie3Values.2", "xlnc_tools"),
                                    \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.serie3Values.3", "xlnc_tools"),
                                    \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.serie3Values.4", "xlnc_tools"),
                                    \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.serie3Values.5", "xlnc_tools"),
                                    \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate("reports.styles.charts.serie3Values.6", "xlnc_tools")
                                ),
	'serie3Name'			=> 'Absissa',

	// Axis
	'axisPosition'			=> AXIS_POSITION_LEFT,

	// Dimensions and coordinates
	'imageWidth'			=> 720 * $scaleFactor,
	'imageHeight'			=> 510 * $scaleFactor,

	'graphareaX1'			=> 100 * $scaleFactor,
	'graphareaY1'			=> 70 * $scaleFactor,
	'graphareaX2'			=> 650 * $scaleFactor,
	'graphareaY2'			=> 475 * $scaleFactor,

	'gradientarea3X1'		=> 100 * $scaleFactor,
	'gradientarea3Y1'		=> 70 * $scaleFactor,
	'gradientarea3X2'		=> 320 * $scaleFactor,
	'gradientarea3Y2'		=> 475 * $scaleFactor,

	'gradientarea4X1'		=> 320 * $scaleFactor,
	'gradientarea4Y1'		=> 70 * $scaleFactor,
	'gradientarea4X2'		=> 485 * $scaleFactor,
	'gradientarea4Y2'		=> 475 * $scaleFactor,

	'gradientarea5X1'		=> 485 * $scaleFactor,
	'gradientarea5Y1'		=> 70 * $scaleFactor,
	'gradientarea5X2'		=> 650 * $scaleFactor,
	'gradientarea5Y2'		=> 475 * $scaleFactor,

	'threshold1X'			=> 144 * $scaleFactor,
	'threshold1Y'			=> 40 * $scaleFactor,
	'threshold2X'			=> 350 * $scaleFactor,
	'threshold2Y'			=> 36 * $scaleFactor,
	'threshold3X'			=> 514 * $scaleFactor,
	'threshold3Y'			=> 40 * $scaleFactor,

	// Rect
	'rectFormat'			=> array("R"=>0, "G"=>0, "B"=>0, "Dash"=>1, "DashR"=>20, "DashG"=>20, "DashB"=>20),

	// Gradient Backgrounds
	'gradient1Direction'	=> DIRECTION_HORIZONTAL,
	'gradient2Direction'	=> DIRECTION_VERTICAL,
	'gradient1Format'		=> array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>238, "EndG"=>238, "EndB"=>238, "Alpha"=>95),
	'gradient2Format'		=> array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>238, "EndG"=>238, "EndB"=>238, "Alpha"=>95),
	'gradient3Format'		=> array("StartR"=>90, "StartG"=>156, "StartB"=>213, "EndR"=>90, "EndG"=>156, "EndB"=>213, "Alpha"=>30),
	'gradient4Format'		=> array("StartR"=>238, "StartG"=>125, "StartB"=>50, "EndR"=>238, "EndG"=>125, "EndB"=>50, "Alpha"=>30),
	'gradient5Format'		=> array("StartR"=>165, "StartG"=>165, "StartB"=>165, "EndR"=>165, "EndG"=>165, "EndB"=>165, "Alpha"=>30),

	// Shadow
	'shadowEnabled'			=> 1,
	'shadowFormat'			=> array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>10),

	// Fonts
	'mainFontFormat'		=> array("R"=>$fontR,"G"=>$fontG,"B"=>$fontB,"FontName"=>"typo3conf/ext/vhs_royal/Lib/pChart/fonts/" . $fontFamily . ".ttf","FontSize"=>$smallFontSize),

	// Scale Format
	'scaleFormat'			=> array(
								"Pos"=>SCALE_POS_TOPBOTTOM,
								"Mode"=>SCALE_MODE_MANUAL,
								"ManualScale"=>array(0=>array("Min"=>0,"Max"=>100)),
								"LabelingMethod"=>LABELING_ALL,
								"LabelSkip"=>0,
								"GridR"=>180, "GridG"=>180, "GridB"=>180, "GridAlpha"=>50,
								"TickR"=>0, "TickG"=>0, "TickB"=>0, "TickAlpha"=>50,
								"LabelRotation"=>0,
								"DrawXLines"=>0,
								"DrawSubTicks"=>0,
								"SubTickR"=>255,
								"SubTickG"=>0,
								"SubTickB"=>0,
								"SubTickAlpha"=>50,
								"DrawYLines"=>ALL),
	'axisDisplayType'		=> AXIS_FORMAT_CUSTOM,
	'axisDisplayFunc'		=> 'renderAxisLabel',

	// Bar chart format
	'chartFormat'			=> array(
								"DisplayValues"=>1,
								"Rounded"=>1,
								"AroundZero"=>1,
								"DisplayPos"=>LABEL_POS_INSIDE,
								"DisplayR"=>255,
								"DisplayG"=>255,
								"DisplayB"=>255,
								"DisplayShadow"=>0,
								"Gradient"=>1,
								"Surrounding"=>30
								),

	// Legend
	'legendX'				=> 100 * $scaleFactor,
	'legendY'				=> 490 * $scaleFactor,
	'legendFormat'			=> array(
								"FontR"=>$fontR, "FontG"=>$fontG, "FontB"=>$fontB, "FontName"=>"typo3conf/ext/vhs_royal/Lib/pChart/fonts/" . $fontFamily . ".ttf", "FontSize"=>$largeFontSize,
								"Margin"=>10,
								"Alpha"=>90,
								"BoxSize"=>15,
								"Style"=>LEGEND_NOBORDER,
								"Mode"=>LEGEND_HORIZONTAL,
								"Family"=>LEGEND_FAMILY_BOX
								),
	// Color
	'serie1Palette'			=> array("R"=>214,"G"=>130,"B"=>129,"Alpha"=>90),
	'serie2Palette'			=> array("R"=>184,"G"=>75,"B"=>73,"Alpha"=>90),
	// Threshholds
	'threshold1'			=> 40,
	'threshold2'			=> 70,
	'threshold3'			=> 100,
	'threshold1Format'		=> array("R"=>0, "G"=>0, "B"=>0, "Alpha"=>50, "AxisID"=>0, "Ticks"=>8, "WriteCaption"=>0, "Caption"=>"Entwicklungsbereich", "CaptionOffset"=>-80),
	'threshold2Format'		=> array("R"=>0, "G"=>0, "B"=>0, "Alpha"=>50, "AxisID"=>0, "Ticks"=>8, "WriteCaption"=>0, "Caption"=>"Potenzialbereich", "CaptionOffset"=>-80),
	'threshold3Format'		=> array("R"=>0, "G"=>0, "B"=>0, "Alpha"=>50, "AxisID"=>0, "Ticks"=>8, "WriteCaption"=>0, "Caption"=>"Signaturbereich", "CaptionOffset"=>-80),
);
?>
