<?php
$preset = require(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/climatePreset.php');

$preset['palettes']['signature'] = [
    'R' => 73,
    'G' => 75,
    'B' => 97
];
$preset['palettes']['potential'] = [
    'R' => 128,
    'G' => 132,
    'B' => 164
];
$preset['palettes']['development'] = [
    'R' => 188,
    'G' => 193,
    'B' => 212
];

$preset['values'] = [
    "development"   => [98,40,20,60,80,5,30,80,10],
    "potential"     => [2,20,40,30,13,25,40,10,30],
    "signature"     => [0,40,40,10,7,70,30,10,60]
];

$preset['legendBox2X2'] = $graphOffsetX +  315 * $scaleFactor;
$preset['legendBox2X1'] = $graphOffsetX +  300 * $scaleFactor;
$preset['legendText2X'] = $graphOffsetX +  320 * $scaleFactor;

$preset['legendBox2X1'] -= 20;
$preset['legendBox2X2'] -= 20;
$preset['legendText2X'] -= 20;

$preset['legendBox3X1'] =
2* $preset['legendBox2X1']
-  $preset['legendBox1X1'];

$preset['legendBox3X2'] =
    $preset['legendBox3X1']
+   $preset['legendBox2X2']
-   $preset['legendBox2X1'];

$preset['legendText3X'] =
    $preset['legendBox3X2']
+   $preset['legendText2X']
-   $preset['legendBox2X2'];

$legendTextY += 3;


$preset['thresholds']['items'][0]['value'] = 0.1;

$preset['legendBox1Y1']
= $preset['legendBox2Y1']
= $preset['legendBox3Y1']
= $legendBoxY1 * $scaleFactor;

$preset['legendBox1Y2']
= $preset['legendBox2Y2']
= $preset['legendBox3Y2']
= $legendBoxY2 * $scaleFactor;

$preset['legendTextY']
= $legendTextY * $scaleFactor;

$preset['legendTextYalt']
= $legendTextY * $scaleFactor - 7 ;

$preset['chartFormat']['Rounded'] = 0;


$preset['names'] = [
    "development"   => "development",
    "potential"     => "potential",
    "signature"     => "signature"
];


$preset['palettes']['signature'] = [
    'R' => 242,
    'G' => 145,
    'B' => 23
];
$preset['palettes']['potential'] = [
    'R' => 248,
    'G' => 178,
    'B' => 71
];
$preset['palettes']['development'] = [
    'R' => 253,
    'G' => 217,
    'B' => 154
];


return $preset;
