<?php
$preset = require(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/climatePreset.php');

$preset['serie1Palette'] = $preset["legendBox1Format"] = [
    "R"         => 245,
    "G"         => 159,
    "B"         => 53,
    "Alpha"     => 100,
];

$preset['serie2Palette'] = $preset["legendBox2Format"] = [
    "R"         => 253,
    "G"         => 212,
    "B"         => 140,
    "Alpha"     => 100,
];

return $preset;
