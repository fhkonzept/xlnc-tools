<?php
$preset = require(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/stylesPreset.php');
$imageH = 360;
$graphEndY = $imageH - 85;
$thresholdLabelY = $graphEndY + 15;
$legendHeaderY = $thresholdLabelY + 44;
$legendBoxY1 = $legendHeaderY + 5;
$legendBoxY2 = $legendBoxY1 + 15;
$legendTextY = $legendBoxY2 - 1;

$preset['imageHeight'] = $imageH * $scaleFactor;
$preset['graphareaY2'] = $graphEndY * $scaleFactor;
$preset['thresholds']['lines']['labelY'] = $thresholdLabelY * $scaleFactor;
$preset['legendHeaderY'] = $legendHeaderY * $scaleFactor;

  $preset['legendBox1Y1']
= $preset['legendBox2Y1']
= $preset['legendBox3Y1']
= $preset['legendBox4Y1']
= $preset['legendBox5Y1']
= $legendBoxY1 * $scaleFactor;

  $preset['legendBox1Y2']
= $preset['legendBox2Y2']
= $preset['legendBox3Y2']
= $preset['legendBox4Y2']
= $preset['legendBox5Y2']
= $legendBoxY2 * $scaleFactor;

  $preset['legendTextY']
= $legendTextY * $scaleFactor;

  $preset['legendTextYalt']
= $legendTextY * $scaleFactor - 7 ;

$preset["chartFormat"]["OverrideColors"] =
	array(
		"0" => array(
			"R"		=> 188,
			"G"		=> 224,
			"B"		=> 46,
			"Alpha"	=> 100
		),
		"1" => array(
			"R"		=> 224,
			"G"		=> 100,
			"B"		=> 46,
			"Alpha" => 100
		),
		"2" => array(
			"R"		=> 224,
			"G"		=> 214,
			"B"		=> 46,
			"Alpha" => 100
		),
		"3" => array(
			"R"		=> 46,
			"G"		=> 151,
			"B"		=> 224,
			"Alpha" => 100
		),
		"4" => array(
			"R"		=> 176,
			"G"		=> 46,
			"B"		=> 224,
			"Alpha" => 100
		),
		"5" => array(
			"R"		=> 224,
			"G"		=> 46,
			"B"		=> 117,
			"Alpha" => 100
		)
	);

$preset["legendBox1Format"]["R"] = 255;
$preset["legendBox1Format"]["G"] = 0;
$preset["legendBox1Format"]["B"] = 0;

$preset["legendBox2Format"]["R"] = 255;
$preset["legendBox2Format"]["G"] = 0;
$preset["legendBox2Format"]["B"] = 0;

$preset["legendBox3Format"]["R"] = 255;
$preset["legendBox3Format"]["G"] = 0;
$preset["legendBox3Format"]["B"] = 0;

$preset["legendBox4Format"]["R"] = 255;
$preset["legendBox4Format"]["G"] = 0;
$preset["legendBox4Format"]["B"] = 0;

$preset["legendBox5Format"]["R"] = 255;
$preset["legendBox5Format"]["G"] = 0;
$preset["legendBox5Format"]["B"] = 0;

return $preset;
