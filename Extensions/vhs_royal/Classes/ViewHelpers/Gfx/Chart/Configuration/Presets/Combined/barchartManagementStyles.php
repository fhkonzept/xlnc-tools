<?php
$preset = require(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Classes/ViewHelpers/Gfx/Chart/Configuration/Presets/barchartSelf.php');

unset($preset['chartFormat']['OverrideColors']);

$preset['serie1Palette'] = [
    'R' => 73,
    'G' => 75,
    'B' => 97
];

return $preset;
