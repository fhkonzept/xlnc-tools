<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Set;

class AxisdisplayViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $AxisID
	* @param string $Mode
	* @param string $Format
	* @return void
	*/
	public function render($AxisID, $Mode = "", $Format = "") {
		$pData = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pData");
		$pData->setAxisDisplay($AxisID, $Mode, $Format);
	}
}
