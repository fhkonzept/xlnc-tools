<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Set;

class ScatterserieticksViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $ID
	* @param string $Width
	* @return void
	*/
	public function render($ID, $Width = "") {
		$pData = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pData");
		$pData->setScatterSerieTicks($ID, $Width);
	}
}
