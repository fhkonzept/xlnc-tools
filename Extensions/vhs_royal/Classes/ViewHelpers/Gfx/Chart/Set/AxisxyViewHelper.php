<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Set;

class AxisxyViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $AxisID
	* @param string $Identity
	* @return void
	*/
	public function render($AxisID, $Identity = "") {
		$pData = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pData");
		$pData->setAxisXY($AxisID, $Identity);
	}
}
