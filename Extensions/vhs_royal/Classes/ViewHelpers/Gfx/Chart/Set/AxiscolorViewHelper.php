<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Set;

class AxiscolorViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $AxisID
	* @param array $Format
	* @return void
	*/
	public function render($AxisID, $Format) {
		$pData = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pData");
		$pData->setAxisColor($AxisID, $Format);
	}
}
