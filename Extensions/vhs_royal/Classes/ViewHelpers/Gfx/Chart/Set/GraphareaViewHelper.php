<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Set;

class GraphareaViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $X1
	* @param string $Y1
	* @param string $X2
	* @param string $Y2
	* @return void
	*/
	public function render($X1, $Y1, $X2, $Y2) {
		$pImage = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pImage");
		$pImage->setGraphArea($X1, $Y1, $X2, $Y2);
	}
}
