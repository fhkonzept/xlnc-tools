<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Set;

class SeriedrawableViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $Series
	* @param string $Drawable
	* @return void
	*/
	public function render($Series, $Drawable = "") {
		$pData = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pData");
		$pData->setSerieDrawable($Series, $Drawable);
	}
}
