<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx\Chart\Set;

class ScatterserieViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	* @param string $SerieX
	* @param string $SerieY
	* @param string $ID
	* @return void
	*/
	public function render($SerieX, $SerieY, $ID = "") {
		$pData = $this->viewHelperVariableContainer->get("Fhkonzept\VhsRoyal\ViewHelpers\Gfx\ChartViewHelper", "pData");
		$pData->setScatterSerie($SerieX, $SerieY, $ID);
	}
}
