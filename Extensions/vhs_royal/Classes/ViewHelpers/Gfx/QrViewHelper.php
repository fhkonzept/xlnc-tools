<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx;

require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Lib/qrcode/qrcode.php');

/**
* CURRENT: QR Code Class (http://sourceforge.net/projects/phpqrclass)
*/
class QrViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	private $extConf;

	public function initialize() {

		$this->extConf = @unserialize($GLOBALS['TYPO3_CONF_VARS'] ['EXT'] ['extConf'] ['vhs_royal']);
		$this->extConf['tempPath'] = $this->extConf['tempPath'] ? rtrim($this->extConf['tempPath'], '/') . '/' : 'typo3temp/pics/';

	}

	/**
	 * @param integer $z (QR Code size factor * 61), default 4
	 * @param string $dest (A: absolute path, R: relative path, B: image tag)
	 * @param string $filename (filename without file extension)
	 * @return string
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:gfx.qr z="50" dest="B" filename="meinQrCode">Mein QR Text</vhsroyal:gfx.qr>
	 * </code>
	 */
	public function render($z = 4, $dest = 'B', $filename = 'qr') {

		$a = new \QR(trim($this->renderChildren()));
		$buffer = $a->image($z);

		$relFilePath = $this->extConf['tempPath'] . $filename. '.png';
		$absFilePath = PATH_site . $relFilePath;

		@file_put_contents($absFilePath, $buffer);
		unset($buffer);

		if($dest == 'B') {
			return '<img src="' . $relFilePath . '" width="' . ($z*61) . '" height="' . ($z*61) . '" />';
		} else if($dest == 'A') {
			return $absFilePath;
		} else if($dest == 'R') {
			return $relFilePath;
		}
	}

}
