<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Gfx;

require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Lib/gd-heatmap/gd_heatmap.php');

class HeatmapViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	private $extConf;

	public function initialize() {

		$this->extConf = @unserialize($GLOBALS['TYPO3_CONF_VARS'] ['EXT'] ['extConf'] ['vhs_royal']);
		$this->extConf['tempPath'] = $this->extConf['tempPath'] ? rtrim($this->extConf['tempPath'], '/') . '/' : 'typo3temp/pics/';

	}

	/**
	 * @param mixed $data
	 * @param integer $width
	 * @param integer $height
	 * @return string
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:gfx.heatmap data="{mydata}" width="640" height="480" />
	 * </code>
	 */
	public function render($data, $width, $height) {

		return $this->getTestHeatmap();
	}

	private function getTestHeatmap() {

		// Generate some test data using the static function provided for that purpose.
		$data = \gd_heatmap::get_test_data(1240, 600, 50, 500);

		// Config array with all the available options. See the constructor's doc block
		// for explanations.
		$config = array(
		  'debug' => TRUE,
		  'width' => 1240,
		  'height' => 600,
		  'noc' => 32,
		  'r' => 50,
		  'dither' => FALSE,
		  'format' => 'png',
		);

		// Create a new heatmap based on the data and the config.
		$heatmap = new \gd_heatmap($data, $config);

		// And print it out. If you're having trouble getting any images out of the
		// library , comment this out to allow your browser to show you the error
		// messages.
		// $heatmap->output();

		// Or save it to a file. Don't forget to set correct file permissions in the
		// target directory.
		$tmpFile = PATH_site . $this->extConf['tempPath'] . 'heatmap.png';
		$heatmap->output($tmpFile);

		return $tmpFile;

	}
}
