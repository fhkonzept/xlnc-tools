<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf;

require_once('Tcpdf.php');

class TcpdfViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	private $extConf;

	public function initialize() {

		$this->extConf = @unserialize($GLOBALS['TYPO3_CONF_VARS'] ['EXT'] ['extConf'] ['vhs_royal']);
		$this->extConf['tempPath'] = $this->extConf['tempPath'] ? rtrim($this->extConf['tempPath'], '/') . '/' : 'typo3temp/';

	}

	/**
	 * @param array $configuration
	 * @param \ZipArchive $zip
	 * @return void|string
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:pdf.tcpdf configuration="{format:'A4',orientation:'landscape',dest:'D',destname:'test.pdf'}">
	 *		<children /> ...
	 *	</vhsroyal:pdf.tcpdf>
	 * </code>
	 */
	public function render(array $configuration = array('format' => 'A4', 'orientation' => 'portrait', 'dest' => 'D', 'destname' => 'doc.pdf'), $zip = NULL) {

		$tcpdf = new Tcpdf($configuration['orientation'], 'mm', $configuration['format']);

		if($this->viewHelperVariableContainer->exists(get_class($this), 'tcpdf')) {
			$this->viewHelperVariableContainer->remove(get_class($this), 'tcpdf');
		}
		$this->viewHelperVariableContainer->add(get_class($this), 'tcpdf', $tcpdf);

		$this->renderChildren();
		
		if($configuration['dest'] == 'D') {

			// Generate and send to browser
			$tcpdf->Output($configuration['destname'], $configuration['dest']);

		} else if($configuration['dest'] == 'F') {

			$absExportedFilePath = PATH_site . $this->extConf['tempPath'] . $configuration['destname'];

			// Generate and save to disk
			$tcpdf->Output($absExportedFilePath, $configuration['dest']);

			if( is_null($zip))
				return $absExportedFilePath;
			else
				$zip->addFile($absExportedFilePath, basename($absExportedFilePath));
				
			return;
		}

	}

}
