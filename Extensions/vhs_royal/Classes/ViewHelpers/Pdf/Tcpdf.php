<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf;

require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath ( 'vhs_royal' ) . 'Lib/tcpdf/tcpdf.php');

class Tcpdf extends \TCPDF { 

	private $footerHtml = '';
	private $footerImage = NULL;

	/**
	 * This method is used to render the page footer.
	 * It is automatically called by AddPage() and could be overwritten in your own inherited class.
	 * @public
	 */
	public function Footer() {

		$currentMargins = $this->getMargins();

		$this->SetMargins(0, 0, 0, 1);
		$this->SetY(257);
		
		if($this->getFooterHtml() == '') {
			$html = '<table width="494">
				<tr>
				<td style="color: #666666; font-size: 10pt"></td>
				<td style="color: #666666; font-size: 10pt; text-align: right">###PAGENUM###</td>
				</tr>
			</table>';
		} else {
			$html = $this->getFooterHtml();
		}

		if($this->getFooterImage() != NULL && $this->getFooterImage()['file'] != '') {
			$footerImage = $this->getFooterImage();
			if(is_file(PATH_site . $footerImage['file']))
				$this->Image($footerImage['file'], $footerImage['x'], $footerImage['y'], $footerImage['w'], $footerImage['h'], $footerImage['type']);
		}

		$html = str_replace('###PAGENUM###', $this->getAliasNumPage(), $html);

		$this->writeHTML($html, true, false, false, false, 'L');

		$this->SetMargins($currentMargins['left'], $currentMargins['top'], $currentMargins['right'], 1);
	}

	public function setFooterHtml($html) {
		$this->footerHtml = $html;
	}

	public function getFooterHtml() {
		return $this->footerHtml;
	}

	public function setFooterImage($file, $w, $h, $x, $y, $type) {
		$this->footerImage = array(
			'file'	=> $file,
			'w'		=> $w,
			'h'		=> $h,
			'x'		=> $x, 
			'y'		=> $y,
			'type'	=> $type
			);
	}

	public function getFooterImage() {
		return $this->footerImage;
	}
}
