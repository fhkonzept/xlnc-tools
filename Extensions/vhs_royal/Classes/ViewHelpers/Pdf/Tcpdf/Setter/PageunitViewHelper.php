<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf\Tcpdf\Setter;

class PageunitViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param string $unit (string) User measure unit. Possible values are:<ul><li>pt: point</li><li>mm: millimeter (default)</li><li>cm: centimeter</li><li>in: inch</li></ul><br />A point equals 1/72 of inch, that is to say about 0.35 mm (an inch being 2.54 cm). This is a very common unit in typography; font sizes are expressed in that unit.
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:pdf.tcpdf.setter.pageunit unit="mm"></vhsroyal:pdf.tcpdf.setter.pageunit>
	 * </code>
	 */
	public function render($unit = 'mm') {

		$tcpdf = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Pdf\TcpdfViewHelper', 'tcpdf');
		$tcpdf->setPageUnit($unit);

	}
}
