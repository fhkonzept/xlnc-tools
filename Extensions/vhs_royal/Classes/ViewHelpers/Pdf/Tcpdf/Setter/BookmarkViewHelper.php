<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf\Tcpdf\Setter;

class BookmarkViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Adds a bookmark - alias for Bookmark().
	 * @param $txt (string) Bookmark description.
	 * @param $level (int) Bookmark level (minimum value is 0).
	 * @param $y (float) Y position in user units of the bookmark on the selected page (default = -1 = current position; 0 = page start;).
	 * @param $page (int|string) Target page number (leave empty for current page). If you prefix a page number with the * character, then this page will not be changed when adding/deleting/moving pages.
	 * @param $style (string) Font style: B = Bold, I = Italic, BI = Bold + Italic.
	 * @param $color (array) RGB color array (values from 0 to 255).
	 * @param $x (float) X position in user units of the bookmark on the selected page (default = -1 = current position;).
	 * @param $link (mixed) URL, or numerical link ID, or named destination (# character followed by the destination name), or embedded file (* character followed by the file name).
	 * @public
	 */
	public function render($txt, $level = 0, $y = -1, $page = 0, $style = 'B', $color = array(0,0,0), $x = -1, $link = '') {

		$tcpdf = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Pdf\TcpdfViewHelper', 'tcpdf');
		$tcpdf->Bookmark($txt, $level, $y, $page, $style, $color, $x, $link);

	}
}
