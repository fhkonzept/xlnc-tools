<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf\Tcpdf\Setter;

class PageViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param string $pageNo (integer) Number of page to set pointer to
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:pdf.tcpdf.setter.page pageNo="2"></vhsroyal:pdf.tcpdf.setter.page>
	 * </code>
	 */
	public function render($pageNo) {

		$tcpdf = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Pdf\TcpdfViewHelper', 'tcpdf');
		$tcpdf->setPage($pageNo);

	}
}
