<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf\Tcpdf\Setter;

class MarginsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $left (float) Left margin.
	 * @param $top (float) Top margin.
	 * @param $right (float) Right margin. Default value is the left one.
	 * @param $keepmargins (boolean) if true overwrites the default page margins
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:pdf.tcpdf.setter.margins left="20" top="40" right="20" keepmargins="1"></vhsroyal:pdf.tcpdf.setter.margins>
	 * </code>
	 */
	public function render($left, $top, $right=-1, $keepmargins=false) {

		$tcpdf = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Pdf\TcpdfViewHelper', 'tcpdf');
		$tcpdf->SetMargins($left, $top, $right, $keepmargins);

	}
}
