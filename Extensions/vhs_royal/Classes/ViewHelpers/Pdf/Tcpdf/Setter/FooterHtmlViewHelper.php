<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf\Tcpdf\Setter;

class FooterHtmlViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:pdf.tcpdf.setter.footerHtml><p>My footer</p></vhsroyal:pdf.tcpdf.setter.footerHtml>
	 * </code>
	 */
	public function render() {

		$tcpdf = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Pdf\TcpdfViewHelper', 'tcpdf');
		$tcpdf->setFooterHtml(self::$defaultStyleSheet . $this->renderChildren());

	}

	static protected $defaultStyleSheet = <<<HTML
	<style>
		.footer {
			width: 93.2%;
			width: 534px;

		}

		.footer td {
			text-align: right;
			font-size: 11pt;
			color: #767a9f;
			vertical-align: middle;
			height: 36px;
		}
	</style>
HTML;
}
