<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf\Tcpdf\Setter;

class RtlViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $enable (Boolean) if true enable Right-To-Left language mode.
	 * @param $resetx (Boolean) if true reset the X position on direction change.
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:pdf.tcpdf.setter.rtl enable="1"></vhsroyal:pdf.tcpdf.setter.rtl>
	 * </code>
	 */
	public function render($enable, $resetx=true) {

		$tcpdf = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Pdf\TcpdfViewHelper', 'tcpdf');
		$tcpdf->setRTL($enable, $resetx);

	}
}
