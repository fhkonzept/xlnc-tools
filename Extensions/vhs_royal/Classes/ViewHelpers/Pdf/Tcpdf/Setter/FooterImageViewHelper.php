<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf\Tcpdf\Setter;

class FooterImageViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $w (float)
	 * @param $h (float)
	 * @param $x (float)
	 * @param $y (float)
	 * @param $type (string)
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:pdf.tcpdf.setter.footerImage x="" y="" w="" h="20" type="PNG">relative/path/from/docroot/to/myimage.png</vhsroyal:pdf.tcpdf.setter.footerImage>
	 * </code>
	 */
	public function render($w = '', $h = '', $x = '', $y = '', $type = '') {

		$tcpdf = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Pdf\TcpdfViewHelper', 'tcpdf');
		$tcpdf->SetFooterImage($this->renderChildren(), $w, $h, $x, $y, $type);

	}
}
