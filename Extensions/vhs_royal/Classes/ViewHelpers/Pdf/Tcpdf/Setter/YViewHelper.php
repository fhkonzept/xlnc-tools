<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf\Tcpdf\Setter;

class YViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $y (float) The value of the ordinate in user units.
	 * @param $mode (string) absolute, add or subtract. Default: absolute
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:pdf.tcpdf.setter.y y="70"></vhsroyal:pdf.tcpdf.setter.y>
	 * </code>
	 */
	public function render($y, $mode = 'absolute') {

		$tcpdf = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Pdf\TcpdfViewHelper', 'tcpdf');

		switch($mode) {

			case 'add':
				$tcpdf->SetY($tcpdf->GetY() + $y);
				break;

			case 'subtract':
				$tcpdf->SetY($tcpdf->GetY() - $y);
				break;

			case 'absolute':
			default:
				$tcpdf->SetY($y);
				break;
		}

	}
}
