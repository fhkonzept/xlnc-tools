<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf\Tcpdf\Setter;

class FooterfontViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $family (string) Family font.
	 * @param $style (string) Font style.
	 * @param $size (float) Font size in points. The default value is the current size.
	 * @param $fontfile (string) The font definition file.
	 * @param $subset (mixed)
	 * @see FontViewHelper
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:pdf.tcpdf.setter.footerfont family="times" style="regular" size="12"></vhsroyal:pdf.tcpdf.setter.footerfont>
	 * </code>
	 */
	public function render($family, $style='', $size=null, $fontfile='', $subset='default') {

		$tcpdf = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Pdf\TcpdfViewHelper', 'tcpdf');
		$tcpdf->SetFooterFont(array($family, $style, $size));

	}
}
