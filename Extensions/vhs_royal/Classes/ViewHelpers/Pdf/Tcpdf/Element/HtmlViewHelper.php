<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf\Tcpdf\Element;

class HtmlViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Allows to preserve some HTML formatting (limited support).<br />
	 * IMPORTANT: The HTML must be well formatted - try to clean-up it using an application like HTML-Tidy before submitting.
	 * Supported tags are: a, b, blockquote, br, dd, del, div, dl, dt, em, font, h1, h2, h3, h4, h5, h6, hr, i, img, li, ol, p, pre, small, span, strong, sub, sup, table, tcpdf, td, th, thead, tr, tt, u, ul
	 * NOTE: all the HTML attributes must be enclosed in double-quote.
	 * @param $ln (boolean) if true add a new line after text (default = true)
	 * @param $fill (boolean) Indicates if the background must be painted (true) or transparent (false).
	 * @param $reseth (boolean) if true reset the last cell height (default false).
	 * @param $cell (boolean) if true add the current left (or right for RTL) padding to each Write (default false).
	 * @param $align (string) Allows to center or align the text. Possible values are:<ul><li>L : left align</li><li>C : center</li><li>R : right align</li><li>'' : empty string : left for LTR or right for RTL</li></ul>
	 * @public
	 */
	public function render($ln=true, $fill=false, $reseth=false, $cell=false, $align='') {

		$tcpdf = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Pdf\TcpdfViewHelper', 'tcpdf');
		$tcpdf->writeHTML(self::$defaultStyleSheet . $this->renderChildren(), $ln, $fill, $reseth, $cell, $align);

	}

	static protected $defaultStyleSheet = <<<HTML
	<style>
		* {
			font-size: 11pt;
			color: #3C3C3C;
			line-height: 15pt;
		}

		b, strong {
			font-family: 'sourcesanspro';
			font-weight: normal;
		}

		i, em {
			font-family: 'sourcesansproi';
		}

		h1.title {
			font-family: 'sourcesanspro';
			font-weight: normal;
			color: #494B61;
			font-size: 28pt
		}

		h2.cover {
			font-size: 14pt;
			color: #3C3C3C;
			line-height: 22pt;
		}

		h2 {
			font-size: 24pt;
			line-height: 30pt;
			font-weight: normal;
			font-family: 'sourcesanspro';
			color: #494B61;
			text-align: center;
		}

		h3 {
			font-family: 'sourcesanspro';
			font-weight: normal;
			color: #494B61;
			font-size: 18pt;
			line-height: 24pt;
			text-align: center;
		}

		h3 em, h3 i {
			font-family: 'sourcesansproit';
		}

		.value-header {
			font-size: 8pt;
			text-align: center;
			font-family: 'sourcesansprob';
		}

		table th, .small {
			font-size: 8pt;
		}

		table td {
			font-size: 11pt;
		}
	</style>
HTML;
}
