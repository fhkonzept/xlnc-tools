<?php
namespace Fhkonzept\VhsRoyal\ViewHelpers\Pdf\Tcpdf\Element;

class ImageViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param $x (float) Abscissa of the upper-left corner (LTR) or upper-right corner (RTL).
	 * @param $y (float) Ordinate of the upper-left corner (LTR) or upper-right corner (RTL).
	 * @param $w (float) Width of the image in the page. If not specified or equal to zero, it is automatically calculated.
	 * @param $h (float) Height of the image in the page. If not specified or equal to zero, it is automatically calculated.
	 * @param $type (string) Image format. Possible values are (case insensitive): JPEG and PNG (whitout GD library) and all images supported by GD: GD, GD2, GD2PART, GIF, JPEG, PNG, BMP, XBM, XPM;. If not specified, the type is inferred from the file extension.
	 * @param $link (mixed) URL or identifier returned by AddLink().
	 * @param $align (string) Indicates the alignment of the pointer next to image insertion relative to image height. The value can be:<ul><li>T: top-right for LTR or top-left for RTL</li><li>M: middle-right for LTR or middle-left for RTL</li><li>B: bottom-right for LTR or bottom-left for RTL</li><li>N: next line</li></ul>
	 * @param $resize (mixed) If true resize (reduce) the image to fit $w and $h (requires GD or ImageMagick library); if false do not resize; if 2 force resize in all cases (upscaling and downscaling).
	 * @param $dpi (int) dot-per-inch resolution used on resize
	 * @param $palign (string) Allows to center or align the image on the current line. Possible values are:<ul><li>L : left align</li><li>C : center</li><li>R : right align</li><li>'' : empty string : left for LTR or right for RTL</li></ul>
	 * @param $ismask (boolean) true if this image is a mask, false otherwise
	 * @param $imgmask (mixed) image object returned by this function or false
	 * @param $border (mixed) Indicates if borders must be drawn around the cell. The value can be a number:<ul><li>0: no border (default)</li><li>1: frame</li></ul> or a string containing some or all of the following characters (in any order):<ul><li>L: left</li><li>T: top</li><li>R: right</li><li>B: bottom</li></ul> or an array of line styles for each border group - for example: array('LTRB' => array('width' => 2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)))
	 * @param $fitbox (mixed) If not false scale image dimensions proportionally to fit within the ($w, $h) box. $fitbox can be true or a 2 characters string indicating the image alignment inside the box. The first character indicate the horizontal alignment (L = left, C = center, R = right) the second character indicate the vertical algnment (T = top, M = middle, B = bottom).
	 * @param $hidden (boolean) If true do not display the image.
	 * @param $fitonpage (boolean) If true the image is resized to not exceed page dimensions.
	 * @param $alt (boolean) If true the image will be added as alternative and not directly printed (the ID of the image will be returned).
	 * @param $altimgs (array) Array of alternate images IDs. Each alternative image must be an array with two values: an integer representing the image ID (the value returned by the Image method) and a boolean value to indicate if the image is the default for printing.
	 * @return void
	 * Fluid example:
	 * <code>
	 *	<vhsroyal:pdf.tcpdf.element.image x="0" y="" w="60" file="/Volumes/HTDOCS/htdocs/casting-network-relaunch/typo3conf/ext/vhs_royal/Resources/Public/Images/logo.png" type="PNG" dpi="72" palign="C"></vhsroyal:pdf.tcpdf.element.image>
	 * </code>
	 */
	public function render($x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array()) {

		global $CURRENT_IMAGE_END_Y;
		global $CURRENT_PAGE_NO;

		$tcpdf = $this->viewHelperVariableContainer->get('Fhkonzept\VhsRoyal\ViewHelpers\Pdf\TcpdfViewHelper', 'tcpdf');
		$file = trim($this->renderChildren());

		if($CURRENT_IMAGE_END_Y > $tcpdf->getY() && (!$CURRENT_PAGE_NO || $tcpdf->pageNo() == $CURRENT_PAGE_NO)) {
			$tcpdf->setY($CURRENT_IMAGE_END_Y + 10);
		}

		if(strstr($file, 'http')) {
			
			// Temp (cache) dir for tcpdf
			$tempDir = PATH_site . 'typo3temp/tcpdf_images';
			if(!is_dir($tempDir)) {
				\TYPO3\CMS\Core\Utility\GeneralUtility::mkdir_deep($tempDir);
			}

			$fileAbsPath = $tempDir . '/'. md5($file) . '.jpg';
			// Cached
			if(!file_exists($fileAbsPath)) {
				$filecontent = file_get_contents($file);
				if($filecontent) {
					file_put_contents($fileAbsPath, $filecontent);
					unset($filecontent);
				} else {
					return;
				}
			}

			$file = $fileAbsPath;
			
		} else {

			if(!file_exists($file))
				$file = PATH_site . $file;

			if(!file_exists($file))
				return;

		}

		// Get image dimensions to force pagebreak
		$imageDimensions = getimagesize($file);
		if(is_array($imageDimensions)) {
			$widthUnits = $tcpdf->pixelsToUnits($imageDimensions[0]);
			$heightUnits = $tcpdf->pixelsToUnits($imageDimensions[1]);
			$ratio = $heightUnits / $widthUnits;
			if(!$h && $w) {
				$h = $w * $ratio;
			}
			$y = $y ? $y : $tcpdf->getY();
		}

		$prevPage = $tcpdf->pageNo();

		$tcpdf->Image($file, $x, $y, $w, $h, $type, $link, $align, $resize, $dpi, $palign, $ismask, $imgmask, $border, $fitbox, $hidden, $fitonpage, $alt, $altimgs);

		$curPage = $tcpdf->pageNo();

		$CURRENT_PAGE_NO = $tcpdf->pageNo();
		$CURRENT_IMAGE_END_Y = $tcpdf->getY();

		if($align == 'N' && $y) {
			$tcpdf->setY($y);
			if($prevPage != $curPage) {
				$tcpdf->setY($CURRENT_IMAGE_END_Y - $h);
			}
		}
	}
}
