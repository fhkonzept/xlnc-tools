<?php
$EM_CONF[$_EXTKEY] = array(
	'title' => 'ViewHelpers Royal for e.g. PDF/Excel generation',
	'description' => '',
	'category' => 'fh-konzept',
	'author' => 'Frank Fischer',
	'author_email' => 'fischer@fh-konzept.de',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '0.0.1',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.2',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);