angular.module('Xlnc.Backend.Tooltip', []).directive('tooltip', function(){

    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            var $el = TYPO3.jQuery(element),
                position = $el.data('tooltipPosition') || 'top',
                initialized = false;

            $el.hover(function(){
                // on mouseenter
                if(!initialized) {
                    $el.tooltip( {placement: position, container: 'body'});
                    initialized = true;
                }
                $el.tooltip('show');
            }, function(){
                // on mouseleave
                $el.tooltip('hide');
            });
        }
    };
});
