/* global angular */
angular.module('Xlnc.Backend.ManagementController', ['ngSanitize']).controller('Management', function($scope, ManagementDataService, $sce) {
    "use strict";

    this.searchTerm = '';

    this.mailInvitations = [];

    this.currentInvitationTextLanguageUid = 0;

    this.sanitizer = $sce;

    this.init = function () {
        this.data = new ManagementDataService();
        this.data.load();
        // @todo investigate where this is being used.
        this.appHref = T3_THIS_LOCATION;
    };

    this.downloadReport = function (team, tool, language, variant) {
        
        var url = '/api/teams/' + team.uid + '/report/tool/' + tool.uid + '/language/' + language.uid,
            fileName = team.title + '-' + tool.title + '.pdf';
        
        if ( typeof variant !== 'undefined' ) {
            url = url + '/variant/' + variant,
            fileName = team.title + '-' + tool.title + '-SE-' + '.pdf';
        }

        this.data.download(url, fileName);
    };

    this.downloadSingleReport = function (team, tool, language) {
        var url = '/api/teams/' + team.uid + '/report/tool/' + tool.uid + '/language/' + language.uid,
            fileName = team.title + '-' + tool.title + '-SE-' + '.pdf';

        this.data.download(url, fileName);
    };

    this.sendReport = function (team, tool) {
        this.data.sendReport(team, tool);
    };

    this.updateTeam = function (team) {
        this.data.repositories.teams.update(team);
    };

    this.lockTeam = function (team) {
        this.data.lockTeam(team);
    };

    this.unlockTeam = function (team) {
        this.data.unlockTeam(team);
    };

    this.archiveTeam = function (team) {
        this.data.archiveTeam(team);
    };

    this.initializeTeamInfoModal = function (team) {
        this.currentTeam = team;
    };

    this.initializeTeamReminderModal = function (team) {
        this.currentTeam = team;
    };

    this.initializeInviteByCouponModal = function(team) {
        this.currentTeam = team;
        this.newCouponCount = 0;
    };

    this.createCouponInvitations = function () {
        this.newCouponCount = -(-this.newCouponCount);
        var newCouponInvitation = this.data.newCouponInvitation(this.currentTeam);
        newCouponInvitation.language = this.currentInvitationTextLanguageUid;
        for( var i = 0; i < this.newCouponCount; i++) {
            this.data.createInvitation(newCouponInvitation);
        }
    };

    this.initializeInviteByEmailModal = function(team) {
        this.currentTeam = team;
        this.mailInvitations = [];
        this.pushMailInvitation();
    };

    this.sendMailInvitations = function() {
        var dataService = this.data;
        this.mailInvitations.forEach( function (invitation) {
            dataService.createInvitation(invitation);
        });
    };

    this.pushMailInvitation = function () {
        this.mailInvitations.push(this.data.newMailInvitation(this.currentTeam));
    };

    this.initializeInviteLeaderByEmailModal = function (team) {
        this.leaderMailInvitation = this.data.newLeaderMailInvitation(team);
    };

    this.sendReminder = function () {
        this.data.sendReminder(this.currentTeam);
    };

    this.inviteLeaderByCoupon = function (team, language) {
        this.data.createLeaderCouponInvitation(team, language);
    };

    this.createLeaderMailInvitation = function () {
        this.data.createInvitation(this.leaderMailInvitation);
    };

    this.depastify = function ( invitation ) {
        var team = this.currentTeam,
            dataService = this.data,
            addresses =
            invitation.address.split(/\s+/).filter( function (address) {
                return address.length > 0;
            }).map( function (address) {
                return address;
            }),
            invitations = addresses.map( function (address) {
                var newInvitation = dataService.newMailInvitation(team);
                newInvitation.language = invitation.language;
                newInvitation.address = address;
                return newInvitation;
            });
        this.mailInvitations = invitations;
    };

});
