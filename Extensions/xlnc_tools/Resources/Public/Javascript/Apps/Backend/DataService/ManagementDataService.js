/* global angular, require */
angular.module('Xlnc.Backend.ManagementDataService', [
    'Xlnc.Backend.Project', 'Xlnc.Backend.Team', 'Xlnc.Backend.Invitation', 'Xlnc.Backend.Language', 'Xlnc.Backend.Repository', 'Xlnc.Backend.Typo3BackendNotifications'
]).service('ManagementDataService', function(Project, Team, Invitation, Language, Repository, Typo3BackendNotifications, $http, $window) {

    var success = function(response) {
            Typo3BackendNotifications.success(response.title, response.message);
            Typo3BackendNotifications.finish();
        },
        error = function(errorResponse) {
            Object.keys(errorResponse.data).forEach ( function (errorKey) {
                errorResponse.data[errorKey].forEach ( function (error) {
                    Typo3BackendNotifications.error(error.title, error.message);
                });
            });
        };

    return function() {
        this.repositories = {
            teams: new Repository(),
            projects: new Repository(),
            customers: new Repository(),
            languages: new Repository()
        };

        this.services = {
            teams: Team,
            invitations: Invitation,
            languages: Language,
        };

        this.projects   = this.repositories.projects.items;
        this.teams      = this.repositories.teams.items;
        this.customers  = this.repositories.customers.items;
        this.languages  = this.repositories.languages.items;

        this.reindex = function() {
            this.repositories.teams.reindex();
        };

        this.download = function(url, fileName) {
            Typo3BackendNotifications.progress();
            $http({
              method: 'GET',
              url: url,
              responseType: 'blob'
            }).then(function successCallback(response) {
                var blobUrl = $window.URL.createObjectURL(response.data),
                    a = $window.document.createElement("a");

                $window.document.body.appendChild(a);

                a.href = blobUrl;
                a.download = fileName;
                a.click();
                $window.URL.revokeObjectURL(url);

                Typo3BackendNotifications.finish();
              }, function errorCallback(response) {
                  Typo3BackendNotifications.finish();
              });
        };

        this.sendReport = function(team, tool) {
            Typo3BackendNotifications.progress();
            var that = this;
            Team.sendReport({uid: team.uid, toolUid: tool.uid}).$promise.then( function(data) {
                success(data);
                Typo3BackendNotifications.finish();
                that.update(team);
            }, function(errorData) {
                error(errorData);
                Typo3BackendNotifications.finish();
            });
        };

        this.load = function() {
            var that = this;
            Typo3BackendNotifications.progress();
            return this.loadItems("teams", Team)
            .then(function () {
                that.reindex();
                that.collectProjects();
                that.collectCustomers();
                that.linkProjectsToCustomers();
                that.linkTeamsToProjects();
                that.collectToolsInProjects();
                that.collectToolsInCustomers();
                that.loadItems("languages", Language);
                Typo3BackendNotifications.finish();
            });
        };

        this.collectProjects = function() {
            var projectRepository = this.repositories.projects;
            this.teams.forEach ( function (team) {
                if(angular.isUndefined(projectRepository.findByUid(team.project.uid))) {
                    projectRepository.items.push(team.project);
                    team.project.teams = [];
                    projectRepository.reindex();
                }
            });
        };

        this.collectCustomers = function() {
            var customerRepository = this.repositories.customers;
            this.projects.forEach ( function (project) {
                if(angular.isUndefined(customerRepository.findByUid(project.customer.uid))) {
                    customerRepository.items.push(project.customer);
                    project.customer.projects = [];
                    customerRepository.reindex();
                }
            });
        };

        this.collectToolsInCustomers = function() {
            var customerRepository = this.repositories.customers;
            customerRepository.items.forEach ( function (customer) {
                customer.tools = {};

                customer.projects.forEach ( function (project) {
                    project.tools.forEach ( function (tool) {
                        customer.tools[tool.uid] = tool;
                    });
                });
                customer.tools = Object.keys(customer.tools).map( function(k) { return customer.tools[k]; });
            });
        };

        this.collectToolsInProjects = function() {
            var hasToolFunc = function (tool) {
                    var returnValue = false;
                    this.tools.forEach( function(t) {
                        if (t.uid === tool.uid) {
                            returnValue = true;
                        }
                    });
                    return returnValue;
                },
                getToolFunc = function (tool) {
                    if (!angular.isArray(this.tools)) return {};
                    var foundTool = {};
                    this.tools.forEach( function (t) {
                        if(t.uid === tool.uid) foundTool = t;
                    });
                    return foundTool;
                };
            this.repositories.projects.items.forEach ( function (project) {
                project.tools = {};

                project.hasTool = hasToolFunc;

                project.teams.forEach ( function (team) {
                    team.hasTool = hasToolFunc;
                    team.getTool = getToolFunc;
                    team.tools.forEach ( function (tool) {

                        project.tools[tool.uid] = tool;
                    });
                });
                project.tools = Object.keys(project.tools).map( function(k) { return project.tools[k]; });
            });
        };

        this.linkProjectsToCustomers = function () {
            var customerRepository = this.repositories.customers;
            this.repositories.projects.items.forEach( function (project) {
                var customer = customerRepository.findByUid(project.customer.uid);
                if(!angular.isUndefined(customer)) {
                    if(!angular.isArray(customer.projects)) {
                        customer.projects = [];
                    }
                    customer.projects.push(project);

                }
                delete project.customer;
            });
        };

        this.linkTeamsToProjects = function () {
            var projectRepository = this.repositories.projects;
            this.repositories.teams.items.forEach( function (team) {
                var project = projectRepository.findByUid(team.project.uid);
                if(!angular.isUndefined(project)) {
                    if(!angular.isArray(project.teams)) {
                        project.teams = [];
                    }
                    project.teams.push(team);
                }
                delete team.project;
            });
        };

        this.loadItems = function ( key, constructor ) {
            var that = this;
            var repository = this.repositories[key];

            var items = this.services[key].query({}, function (data, headers) {
                items.forEach( function (item, index)  {
                    var newItem = new constructor(item);
                    repository.items.push(newItem);
                });
            });

            return items.$promise.then( function () {
                repository.reindex();
            });
        };

        /**
         * update the given item
         * @param  {Object} item
         */
        this.update = function (item) {
            if(item instanceof Project) {
                this.repositories.projects.update(item);
            }
            if(item instanceof Team) {
                this.repositories.teams.update(item);
            }
        };

        this.sendReminder = function (team) {
            Team.remind({uid: team.uid}).$promise.then( function(data) {
                Typo3BackendNotifications.success(data.title, data.message);
            });
        };

        this.newInvitation = function (team, invitationType, sheetType) {
            var invitation = new Invitation();
            invitation.team = team;
            invitation.invitationType = invitationType;
            invitation.sheetType = sheetType;
            invitation.language = 0;

            return invitation;
        };

        this.newMailInvitation = function (team) {
            return this.newInvitation(team, 'Mail', 'Sheet');
        };

        this.newCouponInvitation = function (team) {
            return this.newInvitation(team, 'Coupon', 'Sheet');
        };

        this.newLeaderMailInvitation = function (team) {
            var leaderMailInvitation = this.newInvitation(team, "Mail", "Leader");
            leaderMailInvitation.address = team.leaderEmail;
            return leaderMailInvitation;

        };

        this.createLeaderCouponInvitation = function (team, language) {
            var leaderCouponInvitation = this.newInvitation(team, "Coupon", "Leader");
            leaderCouponInvitation.language = language.uid;
            this.createInvitation(leaderCouponInvitation);
        };

        this.createInvitation = function (invitation) {
            Typo3BackendNotifications.progress();
            var team = invitation.team,
                that = this;
            invitation.$save(
                function(response) {
                    Typo3BackendNotifications.success(response.title, response.message);
                    Typo3BackendNotifications.finish();
                    that.update(team);
                },
                function(errorResponse) {
                    errorMessages = [];
                    Object.keys(errorResponse.data).forEach ( function (errorKey) {
                        errorResponse.data[errorKey].forEach ( function (error) {
                            errorMessages.push(error.message);
                            Typo3BackendNotifications.error(error.title, error.message);
                        });
                    });
                    Typo3BackendNotifications.finish();
                }
            );
        };

        this.lockTeam  = function (team) {
            Typo3BackendNotifications.progress();
            var that = this;
            Team.lock({uid: team.uid}).$promise.then( function(data) {
                success(data);
                Typo3BackendNotifications.finish();
                that.update(team);
            }, function(errorData) {
                error(errorData);
                Typo3BackendNotifications.finish();
            });
        };

        this.unlockTeam  = function (team) {
            Typo3BackendNotifications.progress();
            var that = this;
            Team.unlock({uid: team.uid}).$promise.then( function(data) {
                success(data);
                Typo3BackendNotifications.finish();
                that.update(team);
            }, function(errorData) {
                error(errorData);
                Typo3BackendNotifications.finish();
            });
        };

        this.archiveTeam  = function (team) {
            Typo3BackendNotifications.progress();
            var that = this;
            Team.archive({uid: team.uid}).$promise.then( function(data) {
                success(data);
                Typo3BackendNotifications.finish();
                that.update(team);
            }, function(errorData) {
                error(errorData);
                Typo3BackendNotifications.finish();
            });
        };
    };

});
