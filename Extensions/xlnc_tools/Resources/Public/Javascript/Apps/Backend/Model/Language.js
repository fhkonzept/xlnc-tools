/* global angular */
angular.module('Xlnc.Backend.Language', ['ngResource']).factory('Language', function($resource) {

	var Language = $resource('/api/languages/:uid', {uid: '@uid'});

	return Language;


});
