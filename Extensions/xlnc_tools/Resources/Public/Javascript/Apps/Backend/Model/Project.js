/* global angular */
angular.module('Xlnc.Backend.Project', ['ngResource']).factory('Project', function($resource) {

	var Project = $resource('/api/projects');

	return Project;
});
