/* global angular */
angular.module('Xlnc.Backend.Team', ['ngResource']).factory('Team', function($resource) {

	var Team = $resource('/api/teams/:uid', {uid: '@uid'}, {
		'update': {method: 'PUT', params: {}},
		'remind': {method: 'POST', url: '/api/teams/:uid/remind', params: {uid: '@uid'}},
		'requestText': {method: 'POST', url: '/api/teams/:uid/requestText/type/:type/language/:language', params: {uid: '@uid', type: '@type', language: '@language'}},
		'lock': {method: 'POST', url: '/api/teams/:uid/lock', params:{uid: '@uid'}},
		'unlock': {method: 'POST', url: '/api/teams/:uid/unlock', params:{uid: '@uid'}},
		'archive': {method: 'POST', url: '/api/teams/:uid/archive', params:{uid: '@uid'}},
		'sendReport': {method: 'GET', url: '/api/teams/:uid/sendReport/tool/:toolUid', params:{uid: '@uid', toolUid: '@toolUid'}}
	});

	Team.prototype.leaderFullName = function () {
		return this.leaderFirstName + " " + this.leaderLastName;
	};

	Team.prototype.getInvitationsByType = function () {
		if(angular.isUndefined(this.__invitationsByType)) {
			this.mapInvitationsByType();
		}

		return this.__invitationsByType;
	};

	Team.prototype.mapInvitationsByType = function () {
		var sheetTypeMap = {
				'Xlnc\\XlncTools\\Domain\\Model\\LeaderSheet': 'Leader',
				'Xlnc\\XlncTools\\Domain\\Model\\Sheet': 'Sheet'
			},
			invitationTypeMap = {
				'MailInvitation': 'Mail',
				'CouponInvitation': 'Coupon'
			};
			var invitationsByType = {
				"Mail": {
					"Leader": [],
					"Sheet": []
				},
				"Coupon": {
					"Leader": [],
					"Sheet": []
				}
			};

		this.invitations.forEach( function ( invitation ) {
			invitationsByType[invitationTypeMap[invitation.type]][sheetTypeMap[invitation.sheetType]].push(invitation);
		});

		this.__invitationsByType = invitationsByType;
	}

	Team.prototype.getMailInvitations = function () {
		var invitationsByType = this.getInvitationsByType();

		return invitationsByType.Mail.Leader.concat(invitationsByType.Mail.Sheet);
	};

	Team.prototype.hasLeaderMailInvitation = function () {
		var invitationsByType = this.getInvitationsByType();
		return invitationsByType.Mail.Leader.length > 0;
	};

	Team.prototype.hasLeaderCouponInvitation = function () {
		var invitationsByType = this.getInvitationsByType();
		return invitationsByType.Coupon.Leader.length > 0;
	};

	Team.prototype.getLeaderInvitations = function () {
		var invitationsByType = this.getInvitationsByType();
		return invitationsByType.Mail.Leader.concat(invitationsByType.Coupon.Leader);
	};

	Team.prototype.getSheetsByType = function () {
		if (angular.isUndefined(this.__sheetsByType)) {
			this.mapSheetsByType();
		}

		return this.__sheetsByType;
	};

	Team.prototype.getText = function (type) {
		var applicableTexts = this.texts.filter( function (text) {
			return text.type === type;
		});

		return {
			texts: applicableTexts,
			inLanguage: function (language) {
				var applicableTexts = this.texts.filter( function (text) {
					return text.language.uid === language.uid;
				});

				return applicableTexts[0];
			}
		}
	};

	Team.prototype.hasText = function (type) {
		var applicableTexts = this.texts.filter( function (text) {
			return text.type === type;
		});

		return {
			texts: applicableTexts,
			inLanguage: function (language) {
				var applicableTexts = this.texts.filter( function (text) {
					return text.language.uid === language.uid;
				});

				return applicableTexts.length > 0;
			}
		}
	};

	Team.prototype.requestText = function (type) {
		var team = this;
		return {
			inLanguage: function (language) {
				Team.requestText({uid: team.uid, type: type, language: language.uid}).$promise.then( function (data) {
					team.$update();
				});
			}
		}
	};

	Team.prototype.mapSheetsByType = function () {
		var sheetTypeMap = {
				LeaderSheet: 'Leader',
				Sheet: 'Sheet'
			},
			sheetsByType = {
				Leader: [],
				Sheet: []
			};

		this.sheets.forEach( function (sheet) {
			sheetsByType[sheetTypeMap[sheet.type]].push(sheet);
		});

		this.__sheetsByType = sheetsByType;
	}

	Team.prototype.selfUpdate = function () {
		this.mapSheetsByType();
		this.mapInvitationsByType();
	}

	return Team;


});
