/* global angular */
angular.module('Xlnc.Backend.Invitation', ['ngResource']).factory('Invitation', function($resource) {

	var Invitation = $resource('/api/invitations/:uid', {uid: '@uid'}, {
		'download': {
			method: 'GET',
			url: '/api/invitations/:uid/download'
		}
	});

	return Invitation;
});
