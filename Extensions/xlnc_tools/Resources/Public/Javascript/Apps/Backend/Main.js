/* @global angular */
angular.module('Xlnc.BackendApp',
    [
        'Xlnc.Backend.ManagementController',
        'Xlnc.Backend.ManagementDataService',
        'Xlnc.Backend.Tooltip',

    ]);
