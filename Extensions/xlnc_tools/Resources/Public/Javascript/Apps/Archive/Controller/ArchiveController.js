/* global angular */
angular.module('Xlnc.Archive.ArchiveController', ['ngSanitize']).controller('Archive', function($scope, ArchiveDataService, $sce) {
    "use strict";

    this.searchTerm = '';

    this.sanitizer = $sce;

    this.view = {
        filters: {},
        showSearch: false
    }

    this.resetFilter = function () {
        this.view.filters = {
            searchTerm: '',
            project: {
                reference: 0,
                customer: {
                    numEmployees: 0,
                    industry: 0,
                    internationality: 0,
                    turnover: 0
                }
            },
        }
    };

    this.init = function () {
        this.data = new ArchiveDataService();
        this.data.load();
        this.appHref = T3_THIS_LOCATION;
        this.resetFilter();
    };

    this.downloadExport = function (tool) {
        var teamUids = this.selectedTeams.map( function (team) {
                return team.uid;
            }),
            teamUidString = teamUids.join(','),
            url = '/api/export/spss/teams/' + teamUidString + '/tool/' + tool.uid,
            fileName = tool.title + '.csv';

        this.data.download(url, fileName);
    };

    this.downloadReport = function (language) {
        var teamUids = this.selectedTeams.map( function (team) {
                return team.uid;
            }),
            teamUidString = teamUids.join(','),
            url = '/api/report/combine/teams/' + teamUidString + '/language/' + language.uid ,
            
            fileName = 'report.pdf';
        
        this.data.download(url, fileName);
    };

    this.downloadTeamReport = function (team, tool, language, variant) {
        var url = '/api/teams/' + team.uid + '/report/tool/' + tool.uid + '/language/' + language.uid,
            fileName = team.title + '-' + tool.title + '.pdf';
            
        if ( typeof variant !== 'undefined' ) {
            url = url + '/variant/' + variant,
            fileName = team.title + '-' + tool.title + '-SE-' + '.pdf';
        }

        this.data.download(url, fileName);
    };

    this.selectTeams = function(teams) {
        teams.forEach( function (team) {
            team.selected = true;
        });
    };

});
