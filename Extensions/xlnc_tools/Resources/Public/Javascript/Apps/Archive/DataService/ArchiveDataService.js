/* global angular, require */
angular.module('Xlnc.Archive.ArchiveDataService', [
    'Xlnc.Archive.Team', 'Xlnc.Archive.Repository', 'Xlnc.Archive.Language', 'Xlnc.Archive.Typo3BackendNotifications'
]).service('ArchiveDataService', function(Team, Repository, Language, Typo3BackendNotifications, $http, $window) {

    var success = function(response) {
            Typo3BackendNotifications.success(response.title, response.message);
            Typo3BackendNotifications.finish();
        },
        error = function(errorResponse) {
            Object.keys(errorResponse.data).forEach ( function (errorKey) {
                errorResponse.data[errorKey].forEach ( function (error) {
                    Typo3BackendNotifications.error(error.title, error.message);
                });
            });
        };

    return function() {
        this.repositories = {
            teams: new Repository(),
            languages: new Repository()
        };

        this.services = {
            teams: Team,
            languages: Language
        };

        this.teams      = this.repositories.teams.items;
        this.languages  = this.repositories.languages.items;

        this.reindex = function() {
            this.repositories.teams.reindex();
        };

        this.download = function(url, fileName) {
            Typo3BackendNotifications.progress();
            $http({
              method: 'GET',
              url: url,
              responseType: 'blob'
            }).then(function successCallback(response) {
                var blobUrl = $window.URL.createObjectURL(response.data),
                    a = $window.document.createElement("a");

                $window.document.body.appendChild(a);

                a.href = blobUrl;
                a.download = fileName;
                a.click();
                //$window.URL.revokeObjectURL(url);

                Typo3BackendNotifications.finish();
              }, function errorCallback(response) {
                  Typo3BackendNotifications.finish();
              });
        };

        this.load = function() {
            var that = this;
            Typo3BackendNotifications.progress();
            return this.loadArchive()
            .then(function () {
                that.reindex();
                that.loadLanguages();
                Typo3BackendNotifications.finish();
            });
        };


        this.loadArchive = function () {
            var teams = this.repositories.teams,
                items = this.services.teams.archived({}, function (data, headers) {
                items.forEach( function (item, index)  {
                    var newItem = new Team(item);
                    teams.items.push(newItem);
                });
            });

            return items.$promise.then( function () {
                teams.reindex();
            });
        };

        this.loadLanguages = function () {
            var languages = this.repositories.languages,
                items = this.services.languages.query({}, function (data, headers) {
                items.forEach( function (item, index)  {
                    var newItem = new Language(item);
                    languages.items.push(newItem);
                });
            });

            return items.$promise.then( function () {
                languages.reindex();
            });
        };


    };

});
