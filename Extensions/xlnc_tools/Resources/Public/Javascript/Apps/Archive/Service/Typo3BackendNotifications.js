/* global angular, require */
angular.module('Xlnc.Archive.Typo3BackendNotifications', []).service('Typo3BackendNotifications', function($window) {
    var notifier, progressBar, defaultDuration = 0;
    require(['TYPO3/CMS/Backend/Notification'], function(Notification) {
        notifier = Notification;
    });
    require(['nprogress'], function(NProgress) {
        // NProgress.configure({ parent: '#typo3-contentContainer' });
        progressBar = NProgress;
    });

    this.error = function(title, message, duration) {
        if(angular.isUndefined(duration)) duration = defaultDuration;
        notifier.error(title, message, duration);
    };

    this.success = function(title, message, duration) {
        if(angular.isUndefined(duration)) duration = defaultDuration;
        notifier.success(title, message, duration);
    };

    this.progress = function(value) {
        if(!progressBar) return;
        if(angular.isUndefined(value)) {
            progressBar.start();
        } else {
            progressBar.set(value);
        }
    };

    this.finish = function() {
        progressBar.done();
    };
});
