/* global angular */
angular.module('Xlnc.Archive.Repository', []).factory('Repository', function() {
	return function () {
        /**
         * stored items
         * @type {Array}
         */
		this.items = [];
        /**
         * uid-to-item-index map
         * @type {Object}
         */
		this.map = {};

        /**
         * rebuild the index from scratch
         */
        this.reindex = function() {
            var map = {};
            this.items.forEach(function (item, index) {
                map[item.uid] = index;
            });
            this.map = map;
        };

        /**
         * return the item identified by the uid
         * @param  {Number} uid
         * @return {Object}
         */
        this.findByUid = function (uid) {
            return this.items[this.map[uid]];
        };

        /**
         * update the given item
         * @param {Object} item
         */
        this.update = function (item) {
            var that = this;
            // store a copy of the item
            var originalItem = angular.extend({}, item);
            // raise the spinners!
            item.loading = true;
            item.$update().then(function() {

                // this is like .extend(), but we don't overwrite anything already present
                // in theory, this is limited to properties that have been added during runtime
                Object.keys(originalItem).forEach( function (key) {
                    if(typeof item[key] == "undefined") {
                        item[key] = originalItem[key];
                    }
                });

                item.loading = false;
            });
        };

        /**
         * delete the given item
         * @param  {Object} item
         */
        this.delete = function (item) {
            item.loading = true;
            item.$remove();
        };


	};
});
