/* global angular */
angular.module('Xlnc.Archive.Language', ['ngResource']).factory('Language', function($resource) {

	var Language = $resource('/api/languages/:uid', {uid: '@uid'});

	return Language;


});
