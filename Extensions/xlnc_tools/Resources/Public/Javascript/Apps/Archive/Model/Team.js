/* global angular */
angular.module('Xlnc.Archive.Team', ['ngResource']).factory('Team', function($resource) {

	var Team = $resource('/api/teams/:uid', {uid: '@uid'}, {
		'update': {method: 'PUT', params: {}},
		'remind': {method: 'POST', url: '/api/teams/:uid/remind', params: {uid: '@uid'}},
		'lock': {method: 'POST', url: '/api/teams/:uid/lock', params:{uid: '@uid'}},
		'unlock': {method: 'POST', url: '/api/teams/:uid/unlock', params:{uid: '@uid'}},
		'sendReport': {method: 'GET', url: '/api/teams/:uid/sendReport/tool/:toolUid', params:{uid: '@uid', toolUid: '@toolUid'}},
		'archived': {method: 'GET', url: '/api/teams/archived', isArray: true}
	});

	Team.prototype.leaderFullName = function () {
		return this.leaderFirstName + " " + this.leaderLastName;
	};

	Team.prototype.getInvitationsByType = function () {
		if(angular.isUndefined(this.__invitationsByType)) {
			var sheetTypeMap = {
					'Xlnc\\XlncTools\\Domain\\Model\\LeaderSheet': 'Leader',
					'Xlnc\\XlncTools\\Domain\\Model\\Sheet': 'Sheet'
				},
				invitationTypeMap = {
					'MailInvitation': 'Mail',
					'CouponInvitation': 'Coupon'
				};
				var invitationsByType = {
					"Mail": {
						"Leader": [],
						"Sheet": []
					},
					"Coupon": {
						"Leader": [],
						"Sheet": []
					}
				};

			this.invitations.forEach( function ( invitation ) {
				invitationsByType[invitationTypeMap[invitation.type]][sheetTypeMap[invitation.sheetType]].push(invitation);
			});

			this.__invitationsByType = invitationsByType;
		}

		return this.__invitationsByType;
	};

	Team.prototype.getMailInvitations = function () {
		var invitationsByType = this.getInvitationsByType();

		return invitationsByType.Mail.Leader.concat(invitationsByType.Mail.Sheet);
	};

	Team.prototype.hasLeaderMailInvitation = function () {
		var invitationsByType = this.getInvitationsByType();
		return invitationsByType.Mail.Leader.length > 0;
	};

	Team.prototype.hasLeaderCouponInvitation = function () {
		var invitationsByType = this.getInvitationsByType();
		return invitationsByType.Coupon.Leader.length > 0;
	};

	Team.prototype.getLeaderInvitations = function () {
		var invitationsByType = this.getInvitationsByType();
		return invitationsByType.Mail.Leader.concat(invitationsByType.Coupon.Leader);
	};

	Team.prototype.getSheetsByType = function () {
		if (angular.isUndefined(this.__sheetsByType)) {
			var sheetTypeMap = {
					LeaderSheet: 'Leader',
					Sheet: 'Sheet'
				},
				sheetsByType = {
					Leader: [],
					Sheet: []
				};

			this.sheets.forEach( function (sheet) {
				sheetsByType[sheetTypeMap[sheet.type]].push(sheet);
			});

			this.__sheetsByType = sheetsByType;
		}

		return this.__sheetsByType;
	};

	Team.prototype.hasTool = function (tool) {
		return this.tools.map( function (tool) { return tool.uid; } ).indexOf(tool.uid) > -1;
	};

	Team.prototype.getTool = function (tool) {
		var foundTool = {};
		this.tools.forEach( function (t) {
			if(t.uid === tool.uid) foundTool = t;
		});
		return foundTool;
	};

	return Team;


});
