angular.module('Xlnc.Archive.DictionaryFilter', []).filter('dictionary', function(){
    return function (teams, filter) {
        return teams.filter( function (team) {
            return ((0 === filter.project.reference) || (team.project.reference === filter.project.reference))
               &&  ((0 === filter.project.customer.numEmployees) || (team.project.customer.numEmployees === filter.project.customer.numEmployees))
               &&  ((0 === filter.project.customer.internationality) || (team.project.customer.internationality === filter.project.customer.internationality))
               &&  ((0 === filter.project.customer.turnover) || (team.project.customer.turnover === filter.project.customer.turnover))
               &&  ((0 === filter.project.customer.industry) || (team.project.customer.industry === filter.project.customer.industry))
            ;
        });
    }
});
