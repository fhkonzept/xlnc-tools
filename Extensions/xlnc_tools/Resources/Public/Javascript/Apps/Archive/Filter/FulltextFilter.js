// global angular
angular.module('Xlnc.Archive.FulltextFilter', []).filter('fulltext', function() {
	return function(teams, searchTermString) {
		var validSearchTerms = searchTermString.split(" ").filter( function (item) {
			return item.length > 2;
		});

		if( validSearchTerms.length > 0) {
			//var searchableAttributes = ["uid", "project_title", "title", "contact_name", "contact_email", "contact_phone", "company"];

			var searchExpressions = {};
            for(var i=0; i<validSearchTerms.length; i++) {
                searchExpressions[validSearchTerms[i]] = new RegExp(validSearchTerms[i], "i");
            }

			teams.forEach( function ( team, index ) {
                var unfoundSearchTerms = [];
                team.score = 0;
                team.view = {};
                validSearchTerms.forEach( function (searchTerm, searchTermIndex) {
                    if(
                            searchExpressions[searchTerm].test(team.title)
                        ||  searchExpressions[searchTerm].test(team.project.title)
                        ||  searchExpressions[searchTerm].test(team.project.customer.title)
                    ) {
                        if(unfoundSearchTerms.indexOf(searchTerm) == -1) {
                            team.score = team.score + 1;
                            unfoundSearchTerms.push(searchTerm);
                        }
                    }
                });
            });

			return teams.filter( function ( team ) {
                return team.score === validSearchTerms.length;
            });

		} else {
			return teams;
		}


	};
});
