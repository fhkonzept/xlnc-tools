angular.module('Xlnc.Archive.WithProjectFilter', []).filter('withProject', function(){

    return function (teams, project) {
        return teams.filter( function (team) {
            return team.project.uid === project.uid;
        });
    }
});
