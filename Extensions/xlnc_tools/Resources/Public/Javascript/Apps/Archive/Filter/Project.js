angular.module('Xlnc.Archive.ProjectFilter', []).filter('project', function(){

    return function (teams) {
        return teams.map( function (team) {
            return team.project;
        });
    }
});
