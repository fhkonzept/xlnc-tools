angular.module('Xlnc.Archive.ToolsFilter', []).filter('tools', function(){
    return function (teams) {
        var tools = [],
            foundToolUids = [];
        if(typeof teams === 'undefined') {
            // TODO: investigate why there's 'undefined' being passed here
            return [];
        } else {
            teams.forEach( function (team) {
                team.tools.forEach( function (tool) {
                    if(foundToolUids.indexOf(tool.uid) === -1) {
                        foundToolUids.push(tool.uid);
                        tools.push(tool);
                    }
                });
            });
            return tools;
        }
    }
});
