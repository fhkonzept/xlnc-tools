angular.module('Xlnc.Archive.CountFilter', []).filter('count', function(){

    return function (item) {
        return angular.isUndefined(item.length) ? 0 : item.length;
    }
});
