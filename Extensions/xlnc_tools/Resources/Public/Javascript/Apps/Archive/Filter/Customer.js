angular.module('Xlnc.Archive.CustomerFilter', []).filter('customer', function(){

    return function (teams) {
        return teams.map( function (team) {
            return team.project.customer;
        });
    }
});
