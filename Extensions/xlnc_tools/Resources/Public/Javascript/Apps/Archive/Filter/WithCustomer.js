angular.module('Xlnc.Archive.WithCustomerFilter', []).filter('withCustomer', function(){

    return function (teams, customer) {
        return teams.filter( function (team) {
            return team.project.customer.uid === customer.uid;
        });
    }
});
