angular.module('Xlnc.Archive.SelectedFilter', []).filter('selected', function(){

    return function (teams) {
        return teams.filter( function (team) {
            return team.selected;
        });
    }
});
