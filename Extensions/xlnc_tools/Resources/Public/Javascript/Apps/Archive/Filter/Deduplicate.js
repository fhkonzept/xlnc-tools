angular.module('Xlnc.Archive.DeduplicateFilter', []).filter('deduplicate', function(){

    return function (items) {
        var deduplicatedItems = [],
            uidsAlreadySeen = [];
        
        items.forEach( function (item) {
            if(uidsAlreadySeen.indexOf(item.uid) === -1) {
                uidsAlreadySeen.push(item.uid);
                deduplicatedItems.push(item);
            }
        })

        return deduplicatedItems;
    }
});
