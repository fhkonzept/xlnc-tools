// global app
angular.module('Xlnc.Archive.HighlightSearchResultsFilter', []).filter('highlightSearchResults', function($sce) {
	return function(input, searchTermString) {
        var searchTerms = searchTermString.split(" ").filter(function (item, index) {
            return item.length > 2;
        });

        var searchExpressionString = searchTerms.join("|");
        var searchExpression = new RegExp(searchExpressionString, "ig");

        if(searchTerms.length > 0) {
            var matches = input.match(searchExpression),
                parts = input.split(searchExpression),
                viewString = parts[0];

            if(null === matches) {
                return input;
            } else {
                matches.forEach(function (item, index) {
                    viewString +=
                        $sce.trustAsHtml('<span class="search-hit">')
                        + item
                        + $sce.trustAsHtml('</span>');
                    if( typeof parts[index + 1] === "string" )
                        viewString += parts[index + 1];
                });
                return viewString;
            }
        } else {
            return input;
        }
	};
});
