<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_surveyresult',
		'label' => 'sheet',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'dividers2tabs' => TRUE,
		'hideTable' => TRUE,
		'enablecolumns' => [],
		'searchFields' => '',
		'rootLevel' => 1,
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('xlnc_tools') . 'Resources/Public/Icons/tx_xlnctools_domain_model_surveyresult.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'customer, project, team, tool, sheet, status, language',
	),
	'types' => array(
		'1' => array('showitem' => 'customer, project, team, tool, sheet, status, language'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'language' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'customer' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_customer',
			'config' => array(
				'type' => 'input',
				'size' => 11,
				'eval' => 'int',
				'readOnly' => 1
			)
		),
		'project' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_project',
			'config' => array(
				'type' => 'input',
				'size' => 11,
				'eval' => 'int',
				'readOnly' => 1
			)
		),
		'team' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team',
			'config' => array(
				'type' => 'input',
				'size' => 11,
				'eval' => 'int',
				'readOnly' => 1
			)
		),
		'tool' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool',
			'config' => array(
				'type' => 'input',
				'size' => 11,
				'eval' => 'int',
				'readOnly' => 1
			)
		),
		'sheet' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_sheet',
			'config' => array(
				'type' => 'input',
				'size' => 11,
				'eval' => 'int',
				'readOnly' => 1
			)
		),
		'status' => array(
			'exclude' => 0,
			'label' => 'Status',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('Started', 0),
					array('Finished', 1)
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => '',
				'readOnly' => 1
			)
		)

	),
);
