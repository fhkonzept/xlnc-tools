<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_invitation',
		'label' => 'tx_extbase_type',
		'label_alt' => 'sheet_type, address',
		'label_alt_force' => '1',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'delete' => 'deleted',
		'enablecolumns' => array(
		),
		'searchFields' => 'address, team, sheet_type',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('xlnc_tools') . 'Resources/Public/Icons/tx_xlnctools_domain_model_invitation.gif',
		'type' => 'tx_extbase_type',
	),
	'interface' => array(
		'showRecordFieldList' => 'hidden, address, team, sheet_type, language',
	),
	'types' => array(
		'Tx_XlncTools_MailInvitation' => array('showitem' => '
			tx_extbase_type, sheet_type, language, address'),
		'Tx_XlncTools_CouponInvitation' => array('showitem' => '
			tx_extbase_type, sheet_type, language'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'address' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_invitation.address',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'sheet_type' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_invitation.sheet_type',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'team' => array(
			'config' => array(

			)
		),

		'tx_extbase_type' => array(
			'exclude' => 0,
			'label'   => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools.tx_extbase_type',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_invitation.tx_extbase_type.Tx_XlncTools_MailInvitation','Tx_XlncTools_MailInvitation'),
					array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_invitation.tx_extbase_type.Tx_XlncTools_CouponInvitation','Tx_XlncTools_CouponInvitation')
				),
				'default' => 'Tx_XlncTools_MailInvitation',
				'size' => 1,
				'maxitems' => 1,
			)
		),

		'language' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),

		'crdate' => array(
			'exclude' => 0,
			'label' => 'Angelegt am',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'date'
			),
		),

	),
);
