<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_sheet',
		'label' => 'access_key',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'searchFields' => 'access_key,age,gender,degree,experience,cultural_background,team,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('xlnc_tools') . 'Resources/Public/Icons/tx_xlnctools_domain_model_sheet.gif',
		'type' => 'tx_extbase_type',
	),
	'interface' => array(
		'showRecordFieldList' => 'hidden, access_key, age, gender, education, graduation, experience, managementexperience, managementlevel, businessunit, team',
	),
	'types' => array(
		'Tx_XlncTools_Sheet' => array('showitem' => '
			tx_extbase_type, access_key,
			--div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_sheet.tabs.userdata, age, gender, education, graduation, experience, managementexperience, managementlevel, businessunit, country,
			--div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_sheet.tabs.answers, answers,
			--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden'),
		'Tx_XlncTools_LeaderSheet' => array('showitem' => '
			tx_extbase_type, access_key,
			--div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_sheet.tabs.userdata, age, gender, education, graduation, experience, managementexperience, managementlevel, businessunit, country,
			--div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_sheet.tabs.answers, answers,
			--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		'hidden' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),

		'access_key' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_sheet.access_key',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'age' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_sheet_age',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', -1),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_sheet_age'
			),
		),
		'gender' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_sheet_gender',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', -1),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_sheet_gender'
			),
		),
		'education' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_sheet_education',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', -1),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_sheet_education'
			),
		),
		'graduation' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_sheet_graduation',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', -1),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_sheet_graduation'
			),
		),
		'experience' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_sheet_experience',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', -1),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_sheet_experience'
			),
		),
		'managementexperience' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_sheet_managementexperience',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', -1),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_sheet_managementexperience'
			),
		),
		'managementlevel' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_sheet_managementlevel',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', -1),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_sheet_managementlevel'
			),
		),
		'businessunit' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_sheet_businessunit',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', -1),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_sheet_businessunit'
			),
		),
		'country' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_sheet.country',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', -1),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_sheet_country'
			),
		),
		'answers' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_sheet.answers',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_xlnctools_domain_model_answer',
				'foreign_field' => 'sheet',
				'minitems' => 0,
				'maxitems' => 10000,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),

		'team' => array(
			'config' => array(

			)
		),

		'tx_extbase_type' => array(
			'exclude' => 0,
			'label'   => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools.tx_extbase_type',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_sheet.tx_extbase_type.Tx_XlncTools_Sheet','Tx_XlncTools_Sheet'),
					array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_sheet.tx_extbase_type.Tx_XlncTools_LeaderSheet','Tx_XlncTools_LeaderSheet')
				),
				'default' => 'Tx_XlncTools_LeaderSheet',
				'size' => 1,
				'maxitems' => 1,
			)
		),

	),
);
