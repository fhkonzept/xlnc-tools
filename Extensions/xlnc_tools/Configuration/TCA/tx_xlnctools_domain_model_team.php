<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'searchFields' => 'title,gender,first_name,last_name,email,participant_count,tools,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('xlnc_tools') . 'Resources/Public/Icons/tx_xlnctools_domain_model_team.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'hidden, title, gender, first_name, last_name, email, participant_count, participant_emails',
	),
	'types' => array(
		'1' => array('showitem' =>
			'title, has_automatic_end, automatic_end_at, is_finished, is_archived,
			--div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.executive_data, age, gender, education, graduation, experience, managementexperience, managementlevel, businessunit, first_name, last_name, email,
			--div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.sheets, sheets,
			--div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.invitations, mail_text;;;richtext:rte_transform[mode=ts_links], alt_mail_text;;;richtext:rte_transform[mode=ts_links], reminder_text;;;richtext:rte_transform[mode=ts_links], invitations,
			--div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.mail_texts, mail_texts,
			--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden'
		)
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'gender' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_sheet_gender',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', -1),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_sheet_gender',
				'foreign_table_where' => ' AND tx_xlnctools_domain_model_dictionary_sheet_gender.uid IN (1,2)'
			),
		),
		'first_name' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.first_name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'last_name' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.last_name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'email' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'sheets' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.sheets',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_xlnctools_domain_model_sheet',
				'foreign_field' => 'team',
				'minitems' => 0,
				'maxitems' => 100,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'mail_texts' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.mail_texts',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_xlnctools_domain_model_mailtext',
				'foreign_field' => 'team',
				'minitems' => 0,
				'maxitems' => 100,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
				),
			),
		),
		'invitations' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.invitations',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_xlnctools_domain_model_invitation',
				'foreign_field' => 'team',
				'minitems' => 0,
				'maxitems' => 100,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'project' => array(
			'config' => array(

			)
		),
		'has_automatic_end' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.has_automatic_end',
			'config' => array(
				'type' => 'check',
			),
		),
		'automatic_end_at' => array(
			'exclude' => 1,
			'label' => 'Ende der Befragung',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'required' => 1,
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'is_finished' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.is_finished',
			'config' => array(
				'type' => 'check',
			),
		),
		'is_archived' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.is_archived',
			'config' => array(
				'type' => 'check',
			),
		),
		'mail_text' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.mail_text',
			'config' => array(
				'type' => 'text',
				'default' => '<p>wie bereits durch Herr/Frau XY angekündigt, haben Sie die Gelegenheit, im Rahmen der XLNC Leadership Diagnostik, Herrn/Frau XY vollkommen anonym eine Rückmeldung zu verschiedenen Aspekten seines/Ihres Führungsstils und zum Klima innerhalb Ihres Teams zu geben. Ihre Einschätzung und die Ihrer Kollegen werden jeweils der Selbsteinschätzung Ihrer Führungskraft gegenübergestellt und ausgewertet. Die Ergebnisse der Befragung werden anschließend in Mittelwerten betrachtet, so dass keine Zuordnung zu den einzelnen Antworten erfolgen kann.</p><p>Bitte beantworten Sie beide Fragebögen bis zum TT.MM.JJJJ. Zum XLNC Portal und den Fragebögen gelangen Sie mit dem untenstehenden Link. Die Teilnahme ist sowohl über einen Desktop-PC als auch über mobile Endgeräte möglich.</p>',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'readOnly' => 1,
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'module' => array(
							'name' => 'wizard_rich_text_editor',
							'urlParameters' => array(
								'mode' => 'wizard',
								'act' => 'wizard_rte.php'
							)
						),
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'alt_mail_text' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.alt_mail_text',
			'config' => array(
				'type' => 'text',
				'default' => '<p>heute beginnt XLNC, die Einschätzung Ihrer Führungsstile und des Klimas in Ihrem Team. Dabei haben die von Ihnen ausgewählten Feedbackgeber Gelegenheit, Ihnen vollkommen anonym eine Rückmeldung zu Ihren Führungsstilen und zum Teamklima zu geben. Diese Einschätzungen werden jeweils Ihrer Selbsteinschätzung gegenübergestellt und ausgewertet. Hierzu erhalten Sie für Ihre Selbsteinschätzung die untenstehenden Login-Daten. Ihre nominierten Mitarbeiter haben ebenfalls ihre persönlichen Einladungen mit den Login-Daten erhalten.</p><p>Bitte beantworten Sie beide Fragebögen bis zum TT.MM.JJJJ. Zum XLNC Portal und den Fragebögen gelangen Sie mit dem untenstehenden Link. Die Teilnahme ist sowohl über einen Desktop-PC als auch über mobile Endgeräte möglich.</p>',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'readOnly' => 1,
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'RTEonly' => 1,
						'module' => array(
							'name' => 'wizard_rich_text_editor',
							'urlParameters' => array(
								'mode' => 'wizard',
								'act' => 'wizard_rte.php'
							)
						),
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'reminder_text' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_team.reminder_text',
			'config' => array(
				'type' => 'text',
				'default' => '<p>am TT.MM. endet der Abgabezeitraum für die XLNC Leadership Diagnostik für Herrn/Frau XY.</p><p>Die meisten von Ihnen haben bereits an der Befragung teilgenommen. <strong> Alle weiteren Feedbackgeber möchten wir hiermit freundlich an die Abgabe der Einschätzungen erinnern, so dass wir fristgerecht mit der Auswertung beginnen können.</strong> Vielen Dank dafür!</p><p>Die Login-Daten haben Sie bereits am TT.MM. per E-Mail erhalten. Für Fragen stehen wir Ihnen unter der Telefonnummer +49 221 423 180 81 oder unter <a href="mailto:office@xlnc-leadership.com">office@xlnc-leadership.com</a> gerne zur Verfügung.</p>',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'readOnly' => 1,
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'RTEonly' => 1,
						'module' => array(
							'name' => 'wizard_rich_text_editor',
							'urlParameters' => array(
								'mode' => 'wizard',
								'act' => 'wizard_rte.php'
							)
						),
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),

	)
);
