<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_answer',
		'label' => 'value',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'dividers2tabs' => TRUE,
		'delete' => 'deleted',
		'hideTable' => true,
		'enablecolumns' => [],
		'searchFields' => '',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('xlnc_tools') . 'Resources/Public/Icons/tx_xlnctools_domain_model_answer.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'hidden, item, value, text_value',
	),
	'types' => array(
		'1' => array('showitem' => 'item, status, value, text_value'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'value' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_answer.value',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'text_value' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_answer.text_value',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'sheet' => array(
			'config' => array(
				'readOnly' => 1
			),
		),

		'item' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_answer.item',
 			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_xlnctools_domain_model_item',
				'minitems' => 0,
				'maxitems' => 1,
				'readOnly' => 1
			),
		),
		'answer_index' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_answer.answer_index',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('Main question', 1),
					array('Compare question', 2)
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => '',
				'readOnly' => 1
			)
		),
		'status' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_answer.status',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('New', 0),
					array('Edit', 1),
					array('Finished', 2)
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => '',
				'readOnly' => 1
			)
		)

	),
);
