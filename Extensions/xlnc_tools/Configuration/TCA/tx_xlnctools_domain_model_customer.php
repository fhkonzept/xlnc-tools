<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_customer',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'searchFields' => 'title,projects,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('xlnc_tools') . 'Resources/Public/Icons/tx_xlnctools_domain_model_customer.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'hidden, title, projects',
	),
	'types' => array(
		'1' => array('showitem' => 'title, industry, num_employees, turnover, internationality, --div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_customer.projects, projects, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),

		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_customer.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'industry' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_customer_industry',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('bitte wählen', 0),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_customer_industry'
			)
		),
		'num_employees' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_customer_employees',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('bitte wählen', 0),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_customer_employees'
			),
		),
		'turnover' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_customer_turnover',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('bitte wählen', 0),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_customer_turnover'
			),
		),
		'internationality' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_customer_internationality',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('bitte wählen', 0),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_customer_internationality'
			)
		),
		'projects' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_customer.projects',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_xlnctools_domain_model_project',
				'foreign_field' => 'customer',
				'minitems' => 0,
				'maxitems' => 1000,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),

	),
);
