<?php

if (!isset($GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['ctrl']['type'])) {
	if (file_exists($GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['ctrl']['dynamicConfigFile'])) {
		require_once($GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['ctrl']['dynamicConfigFile']);
	}
	// no type field defined, so we define it here. This will only happen the first time the extension is installed!!
	$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['ctrl']['type'] = 'tx_extbase_type';
	$tempColumnstx_xlnctools_tx_xlnctools_domain_model_tool = array();
	$tempColumnstx_xlnctools_tx_xlnctools_domain_model_tool[$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['ctrl']['type']] = array(
		'exclude' => 0,
		'label'   => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools.tx_extbase_type',
		'config' => array(
			'type' => 'select',
			'items' => array(
				// array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool.tx_extbase_type.Tx_XlncTools_Tool','Tx_XlncTools_Tool')
			),
			'default' => 'Tx_XlncTools_LeadershipTool',
			'size' => 1,
			'maxitems' => 1,
		)
	);
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_xlnctools_domain_model_tool', $tempColumnstx_xlnctools_tx_xlnctools_domain_model_tool, 1);
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tx_xlnctools_domain_model_tool',
	$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['ctrl']['type'],
	'',
	'before:' . $GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['ctrl']['label']
);

/* inherit and extend the show items from the parent class */

if(isset($GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_Tool']['showitem'])) {
	$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_LeadershipTool']['showitem'] = $GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_Tool']['showitem'];
} elseif(is_array($GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types'])) {
	$tx_xlnctools_domain_model_tool_type_definition = reset($GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']);
	$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_LeadershipTool']['showitem'] = $tx_xlnctools_domain_model_tool_type_definition['showitem'];
} else {
	$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_LeadershipTool']['showitem'] = '';
}
$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_LeadershipTool']['showitem'] .= ',--div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_leadershiptool,';
$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_LeadershipTool']['showitem'] .= '';

$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['columns'][$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['ctrl']['type']]['config']['items'][] = array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool.tx_extbase_type.Tx_XlncTools_LeadershipTool','Tx_XlncTools_LeadershipTool');

/* inherit and extend the show items from the parent class */

if(isset($GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_Tool']['showitem'])) {
	$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_ClimateTool']['showitem'] = $GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_Tool']['showitem'];
} elseif(is_array($GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types'])) {
	// use first entry in types array
	$tx_xlnctools_domain_model_tool_type_definition = reset($GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']);
	$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_ClimateTool']['showitem'] = $tx_xlnctools_domain_model_tool_type_definition['showitem'];
} else {
	$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_ClimateTool']['showitem'] = '';
}
$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_ClimateTool']['showitem'] .= ',--div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_climatetool,';
$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['types']['Tx_XlncTools_ClimateTool']['showitem'] .= '';

$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['columns'][$GLOBALS['TCA']['tx_xlnctools_domain_model_tool']['ctrl']['type']]['config']['items'][] = array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool.tx_extbase_type.Tx_XlncTools_ClimateTool','Tx_XlncTools_ClimateTool');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
	'',
	'EXT:/Resources/Private/Language/locallang_csh_.xlf'
);