<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_project',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',		),
		'searchFields' => 'title,teams,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('xlnc_tools') . 'Resources/Public/Icons/tx_xlnctools_domain_model_project.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'hidden, title, tools, teams',
	),
	'types' => array(
		'1' => array(
			'showitem' => 'title, owner, reference, profile,
			--div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_project.tools, tools,
			--div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_project.teams, teams,
			--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, --palette--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access;access'),
	),
	'palettes' => array(
		'access' => array('showitem' => 'hidden'),
	),
	'columns' => array(
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),

		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_project.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'reference' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_dictionary_project_reference',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('Bitte wählen', 0),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_project_reference'
			)
		),
		'teams' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_project.teams',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_xlnctools_domain_model_team',
				'foreign_field' => 'project',
				'minitems' => 0,
				'maxitems' => 100,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'tools' => array(
	        'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_project.tools',
	        'config' => array(
	                'type' => 'select',
	                'foreign_table' => 'tx_xlnctools_domain_model_tool',
	                'foreign_table_where' => 'ORDER BY title ASC',
	                'size' => 3,
	                'minitems' => 1,
	                'maxitems' => 10,
	                'enableMultiSelectFilterTextfield' => FALSE
	        )
		),
		'owner' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_project.owner',
			'config' => array(
				'type' => 'select',
				'items' => array(

				),
				'foreign_table' => 'be_users',
				'minitems' => 1,
				'maxitems' => 1,
				'size' => 2
			)
		),
		'customer' => array(
			'config' => array(

			)
		)
	),
);
