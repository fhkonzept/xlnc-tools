<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'searchFields' => 'title,display_intro,intro,display_outro,outro,items,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('xlnc_tools') . 'Resources/Public/Icons/tx_xlnctools_domain_model_tool.gif',
		'type' => 'tx_extbase_type',
	),
	'interface' => array(
		'showRecordFieldList' => 'hidden, title, display_intro, intro, display_outro, outro, items',
	),
	'types' => array(
		'Tx_XlncTools_Tool' => array('showitem' => 'title, display_intro, intro;;;richtext:rte_transform[mode=ts_links], display_outro, outro;;;richtext:rte_transform[mode=ts_links], --div--;LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool.tabs.items,items_per_page,items, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'display_intro' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool.display_intro',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'intro' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool.intro',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'module' => array(
							'name' => 'wizard_rich_text_editor',
							'urlParameters' => array(
								'mode' => 'wizard',
								'act' => 'wizard_rte.php'
							)
						),
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'display_outro' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool.display_outro',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'outro' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool.outro',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'module' => array(
							'name' => 'wizard_rich_text_editor',
							'urlParameters' => array(
								'mode' => 'wizard',
								'act' => 'wizard_rte.php'
							)
						),
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'items' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool.items',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_xlnctools_domain_model_item',
				'foreign_field' => 'parentid',
				'foreign_table_field' => 'parenttable',
				'minitems' => 0,
				'maxitems' => 1000,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'items_per_page' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool.items_per_page',
			'config' => array(
				'type' => 'input',
				'size' => 3,
				'eval' => 'trim'
			),
		),
		'tx_extbase_type' => array(
			'exclude' => 1,
			'label'   => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools.tx_extbase_type',
			'config' => array(
				'type' => 'select',
				'items' => array(
					// array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_tool.tx_extbase_type.Tx_XlncTools_Tool','Tx_XlncTools_Tool')
				),
				'default' => 'Tx_XlncTools_LeadershipTool',
				'size' => 1,
				'maxitems' => 1,
			)
		)


	),
);
