<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_item',
		'label' => 'question',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
		),
		'searchFields' => 'question,alternative_question',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('xlnc_tools') . 'Resources/Public/Icons/tx_xlnctools_domain_model_item.gif',
		'type' => 'tx_extbase_type',
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, question, alternative_question',
	),
	'types' => array(
		'Tx_XlncTools_Item' => array(
			'showitem' => 'tx_extbase_type, criteria, question, alternative_question, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden, --div--;LLL:EXT:cms/locallang_ttc.xlf:sys_language_uid_formlabel,sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource'
		),
		'Tx_XlncTools_TextItem' => array(
			'showitem' => 'tx_extbase_type, criteria, question, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden, --div--;LLL:EXT:cms/locallang_ttc.xlf:sys_language_uid_formlabel,sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource'
		),
		'Tx_XlncTools_LeadershipItem' => array(
			'showitem' => 'tx_extbase_type, criteria, question, alternative_question, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden, --div--;LLL:EXT:cms/locallang_ttc.xlf:sys_language_uid_formlabel,sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource'
		),
		'Tx_XlncTools_ClimateItem' => array(
			'showitem' => 'tx_extbase_type, criteria, question, alternative_question, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden, --div--;LLL:EXT:cms/locallang_ttc.xlf:sys_language_uid_formlabel,sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource'
		),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_item',
				'foreign_table_where' => 'AND tx_xlnctools_domain_model_item.pid=###CURRENT_PID### AND tx_xlnctools_domain_model_item.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'question' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_item.question',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 5,
				'eval' => 'trim'
			),
		),
		'alternative_question' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_item.alternative_question',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 5,
				'eval' => 'trim'
			),
		),
		'criteria' => array(
			'exclude' => 1,
            'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_item.criteria',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', -1),
				),
				'foreign_table' => 'tx_xlnctools_domain_model_dictionary_item_criteria'
			)
		),
		'tx_extbase_type' => array(
			'exclude' => 1,
			'l10n_mode' => 'exclude',
			'label'   => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools.tx_extbase_type',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_item.tx_extbase_type.Tx_XlncTools_TextItem','Tx_XlncTools_TextItem'),
					array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_item.tx_extbase_type.Tx_XlncTools_LeadershipItem','Tx_XlncTools_LeadershipItem'),
					array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_item.tx_extbase_type.Tx_XlncTools_ClimateItem','Tx_XlncTools_ClimateItem'),
					//array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_item.tx_extbase_type.Tx_XlncTools_Item','Tx_XlncTools_Item')
				),
				'default' => 'Tx_XlncTools_TextItem',
				'size' => 1,
				'maxitems' => 1,
			)

		),
		'parentid' => array(
			'l10n_mode' => 'exclude',
			'config' => array(

			)
		)

	),
);
