<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_mailtext',
		'label' => 'tx_extbase_type',
		'label_alt' => 'language',
		'label_alt_force' => 1,
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'dividers2tabs' => TRUE,
		'hideTable' => FALSE,
		'enablecolumns' => [],
		'searchFields' => '',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('xlnc_tools') . 'Resources/Public/Icons/tx_xlnctools_domain_model_mailtext.gif',
		'type' => 'tx_extbase_type',
		'security' => [
			'ignoreWebMountRestriction' => 1,
         	'ignoreRootLevelRestriction' => 1,
		]
	),
	'interface' => array(
		'showRecordFieldList' => 'tx_extbase_type, language, l10n_parent, l10n_diffsource, team, bodytext;;;richtext:rte_transform[mode=ts_links]',
	),
	'types' => array(
		'Tx_XlncTools_InvitationText' => array(
			'showitem' => 'tx_extbase_type, language, l10n_parent, l10n_diffsource, team, bodytext;;;richtext:rte_transform[mode=ts_links]'
		),
		'Tx_XlncTools_LeaderInvitationText' => array(
			'showitem' => 'tx_extbase_type, language, l10n_parent, l10n_diffsource, team, bodytext;;;richtext:rte_transform[mode=ts_links]'
		),
		'Tx_XlncTools_ReminderText' => array(
			'showitem' => 'tx_extbase_type, language, l10n_parent, l10n_diffsource, team, bodytext;;;richtext:rte_transform[mode=ts_links]'
		),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		'language' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),

		'team' => array(
			'exclude' => 0,
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'bodytext' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_mailtext.bodytext',
			'config' => array(
				'type' => 'text',
				'default' => '',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'module' => array(
							'name' => 'wizard_rich_text_editor',
							'urlParameters' => array(
								'mode' => 'wizard',
								'act' => 'wizard_rte.php'
							)
						),
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'tx_extbase_type' => array(
			'exclude' => 1,
			'l10n_mode' => 'exclude',
			'label'   => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools.tx_extbase_type',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_mailtext.tx_extbase_type.Tx_XlncTools_InvitationText','Tx_XlncTools_InvitationText'),
					array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_mailtext.tx_extbase_type.Tx_XlncTools_LeaderInvitationText','Tx_XlncTools_LeaderInvitationText'),
					array('LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_mailtext.tx_extbase_type.Tx_XlncTools_ReminderText','Tx_XlncTools_ReminderText'),
				),
				'default' => 'Tx_XlncTools_InvitationText',
				'size' => 1,
				'maxitems' => 1,
			)

		),


	),
);
