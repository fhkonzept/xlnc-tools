<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_maillog',
		'label' => 'email',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'hideTable' => TRUE,
		'enablecolumns' => [],
		'searchFields' => '',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('xlnc_tools') . 'Resources/Public/Icons/tx_xlnctools_domain_model_maillog.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'team, email, scope',
	),
	'types' => array(
		'1' => array('showitem' => 'team, email, scope'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		'team' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_maillog.team',
			'config' => array(
				'type' => 'input',
				'size' => 11,
				'eval' => 'int'
			)
		),
		'email' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_maillog.email',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'scope' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:xlnc_tools/Resources/Private/Language/locallang_db.xlf:tx_xlnctools_domain_model_maillog.scope',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('Invitation', 1),
					array('Reminder', 2)
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => '',
				'readOnly' => 1
			)
		)

	),
);
