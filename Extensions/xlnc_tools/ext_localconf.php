<?php
if (! defined ( 'TYPO3_MODE' ))
	die ( 'Access denied.' );

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin (
	'Xlnc.' . $_EXTKEY,
	'Access',
	array (
		'Access' => 'form,authenticate,landingPage'
		),
		// landingPag is changing for LeaderSheets and Sheets so it is not chachable
	array (
		'Access' => 'form,authenticate,landingPage'
		)
	);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin (
	'Xlnc.' . $_EXTKEY,
	'Tool',
	array (
		'Tool' => 'dashboard,form,intro,survey,interrupt,outro,updateSheet,updateSheetAnswers,logout'
		),
	array (
		'Tool' => 'dashboard,form,intro,survey,interrupt,outro,updateSheet,updateSheetAnswers,logout'
		)
	);

// SignalSlot

// Before survey rendering
$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
$signalSlotDispatcher->connect(
	\Xlnc\XlncTools\Controller\ToolController::class,
	'beforeSurvey',
	\Xlnc\XlncTools\SignalSlot\Tool::class,
	'beforeSurvey'
);

// Tool started
$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
$signalSlotDispatcher->connect(
	\Xlnc\XlncTools\Controller\ToolController::class,
	'toolStarted',
	\Xlnc\XlncTools\SignalSlot\Tool::class,
	'toolStarted'
);

// Tool finished
$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
$signalSlotDispatcher->connect(
	\Xlnc\XlncTools\Controller\ToolController::class,
	'toolFinished',
	\Xlnc\XlncTools\SignalSlot\Tool::class,
	'toolFinished'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin (
	'Xlnc.' . $_EXTKEY,
	'RestDispatch',
	array (
		'Rest\RestDispatch' => 'dispatch'
		),
	array (
		'Rest\RestDispatch' => 'dispatch'
		)
	);
$TYPO3_CONF_VARS['SC_OPTIONS']['tslib/class.tslib_fe.php']['checkAlternativeIdMethods-PostProc']['xlnc_tools'] = 'Xlnc\XlncTools\Hook\RestRouteHook->hookUrl';
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['decodeSpURL_preProc'][] =  'Xlnc\XlncTools\Hook\RestRouteHook->grabUrl';
