<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Xlnc.' . $_EXTKEY,
	'Access',
	'XLNC Tools Access'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Xlnc.' . $_EXTKEY,
	'Tool',
	'XLNC Tool'
);

if (TYPO3_MODE === 'BE') {

	/**
	 * Registers a Backend Module
	 */
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'Xlnc.' . $_EXTKEY,
		'tools',	 // Make module a submodule of 'tools'
		'toolmanagement',	// Submodule key
		'',						// Position
		array(
			'Backend\Management' => 'index',

		),
		array(
			'access' => 'user,group',
			'icon'   => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/module.svg',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_toolmanagement.xlf',
		)
	);

	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'Xlnc.' . $_EXTKEY,
		'tools',	 // Make module a submodule of 'tools'
		'toolarchive',	// Submodule key
		'',						// Position
		array(
			'Backend\Archive' => 'index',

		),
		array(
			'access' => 'user,group',
			'icon'   => 'EXT:' . $_EXTKEY . '/Resources/Public/Icons/module.svg',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_toolarchive.xlf',
		)
	);

}

\Xlnc\XlncTools\Utility\RestServiceUtility::registerResource(
	"teams",
	"team",
	"Backend\\Team",
	'XlncTools'
);

\Xlnc\XlncTools\Utility\RestServiceUtility::registerResource(
	"report",
	"report",
	"Backend\\Report",
	'XlncTools'
);

\Xlnc\XlncTools\Utility\RestServiceUtility::registerResource(
	"export",
	"export",
	"Backend\\Export",
	'XlncTools'
);

\Xlnc\XlncTools\Utility\RestServiceUtility::registerResource(
	"invitations",
	"invitation",
	"Backend\\Invitation",
	'XlncTools'
);

\Xlnc\XlncTools\Utility\RestServiceUtility::registerResource(
	"languages",
	"language",
	"Backend\\Language",
	'XlncTools'
);

\Xlnc\XlncTools\Utility\RestServiceUtility::registerResource(
	"demo",
	"demo",
	"Backend\\Demo",
	'XlncTools'
);




\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Tools');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_xlnctools_domain_model_tool', 'EXT:xlnc_tools/Resources/Private/Language/locallang_csh_tx_xlnctools_domain_model_tool.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_xlnctools_domain_model_item', 'EXT:xlnc_tools/Resources/Private/Language/locallang_csh_tx_xlnctools_domain_model_item.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_xlnctools_domain_model_team', 'EXT:xlnc_tools/Resources/Private/Language/locallang_csh_tx_xlnctools_domain_model_team.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_xlnctools_domain_model_sheet', 'EXT:xlnc_tools/Resources/Private/Language/locallang_csh_tx_xlnctools_domain_model_sheet.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_xlnctools_domain_model_answer', 'EXT:xlnc_tools/Resources/Private/Language/locallang_csh_tx_xlnctools_domain_model_answer.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_xlnctools_domain_model_customer', 'EXT:xlnc_tools/Resources/Private/Language/locallang_csh_tx_xlnctools_domain_model_customer.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_xlnctools_domain_model_project', 'EXT:xlnc_tools/Resources/Private/Language/locallang_csh_tx_xlnctools_domain_model_project.xlf');
