<?php
namespace Xlnc\XlncTools\View\Rest\Backend;
/**
 * @todo code review
 * @todo document why transform is static
 */
class JsonView extends \TYPO3\CMS\Extbase\Mvc\View\JsonView {

    const VARIABLE_NAME = "var";
    const COLLECTION = 0;
    const MEMBER = 1;

    protected $defaultConfiguration = [];

    protected $value = null;

    protected $mode = self::COLLECTION;

    protected $memberClassName = "";

    public function assign($_, $value) {
        $this->value = $value;
    }

    public function render() {
        if($this->mode === self::MEMBER) {
            $data = static::transform($this->value);
        } else {
            $memberView = new $this->memberClassName;
            if( !is_array($this->value) ) {
                if(method_exists($this->value, "toArray")) {
                    $this->value = $this->value->toArray();
                } else {
                    $this->value = (array)$value;
                }
            }
            $data = array_map(function($item) use ($memberView){
                return $memberView->transform($item);
            }, $this->value);
        }

        return json_encode($data);
    }

    public static function transform($value) {
        return [];
    }
}
