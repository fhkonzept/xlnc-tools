<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Project;

use Xlnc\XlncTools\View\Rest\Backend\JsonView;

use TYPO3\CMS\Backend\Utility\BackendUtility,
    TYPO3\CMS\Core\Utility\GeneralUtility,
    TYPO3\CMS\Extbase\Object\ObjectManager;

use TYPO3\CMS\Core\Authentication\BackendUserAuthentication,
    TYPO3\CMS\Core\FormProtection\BackendFormProtection,
    TYPO3\CMS\Core\FormProtection\FormProtectionFactory,
    TYPO3\CMS\Core\Registry;

use TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper,
    TYPO3\CMS\Fluid\Core\Rendering\RenderingContext;

class Show extends JsonView {

    protected $mode = JsonView::MEMBER;

    public static function transform($project) {
        $customer = $project->getCustomer();
        $tools = array_map(function($tool) {
            return [
                "uid" => $tool->getUid(),
                "title" => $tool->getTitle(),
                "type" => (new \ReflectionClass($tool))->getShortName(),
                "itemCount" => $tool->getItems()->count(),
                "requiredItemCount" => $tool->getRequiredItemsNumber()
            ];
        }, $project->getTools()->toArray());
        return [
            "uid" => $project->getUid(),
            "customer" => [
                "uid" => $customer->getUid(),
                "title" => $customer->getTitle(),
                "actions" => [
                    "edit" => self::editCustomerUrl($customer)
                ]
            ],
            #"tools" => $tools,
            "title" => $project->getTitle(),
            "actions" => [
                "edit" => self::editUrl($project)
            ]
        ];
    }

    private static function editUrlParams($project) {
        return 'edit[tx_xlnctools_domain_model_project][' . $project->getUid() . ']=edit';
    }

    private static function editCustomerUrlParams($customer) {
        return 'edit[tx_xlnctools_domain_model_customer][' . $customer->getUid() . ']=edit';
    }

    private static function editUrl($project) {
        // this only works if $GLOBALS['BE_USER'] is set (I'm not kidding)
        // this is true for be_users that are admins, but not necessarily for "normal" users
        // we need to initialize $GLOBALS['BE_USER'] with something that will make the UriBuilder
        // convince that we're actually in the backend
        if(!isset($GLOBALS['BE_USER'])) {
            self::buildBackendUserAndFormProtection();
        }

        return BackendUtility::getModuleUrl('record_edit') . '&' . self::editUrlParams($project);
    }

    private static function editCustomerUrl($customer) {
        // this only works if $GLOBALS['BE_USER'] is set (I'm not kidding)
        // this is true for be_users that are admins, but not necessarily for "normal" users
        // we need to initialize $GLOBALS['BE_USER'] with something that will make the UriBuilder
        // convince that we're actually in the backend
        if(!isset($GLOBALS['BE_USER'])) {
            self::buildBackendUserAndFormProtection();
        }
        return BackendUtility::getModuleUrl('record_edit') . '&' . self::editCustomerUrlParams($customer);
    }

    private static function buildBackendUserAndFormProtection() {
        $GLOBALS['BE_USER'] = GeneralUtility::makeInstance(ObjectManager::class)->get(BackendUserAuthentication::class);
        $GLOBALS['BE_USER']->start();

        // also initialize LANG if not already done;
        if(!isset($GLOBALS['LANG'])) {
            $GLOBALS['LANG'] = GeneralUtility::makeInstance(\TYPO3\CMS\Lang\LanguageService::class);
        }

        FormProtectionFactory::set(BackendFormProtection::class,
            GeneralUtility::makeInstance(BackendFormProtection::class,
                $GLOBALS['BE_USER'],
                GeneralUtility::makeInstance(Registry::class),
                function() {}
            )
        );
        FormProtectionFactory::set('default', FormProtectionFactory::get(BackendFormProtection::class));
    }

}
