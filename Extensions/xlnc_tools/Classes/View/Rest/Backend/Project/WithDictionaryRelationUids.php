<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Project;

class WithDictionaryRelationUids extends Show {

    public static function transform($project) {
        $transformedProject = parent::transform($project);
        $transformedProject['reference'] = $project->getReference();
        $transformedProject['profile'] = $project->getProfile();
        $transformedProject['customer']['numEmployees'] = $project->getCustomer()->getNumEmployees();
        $transformedProject['customer']['industry'] = $project->getCustomer()->getIndustry();
        $transformedProject['customer']['internationality'] = $project->getCustomer()->getInternationality();
        $transformedProject['customer']['turnover'] = $project->getCustomer()->getTurnover();
        return $transformedProject;
    }
}
