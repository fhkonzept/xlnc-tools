<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Customer;

use Xlnc\XlncTools\View\Rest\Backend\JsonView;

class Index extends JsonView {
    protected $memberClassName = Show::class;
}
