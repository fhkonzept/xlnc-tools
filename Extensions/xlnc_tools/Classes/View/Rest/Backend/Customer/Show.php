<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Customer;

use Xlnc\XlncTools\View\Rest\Backend\JsonView;

class Show extends JsonView {

    protected $mode = JsonView::MEMBER;

    public static function transform($customer) {
        return [
            "uid" => $customer->getUid(),
            "title" => $customer->getTitle()
        ];
    }

}
