<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Team;

use Xlnc\XlncTools\View\Rest\Backend\JsonView;

class SendReport extends JsonView {

    protected $mode = JsonView::MEMBER;

    public static function transform($teamAndTool) {
        return [
            "title" => "Report versandt.",
            "message" => "Der Report wurde an " . $teamAndTool->getTeam()->getEmail() . " versandt."
        ];
    }
}
