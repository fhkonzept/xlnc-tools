<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Team;

use Xlnc\XlncTools\View\Rest\Backend\JsonView;

class Unlock extends JsonView {

    protected $mode = JsonView::MEMBER;

    public static function transform($team) {
        return [
            "title" => "Wieder geöffnet",
            "message" => "Team " . $team->getTitle() . " ist wieder freigeschaltet."
        ];
    }
}
