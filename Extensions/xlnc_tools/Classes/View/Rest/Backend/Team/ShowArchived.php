<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Team;

class ShowArchived extends Show {

    public static function transform($team) {
        $sheets = [];

        $invitations = [];

        $tools = array_map( function($tool) use ($team) {
            return [
                "uid" => $tool->getUid(),
                "title" => $tool->getTitle(),
                "type" => get_class($tool),
                "hasEnoughFinishedSheets" => $team->hasEnoughFinishedSheets($tool),
                "finishedSheetCount" => $team->getFinishedSheetCount($tool),
                "leaderSheetFinished" => $team->leaderSheetFinished($tool)
            ];
        }, $team->getProject()->getTools()->toArray());

        $project = \Xlnc\XlncTools\View\Rest\Backend\Project\WithDictionaryRelationUids::transform($team->getProject());

        return [
            "uid" => $team->getUid(),
            "project" => $project,
            "tools" => $tools,
            "title" => $team->getTitle(),
            "leaderEmail" => $team->getEmail(),
            "leaderFirstName" => $team->getFirstName(),
            "leaderLastName" => $team->getLastName(),
            "hidden" => $team->getHidden(),
            "sheets" => $sheets,
            "hasAutomaticEnd" => $team->getHasAutomaticEnd(),
            "automaticEndAt" => ($team->getAutomaticEndAt() ? $team->getAutomaticEndAt()->format(\DateTime::ISO8601) : NULL),
            "isFinished" => $team->getIsFinished(),
            "isArchived" => $team->getIsArchived(),
            "invitations" => $invitations,
            "hasEnoughParticipants" => $team->hasEnoughParticipants(),
            "mailText" => self::fromRte($team->getMailText()),
            "altMailText" => self::fromRte($team->getAltMailText()),
            "reminderText" => self::fromRte($team->getReminderText()),
            "actions" => [
                "edit" => self::editUrl($team)
            ],
        ];
    }
}
