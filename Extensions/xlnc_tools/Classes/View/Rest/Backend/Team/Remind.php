<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Team;

use Xlnc\XlncTools\View\Rest\Backend\JsonView;

class Remind extends JsonView {

    protected $mode = JsonView::MEMBER;

    public static function transform($emails) {
        if(empty($emails)) {
            return [
                "message" => "Alle Teilnehmer wurden bereits erinnert.",
                "title" => "keine Erinnerung nötig"
            ];
        } else {
            return [
                "message" => "an: " . implode(", ", $emails),
                "title" => "Erinnerung wurde verschickt"
            ];
        }

    }

}
