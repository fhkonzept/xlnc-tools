<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Team;

use Xlnc\XlncTools\View\Rest\Backend\JsonView;

use TYPO3\CMS\Backend\Utility\BackendUtility,
    TYPO3\CMS\Core\Utility\GeneralUtility,
    TYPO3\CMS\Extbase\Object\ObjectManager;

use TYPO3\CMS\Core\Authentication\BackendUserAuthentication,
    TYPO3\CMS\Core\FormProtection\BackendFormProtection,
    TYPO3\CMS\Core\FormProtection\FormProtectionFactory,
    TYPO3\CMS\Core\Registry;

use TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper,
    TYPO3\CMS\Fluid\Core\Rendering\RenderingContext;

class Show extends JsonView {

    protected $mode = JsonView::MEMBER;

    public static function transform($team) {
        $sheets = array_map( function($sheet) use ($team) {
            return [
                "uid" => $sheet->getUid(),
                "accessKey" => $sheet->getAccessKey(),
                "type" => (new \ReflectionClass($sheet))->getShortName(),
            ];
        }, $team->getSheets()->toArray());

        $invitations = array_map( function($invitation) {
            return [
                "uid" => $invitation->getUid(),
                "address" => $invitation->getAddress(),
                "sheetType" => $invitation->getSheetType(),
                "type" => (new \ReflectionClass($invitation))->getShortName()
            ];
        }, $team->getInvitations()->toArray());

        $tools = array_map( function($tool) use ($team) {
            return [
                "uid" => $tool->getUid(),
                "title" => $tool->getTitle(),
                "type" => get_class($tool),
                "hasEnoughFinishedSheets" => $team->hasEnoughFinishedSheets($tool),
                "finishedSheetCount" => $team->getFinishedSheetCount($tool),
                "leaderSheetFinished" => $team->leaderSheetFinished($tool)
            ];
        }, $team->getProject()->getTools()->toArray());

        $texts = array_map( function($text) use ($team) {
            return [
                "uid" => $text->getUid(),
                "type" => $text->getTxExtbaseType(),
                "content" => self::fromRte($text->getBodytext()),
                "language" => [
                    "uid" => $text->getLanguage() ? $text->getLanguage()->getUid() : 0
                ]
            ];
        }, $team->getMailTexts()->toArray());

        $project = \Xlnc\XlncTools\View\Rest\Backend\Project\Show::transform($team->getProject());

        return [
            "uid" => $team->getUid(),
            "project" => $project,
            "tools" => $tools,
            "title" => $team->getTitle(),
            "leaderEmail" => $team->getEmail(),
            "leaderFirstName" => $team->getFirstName(),
            "leaderLastName" => $team->getLastName(),
            "hidden" => $team->getHidden(),
            "sheets" => $sheets,
            "hasAutomaticEnd" => $team->getHasAutomaticEnd(),
            "automaticEndAt" => ($team->getAutomaticEndAt() ? $team->getAutomaticEndAt()->format(\DateTime::ISO8601) : NULL),
            "isFinished" => $team->getIsFinished(),
            "isArchived" => $team->getIsArchived(),
            "invitations" => $invitations,
            "hasEnoughParticipants" => $team->hasEnoughParticipants(),
            "mailText" => self::fromRte($team->getMailText()),
            "altMailText" => self::fromRte($team->getAltMailText()),
            "reminderText" => self::fromRte($team->getReminderText()),
            "texts" => $texts,
            "actions" => [
                "edit" => self::editUrl($team)
            ],
        ];
    }

    protected static function editUrlParams($team) {
        return 'edit[tx_xlnctools_domain_model_team][' . $team->getUid() . ']=edit';
    }

    protected static function editUrl($team) {
        // this only works if $GLOBALS['BE_USER'] is set (I'm not kidding)
        // this is true for be_users that are admins, but not necessarily for "normal" users
        // we need to initialize $GLOBALS['BE_USER'] with something that will make the UriBuilder
        // convince that we're actually in the backend
        if(!isset($GLOBALS['BE_USER'])) {
            self::buildBackendUserAndFormProtection();
        }
        return BackendUtility::getModuleUrl('record_edit') . '&' . self::editUrlParams($team);
    }

    protected static function fromRte($value) {
        return HtmlViewHelper::renderStatic(['parseFuncTSPath' => 'lib.parseFunc_RTE'],
            function() use ($value) {
                return $value;
            }, GeneralUtility::makeInstance(RenderingContext::class));
    }

    protected static function buildBackendUserAndFormProtection() {
        $GLOBALS['BE_USER'] = GeneralUtility::makeInstance(ObjectManager::class)->get(BackendUserAuthentication::class);
        $GLOBALS['BE_USER']->start();

        // also initialize LANG if not already done;
        if(!isset($GLOBALS['LANG'])) {
            $GLOBALS['LANG'] = GeneralUtility::makeInstance(\TYPO3\CMS\Lang\LanguageService::class);
        }

        FormProtectionFactory::set(BackendFormProtection::class,
            GeneralUtility::makeInstance(BackendFormProtection::class,
                $GLOBALS['BE_USER'],
                GeneralUtility::makeInstance(Registry::class),
                function() {}
            )
        );
        FormProtectionFactory::set('default', FormProtectionFactory::get(BackendFormProtection::class));
    }

}
