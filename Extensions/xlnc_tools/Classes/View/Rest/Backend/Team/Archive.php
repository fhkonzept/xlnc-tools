<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Team;

use Xlnc\XlncTools\View\Rest\Backend\JsonView;

class Archive extends JsonView {

    protected $mode = JsonView::MEMBER;

    public static function transform($team) {
        return [
            "title" => "Im Archiv",
            "message" => "Team " . $team->getTitle() . " ist jetzt archiviert."
        ];
    }
}
