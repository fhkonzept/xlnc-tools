<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Team;

use Xlnc\XlncTools\View\Rest\Backend\JsonView;

use TYPO3\CMS\Core\Utility\GeneralUtility,
    TYPO3\CMS\Extbase\Object\ObjectManager;


use TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper,
    TYPO3\CMS\Fluid\Core\Rendering\RenderingContext;


class RequestText extends JsonView {

    protected $mode = JsonView::MEMBER;

    public static function transform($text) {
        return [
            "uid" => $text->getUid(),
            "type" => $text->getTxExtbaseType(),
            "content" => self::fromRte($text->getBodytext()),
            "language" => [
                "uid" => $text->getLanguage() ? $text->getLanguage()->getUid() : 0
            ]
        ];
    }

    protected static function fromRte($value) {
        return HtmlViewHelper::renderStatic(['parseFuncTSPath' => 'lib.parseFunc_RTE'],
            function() use ($value) {
                return $value;
            }, GeneralUtility::makeInstance(RenderingContext::class));
    }

}
