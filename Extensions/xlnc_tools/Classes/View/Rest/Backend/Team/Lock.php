<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Team;

use Xlnc\XlncTools\View\Rest\Backend\JsonView;

class Lock extends JsonView {

    protected $mode = JsonView::MEMBER;

    public static function transform($team) {
        return [
            "title" => "Abgeschlossen",
            "message" => "Team " . $team->getTitle() . " ist jetzt geschlossen."
        ];
    }
}
