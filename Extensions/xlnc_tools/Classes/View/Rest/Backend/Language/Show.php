<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Language;

use Xlnc\XlncTools\View\Rest\Backend\JsonView;

use TYPO3\CMS\Core\Imaging\IconFactory,
    TYPO3\CMS\Core\Imaging\Icon,
    TYPO3\CMS\Core\Utility\GeneralUtility,
    TYPO3\CMS\Extbase\Object\ObjectManager;

class Show extends JsonView {

    protected $mode = JsonView::MEMBER;

    public static function transform($language) {
        $iconFactory = GeneralUtility::makeInstance(ObjectManager::class)->get(IconFactory::class);
        return [
            "uid" => $language->getUid(),
            "title" => $language->getTitle(),
            "isoCode" => $language->getIsoCode(),
            "flagIcon" => $iconFactory->getIcon('flags-' . $language->getFlag(), Icon::SIZE_SMALL)->render()
        ];
    }
}
