<?php
namespace Xlnc\XlncTools\View\Rest\Backend\Invitation;

use Xlnc\XlncTools\View\Rest\Backend\JsonView;

class Create extends JsonView {
    protected $mode = JsonView::MEMBER;

    public static function transform($invitation) {
        return [
            "message" => "Einladung für " . $invitation->getAddress() . " erstellt.",
            "title" => "Einladung gespeichert."
        ];
    }
}
