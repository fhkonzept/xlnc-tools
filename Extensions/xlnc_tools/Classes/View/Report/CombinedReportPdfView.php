<?php
namespace Xlnc\XlncTools\View\Report;

use \TYPO3\CMS\Fluid\View\StandaloneView;

use \Xlnc\XlncTools\Domain\Model\LeadershipTool,
    \Xlnc\XlncTools\Domain\Model\ClimateTool;


class CombinedReportPdfView {

    /**
     *
     * @var string
     */
    protected $templateFileName;

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;

    /**
     * barchart and text value container
     * @var array
     */
    protected $values = [];

    /**
     * @var array<\Xlnc\XlncTools\Domain\Model\Team>
     */
    protected $teams;

    /**
     * @var \Xlnc\XlncTools\Report\CombinedReport
     */
    protected $report;

    protected $criteriaIds = [
        "1" => [
            StyleReportPdfView::NORMATIVE,
            StyleReportPdfView::DIRECTIVE,
            StyleReportPdfView::PARTICIPATIVE,
            StyleReportPdfView::INTEGRATIVE,
            StyleReportPdfView::COACHIVE,
            StyleReportPdfView::INSPIRATIVE
        ],
        "3" => [
            StyleReportPdfView::NORMATIVE,
            StyleReportPdfView::DIRECTIVE,
            StyleReportPdfView::PARTICIPATIVE,
            StyleReportPdfView::INTEGRATIVE,
            StyleReportPdfView::COACHIVE,
            StyleReportPdfView::INSPIRATIVE
        ],
        "2" => [
            ClimateReportPdfView::AUTONOMY      ,
            ClimateReportPdfView::CREDIBILITY   ,
            ClimateReportPdfView::SAFETY        ,

            ClimateReportPdfView::DEVELOPMENT   ,
            ClimateReportPdfView::CLEARNESS     ,
            ClimateReportPdfView::FLEXIBILITY   ,
            ClimateReportPdfView::RECOGNITION   ,

            ClimateReportPdfView::IDENTIFICATION,
            ClimateReportPdfView::TEAMSPIRIT    ,
        ]
    ];

    public function getCriteriaUidsForTool($tool) {
        return $this->criteriaIds[(string)$tool->getUid()];
    }

    public function setPercentage($key, $criterium, $value) {
        if(!is_array($this->values[$key])) {
            $this->values['key'] = [];
        }

        $this->values[$key][$criterium]['percentage'] = $value;
    }

    public function setRanking($key, $criterium, $value) {
        if(!is_array($this->values[$key])) {
            $this->values['key'] = [];
        }

        $this->values[$key][$criterium]['fineRanking'] = $value;
    }

    public function addScoreDifference($key, $question, $firstValue, $secondValue) {
        if(!is_array($this->values[$key])) {
            $this->values[$key] = [];
        }

        $newItem = [];
        $newItem["item"]["question"] = $question;

        $newItem["score"] = $firstValue;
        $newItem["selfScore"] = $secondValue;

        // calculate the correct x offset for the circle in the diagram
        $newItem["pos"] = 247.5 + $firstValue * 40;
        $newItem["posSelf"] = 247.5 + $secondValue * 40;

        $this->values[$key][] = $newItem;
    }

    public function setTeam($team) {
        $this->team = $team;
    }

    public function setTool($tool) {
        $this->tool = $tool;
    }

    public function setReport($report) {
        $this->report = $report;
    }

    public function setValue($name, $value) {
        $this->values[$name] = $value;
    }

    public function setDynamicTexts($dynamicTexts) {
        $this->values["dynamicTexts"] = $dynamicTexts;
    }

    public function setTextAnswerResults($textAnswerResults) {
        $this->values["textAnswerResults"] = $textAnswerResults;
    }

    public function getPdfView() {
        if(NULL === $this->pdfView) {
            $pdfTemplatePath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('xlnc_tools') . 'Resources/Private/Templates/Pdf/Reports/Combined.html';
            $this->pdfView = $this->objectManager->get(StandaloneView::class);
            $this->pdfView->setTemplatePathAndFilename($this->pdfTemplatePath());
        }

        return $this->pdfView;
    }

    public function render() {
        $this->getPdfView()->assign(
            'settings', array(
                'partialRootPath'	=> 'EXT:xlnc_tools/Resources/Private/Templates/Pdf/Reports/Partials'
            )
        );
        $this->getPdfView()->assign('now', time());
        $this->getPdfView()->assign('report', $this->report);
        $this->getPdfView()->assign('managementStylesSampleSize', [
            "sampleSize" => $this->report->getSampleSizeForTool($this->getManagementStylesTool())
        ]);
        $this->getPdfView()->assign('teamClimateSampleSize', [
            "sampleSize" => $this->report->getSampleSizeForTool($this->getTeamClimateTool())
        ]);
        $this->getPdfView()->assign('dynamicTexts', $this->values["dynamicTexts"]);

        $this->getPdfView()->assignMultiple(
            [
                'managementAverages'			=> [
                    'serie1Values' => [
                        (int) $this->values['managementAverages'][StyleReportPdfView::NORMATIVE]['percentage'],
                        (int) $this->values['managementAverages'][StyleReportPdfView::DIRECTIVE]['percentage'],
                        (int) $this->values['managementAverages'][StyleReportPdfView::PARTICIPATIVE]['percentage'],
                        (int) $this->values['managementAverages'][StyleReportPdfView::INTEGRATIVE]['percentage'],
                        (int) $this->values['managementAverages'][StyleReportPdfView::COACHIVE]['percentage'],
                        (int) $this->values['managementAverages'][StyleReportPdfView::INSPIRATIVE]['percentage']
                    ]
                ],

                'managementDistribution' => [
                    'values' => [
                        'signature' => [
                            $this->values['managementDistribution'][StyleReportPdfView::NORMATIVE]['percentage']['signature'],
                            $this->values['managementDistribution'][StyleReportPdfView::DIRECTIVE]['percentage']['signature'],
                            $this->values['managementDistribution'][StyleReportPdfView::PARTICIPATIVE]['percentage']['signature'],
                            $this->values['managementDistribution'][StyleReportPdfView::INTEGRATIVE]['percentage']['signature'],
                            $this->values['managementDistribution'][StyleReportPdfView::COACHIVE]['percentage']['signature'],
                            $this->values['managementDistribution'][StyleReportPdfView::INSPIRATIVE]['percentage']['signature']
                        ],
                        'potential' => [
                            $this->values['managementDistribution'][StyleReportPdfView::NORMATIVE]['percentage']['potential'],
                            $this->values['managementDistribution'][StyleReportPdfView::DIRECTIVE]['percentage']['potential'],
                            $this->values['managementDistribution'][StyleReportPdfView::PARTICIPATIVE]['percentage']['potential'],
                            $this->values['managementDistribution'][StyleReportPdfView::INTEGRATIVE]['percentage']['potential'],
                            $this->values['managementDistribution'][StyleReportPdfView::COACHIVE]['percentage']['potential'],
                            $this->values['managementDistribution'][StyleReportPdfView::INSPIRATIVE]['percentage']['potential']
                        ],
                        'development' => [
                            $this->values['managementDistribution'][StyleReportPdfView::NORMATIVE]['percentage']['development'],
                            $this->values['managementDistribution'][StyleReportPdfView::DIRECTIVE]['percentage']['development'],
                            $this->values['managementDistribution'][StyleReportPdfView::PARTICIPATIVE]['percentage']['development'],
                            $this->values['managementDistribution'][StyleReportPdfView::INTEGRATIVE]['percentage']['development'],
                            $this->values['managementDistribution'][StyleReportPdfView::COACHIVE]['percentage']['development'],
                            $this->values['managementDistribution'][StyleReportPdfView::INSPIRATIVE]['percentage']['development']
                        ],
                    ]
                ],

                'climateAverages' => $this->_comparisonBarchart($this->values['employees'], $this->values['employeesSecond']),

                'climateDistribution' => [
                    'values' => [
                        'signature' => [
                            $this->values['climateDistribution'][ClimateReportPdfView::AUTONOMY]['percentage']['signature'],
                            $this->values['climateDistribution'][ClimateReportPdfView::CREDIBILITY]['percentage']['signature'],
                            $this->values['climateDistribution'][ClimateReportPdfView::SAFETY]['percentage']['signature'],
                            $this->values['climateDistribution'][ClimateReportPdfView::DEVELOPMENT]['percentage']['signature'],
                            $this->values['climateDistribution'][ClimateReportPdfView::CLEARNESS]['percentage']['signature'],
                            $this->values['climateDistribution'][ClimateReportPdfView::FLEXIBILITY]['percentage']['signature'],
                            $this->values['climateDistribution'][ClimateReportPdfView::RECOGNITION]['percentage']['signature'],
                            $this->values['climateDistribution'][ClimateReportPdfView::IDENTIFICATION]['percentage']['signature'],
                            $this->values['climateDistribution'][ClimateReportPdfView::TEAMSPIRIT]['percentage']['signature']
                        ],
                        'potential' => [
                            $this->values['climateDistribution'][ClimateReportPdfView::AUTONOMY]['percentage']['potential'],
                            $this->values['climateDistribution'][ClimateReportPdfView::CREDIBILITY]['percentage']['potential'],
                            $this->values['climateDistribution'][ClimateReportPdfView::SAFETY]['percentage']['potential'],
                            $this->values['climateDistribution'][ClimateReportPdfView::DEVELOPMENT]['percentage']['potential'],
                            $this->values['climateDistribution'][ClimateReportPdfView::CLEARNESS]['percentage']['potential'],
                            $this->values['climateDistribution'][ClimateReportPdfView::FLEXIBILITY]['percentage']['potential'],
                            $this->values['climateDistribution'][ClimateReportPdfView::RECOGNITION]['percentage']['potential'],
                            $this->values['climateDistribution'][ClimateReportPdfView::IDENTIFICATION]['percentage']['potential'],
                            $this->values['climateDistribution'][ClimateReportPdfView::TEAMSPIRIT]['percentage']['potential']
                        ],
                        'development' => [
                            $this->values['climateDistribution'][ClimateReportPdfView::AUTONOMY]['percentage']['development'],
                            $this->values['climateDistribution'][ClimateReportPdfView::CREDIBILITY]['percentage']['development'],
                            $this->values['climateDistribution'][ClimateReportPdfView::SAFETY]['percentage']['development'],
                            $this->values['climateDistribution'][ClimateReportPdfView::DEVELOPMENT]['percentage']['development'],
                            $this->values['climateDistribution'][ClimateReportPdfView::CLEARNESS]['percentage']['development'],
                            $this->values['climateDistribution'][ClimateReportPdfView::FLEXIBILITY]['percentage']['development'],
                            $this->values['climateDistribution'][ClimateReportPdfView::RECOGNITION]['percentage']['development'],
                            $this->values['climateDistribution'][ClimateReportPdfView::IDENTIFICATION]['percentage']['development'],
                            $this->values['climateDistribution'][ClimateReportPdfView::TEAMSPIRIT]['percentage']['development']
                        ],
                    ]
                ],
            ]
        );

        return $this->pdfView->render();
    }

    public function getManagementStylesTool() {
        return $this->report->getToolForType(LeadershipTool::class);
    }

    public function getTeamClimateTool() {
        return $this->report->getToolForType(ClimateTool::class);
    }

    protected function _comparisonBarchart($a, $b) {
        $graphMaxLength = 1100;
		$graphStartX = 202 + 100;
		$comparisonBarchart = array(
			'serie1Values'			=> array(),
			'serie2Values'			=> array(),
			'diffs'					=> array(),
			'diffBars'				=> array(),
			'diffTextsY'			=> array(),
			#'legendBox1Format'		=> $this->colors["self"]['score'],
			#'legendBox2Format'		=> $this->colors["self"]['secondScore'],
			'legendY'				=> 10000 // Hide legend (we have a custom legend)
		);


        $diffBarsStartY1 = 150;
		$diffBarsStartY2 = 181;

		$diffBarsDistanceNextBar = 34;
		$diffBarsDistanceNextCriteria = 87.75;

        $diffTextDefaultFormat = array(
            "DrawBox" => TRUE,
        	"R" => 255,
        	"G" => 255,
        	"B" => 255,
        	"Alpha" => 100,
        	"FontName" => 'typo3conf/ext/vhs_royal/Lib/pChart/fonts/SourceSansPro-Bold.ttf',
        	"BoxR" => 125,
        	"BoxG" => 125,
        	"BoxB" => 125,
        	"BoxAlpha" => 100,
        	"BoxRounded" => TRUE,
        	"RoundedRadius" => 4,
		);

        $redHighlight = [
            "R" => 185,
    		"G" => 14,
    		"B" => 12,
        ];

        $employees = $b;
        $self = $a;

        $criteriaIds = $this->criteriaIds["2"];

		for( $i = 0; $i < 9; $i++) {
			$comparisonBarchart['diffFormats'][$i] = $diffTextDefaultFormat;
			if(abs((int) $self[$criteriaIds[$i]]['percentage'] - (int) $employees[$criteriaIds[$i]]['percentage']) >= ReportPdfView::CRITICAL_DIFFERENCE_LIMIT) {
                $comparisonBarchart['diffFormats'][$i]['BoxR'] = $redHighlight['R'];
				$comparisonBarchart['diffFormats'][$i]['BoxG'] = $redHighlight['G'];
				$comparisonBarchart['diffFormats'][$i]['BoxB'] = $redHighlight['B'];
			}
			$y1 = $diffBarsStartY1 + ($i*$diffBarsDistanceNextCriteria);
			$y2 = $diffBarsStartY2 + ($i*$diffBarsDistanceNextCriteria);
			if((int) $self[$criteriaIds[$i]]['percentage'] - (int) $employees[$criteriaIds[$i]]['percentage'] > 0) {
				$y1 += $diffBarsDistanceNextBar + 1;
				$y2 += $diffBarsDistanceNextBar ;
			}
			$comparisonBarchart['diffBars'][$i] = array(
                'X1' => $graphStartX,
                'X2' => (int)($graphStartX+$graphMaxLength*(
                    (max($self[$criteriaIds[$i]]['percentage'],
                    $employees[$criteriaIds[$i]]['percentage']))/100)) -2 - 2,
                'Y1' => $y1,
                'Y2' => $y2+1);
			$comparisonBarchart['diffs'][$i] = abs((int) $self[$criteriaIds[$i]]['percentage'] - (int) $employees[$criteriaIds[$i]]['percentage']);
			$comparisonBarchart['serie1Values'][$i] = (int) $self[$criteriaIds[$i]]['percentage'];
			$comparisonBarchart['serie2Values'][$i] = (int) $employees[$criteriaIds[$i]]['percentage'];
			$comparisonBarchart['diffTextsY'][$i] = $y2 - 10;
		}

        return $comparisonBarchart;
    }

    /**
     * returns the path to the generated PDF file
     * @todo @frank document this
     * @return string
     */
    public function getFilePath() {
        trim( preg_replace( '/<!--(.|\s)*?-->/' , '' , htmlspecialchars_decode( strip_tags( $this->render() ) ) ) );
    }

    /**
     * the EXT:path to the template to be used
     * @return string
     */
    protected function pdfTemplatePath() {
        return \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('xlnc_tools') . 'Resources/Private/Templates/Pdf/Reports/Combined.html';
    }

};
