<?php

namespace Xlnc\XlncTools\View\Report;

use \TYPO3\CMS\Fluid\View\StandaloneView;

class ReportPdfView {
    /* above this number, differences in percentages are marked red */

    const CRITICAL_DIFFERENCE_LIMIT = 15;

    /**
     *
     * @var string
     */
    protected $templateFileName;

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;

    /**
     * barchart and text value container
     * @var array
     */
    protected $values = [];

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Team
     */
    protected $team;

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Tool
     */
    protected $tool;

    /**
     * @var \Xlnc\XlncTools\Report\ToolReport
     */
    protected $report;
    protected $criteriaIds = [];

    public function getCriteriaUids() {
        return $this->criteriaIds;
    }

    public function setPercentage($key, $criterium, $value) {
        if (!is_array($this->values[$key])) {
            $this->_buildValueArray($key);
        }

        $this->values[$key][$criterium]['percentage'] = $value;
    }

    public function setRanking($key, $criterium, $value) {
        if (!is_array($this->values[$key])) {
            $this->_buildValueArray($key);
        }

        $this->values[$key][$criterium]['fineRanking'] = $value;
    }

    /**
     * adds a score difference to be rendered as dot matrix
     * @param string $key         the key used to assign to the correct template
     * @param Item   $item        the item the score difference relates to
     * @param int    $firstValue  the first value in this item's comparison
     * @param int    $secondValue the second value in this item's comparison
     */
    public function addScoreDifference($key, $item, $firstValue, $secondValue) {
        if (!is_array($this->values[$key])) {
            $this->values[$key] = [];
        }

        $newItem = [];
        $newItem["item"] = $item;

        $newItem["score"] = $firstValue;
        $newItem["selfScore"] = $secondValue;

        // calculate the correct x offset for the circle in the diagram
        $newItem["pos"] = 247.5 + $firstValue * 40;
        $newItem["posSelf"] = 247.5 + $secondValue * 40;

        $this->values[$key][] = $newItem;
    }

    public function setTeam($team) {
        $this->team = $team;
    }

    public function setTool($tool) {
        $this->tool = $tool;
    }

    public function setReport($report) {
        $this->report = $report;
    }

    public function setValue($name, $value) {
        $this->values[$name] = $value;
    }

    public function setDynamicTexts($dynamicTexts) {
        $this->values["dynamicTexts"] = $dynamicTexts;
    }

    public function setTextAnswerResults($textAnswerResults) {
        $this->values["textAnswerResults"] = $textAnswerResults;
    }

    public function getPdfView() {
        if (NULL === $this->pdfView) {
            $this->pdfView = $this->objectManager->get(StandaloneView::class);
            $this->pdfView->setTemplatePathAndFilename($this->pdfTemplatePath());
        }

        return $this->pdfView;
    }

    public function render() {
        $this->getPdfView()->assign(
                'settings', array(
            'partialRootPath' => 'EXT:xlnc_tools/Resources/Private/Templates/Pdf/Reports/Partials'
                )
        );
        $this->getPdfView()->assign('now', time());
        $this->getPdfView()->assign('team', $this->team);
        $this->getPdfView()->assign('tool', $this->tool);
        $this->getPdfView()->assign('report', $this->report);
        $this->getPdfView()->assign('textAnswerResults', $this->values["textAnswerResults"]);
        $this->getPdfView()->assign('dynamicTexts', $this->values["dynamicTexts"]);

        return $this->pdfView->render();
    }

    protected function agreementBarchart() {
        return [
            'serie1Values' => $this->valueSeries("agreement"),
            'serie2Values' => $this->valueSeries("agreementSecond"),
            'legendBox1Format' => array(
                "R" => 125,
                "G" => 125,
                "B" => 125,
                "Alpha" => 100
            ),
            'legendBox2Format' => array(
                "R" => 238,
                "G" => 239,
                "B" => 239,
                "BorderR" => 125,
                "BorderG" => 125,
                "BorderB" => 125,
                "Alpha" => 100),
            'legendY' => 10000 // Hide legend (we have a custom legend)
        ];
    }

    /**
     * returns the path to the generated PDF file
     * @todo @frank document this
     * @return string
     */
    public function getFilePath() {
        trim(preg_replace('/<!--(.|\s)*?-->/', '', htmlspecialchars_decode(strip_tags($this->render()))));
    }

    /**
     * the EXT:path to the template to be used
     * @return string
     */
    protected function pdfTemplatePath() {
        return "";
    }

    /**
     * prepare the given key in $this->values as array structure
     * @param  string $key
     */
    protected function _buildValueArray($key) {
        $this->values[$key] = [];
        foreach ($this->criteriaIds as $id) {
            $this->values[$key][$id] = [
                'percentage' => 0,
                'fineRanking' => ''
            ];
        }
    }

}
