<?php
namespace Xlnc\XlncTools\View\Report;

class ClimateSelfReportPdfView extends ReportPdfView {

    /* criteria uids hardcoded */
    const AUTONOMY          = 2;
    const CREDIBILITY       = 7;
    const SAFETY            = 14;

    const DEVELOPMENT       = 5;
    const CLEARNESS         = 11;
    const FLEXIBILITY       = 6;
    const RECOGNITION       = 1;

    const IDENTIFICATION    = 8;
    const TEAMSPIRIT        = 15;

    /**
     * critera uids in the correct order
     * @var array
     */
    protected $criteriaIds = [
        self::CREDIBILITY   ,
        self::AUTONOMY      ,
        self::SAFETY        ,

        self::CLEARNESS     ,
        self::FLEXIBILITY   ,
        self::RECOGNITION   ,
        self::DEVELOPMENT   ,

        self::IDENTIFICATION,
        self::TEAMSPIRIT    ,
    ];

    protected $colors = [
        "self" => [

            'score' => [
                "R"         => 245,
                "G"         => 159,
                "B"         => 53,
                "Alpha"     => 100
            ],

            'secondScore' =>  [
                "R"         => 253,
                "G"         => 212,
                "B"         => 140,
                "Alpha"     => 100
            ],


        ],
        "employees" => [

            'score' => [
                "R"         => 129,
                "G"         => 131,
                "B"         => 164,
                "Alpha"     => 100
            ],

            'secondScore' =>  [
                "R"         => 188,
                "G"         => 193,
                "B"         => 212,
                "Alpha"     => 100
            ],
        ],
    ];

    public function getCriteriaUids() {
        return $this->criteriaIds;
    }

    public function render() {

        $this->getPdfView()->assignMultiple(
			[
                //'overwriteChartConfigAgreement'			=> $this->agreementBarchart(),
                'overwriteChartConfigSelf'		        => $this->selfComparisonBarchart(),
                //'overwriteChartConfigEmployees'		    => $this->employeeComparisonBarchart(),
                //'overwriteChartConfigComparison'		=> $this->comparisonBarchart(),
                //'highestFiveSecondScoreDifferences'		=> $this->values["secondScoreDifference"],
                //'highestFiveScoreDifferences'		    => $this->values["scoreDifference"],
            ]
		);

        return parent::render();
    }
/*
    private function comparisonBarchart() {
        return $this->_comparisonBarchart($this->values["self"], $this->values["employees"]);
    }

    private function employeeComparisonBarchart() {
        return $this->_comparisonBarchart($this->values["employees"], $this->values["secondEmployees"]);
    }
*/
    private function selfComparisonBarchart() {
        return $this->_comparisonBarchart($this->values["self"], $this->values["secondSelf"]);
    }

    protected function _comparisonBarchart($a, $b) {
        $graphMaxLength = 1100;
		$graphStartX = 202 + 100;
		$comparisonBarchart = array(
			'serie1Values'			=> array(),
			'serie2Values'			=> array(),
			'diffs'					=> array(),
			'diffBars'				=> array(),
			'diffTextsY'			=> array(),
			#'legendBox1Format'		=> $this->colors["self"]['score'],
			#'legendBox2Format'		=> $this->colors["self"]['secondScore'],
			'legendY'				=> 10000 // Hide legend (we have a custom legend)
		);


        $diffBarsStartY1 = 150;
		$diffBarsStartY2 = 181;

		$diffBarsDistanceNextBar = 34;
		$diffBarsDistanceNextCriteria = 87.75;

        $diffTextDefaultFormat = array(
            "DrawBox" => TRUE,
        	"R" => 255,
        	"G" => 255,
        	"B" => 255,
        	"Alpha" => 100,
        	"FontName" => 'typo3conf/ext/vhs_royal/Lib/pChart/fonts/SourceSansPro-Bold.ttf',
        	"BoxR" => 125,
        	"BoxG" => 125,
        	"BoxB" => 125,
        	"BoxAlpha" => 100,
        	"BoxRounded" => TRUE,
        	"RoundedRadius" => 4,
		);

        $redHighlight = [
            "R" => 185,
    		"G" => 14,
    		"B" => 12,
        ];

        $employees = $b;
        $self = $a;

		for( $i = 0; $i < 9; $i++) {
			$comparisonBarchart['diffFormats'][$i] = $diffTextDefaultFormat;
			if(abs((int) $self[$this->criteriaIds[$i]]['percentage'] - (int) $employees[$this->criteriaIds[$i]]['percentage']) >= self::CRITICAL_DIFFERENCE_LIMIT) {
                $comparisonBarchart['diffFormats'][$i]['BoxR'] = $redHighlight['R'];
				$comparisonBarchart['diffFormats'][$i]['BoxG'] = $redHighlight['G'];
				$comparisonBarchart['diffFormats'][$i]['BoxB'] = $redHighlight['B'];
			}
			$y1 = $diffBarsStartY1 + ($i*$diffBarsDistanceNextCriteria);
			$y2 = $diffBarsStartY2 + ($i*$diffBarsDistanceNextCriteria);
			if((int) $self[$this->criteriaIds[$i]]['percentage'] - (int) $employees[$this->criteriaIds[$i]]['percentage'] > 0) {
				$y1 += $diffBarsDistanceNextBar + 1;
				$y2 += $diffBarsDistanceNextBar ;
			}
			$comparisonBarchart['diffBars'][$i] = array(
                'X1' => $graphStartX,
                'X2' => (int)($graphStartX+$graphMaxLength*(
                    (max($self[$this->criteriaIds[$i]]['percentage'],
                    $employees[$this->criteriaIds[$i]]['percentage']))/100)) -2 - 2,
                'Y1' => $y1,
                'Y2' => $y2+1);
			$comparisonBarchart['diffs'][$i] = abs((int) $self[$this->criteriaIds[$i]]['percentage'] - (int) $employees[$this->criteriaIds[$i]]['percentage']);
			$comparisonBarchart['serie1Values'][$i] = (int) $self[$this->criteriaIds[$i]]['percentage'];
			$comparisonBarchart['serie2Values'][$i] = (int) $employees[$this->criteriaIds[$i]]['percentage'];
			$comparisonBarchart['diffTextsY'][$i] = $y2 - 10;
		}

        return $comparisonBarchart;
    }

    protected function valueSeries($key) {
        return [
            (int) $this->values[$key][self::CREDIBILITY]['percentage'],
            (int) $this->values[$key][self::AUTONOMY]['percentage'],
            (int) $this->values[$key][self::SAFETY]['percentage'],

            (int) $this->values[$key][self::CLEARNESS]['percentage'],
            (int) $this->values[$key][self::FLEXIBILITY]['percentage'],
            (int) $this->values[$key][self::RECOGNITION]['percentage'],
            (int) $this->values[$key][self::DEVELOPMENT]['percentage'],

            (int) $this->values[$key][self::IDENTIFICATION]['percentage'],
            (int) $this->values[$key][self::TEAMSPIRIT]['percentage'],
        ];
    }

    /**
     * the EXT:path to the template to be used
     * @return string
     */
    protected function pdfTemplatePath() {
        return \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('xlnc_tools') . 'Resources/Private/Templates/Pdf/Reports/ClimateSelf.html';
    }

}
