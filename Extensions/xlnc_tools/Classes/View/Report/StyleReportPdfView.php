<?php
namespace Xlnc\XlncTools\View\Report;

class StyleReportPdfView extends ReportPdfView {

    /* criteria uids hardcoded */
    const NORMATIVE      = 12;
    const DIRECTIVE      = 4;
    const PARTICIPATIVE  = 13;
    const INTEGRATIVE    = 10;
    const COACHIVE       = 3;
    const INSPIRATIVE    = 9;

    /* above this number, differences in percentages are marked red */
    const CRITICAL_DIFFERENCE_LIMIT = 15;

    /**
     * critera uids in the correct order
     * @var array
     */
    protected $criteriaIds = [
        self::NORMATIVE,
        self::DIRECTIVE,
        self::PARTICIPATIVE,
        self::INTEGRATIVE,
        self::COACHIVE,
        self::INSPIRATIVE
    ];

    protected $colors = [
        "self" => [
            #EAF6FE - Blau Pastell 4
            'low' => [
                "R"         => 234,
                "G"         => 246,
                "B"         => 254,
                "Alpha"     => 100
            ],
            #A1DAF8 - Blau Pastell 3 Abgrenzung
            'low_to_medium' => [
                "R"         => 161,
                "G"         => 218,
                "B"         => 248,
                "Alpha"     => 100
            ],
            #0BBBEF - Blau Pastell 2 Abgrenzung
            'medium' => [
                "R"         => 11,
                "G"         => 187,
                "B"         => 239,
                "Alpha"     => 100
            ],
            #0095DB - Blau Pastell 1 Abgrenzung
            'medium_to_high' => [
                "R"         => 0,
                "G"         => 149,
                "B"         => 219,
                "Alpha"     => 100
            ],
            #0069B4 - Blau Highlight Abgrenzung
            'high' => [
                "R"         => 0,
                "G"         => 105,
                "B"         => 180,
                "Alpha"     => 100
            ],
        ],
        "employees" => [
            #D5DBD6 - Flieder Pastell 3
            'low' => [
                "R"         => 213,
                "G"         => 219,
                "B"         => 229,
                "Alpha"     => 100
            ],
            #A2A7C0 - Flieder Pastell 1
            'low_to_medium' =>  [
                "R"         => 162,
                "G"         => 167,
                "B"         => 192,
                "Alpha"     => 100
            ],
            #8084A4 - Flieder Highlight
            'medium' =>  [
                "R"         => 129,
                "G"         => 132,
                "B"         => 164,
                "Alpha"     => 100
            ],
            #616489 - Tiefblau hell
            'medium_to_high' =>  [
                "R"         => 97,
                "G"         => 100,
                "B"         => 137,
                "Alpha"     => 100
            ],
            #494B61 - Tiefblau
            'high' =>  [
                "R"         => 73,
                "G"         => 75,
                "B"         => 97,
                "Alpha"     => 100
            ],
        ],
    ];

    public function render() {

        $this->getPdfView()->assignMultiple(
			array(

				'overwriteChartConfigSelf'				=> $this->barchart("self"),
				'overwriteChartConfigEmployees'			=> $this->barchart("employees"),
				'overwriteChartConfigAgreement'			=> $this->agreementBarchart(),
				'overwriteChartConfigComparison'		=> $this->comparisonBarchart(),
				'highestFiveScoreDifferences'			=> $this->values["scoreDifference"],
				'highestFiveSecondScoreDifferences'		=> $this->values["secondScoreDifference"],
				'lowestFiveSecondScores'				=> $this->values["lowestSecondScore"],
				'highestFiveScores'				        => $this->values["highestFiveScores"],
				'lowestFiveScores'				        => $this->values["lowestFiveScores"],

			)
		);

        return parent::render();
    }


    /**
     * the EXT:path to the template to be used
     * @return string
     */
    protected function pdfTemplatePath() {
        return \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('xlnc_tools') . 'Resources/Private/Templates/Pdf/Reports/Styles.html';
    }

    private function barchart($valueKey) {
        return array(
            'serie1Values'	        => $this->valueSeries($valueKey),
            'legendBox1Format'		=> $this->colors[$valueKey]['low'],
            'legendBox2Format'		=> $this->colors[$valueKey]['low_to_medium'],
            'legendBox3Format'		=> $this->colors[$valueKey]['medium'],
            'legendBox4Format'		=> $this->colors[$valueKey]['medium_to_high'],
            'legendBox5Format'		=> $this->colors[$valueKey]['high'],
            'chartFormat'	=> array(
                'OverrideColors'	=> array(
                                $this->colors[$valueKey][$this->values[$valueKey][self::NORMATIVE]['fineRanking']],
                                $this->colors[$valueKey][$this->values[$valueKey][self::DIRECTIVE]['fineRanking']],
                                $this->colors[$valueKey][$this->values[$valueKey][self::PARTICIPATIVE]['fineRanking']],
                                $this->colors[$valueKey][$this->values[$valueKey][self::INTEGRATIVE]['fineRanking']],
                                $this->colors[$valueKey][$this->values[$valueKey][self::COACHIVE]['fineRanking']],
                                $this->colors[$valueKey][$this->values[$valueKey][self::INSPIRATIVE]['fineRanking']]
                )
            ),
            'legendY'	=> 10000 // Hide legend (we have a custom legend)
        );
    }


    private function comparisonBarchart() {

        $graphMaxLength = 1100;
		$graphStartX = 202;
		$comparisonBarchart = array(
			'serie1Values'			=> array(),
			'serie2Values'			=> array(),
			'diffs'					=> array(),
			'diffBars'				=> array(),
			'diffTextsY'			=> array(),
			'legendBox1Format'		=> $this->colors["self"]['medium_to_high'],
			'legendBox2Format'		=> $this->colors["employees"]['medium_to_high'],
			'legendY'				=> 10000 // Hide legend (we have a custom legend)
		);


        $diffBarsStartY1 = 154;
		$diffBarsStartY2 = 205;

		$diffBarsDistanceNextBar = 54;
		$diffBarsDistanceNextCriteria = 135;

		$diffTextDefaultFormat = array(
            "DrawBox" => TRUE,
        	"R" => 255,
        	"G" => 255,
        	"B" => 255,
        	"Alpha" => 100,
        	"FontName" => 'typo3conf/ext/vhs_royal/Lib/pChart/fonts/SourceSansPro-Bold.ttf',
        	"BoxR" => 125,
        	"BoxG" => 125,
        	"BoxB" => 125,
        	"BoxAlpha" => 100,
        	"BoxRounded" => TRUE,
        	"RoundedRadius" => 4,
		);

        $redHighlight = [
            "R" => 185,
    		"G" => 14,
    		"B" => 12,
        ];

        $employees = $this->values["employees"];
        $self = $this->values["self"];

		for( $i = 0; $i < 6; $i++) {
			$comparisonBarchart['diffFormats'][$i] = $diffTextDefaultFormat;
			if(abs((int) $self[$this->criteriaIds[$i]]['percentage'] - (int) $employees[$this->criteriaIds[$i]]['percentage']) >= self::CRITICAL_DIFFERENCE_LIMIT) {
				$comparisonBarchart['diffFormats'][$i]['BoxR'] = $redHighlight['R'];
				$comparisonBarchart['diffFormats'][$i]['BoxG'] = $redHighlight['G'];
				$comparisonBarchart['diffFormats'][$i]['BoxB'] = $redHighlight['B'];
			}
			$y1 = $diffBarsStartY1 + ($i*$diffBarsDistanceNextCriteria);
			$y2 = $diffBarsStartY2 + ($i*$diffBarsDistanceNextCriteria);
			if((int) $self[$this->criteriaIds[$i]]['percentage'] - (int) $employees[$this->criteriaIds[$i]]['percentage'] > 0) {
				$y1 += $diffBarsDistanceNextBar;
				$y2 += $diffBarsDistanceNextBar;
			}
			$comparisonBarchart['diffBars'][$i] = array(
                'X1' => $graphStartX,
                'X2' => (int)($graphStartX+$graphMaxLength*(
                    (max($self[$this->criteriaIds[$i]]['percentage'],
                    $employees[$this->criteriaIds[$i]]['percentage']))/100)) - 4,
                'Y1' => $y1,
                'Y2' => $y2+1);
			$comparisonBarchart['diffs'][$i] = abs((int) $self[$this->criteriaIds[$i]]['percentage'] - (int) $employees[$this->criteriaIds[$i]]['percentage']);
			$comparisonBarchart['serie1Values'][$i] = (int) $self[$this->criteriaIds[$i]]['percentage'];
			$comparisonBarchart['serie2Values'][$i] = (int) $employees[$this->criteriaIds[$i]]['percentage'];
			$comparisonBarchart['diffTextsY'][$i] = $y2 - 10;
		}

        return $comparisonBarchart;
    }

    protected function valueSeries($key) {
        return [
            (int) $this->values[$key][self::NORMATIVE]['percentage'],
            (int) $this->values[$key][self::DIRECTIVE]['percentage'],
            (int) $this->values[$key][self::PARTICIPATIVE]['percentage'],
            (int) $this->values[$key][self::INTEGRATIVE]['percentage'],
            (int) $this->values[$key][self::COACHIVE]['percentage'],
            (int) $this->values[$key][self::INSPIRATIVE]['percentage']
        ];
    }



}
