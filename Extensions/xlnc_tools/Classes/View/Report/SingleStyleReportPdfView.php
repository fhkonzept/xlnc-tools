<?php
namespace Xlnc\XlncTools\View\Report;

class SingleStyleReportPdfView extends StyleReportPdfView {

    /* criteria uids hardcoded */
    const NORMATIVE      = 12;
    const DIRECTIVE      = 4;
    const PARTICIPATIVE  = 13;
    const INTEGRATIVE    = 10;
    const COACHIVE       = 3;
    const INSPIRATIVE    = 9;

    /* above this number, differences in percentages are marked red */
    const CRITICAL_DIFFERENCE_LIMIT = 15;

    /**
     * critera uids in the correct order
     * @var array
     */
    protected $criteriaIds = [
        self::NORMATIVE,
        self::DIRECTIVE,
        self::PARTICIPATIVE,
        self::INTEGRATIVE,
        self::COACHIVE,
        self::INSPIRATIVE
    ];

    protected $colors = [
        "self" => [
            #EAF6FE - Blau Pastell 4
            'low' => [
                "R"         => 234,
                "G"         => 246,
                "B"         => 254,
                "Alpha"     => 100
            ],
            #A1DAF8 - Blau Pastell 3 Abgrenzung
            'low_to_medium' => [
                "R"         => 161,
                "G"         => 218,
                "B"         => 248,
                "Alpha"     => 100
            ],
            #0BBBEF - Blau Pastell 2 Abgrenzung
            'medium' => [
                "R"         => 11,
                "G"         => 187,
                "B"         => 239,
                "Alpha"     => 100
            ],
            #0095DB - Blau Pastell 1 Abgrenzung
            'medium_to_high' => [
                "R"         => 0,
                "G"         => 149,
                "B"         => 219,
                "Alpha"     => 100
            ],
            #0069B4 - Blau Highlight Abgrenzung
            'high' => [
                "R"         => 0,
                "G"         => 105,
                "B"         => 180,
                "Alpha"     => 100
            ],
        ],
        "employees" => [
            #D5DBD6 - Flieder Pastell 3
            'low' => [
                "R"         => 213,
                "G"         => 219,
                "B"         => 229,
                "Alpha"     => 100
            ],
            #A2A7C0 - Flieder Pastell 1
            'low_to_medium' =>  [
                "R"         => 162,
                "G"         => 167,
                "B"         => 192,
                "Alpha"     => 100
            ],
            #8084A4 - Flieder Highlight
            'medium' =>  [
                "R"         => 129,
                "G"         => 132,
                "B"         => 164,
                "Alpha"     => 100
            ],
            #616489 - Tiefblau hell
            'medium_to_high' =>  [
                "R"         => 97,
                "G"         => 100,
                "B"         => 137,
                "Alpha"     => 100
            ],
            #494B61 - Tiefblau
            'high' =>  [
                "R"         => 73,
                "G"         => 75,
                "B"         => 97,
                "Alpha"     => 100
            ],
        ],
    ];



    /**
     * the EXT:path to the template to be used
     * @return string
     */
    protected function pdfTemplatePath() {
        return \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('xlnc_tools') . 'Resources/Private/Templates/Pdf/Reports/SingleStyles.html';
    }

    private function barchart($valueKey) {
        return array(
            'serie1Values'	        => $this->valueSeries($valueKey),
            'legendBox1Format'		=> $this->colors[$valueKey]['low'],
            'legendBox2Format'		=> $this->colors[$valueKey]['low_to_medium'],
            'legendBox3Format'		=> $this->colors[$valueKey]['medium'],
            'legendBox4Format'		=> $this->colors[$valueKey]['medium_to_high'],
            'legendBox5Format'		=> $this->colors[$valueKey]['high'],
            'chartFormat'	=> array(
                'OverrideColors'	=> array(
                                $this->colors[$valueKey][$this->values[$valueKey][self::NORMATIVE]['fineRanking']],
                                $this->colors[$valueKey][$this->values[$valueKey][self::DIRECTIVE]['fineRanking']],
                                $this->colors[$valueKey][$this->values[$valueKey][self::PARTICIPATIVE]['fineRanking']],
                                $this->colors[$valueKey][$this->values[$valueKey][self::INTEGRATIVE]['fineRanking']],
                                $this->colors[$valueKey][$this->values[$valueKey][self::COACHIVE]['fineRanking']],
                                $this->colors[$valueKey][$this->values[$valueKey][self::INSPIRATIVE]['fineRanking']]
                )
            ),
            'legendY'	=> 10000 // Hide legend (we have a custom legend)
        );
    }






}
