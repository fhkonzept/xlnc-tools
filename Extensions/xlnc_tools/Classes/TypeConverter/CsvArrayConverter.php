<?php
namespace Xlnc\XlncTools\TypeConverter;

use \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface,
    \TYPO3\CMS\Extbase\Property\TypeConverterInterface;

class CsvArrayConverter implements TypeConverterInterface
{

    /**
     * @var \TYPO3\CMS\Extbase\Property\PropertyMapper
     * @inject
     */
    protected $propertyMapper;

    /**
     * @return array<string>
     */
    public function getSupportedSourceTypes() {
        return ["string"];
    }

    /**
     * @return string
     */
    public function getSupportedTargetType() {
        return ["array"];
    }

    /**
     * @return string
     */
    public function getTargetTypeForSource($source, $originalTargetType, PropertyMappingConfigurationInterface $configuration = null) {
        return "array";
    }

    /**
     * @return int
     */
    public function getPriority() {
        return 0;
    }

    /**
     * @param mixed $source the source data
     * @param string $targetType the type to convert to.
     * @return bool TRUE if this TypeConverter can convert from $source to $targetType, FALSE otherwise.
     */
    public function canConvertFrom($source, $targetType)  {
        return $source === 'string' && $targetType === 'array';
    }

    /**
     * @param mixed $source
     * @return array<mixed>
     * @api
     */
    public function getSourceChildPropertiesToBeConverted($source) {
        return [];
    }

    /**
     * @param string $targetType
     * @param string $propertyName
     * @param \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration
     * @return string the type of $propertyName in $targetType
     */
    public function getTypeOfChildProperty($targetType, $propertyName, PropertyMappingConfigurationInterface $configuration) {
        return "mixed";
    }

    /**
     * @param mixed $source
     * @param string $targetType
     * @param array $convertedChildProperties
     * @param PropertyMappingConfigurationInterface $configuration
     * @return mixed|\TYPO3\CMS\Extbase\Error\Error the target type, or an error object if a user-error occurred
     * @throws \TYPO3\CMS\Extbase\Property\Exception\TypeConverterException thrown in case a developer error occurred
     * @api
     */
    public function convertFrom($source, $targetType, array $convertedChildProperties = array(), PropertyMappingConfigurationInterface $configuration = null) {
        $sourceIds = explode(",", $source);
        $targetClass = $configuration->getConfigurationValue(self::class, 'targetClass');
        $mapper = $this->propertyMapper;
        $result = array_map( function ($uid) use ($mapper, $targetClass) {
            return $mapper->convert($uid, $targetClass);
        }, $sourceIds);
        return $result;
    }
}
