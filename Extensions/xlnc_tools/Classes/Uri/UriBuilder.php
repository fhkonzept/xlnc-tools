<?php
namespace Xlnc\XlncTools\Uri;

use \Xlnc\XlncTools\Domain\Model\Invitation;

class UriBuilder extends \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder {

    /**
     * builds the login URL for the given invitation
     * @param  Invitation $invitation
     * @return string absolute URL to login page
     */
    public function buildLoginUrl (Invitation $invitation) {
        // default language does not have a sys_language record
        $languageUid = $invitation->getLanguage() ? $invitation->getLanguage()->getUid() : 0;

        /** @var array */
        $extConf = @unserialize ( $GLOBALS["TYPO3_CONF_VARS"]['EXT']['extConf']['xlnc_tools'] );

        return $this->reset()
            ->setCreateAbsoluteUri(TRUE)
            ->setTargetPageUid((int)$extConf["authenticatePageId"])
            ->setArguments(["L" => (int)$languageUid])
            ->buildFrontendUri();
    }

    /**
     * builds the login URL for the given invitation, enabling an auto login
     * @param  Invitation $invitation
     * @return string absolute URL to login page
     */
    public function buildLoginUrlWithCode (Invitation $invitation) {
        // default language does not have a sys_language record
        $languageUid = $invitation->getLanguage() ? $invitation->getLanguage()->getUid() : 0;

        /** @var array */
        $extConf = @unserialize ( $GLOBALS["TYPO3_CONF_VARS"]['EXT']['extConf']['xlnc_tools'] );

        return $this->reset()
            ->setCreateAbsoluteUri(TRUE)
            ->setTargetPageUid((int)$extConf["authenticatePageId"])
            ->setArguments(["L" => (int)$languageUid])
            ->uriFor('authenticate', ["code" => $invitation->getAddress()], 'Access', 'xlnctools', 'access');
    }

}
