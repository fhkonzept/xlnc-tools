<?php
namespace Xlnc\XlncTools\Domain\Model;

/**
 * A Customer where dictionary relations are replaced with the respective UIDs
 */
class CustomerWithoutDictionaryRelations extends Customer {

	/**
	 * @var int
	 */
	protected $industry;

	/**
	 * @var int
	 */
	protected $numEmployees;

	/**
	 * @var int
	 */
	protected $turnover;

	/**
	 * @var int
	 */
	protected $internationality;

}
