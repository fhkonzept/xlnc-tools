<?php
namespace Xlnc\XlncTools\Domain\Model;

/**
 * a model for a backend SystemLanguage
 */
class SystemLanguage extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
	 * @var string
	 */
	protected $title;

    /**
	 * @var string
	 */
	protected $flag;

    /**
     * @var string
     */
    protected $isoCode;

	/**
	* Generic getter/setter, for simple property types like string or integer
	*/
	public function __call($name, $arguments)
    {
        if($name){
            $method = substr($name, 0, 3);
            $attr = lcfirst(substr($name, 3));
            switch($method){
                case 'set':
                    $this->$attr = $arguments[0];
                    break;
                case 'get':
                    if(!array_key_exists($attr, get_object_vars($this)))
                        return;
                    return $this->$attr;
            }
        }
    }

}
