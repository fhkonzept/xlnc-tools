<?php
namespace Xlnc\XlncTools\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Frank Fischer <fischer@fh-konzept.de>, fh-konzept GmbH
 *           Malte Muth <muth@fh-konzept.de>, fh-konzept GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Item
 */
class Item extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * indicates whether or not this item should be counted when tallying scores
	 * @var bool
	 */
	protected $countForScoring = FALSE;

	/**
	 * @var string
	 */
	protected $question = '';

	/**
	 * @var string
	 */
	protected $alternativeQuestion = '';

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Item\Criteria
	 * @lazy
	 */
	protected $criteria = 0;

	/**
	 * @var string
	 */
	protected $txExtbaseType = '';

	/**
	 * @var integer
	 */
	protected $parentid = 0;

	/**
	* Generic getter/setter, for simple property types like string or integer
	*/
	public function __call($name, $arguments)
    {
        if($name){
            $method = substr($name, 0, 3);
            $attr = lcfirst(substr($name, 3));
            switch($method){
                case 'set':
                    $this->$attr = $arguments[0];
                    break;
                case 'get':
                    if(!array_key_exists($attr, get_object_vars($this)))
                        return;
                    return $this->$attr;
            }
        }
    }

	/**
	 * Returns the question
	 *
	 * @return string $question
	 */
	public function getQuestion() {
		return $this->question;
	}

	/**
	 * Sets the question
	 *
	 * @param string $question
	 * @return void
	 */
	public function setQuestion($question) {
		$this->question = $question;
	}

	/**
	 * Returns the alternativeQuestion
	 *
	 * @return string $alternativeQuestion
	 */
	public function getAlternativeQuestion() {
		return $this->alternativeQuestion;
	}

	/**
	 * Sets the alternativeQuestion
	 *
	 * @param string $alternativeQuestion
	 * @return void
	 */
	public function setAlternativeQuestion($alternativeQuestion) {
		$this->alternativeQuestion = $alternativeQuestion;
	}

	/**
	 * @return bool
	 */
	public function getCountForScoring() {
		return $this->countForScoring;
	}

}
