<?php
namespace Xlnc\XlncTools\Domain\Model;


/**
 * MailLog
 */
class MailText extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * @var string
	 */
	protected $txExtbaseType;

	public function getTxExtbaseType() {
		return $this->txExtbaseType;
	}

	public function setTxExtbaseType($txExtbaseType) {
		$this->txExtbaseType = $txExtbaseType;
	}

	/**
	 * @var string
	 */
	protected $bodytext;

	public function getBodytext() {
		return $this->bodytext;
	}

	public function setBodytext($bodytext) {
		$this->bodytext = $bodytext;
	}

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\SystemLanguage
	 */
	protected $language;

	public function getLanguage() {
		return $this->language;
	}

	public function setLanguage($language) {
		$this->language = $language;
	}
}
