<?php
namespace Xlnc\XlncTools\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Frank Fischer <fischer@fh-konzept.de>, fh-konzept GmbH
 *           Malte Muth <muth@fh-konzept.de>, fh-konzept GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Invitation
 */
class Invitation extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * @warning this is a hack to ensure newly created records get stored
	 * on a certain pid, which is also hardcoded for now
	 * @todo clean this up properly
	 * @var integer
	 */
	protected $pid = 18;

	/**
	 * @var string
	 */
	protected $txExtbaseType = '';

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\Team
	 * @lazy
	 */
	protected $team;

    /**
	 * @var string
	 */
	protected $address = '';

    /**
	 * @var string
	 */
	protected $sheetType = '';

    /**
	 * @var \DateTime
	 */
	protected $crdate;

    /**
	 * @var \Xlnc\XlncTools\Domain\Model\SystemLanguage
	 */
	protected $language;

	/**
	 * @return \Xlnc\XlncTools\Domain\Model\SystemLanguage
	 */
	public function getLanguage() {
		return $this->language;
	}

	/**
	 * @param \Xlnc\XlncTools\Domain\Model\SystemLanguage $language
	 */
	public function setLanguage($language) {
		$this->language = $language;
	}

	/**
	 * temporary variable for holding the sheet this invitation belongs to
	 * @warning do not add this to TCA or we will break anonymity
	 * @var \Xlnc\XlncTools\Domain\Model\Sheet
	 */
	protected $sheet = NULL;

	/**
	 * @param \Xlnc\XlncTools\Domain\Model\Sheet $sheet
	 */
	public function setSheet($sheet) {
		$this->sheet = $sheet;
	}

	/**
	 * @return \Xlnc\XlncTools\Domain\Model\Sheet $sheet
	 */
	public function getSheet() {
		return $this->sheet;
	}

	/**
	 * @return \Xlnc\XlncTools\Domain\Model\Team
	 */
	public function getTeam()
	{
	    return $this->team;
	}


	/**
	 * @param \Xlnc\XlncTools\Domain\Model\Team $team Value to set
	 */
	public function setTeam($team)
	{
	    $this->team = $team;
	}

    /**
	 * @return string
	 */
	public function getAddress()
	{
	    return $this->address;
	}

	/**
	 * @param \string $address
	 */
	public function setAddress($address)
	{
	    $this->address = $address;
	}

    /**
	 * @return string
	 */
	public function getSheetType()
	{
	    return $this->sheetType;
	}

	/**
	 * @param \string $sheetType
	 */
	public function setSheetType($sheetType)
	{
	    $this->sheetType = $sheetType;
	}

    /**
     * send this invitation
     * needs to be implemented in subclasses
     */
    public function send() {

    }

	public function getSalutationKey() {
		return $this->sheetType === LeaderSheet::class ? "leader" : "default";
	}

	public function getCrdate() {
		return $this->crdate;
	}
}
