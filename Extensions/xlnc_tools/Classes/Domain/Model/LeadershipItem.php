<?php
namespace Xlnc\XlncTools\Domain\Model;

/**
 * LeadershipItem
 */
class LeadershipItem extends \Xlnc\XlncTools\Domain\Model\Item {

	/**
	 * LeadershipItems should be counted for scoring
	 * @var bool
	 */
	protected $countForScoring = TRUE;
}
