<?php
namespace Xlnc\XlncTools\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Frank Fischer <fischer@fh-konzept.de>, fh-konzept GmbH
 *           Malte Muth <muth@fh-konzept.de>, fh-konzept GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Customer
 */
class Customer extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * @var string
	 */
	protected $title = '';

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Customer\Industry
	 * @lazy
	 */
	protected $industry = 0;

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Customer\Employees
	 * @lazy
	 */
	protected $numEmployees = 0;

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Customer\Turnover
	 * @lazy
	 */
	protected $turnover = 0;

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Customer\Internationality
	 * @lazy
	 */
	protected $internationality = 0;

	/**
	 * @var TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Project>
	 * @lazy
	 */
	protected $projects = NULL;

	/**
	* Generic getter/setter, for simple property types like string or integer
	*/
	public function __call($name, $arguments)
    {
        if($name){
            $method = substr($name, 0, 3);
            $attr = lcfirst(substr($name, 3));
            switch($method){
                case 'set':
                    $this->$attr = $arguments[0];
                    break;
                case 'get':
                    if(!array_key_exists($attr, get_object_vars($this)))
                        return;
                    return $this->$attr;
            }
        }
    }

	/**
	 * Returns the projects
	 *
	 * @return TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Project> $projects
	 */
	public function getProjects() {
		return $this->projects;
	}

	/**
	 * Sets the projects
	 *
	 * @param TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Project> $projects
	 * @return void
	 */
	public function setProjects(\Xlnc\XlncTools\Domain\Model\Project $projects) {
		$this->projects = $projects;
	}

}
