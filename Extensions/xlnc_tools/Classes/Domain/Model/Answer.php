<?php
namespace Xlnc\XlncTools\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Frank Fischer <fischer@fh-konzept.de>, fh-konzept GmbH
 *           Malte Muth <muth@fh-konzept.de>, fh-konzept GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Answer
 */
class Answer extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * value
	 *
	 * @var int
	 */
	protected $value = 0;

	/**
	 * textValue
	 *
	 * @var string
	 */
	protected $textValue = '';

	/**
	 * sheet
	 *
	 * @var \Xlnc\XlncTools\Domain\Model\Sheet
	 */
	protected $sheet = NULL;

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\Item
	 */
	protected $item;

	/**
	 * answerIndex
	 *
	 * @var int
	 */
	protected $answerIndex = 0;

	/**
	 * status
	 *
	 * @var int
	 */
	protected $status = 0;

	/**
	* Generic getter/setter, for simple property types like string or integer
	*/
	public function __call($name, $arguments) 
    {
        if($name){
            $method = substr($name, 0, 3);
            $attr = lcfirst(substr($name, 3));
            switch($method){
                case 'set':
                    $this->$attr = $arguments[0];
                    break;
                case 'get':
                    if(!array_key_exists($attr, get_object_vars($this)))
                        return;
                    return $this->$attr;
            }
        }
    }
	
	/**
	 * @return \Xlnc\XlncTools\Domain\Model\Item
	 */
	public function getItem()
	{
	    return $this->item;
	}
	
	/**
	 * @param \Xlnc\XlncTools\Domain\Model\Item $item Value to set
	 */
	public function setItem($item)
	{
	    $this->item = $item;
	}

	/**
	 * Returns the sheet
	 *
	 * @return \Xlnc\XlncTools\Domain\Model\Sheet $sheet
	 */
	public function getSheet() {
		return $this->sheet;
	}

	/**
	 * Sets the sheet
	 *
	 * @param \Xlnc\XlncTools\Domain\Model\Sheet $sheet
	 * @return void
	 */
	public function setSheet(\Xlnc\XlncTools\Domain\Model\Sheet $sheet) {
		$this->sheet = $sheet;
	}


}