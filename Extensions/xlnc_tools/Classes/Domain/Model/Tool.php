<?php
namespace Xlnc\XlncTools\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Frank Fischer <fischer@fh-konzept.de>, fh-konzept GmbH
 *           Malte Muth <muth@fh-konzept.de>, fh-konzept GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Tool
 */
class Tool extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * displayIntro
	 *
	 * @var bool
	 */
	protected $displayIntro = FALSE;

	/**
	 * intro
	 *
	 * @var string
	 */
	protected $intro = '';

	/**
	 * displayOutro
	 *
	 * @var bool
	 */
	protected $displayOutro = FALSE;

	/**
	 * outro
	 *
	 * @var string
	 */
	protected $outro = '';

	/**
	 * itemsPerPage
	 *
	 * @var integer
	 */
	protected $itemsPerPage = 0;

	/**
	 * txExtbaseType
	 *
	 * @var string
	 */
	protected $txExtbaseType = '';

	/**
     * @var TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Item>
     * @lazy
     */
	protected $items = NULL;

	/**
	* Generic getter/setter, for simple property types like string or integer
	*/
	public function __call($name, $arguments)
    {
        if($name){
            $method = substr($name, 0, 3);
            $attr = lcfirst(substr($name, 3));
            switch($method){
                case 'set':
                    $this->$attr = $arguments[0];
                    break;
                case 'get':
                    if(!array_key_exists($attr, get_object_vars($this)))
                        return;
                    return $this->$attr;
            }
        }
    }


	/**
	 * Returns the displayIntro
	 *
	 * @return bool $displayIntro
	 */
	public function getDisplayIntro() {
		return $this->displayIntro;
	}

	/**
	 * Sets the displayIntro
	 *
	 * @param bool $displayIntro
	 * @return void
	 */
	public function setDisplayIntro($displayIntro) {
		$this->displayIntro = $displayIntro;
	}

	/**
	 * Returns the boolean state of displayIntro
	 *
	 * @return bool
	 */
	public function isDisplayIntro() {
		return $this->displayIntro;
	}

	/**
	 * Returns the displayOutro
	 *
	 * @return bool $displayOutro
	 */
	public function getDisplayOutro() {
		return $this->displayOutro;
	}

	/**
	 * Sets the displayOutro
	 *
	 * @param bool $displayOutro
	 * @return void
	 */
	public function setDisplayOutro($displayOutro) {
		$this->displayOutro = $displayOutro;
	}

	/**
	 * Returns the boolean state of displayOutro
	 *
	 * @return bool
	 */
	public function isDisplayOutro() {
		return $this->displayOutro;
	}

	/**
	 * Returns the items
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage $items
	 */
	public function getItems() {
		return $this->items;
	}

	/**
	 * Sets the items
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $items
	 * @return void
	 */
	public function setItems(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $items) {
		$this->items = $items;
	}

	/**
	 * returns the number of required items to finish this tool
	 * @todo is this really the same as "scorable" items?
	 * @return int
	 */
	public function getRequiredItemsNumber() {
		return count($this->getScorableItems());
	}

	/**
	 * returns the items that should be counted when scoring
	 * @return array<\Xlnc\XlncTools\Domain\Model\Item>
	 */
	public function getScorableItems() {
		return array_filter($this->getItems()->toArray(),
			function($item) {
				return $item->getCountForScoring();
			}
		);
	}
}
