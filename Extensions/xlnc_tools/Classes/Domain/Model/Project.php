<?php
namespace Xlnc\XlncTools\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Frank Fischer <fischer@fh-konzept.de>, fh-konzept GmbH
 *           Malte Muth <muth@fh-konzept.de>, fh-konzept GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Project
 */
class Project extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * @var string
	 */
	protected $title = '';

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Project\Reference
	 * @lazy
	 */
	protected $reference = '';

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Project\Profile
	 * @lazy
	 */
	protected $profile;

	/**
     * @var TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Team>
     * @lazy
     */
	protected $teams = NULL;

	/**
     * @var TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Tool>
     * @lazy
     */
	protected $tools = NULL;

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\Customer
	 * @lazy
	 */
	protected $customer;

	/**
	 * @var \TYPO3\CMS\Extbase\Domain\Model\BackendUser
	 * @lazy
	 */
	protected $owner;

	/**
	* Generic getter/setter, for simple property types like string or integer
	*/
	public function __call($name, $arguments)
    {
        if($name){
            $method = substr($name, 0, 3);
            $attr = lcfirst(substr($name, 3));
            switch($method){
                case 'set':
                    $this->$attr = $arguments[0];
                    break;
                case 'get':
                    if(!array_key_exists($attr, get_object_vars($this)))
                        return;
                    return $this->$attr;
            }
        }
    }

	/**
	 * @return \Xlnc\XlncTools\Domain\Model\Customer
	 */
	public function getCustomer()
	{
	    return $this->customer;
	}

	/**
	 * @param \Xlnc\XlncTools\Domain\Model\Customer $customer Value to set
	 */
	public function setCustomer($customer)
	{
	    $this->customer = $customer;
	}

	/**
	 * @return \TYPO3\Extbase\Domain\Model\BackendUser
	 */
	public function getOwner()
	{
	    return $this->owner;
	}

	/**
	 * @param \TYPO3\Extbase\Domain\Model\BackendUser $owner
	 */
	public function setOwner($owner)
	{
	    $this->owner = $owner;
	}

	/**
	 * Returns the teams
	 *
	 * @return TYPO3\CMS\Extbase\Persistence\ObjectStorage $teams
	 */
	public function getTeams() {
		return $this->teams;
	}

	/**
	 * Sets the teams
	 *
	 * @param TYPO3\CMS\Extbase\Persistence\ObjectStorage $teams
	 * @return void
	 */
	public function setTeams(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $teams) {
		$this->teams = $teams;
	}

	/**
	 * Returns the tools
	 *
	 * @return TYPO3\CMS\Extbase\Persistence\ObjectStorage $teams
	 */
	public function getTools() {
		return $this->tools;
	}

	/**
	 * Sets the tools
	 *
	 * @param TYPO3\CMS\Extbase\Persistence\ObjectStorage $tools
	 * @return void
	 */
	public function setTools(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $tools) {
		$this->tools = $tools;
	}

}
