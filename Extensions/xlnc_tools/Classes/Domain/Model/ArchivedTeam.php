<?php
namespace Xlnc\XlncTools\Domain\Model;

use \Xlnc\XlncTools\Utility\EmptyFakeQueryResult;

/**
 * ArchivedTeam model
 */
class ArchivedTeam extends Team {

	public function getInvitations() {
		return new EmptyFakeQueryResult();
	}

	public function getSheets() {
		return new EmptyFakeQueryResult();
	}

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\ProjectWithoutDictionaryRelations
	 */
	protected $project;

}
