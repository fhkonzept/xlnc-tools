<?php
namespace Xlnc\XlncTools\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Frank Fischer <fischer@fh-konzept.de>, fh-konzept GmbH
 *           Malte Muth <muth@fh-konzept.de>, fh-konzept GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Team
 */
class Team extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';

	/**
	 * gender
	 *
	 * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Sheet\Gender
	 * @lazy
	 */
	protected $gender = NULL;

	/**
	 * firstName
	 *
	 * @var string
	 */
	protected $firstName = '';

	/**
	 * lastName
	 *
	 * @var string
	 */
	protected $lastName = '';

	/**
	 * email
	 *
	 * @var string
	 */
	protected $email = '';

	/**
	 * @var bool
	 */
	protected $hidden;

	/**
	 * participantEmails
	 *
	 * @var string
	 */
	protected $participantEmails = '';

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\Project
	 * @lazy
	 */
	protected $project;

	/**
	 * @var TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Sheet>
	 * @lazy
	 */
	protected $sheets;

	/**
	 * @var TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Invitation>
	 * @lazy
	 */
	protected $invitations;

	/**
	 * @var bool
	 */
	protected $hasAutomaticEnd;

	/**
	 * @var DateTime
	 */
	protected $automaticEndAt;

	/**
	 * @var bool
	 */
	protected $isFinished;

	/**
	 * @var bool
	 */
	protected $isArchived;

	/**
	 * @var string
	 */
	protected $mailText;

	/**
	 * @var string
	 */
	protected $reminderText;

	/**
	 * @var string
	 */
	protected $altMailText;

	/**
	 * @var TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\MailText>
     * @lazy
	 */
	protected $mailTexts;

	/**
	 * @var array
	 */
	protected $_sheetsByType;

	/**
	 * @var array
	 */
	protected $_invitationsByType;




	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\SurveyresultRepository
	 * @inject
	 */
	protected $surveyresultRepository;

	/**
	* Generic getter/setter, for simple property types like string or integer
	*/
	public function __call($name, $arguments)
    {
        if($name){
            $method = substr($name, 0, 3);
            $attr = lcfirst(substr($name, 3));
            switch($method){
                case 'set':
                    $this->$attr = $arguments[0];
                    break;
                case 'get':
                    if(!array_key_exists($attr, get_object_vars($this)))
                        return;
                    return $this->$attr;
            }
        }
    }

	/**
	 * @return \Xlnc\XlncTools\Domain\Model\Project
	 */
	public function getProject()
	{
	    return $this->project;
	}

	/**
	 * @param \Xlnc\XlncTools\Domain\Model\Project $project Value to set
	 */
	public function setProject($project)
	{
	    $this->project = $project;
	}

	/**
	 * @return TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Sheet>
	 */
	public function getSheets()
	{
	    return $this->sheets;
	}

	/**
	 * @param TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Sheet> $sheets Value to set
	 */
	public function setSheets($sheets)
	{
	    $this->sheets = $sheets;
	}

	/**
	 * @param \Xlnc\XlncTools\Domain\Model\Sheet $sheet Value to set
	 */
	public function addSheet($sheet)
	{
	    $this->sheets->attach($sheet);
	}

	/**
	 * @return TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Invitation>
	 */
	public function getInvitations()
	{
	    return $this->invitations;
	}

	/**
	 * @param TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Invitation> $invitations
	 */
	public function setInvitations($invitations)
	{
	    $this->invitations = $invitations;
	}

	/**
	 * @param \Xlnc\XlncTools\Domain\Model\Invitation $invitation
	 */
	public function addInvitation($invitation)
	{
	    $this->invitations->attach($invitation);
	}

	/**
	 * @return bool
	 */
	public function getHasAutomaticEnd() {
		return $this->hasAutomaticEnd;
	}

	/**
	 * @param bool $hasAutomaticEnd
	 */
	public function setHasAutomaticEnd($hasAutomaticEnd) {
		$this->hasAutomaticEnd = $hasAutomaticEnd;
	}

	/**
	 * @return bool
	 */
	public function getIsFinished() {
		return $this->isFinished;
	}

	/**
	 * @param bool $isFinished
	 */
	public function setIsFinished($isFinished) {
		$this->isFinished = $isFinished;
	}

	/**
	 * @return bool
	 */
	public function getIsArchived() {
		return $this->isArchived;
	}

	/**
	 * @param bool $isArchived
	 */
	public function setIsArchived($isArchived) {
		$this->isArchived = $isArchived;
	}

	/**
	 * @return DateTime
	 */
	public function getAutomaticEndAt() {
		return $this->automaticEndAt;
	}

	/**
	 * @param DateTime $automaticEndAt
	 */
	public function setAutomaticEndAt(\DateTime $automaticEndAt) {
		$this->automaticEndAt = $automaticEndAt;
	}

	/**
	 * @return string
	 */
	public function getMailText() {
		return $this->mailText;
	}

	/**
	 * @return string
	 */
	public function getAltMailText() {
		return $this->altMailText;
	}

	/**
	 * @param string $mailText
	 */
	public function setMailText($mailText) {
		$this->mailText = $mailText;
	}

	/**
	 * @return string
	 */
	public function getReminderText() {
		return $this->reminderText;
	}

	/**
	 * @param string $reminderText
	 */
	public function setReminderText($reminderText) {
		$this->reminderText = $reminderText;
	}

	/**
	 * @param string $altMailText
	 */
	public function setAltMailText($altMailText) {
		$this->altMailText = $altMailText;
	}

	public function hasLeaderInvitation() {
		return count(
			$this->getLeaderInvitations()
		) > 0;
	}

	public function getLeaderInvitations() {
		return array_filter(
			$this->invitations->toArray(),
			function($invitation) {
				return $invitation->getSheetType() === LeaderSheet::class;
			}
		);
	}

	public function getSheetsByType() {
		if(NULL == $this->_sheetsByType) {
			$this->_sheetsByType = [
				Sheet::class => [],
				LeaderSheet::class => []
			];

			foreach($this->sheets as $sheet) {
				$this->_sheetsByType[get_class($sheet)][] = $sheet;
			};
		}

		return $this->_sheetsByType;
	}

	public function getInvitationsByType() {
		if(NULL == $this->_invitationsByType) {
			$this->_invitationsByType = [
				MailInvitation::class => [
					LeaderSheet::class => [],
					Sheet::class => [],
				],
				CouponInvitation::class => [
					LeaderSheet::class => [],
					Sheet::class => [],
				]
			];

			foreach($this->invitations as $invitation) {
				$this->_invitationsByType[get_class($invitation)][$invitation->getSheetType()][] = $invitation;
			};
		}

		return $this->_invitationsByType;
	}

	public function hasLeaderSheet() {
		return NULL !== $this->getLeaderSheet();
	}

	protected function getLeaderSheet() {
		if(count($this->getSheetsByType()[LeaderSheet::class]) > 0) {
			return end($this->getSheetsByType()[LeaderSheet::class]);
		} else {
			return NULL;
		}

	}

	/**
	 * a team needs at least a leader and 4 other members as participants
	 * @todo ask the tools instead? maybe? in the future?
	 * @return boolean TRUE if condition is met
	 */
	public function hasEnoughParticipants() {
		return count($this->sheets->toArray()) > 4  && $this->hasLeaderSheet();
	}

	/**
	 * return the number of participants
	 * @return int
	 */
	public function getParticipantCount() {
		return $this->sheets->count();
	}

	/**
	 * return the emails of participants, where known
	 * @return array<string>
	 * @deprecated not needed anymore since getMailInvitations and corresponding changes
	 */
	public function getParticipantEmails() {
		$mails = [];

		foreach($this->invitations as $invitation) {
			if(get_class($invitation) === MailInvitation::class) {
				$mails[] = $invitation->getAddress();
			}
		}

		return $mails;
	}

	/**
	 * return the mails invitations
	 * @return array<string>
	 */
	public function getMailInvitations() {
		return array_filter($this->invitations->toArray(), function ($invitation) {
			return get_class($invitation) === MailInvitation::class;
		});
	}

	/**
	 * a team needs at least 1 leader sheet and 4 other sheets finished
	 * @param  Tool    $tool
	 * @return boolean
	 */
	public function hasEnoughFinishedSheets(Tool $tool) {
		return $this->getFinishedSheetCount($tool) > 4 && $this->leaderSheetFinished($tool);
	}

	/**
	 * returns true if the team lead has finished the given tool
	 * @param  Tool   $tool
	 * @return boolean
	 */
	public function leaderSheetFinished(Tool $tool) {
		if(NULL === $this->getLeaderSheet()) {
			return FALSE;
		} else {
			return $this->getLeaderSheet()->getSheetAnswersFinishedFromSurveyresult($tool);
		}

	}

	/**
	 * returns the number of finished sheets for the given tool
	 * @param  Tool   $tool
	 * @return int
	 */
	public function getFinishedSheetCount(Tool $tool) {
		return $this->surveyresultRepository->countFinishedSheetsForTeamAndTool($this, $tool);
	}

	public function lock() {
		if(!$this->isFinished) {
			// locking code goes here;
			$this->isFinished = TRUE;
		}
	}

	public function unlock() {
		if($this->isFinished) {
			// unlocking code goes here;
			$this->isFinished = FALSE;
		}
	}

	public function hasTextInLanguage($type, $language) {
		return $this->getTextInLanguage($type, $language) !== NULL;
	}

	public function getTextInLanguage($type, $language) {
		foreach($this->getMailTexts() as $text) {
			if (
				$text->getTxExtbaseType() === $type
				&& (
					($text->getLanguage() === NULL && (NULL === $language || $language->getUid() === 0) )
				||	($text->getLanguage() !== NULL && NULL !== $language && $text->getLanguage()->getUid() === $language->getUid())
				)
			) {
				return $text;
			}
		}
		return NULL;
	}

	public function getEndsAt() {
		return $this->getAutomaticEndAt();
	}

}
