<?php
namespace Xlnc\XlncTools\Domain\Model;

/**
 * A Project that loads dictionary UIDs instead of the real objects
 */
class ProjectWithoutDictionaryRelations extends Project {

	/**
	 * @var int
	 */
	protected $reference;

	/**
	 * @var int
	 */
	protected $profile;

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\CustomerWithoutDictionaryRelations
	 * @lazy
	 */
	protected $customer;

}
