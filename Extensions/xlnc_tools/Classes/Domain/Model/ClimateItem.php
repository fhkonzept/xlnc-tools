<?php
namespace Xlnc\XlncTools\Domain\Model;

/**
 * ClimateItem
 */
class ClimateItem extends \Xlnc\XlncTools\Domain\Model\Item {

	/**
	 * ClimateItems should be counted for scoring
	 * @var bool
	 */
	protected $countForScoring = TRUE;
}
