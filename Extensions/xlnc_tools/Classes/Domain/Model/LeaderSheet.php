<?php
namespace Xlnc\XlncTools\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Frank Fischer <fischer@fh-konzept.de>, fh-konzept GmbH
 *           Malte Muth <muth@fh-konzept.de>, fh-konzept GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * LeaderSheet
 */
class LeaderSheet extends \Xlnc\XlncTools\Domain\Model\Sheet {

	/**
	 * Returns if the user has stored the required personal data already
	 * @return boolean TRUE if personal data has been given.
	 */
	public function getSheetPersonalDataFinished() {
		return
			( 	parent::getSheetPersonalDataFinished()
			&& 	$this->managementexperience !== NULL
			&&  $this->managementexperience->_loadRealInstance() !== NULL
			&&  $this->managementlevel !== NULL
			&&  $this->managementlevel->_loadRealInstance() !== NULL); 

	}

}
