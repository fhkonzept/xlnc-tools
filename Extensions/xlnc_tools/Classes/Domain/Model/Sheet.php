<?php

namespace Xlnc\XlncTools\Domain\Model;

use Xlnc\XlncTools\Utility\SessionUtility;
use Xlnc\XlncTools\Domain\Model\Surveyresult;

/* * *************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Frank Fischer <fischer@fh-konzept.de>, fh-konzept GmbH
 *           Malte Muth <muth@fh-konzept.de>, fh-konzept GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */

/**
 * Sheet
 */
class Sheet extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * @warning this is a hack to ensure newly created records get stored
     * on a certain pid, which is also hardcoded for now
     * @todo clean this up properly
     * @var integer
     */
    protected $pid = 18;

    /**
     * @var string
     */
    protected $accessKey = '';

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Sheet\Age
     * @lazy
     */
    protected $age = NULL;

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Sheet\Gender
     * @lazy
     */
    protected $gender = NULL;

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Sheet\Education
     * @lazy
     */
    protected $education = NULL;

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Sheet\Graduation
     * @lazy
     */
    protected $graduation = NULL;

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Sheet\Experience
     * @lazy
     */
    protected $experience = NULL;

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Sheet\Managementexperience
     * @lazy
     */
    protected $managementexperience = NULL;

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Sheet\Managementlevel
     * @lazy
     */
    protected $managementlevel = NULL;

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Dictionary\Sheet\Businessunit
     * @lazy
     */
    protected $businessunit = NULL;

    /**
     * @var integer
     */
    protected $country = 0;

    /**
     * @var string
     */
    protected $txExtbaseType = '';

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Team
     * @lazy
     */
    protected $team;

    /**
     * @var TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Answer>
     * @lazy
     */
    protected $answers;

    /**
     * @var Xlnc\XlncTools\Domain\Repository\SurveyresultRepository
     * @inject
     */
    protected $surveyresultRepository;

    /**
     * Generic getter/setter, for simple property types like string or integer
     */
    public function __call($name, $arguments) {
        if ($name && $name != "pid") {
            $method = substr($name, 0, 3);
            $attr = lcfirst(substr($name, 3));
            switch ($method) {
                case 'set':
                    $this->$attr = $arguments[0];
                    break;
                case 'get':
                    if (!array_key_exists($attr, get_object_vars($this)))
                        return;
                    return $this->$attr;
            }
        }
    }

    /**
     * @return \Xlnc\XlncTools\Domain\Model\Team
     */
    public function getTeam() {
        return $this->team;
    }

    /**
     * @param \Xlnc\XlncTools\Domain\Model\Team $team Value to set
     */
    public function setTeam($team) {
        $this->team = $team;
    }

    /**
     * @return TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Answer>
     */
    public function getAnswers() {
        return $this->answers;
    }

    /**
     * @param TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Answer> $answers Value to set
     */
    public function setAnswers($answers) {
        $this->answers = $answers;
    }

    /**
     * @param Xlnc\XlncTools\Domain\Model\Answer $answer Value to set
     */
    public function addAnswer($answer) {
        $this->answers->attach($answer);
    }

    /**
     * Returns if the user has stored the required personal data already
     * @return boolean TRUE if personal data has been given.
     */
    public function getSheetPersonalDataFinished() {
        $requiredProperties = ["age", "gender", "graduation", "businessunit", "experience", "education", "country"];
        foreach ($requiredProperties as $property) {
            $value = $this->$property;  // check if it's NULL right away, or if a LazyLoadingProxy resolves to NULL
            if ($value === NULL || (is_object($value) && method_exists($value, '_loadRealinstance') && $value->_loadRealinstance() === NULL)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns if all answers have been given
     *
     * @warning the obvious optimization (use Surveyresults instead of iterating
     * all answer objects) does not apply here since this method is being used
     * to determine if a Surveyresult needs to be written!
     * @todo document how the assumption of this method is important - specifically
     * the use case where items get added and removed from tools does not change
     * this function's return method
     *
     * @param Xlnc\XlncTools\Domain\Model\Tool $selectedTool
     * @return boolean
     */
    public function getSheetAnswersFinished($selectedTool) {
        $answers = SessionUtility::getToolAnswers($selectedTool, $this);
        if (count($answers) > 0) {
            foreach ($answers as $answer) {
                if ($answer->getStatus() != 2) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Returns if all answers have been given by evaluating Surveyresults. This
     * is a faster version of getSheetAnswersFinished which is non-authoritative
     *
     * @param Xlnc\XlncTools\Domain\Model\Tool $tool
     * @return boolean
     */
    public function getSheetAnswersFinishedFromSurveyresult($tool) {
        $surveyresult = $this->surveyresultRepository->findByToolAndSheet($tool, $this);

        if (FALSE === $surveyresult) {
            return FALSE;
        }

        if ($surveyresult->getStatus() === Surveyresult::STATUS_FINISHED) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Returns number of answers that have been given
     *
     * @param Xlnc\XlncTools\Domain\Model\Tool $selectedTool
     */
    public function getSheetAnswersFinishedNumber($selectedTool) {
        $number = 0;
        $answers = SessionUtility::getToolAnswers($selectedTool, $this);

        $numberTextAnswers = 0;
        if (count($answers) > 0) {
            foreach ($answers as $answer) {
                if ($answer->getStatus() == 2) {
                    if ($answer->getItem()->getTxExtbaseType() != 'Tx_XlncTools_TextItem') {
                        $number++;
                    } else {
                        $numberTextAnswers++;
                    }
                }
            }
        }
        if ($this->getTxExtbaseType() == 'Tx_XlncTools_Sheet') {
            $number = $number / 2 + $numberTextAnswers;
        } else if ($this->getTxExtbaseType() == 'Tx_XlncTools_LeaderSheet') {
            $number = $number / 2;
        }
        return $number;
    }

    /*
     * Returns if a sheet has been started
     *
     * @param Xlnc\XlncTools\Domain\Model\Tool $selectedTool
     */

    public function getSheetAnswersStarted($selectedTool) {
        $answers = SessionUtility::getToolAnswers($selectedTool, $this);
        if (count($answers) > 0) {
            foreach ($answers as $answer) {
                if ($answer->getStatus() > 0) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public function getAge() {
        return $this->age;
    }

    public function setAge($age) {
        $this->age = $age;
    }

}
