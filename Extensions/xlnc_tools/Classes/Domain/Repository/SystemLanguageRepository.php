<?php
namespace Xlnc\XlncTools\Domain\Repository;

/**
 * The repository for the SystemLanguage model
 */
class SystemLanguageRepository extends BaseRepository {

	/**
	 * @var Xlnc\XlncTools\Domain\Model\SystemLanguage
	 */
	static private $german = NULL;

	static public function getGerman() {
		if(is_null(static::$german)) {
			$german = new \Xlnc\XlncTools\Domain\Model\SystemLanguage();
			$german->setUid(0);
			$german->setTitle("Deutsch");
			$german->setIsoCode("de");
			$german->setFlag("de");

			static::$german = $german;
		}
		return static::$german;
	}

	public function findAllAndDefault() {
		$allLanguages = $this->findAll()->toArray();
		return array_merge([static::getGerman()], $allLanguages);
	}

	public function getCurrentLanguage() {
		$languageUid = (int)$GLOBALS["TSFE"]->sys_language_uid;

		if(0 === $languageUid) {
			return self::getGerman();
		} else {
			return $this->findByUid($languageUid);
		}
	}
}
