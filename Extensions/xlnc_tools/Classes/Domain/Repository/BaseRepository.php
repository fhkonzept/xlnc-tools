<?php
namespace Xlnc\XlncTools\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Frank Fischer <fischer@fh-konzept.de>, fh-konzept GmbH
 *           Malte Muth <muth@fh-konzept.de>, fh-konzept GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Projects
 */
class BaseRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	public function initializeObject() {
		$this->defaultQuerySettings = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings::class);
	}

	public function setIgnoreStoragePage() {
		$this->defaultQuerySettings->setRespectStoragePage(FALSE);
		return $this;
	}

	public function setShowHidden() {
		$this->defaultQuerySettings->setIgnoreEnableFields(TRUE);
		$this->defaultQuerySettings->setEnableFieldsToBeIgnored(["disabled"]);
		return $this;
	}

	/**
	 * @param  array<int> $uids an array of object uids
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByUids($uids) {
		$q = $this->createQuery();

		$q->matching(
			$q->in('uid', $uids)
		);

		return $q->execute();
	}

	public function persistAll() {
		$this->persistenceManager->persistAll();
	}

	private function debugQuery($q) {
		$parser = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser::class);
		\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($parser->parseQuery($q));
	}

}
