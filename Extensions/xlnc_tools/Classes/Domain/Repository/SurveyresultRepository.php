<?php
namespace Xlnc\XlncTools\Domain\Repository;

use Xlnc\XlncTools\Domain\Model\Surveyresult;

class SurveyresultRepository extends BaseRepository {

	/**
	 * returns the current Surveyresult Model for the given Tool and Sheet
	 * @param  \Xlnc\XlncTools\Domain\Model\Tool $tool
	 * @param  \Xlnc\XlncTools\Domain\Model\Sheet $sheet
	 * @return \Xlnc\XlncTools\Domain\Model\Surveyresult|boolean  FALSE if no Surveyresult could be found
	 */
	public function findByToolAndSheet($tool, $sheet) {
		$query = $this->createQuery();
		return $query->matching(
			$query->logicalAnd(
				array(
					$query->equals('tool', $tool),
					$query->equals('sheet', $sheet)
					)
				)
		)->execute()->current();
	}

	public function countFinishedSheetsForTeamAndTool($team, $tool) {
		$this->setIgnoreStoragePage();
		$query = $this->createQuery();
		$result = $query->matching(
		   $query->logicalAnd(
			   array(
				   $query->equals('tool', $tool),
				   $query->equals('team', $team),
				   $query->equals('status', Surveyresult::STATUS_FINISHED)
				   )
			   )
		   )->execute();
		return $result->count();
	}

}
