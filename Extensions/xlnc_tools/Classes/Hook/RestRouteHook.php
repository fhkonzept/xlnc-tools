<?php
namespace Xlnc\XlncTools\Hook;

/**
 * @author  Malte Muth <muth@fh-konzept.de>
 */
class RestRouteHook {

	/**
	 * intercept calls to /api and route them to a magic page type
	 */
	public static function hookUrl($_, $frontendController) {
		$url = $frontendController->siteScript;
		if(static::isApiCall($url)) {
			// @todo replace these magic numbers
			$frontendController->id = 43;
			$frontendController->type = 9001;
		}

		if(strpos($url, "api/debug/") === 0) {
			$frontendController->mergingWithGetVars(["debugWithHtml" => 1]);
		}

	}

	/**
	 * check for api calls and if given, prevent realURL from doing anything
	 */
	public function grabUrl(&$_, $__) {
		if(static::isApiCall($_['URL'])) {
			$_['URL'] = "/";
		}
	}

	protected static function isApiCall($url) {
		return 0 === strpos($url, "api/");
	}

}
