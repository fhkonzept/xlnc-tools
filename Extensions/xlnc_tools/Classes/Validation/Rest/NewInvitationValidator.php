<?php
namespace Xlnc\XlncTools\Validation\Rest;

use Xlnc\XlncTools\Domain\Model\LeaderSheet;
use Xlnc\XlncTools\Domain\Model\Sheet;
use Xlnc\XlncTools\Domain\Model\MailInvitation;
use Xlnc\XlncTools\Domain\Model\CouponInvitation;

class NewInvitationValidator extends \Xlnc\XlncTools\Validation\BaseValidator {

    const UNSUPPORTED_SHEET_INVITATION_COMBINATION = "unsupported_sheet_invitation_combination";
    const LEADER_ALREADY_INVITED_BY_MAIL = "leader_already_invited_by_mail";
    const MAIL_ALREADY_INVITED = "mail_already_invited";
    const LEADER_ALREADY_INVITED_BY_COUPON = "leader_already_invited_by_coupon";
    const MISSING_TEXT = "missing_text";

    private $validatorFunctionMap = [
        LeaderSheet::class => [
            MailInvitation::class => "validateLeaderMailInvitation",
            CouponInvitation::class => "validateLeaderCouponInvitation",
        ],
        Sheet::class => [
            MailInvitation::class => "validateMailInvitation",
            CouponInvitation::class => "validateCouponInvitation",
        ]
    ];

    private $invitationTextMap = [
        LeaderSheet::class => 'Tx_XlncTools_LeaderInvitationText',
        Sheet::class => 'Tx_XlncTools_InvitationText'
    ];

    public function isValid($invitation) {
        $this->procceed = parent::isValid($invitation);

        $this->isValidRequiredObject("team");
        if(!$this->proceed) {
            // no team? get out.
            return FALSE;
        }

        $this->isValidRequiredStringField("sheetType");
        if(!$this->proceed) {
            // no sheetType? get out.
            return FALSE;
        }

        if(!$invitation->getTeam()->hasTextInLanguage($this->invitationTextMap[$invitation->getSheetType()], $invitation->getLanguage())) {
            $this->pushError("language", self::MISSING_TEXT);
        }

        // now, decend into the specific cases
        $validatorFunction = $this->validatorFunctionMap[$invitation->getSheetType()][get_class($invitation)];

        if("" == $validatorFunction) {
            $this->pushError("sheetType", self::UNSUPPORTED_SHEET_INVITATION_COMBINATION);
        } else {
            $this->$validatorFunction();
        }

        return $this->proceed;
    }

    private function validateCouponInvitation() {
        // coupon invitations can be always be created
    }

    private function validateMailInvitation() {
        $this->isValidEmail("address");
        if(!$this->proceed) return;
        // mail invitations can be created as long as the email has not been used yet.
        $currentInvitations = $this->value->getTeam()->getInvitations();
        $currentDefaultInvitations = array_filter(
            $currentInvitations->toArray(),
            function($invitation) {
                return $invitation->getSheetType() === Sheet::class;
            }
        );
        $currentInvitationAddresses = array_map(function($invitation) {
            return $invitation->getAddress();
        }, $currentDefaultInvitations);
        if(in_array($this->value->getAddress(), $currentInvitationAddresses)) {
            $this->pushError("sheetType", self::MAIL_ALREADY_INVITED);
        };
    }

    /**
     * @todo deduplicate code between here and below
     */
    private function validateLeaderMailInvitation() {
        $this->isValidEmail("address");
        if(!$this->proceed) return;
        // leader mail invitations can be created as long as there's not already one.
        $currentInvitations = $this->value->getTeam()->getInvitations();
        $currentLeaderInvitations = array_filter(
            $currentInvitations->toArray(),
            function($invitation) {
                return $invitation->getSheetType() === LeaderSheet::class;
            }
        );
        $leaderMailInvitation = array_filter(
            $currentLeaderInvitations,
            function($invitation) {
                return get_class($invitation) === MailInvitation::class;
            }
        );
        if(count($leaderMailInvitation) > 0) {
            $this->pushError("sheetType", self::LEADER_ALREADY_INVITED_BY_MAIL);
        };
    }

    private function validateLeaderCouponInvitation() {
        // leader mail invitations can be created as long as there's not already one.
        $currentInvitations = $this->value->getTeam()->getInvitations();
        $currentLeaderInvitations = array_filter(
            $currentInvitations->toArray(),
            function($invitation) {
                return $invitation->getSheetType() === LeaderSheet::class;
            }
        );
        $leaderCouponInvitation = array_filter(
            $currentLeaderInvitations,
            function($invitation) {
                return get_class($invitation) === CouponInvitation::class;
            }
        );
        if(count($leaderCouponInvitation) > 0) {
            $this->pushError("sheetType", self::LEADER_ALREADY_INVITED_BY_COUPON);
        };
    }

}
