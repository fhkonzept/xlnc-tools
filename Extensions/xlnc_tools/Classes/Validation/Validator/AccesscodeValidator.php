<?php
namespace Xlnc\XlncTools\Validation\Validator;

class AccesscodeValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator {

	/**
	 * sheetRepository
	 *
	 * @var \Xlnc\XlncTools\Domain\Repository\SheetRepository
	 * @inject
	 */
	protected $sheetRepository = NULL;

	public function validate($value) {
		$this->result = new \TYPO3\CMS\Extbase\Error\Result();
		$this->isValid($value);
		return $this->result;
	}

	/**
	 * Checks if given access key is valid
	 *
	 * @param mixed $value The value (access key) that should be validated
	 * @return void
	 */
	public function isValid($value) {
		// Access key must not be empty
		if ($value === '') {
			$this->addError(
				$this->translateErrorMessage(
					'validator.accesscode.empty',
					'xlnc_tools'
				), 1221560718);
		}
		if(!$this->result->hasErrors()) {
			// Try to get sheets according to access key
			$sheets = $this->sheetRepository->setIgnoreStoragePage()->findByAccessKey($value);
			// Add error if no sheets found
			if(!is_object($sheets) || $sheets->count() < 1) {
				$this->addError(
				$this->translateErrorMessage(
					'validator.accesscode.notvalid',
					'xlnc_tools'
				), 1221560719);
			}
		}
	}
}

?>