<?php
namespace Xlnc\XlncTools\Validation\Validator;

use Xlnc\XlncTools\Utility\SessionUtility;

class SheetAnswersValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator {

	/**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     * @inject
     */
    protected $objectManager;

    const MISSING_ANSWERS = 1201447101;

	public function validate($value) {
		$this->result = new \TYPO3\CMS\Extbase\Error\Result();
		$this->isValid($value);
		return $this->result;
	}

	/**
	 * Checks if given access key is valid
	 *
	 * @param mixed $sheet
	 * @return void
	 */
	public function isValid($sheet) {

		$hasErrors = FALSE;

		if(empty(SessionUtility::getCurrentToolAnswers())) {
			$hasErrors = TRUE;
		} else {
			foreach(SessionUtility::getCurrentToolAnswers() as $answer) {
				// Don't validate text answer items
				if($answer->getItem()->getTxExtbaseType() != 'Tx_XlncTools_TextItem' && $answer->getStatus() == 1 && $answer->getValue() == 0) {
					$hasErrors = TRUE;
				}
			}
		}

		// General error flashmessage
		if($hasErrors === TRUE) {
			$this->addError(
				$this->translateErrorMessage(
					'validator.sheet.missing_answers',
					'xlnc_tools'
				), self::MISSING_ANSWERS);
		}
	}
}

?>