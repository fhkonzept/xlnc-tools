<?php
namespace Xlnc\XlncTools\Validation\Validator;

class SheetValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator {

	/**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     * @inject
     */
    protected $objectManager;

    const VALUE_EMPTY = 1201447100;

	public function validate($value) {
		$this->result = new \TYPO3\CMS\Extbase\Error\Result();
		$this->isValid($value);
		return $this->result;
	}

	/**
	 * Checks if given access key is valid
	 *
	 * @param mixed $sheet The sheet
	 * @return void
	 */
	public function isValid($sheet) {

		$hasErrors = FALSE;

		// Validate age
		if ($this->notGiven($sheet, "age")) {

			$hasErrors = TRUE;

			$error = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Validation\\Error', $this->translateErrorMessage(
					'validator.sheet.age.empty',
					'xlnc_tools'
				), self::VALUE_EMPTY);
			$this->result->forProperty('age')->addError($error);
		}

		// Validate gender
		if ($this->notGiven($sheet, "gender")) {

			$hasErrors = TRUE;

			$error = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Validation\\Error', $this->translateErrorMessage(
					'validator.sheet.gender.empty',
					'xlnc_tools'
				), self::VALUE_EMPTY);
			$this->result->forProperty('gender')->addError($error);
		}

		// Validate graduation
		if ($this->notGiven($sheet, "graduation")) {

			$hasErrors = TRUE;

			$error = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Validation\\Error', $this->translateErrorMessage(
					'validator.sheet.graduation.empty',
					'xlnc_tools'
				), self::VALUE_EMPTY);
			$this->result->forProperty('graduation')->addError($error);
		}

		// Validate education
		if ($this->notGiven($sheet, "education")) {

			$hasErrors = TRUE;

			$error = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Validation\\Error', $this->translateErrorMessage(
					'validator.sheet.education.empty',
					'xlnc_tools'
				), self::VALUE_EMPTY);
			$this->result->forProperty('education')->addError($error);
		}

		// Validate experience
		if ($this->notGiven($sheet, "experience")) {

			$hasErrors = TRUE;

			$error = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Validation\\Error', $this->translateErrorMessage(
					'validator.sheet.experience.empty',
					'xlnc_tools'
				), self::VALUE_EMPTY);
			$this->result->forProperty('experience')->addError($error);
		}

		// Validate managementexperience
		if ($sheet->getTxExtbaseType() == 'Tx_XlncTools_LeaderSheet' && $this->notGiven($sheet, "managementexperience")) {

			$hasErrors = TRUE;

			$error = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Validation\\Error', $this->translateErrorMessage(
					'validator.sheet.managementexperience.empty',
					'xlnc_tools'
				), self::VALUE_EMPTY);
			$this->result->forProperty('managementexperience')->addError($error);
		}

		// Validate managementlevel
		if ($sheet->getTxExtbaseType() == 'Tx_XlncTools_LeaderSheet' && $this->notGiven($sheet, "managementlevel")) {

			$hasErrors = TRUE;

			$error = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Validation\\Error', $this->translateErrorMessage(
					'validator.sheet.managementlevel.empty',
					'xlnc_tools'
				), self::VALUE_EMPTY);
			$this->result->forProperty('managementlevel')->addError($error);
		}

		// Validate businessunit
		if ($this->notGiven($sheet, "businessunit")) {

			$hasErrors = TRUE;

			$error = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Validation\\Error', $this->translateErrorMessage(
					'validator.sheet.businessunit.empty',
					'xlnc_tools'
				), self::VALUE_EMPTY);
			$this->result->forProperty('businessunit')->addError($error);
		}

		// Validate country
		if ($this->notGiven($sheet, "country")) {

			$hasErrors = TRUE;

			$error = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Validation\\Error', $this->translateErrorMessage(
					'validator.sheet.country.empty',
					'xlnc_tools'
				), self::VALUE_EMPTY);
			$this->result->forProperty('country')->addError($error);
		}

		// General error flashmessage
		if($hasErrors === TRUE) {
			$this->addError(
				$this->translateErrorMessage(
					'validator.sheet.empty',
					'xlnc_tools'
				), self::VALUE_EMPTY);
		}
	}

    private function notGiven($object, $propertyName) {
        $getterFunction = "get" . ucfirst($propertyName);
        $value = $object->$getterFunction();
        return ($value === 0 || $value === '' || $value === NULL);
    }
}

?>
