<?php
namespace Xlnc\XlncTools\Validation;

/**
 * validates a registration mocdel
 *
 * @author Malte Muth <muth@fh-konzept.de>
 */
class BaseValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator {

	/**
	 * error codes - used in locallang.xlf
	 */
	const REQUIRED = "required";
	const NOT_AN_EMAIL = "not_an_email";
	const NOT_A_ZIP = "not_a_zip";
	const INVALID_VALUE = "invalid_value";
	const INVALID_DATE_FORMAT = "invalid_date_format";
	const AT_LEAST_18_YEARS_OLD = "at_least_18_years_old";
	const BETWEEN_1_AND_50 = "between_1_and_50";
	const AT_LEAST_1 = "at_least_1";
	const AT_MOST_50 = "at_most_50";

	/**
	 * return value if isValid
	 * @var boolean
	 */
	protected $proceed = TRUE;

	/**
	 * @var mixed
	 */
	protected $value = NULL;


    /**
     * holds the error code key part for this class
     * @var string
     */
    protected $classKey = NULL;

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
	 * @inject
	 */
	protected $objectManager;

	/**
	 * Validate the given registration
	 *
	 * @param mixed $value
	 * @return boolean
	 */
	public function isValid($value) {

		$this->value = $value;

        // take the FQCN of the validator, discard vendor, package and
        // validation namespace, replace \ with _ and transform to lowercase
        $this->classKey = strtolower(
            implode("_", array_slice(
                explode("\\", get_class($this))
                    , 3
                )
            )
        );

		return $this->proceed;
	}

	protected function isValidRequiredStringField($property) {
		$this->trimStringField($property);
		if( $this->getProperty($property) === "") {
			$this->addError($property, self::REQUIRED);
		} else {

		}
	}

    protected function isValidEmail($property) {
        $filteredEmail = filter_var($this->getProperty($property), FILTER_VALIDATE_EMAIL);
        if(FALSE === $filteredEmail) {
            $this->addError($property, self::NOT_AN_EMAIL);
        } else {
            $this->setProperty($property, $filteredEmail);
        }
    }

    protected function isValidRequiredObject($property) {
        if(NULL == $this->getProperty($property)) {
            $this->addError($property, self::REQUIRED);
        }
    }

	protected function isValidZip($property) {
		if ( !preg_match("/^[0-9]{5}$/", $this->getProperty($property)) ) {
			$this->addError($property, self::NOT_A_ZIP);
		}
	}

	protected function trimStringField($property) {
		$this->setProperty($property,
			trim($this->getProperty($property))
			);
	}

	/**
	 * adds an error object to the validation result with an auto-translated message
	 * @param string $property the property to name to error on
	 * @param string $code the error code to use
	 * @param boolean $appendError when errors exist, only append another error when this is true
	 */
	protected function pushError($property, $code, $appendError = FALSE) {
		// if appendError is given and we already have errors, bail out.
		if( !$appendError && count($this->result->forProperty($property)->getErrors()) ) return;



		$messageKey = "error." . $this->classKey . "." . $property . "." . $code;
		$titleKey = "error.title." . $this->classKey;
		$message = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($messageKey, "xlnc_tools");
		$title = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($titleKey, "xlnc_tools");
		// if no translation is given, set the error key as message instead.
		if( trim($message) === "") {
			$message = $messageKey;
		}

        $error = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Validation\\Error', $message, $code, [], $title);
		$this->result->forProperty($property)->addError($error);
        $this->proceed = FALSE;
	}

	/**
	 * gets $property
	 * @param  string $property
	 * @return mixed
	 */
	protected function getProperty($property) {
		$getter = "get" . ucfirst($property);
		return $this->value->$getter();
	}

	/**
	 * sets $property to $value
	 * @param string $property
	 * @param mixed $value
	 * @return mixed
	 */
	protected function setProperty($property, $value) {
		$setter = "set" . ucfirst($property);
		return $this->value->$setter($value);
	}

}
