<?php
namespace Xlnc\XlncTools\Report\Normalization;

abstract class Normalization {

    /**
     * @var array
     */
    protected $configuration = NULL;

    public function __construct($configuration = NULL) {
        if(static::isValidConfiguration($configuration))
            $this->configuration = static::readConfiguration($configuration);
    }

    public static function isValidConfiguration($configuration) {
        return false;
    }

    public static function readConfiguration($configuration) {
        return $configuration;
    }

    public function normalizeValue($totalScore, $maxScore) {

    }
}
