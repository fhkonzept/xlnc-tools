<?php
namespace Xlnc\XlncTools\Report\Normalization;

use \Xlnc\XlncTools\Domain\Model\Dictionary\Item\Criteria;

class NormalizationFactory {
    /**
     * @param  \Xlnc\XlncTools\Domain\Model\Dictionary\Item\Criteria $criteria The criteria to read the normalization configuration from
     * @return  \Xlnc\XlncTools\Report\Normalization\Normalization             a matching PercentageRankNormalization or IdentityNormalization
     */
    public static function buildForItemCriteria($criteria) {
        $rawConfiguration = $criteria->getNormalization();

        if(PercentageRankNormalization::isValidConfiguration($rawConfiguration)) {
            return new PercentageRankNormalization($rawConfiguration);
        } else {
            return new LinearPercentageRankNormalization($criteria);
        }
    }
}
