<?php
namespace Xlnc\XlncTools\Report\Normalization;

class PercentageRankNormalization extends Normalization {

    public static function isValidConfiguration($configuration) {
        $lines = self::readLines($configuration);
        // we need at least one line
        if (count($lines) < 1) return false;

        // all lines need to have to exactly 2 floats in them
        foreach($lines as $line) {
            if (count($line) != 2) return false;
            if ( !is_float($line[0]) || !is_float($line[1]) ) return false;
        }

        // otherwise, we're happy
        return true;
    }

    public static function readConfiguration($configuration) {
        $lines = self::readLines($configuration);

        // read the maximum Rank
        $maxRank = 0.0;
        foreach ($lines as $line) {
            if($line[0] > $maxRank) {
                $maxRank = $line[0];
            }
        }

        return [
            "lines" => $lines,
            "maxRank" => $maxRank
        ];
    }

    protected static function readLines($rawConfiguration) {
        $lines = array_map( function ($line) {
            return preg_split("/(\s)+/", trim($line));
        }, explode("\n", $rawConfiguration));
        $lines = array_map( function ($line) {
            return array_map( function ($item) {
                return floatval(str_replace(",", ".", $item));
            }, $line);
        }, $lines);

        return $lines;
    }

    public function normalizeValue($totalScore, $maxScore) {
        // convert inputs to a value compatible with the ranks
        $convertibleValue = $totalScore * $this->configuration["maxRank"] / $maxScore;

        // look up the maximum lower key
        $maxLowerKey = 0;
        $value = 0;
        foreach($this->configuration["lines"] as $rank) {
            // @TODO maybe rank sorting can speed this up somehow?
            if($maxLowerKey < $rank[0] && $convertibleValue >= $rank[0]) {
                $maxLowerKey = $rank[0];
                $value = $rank[1];
            }
        }

        return $value;
    }
}
