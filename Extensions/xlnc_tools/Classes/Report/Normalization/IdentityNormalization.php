<?php
namespace Xlnc\XlncTools\Report\Normalization;

class IdentityNormalization extends Normalization {

    public static function isValidConfiguration($configuration) {
        return true;
    }

    public static function readConfiguration($configuration) {
        return $configuration;
    }

    public function normalizeValue($totalScore, $maxScore) {
        return $totalScore;
    }
}
