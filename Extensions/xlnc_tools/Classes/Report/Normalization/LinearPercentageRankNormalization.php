<?php
namespace Xlnc\XlncTools\Report\Normalization;

class LinearPercentageRankNormalization extends Normalization {

    public static function isValidConfiguration($criteria) {
        return true;
    }

    public static function readConfiguration($criteria) {
        return [];
    }

    public function normalizeValue($totalScore, $maxScore) {
        return (int) (100 * ( (float) $totalScore / $maxScore ) );
    }
}
