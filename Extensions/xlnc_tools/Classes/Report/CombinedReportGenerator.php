<?php
namespace Xlnc\XlncTools\Report;

use \Xlnc\XlncTools\Domain\Model\Team,
    \Xlnc\XlncTools\Domain\Model\ClimateTool,
    \Xlnc\XlncTools\Domain\Model\LeadershipTool,
    \Xlnc\XlncTools\View\Report\CombinedReportPdfView,
    \Xlnc\XlncTools\Utility\CombinedReportTextUtility,
    \Xlnc\XlncTools\Utility\ManagementStylesTextUtility;

class CombinedReportGenerator {

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;

    /**
     * @var array<\Xlnc\XlncTools\Domain\Model\Team>
     */
    protected $teams;

    /**
     * @param array<\Xlnc\XlncTools\Domain\Model\Team> $teams
     */
    public function __construct($teams) {
        $this->teams = $teams;
    }

    public function getFilePath() {
        return $this->sanitizeFilepath($this->getReport());
    }

	private function getReport() {

		// Get new report
		$report = $this->objectManager->get(\Xlnc\XlncTools\Report\CombinedReportCalculator::class, $this->teams);

        $view = $this->objectManager->get(CombinedReportPdfView::class);
        $view->setReport($report);

        $managementAverages = $report->getAverageResultForTool($view->getManagementStylesTool());
        $managementDistribution = $report->getCriteriaLevelPercentagesForTool($view->getManagementStylesTool());

        $climateAverages = $report->getAverageResultForTool($view->getTeamClimateTool());
        $climateSecondAverages = $report->getAverageSecondResultForTool($view->getTeamClimateTool());
        $climateDistribution = $report->getCriteriaLevelPercentagesForTool($view->getTeamClimateTool());

		// Dynamic Texts
		$dynamicTexts = array(
		    'chapters'	=> array(

				7 => array(
	                'signature'     => CombinedReportTextUtility::getManagementCriteriaText('management', 'signature', $managementAverages),
                    'potential'     => CombinedReportTextUtility::getManagementCriteriaText('management', 'potential', $managementAverages),
				    'development'   => CombinedReportTextUtility::getManagementCriteriaText('management', 'development', $managementAverages),
				),
                8 => array(
                    'homogenous'                => CombinedReportTextUtility::getHomogenityText(TRUE, 'management', $managementDistribution),
                    'homogenous.signature'      => CombinedReportTextUtility::getHomogenityText(TRUE, 'management', $managementDistribution, 'signature'),
                    'homogenous.development'    => CombinedReportTextUtility::getHomogenityText(TRUE, 'management', $managementDistribution, 'development'),
                    'heterogenous'              => CombinedReportTextUtility::getHomogenityText(FALSE, 'management', $managementDistribution),
                ),
                9 => array(
                    'signature'     => CombinedReportTextUtility::getClimateCriteriaText('climate', 'signature', $climateAverages),
                    'potential'     => CombinedReportTextUtility::getClimateCriteriaText('climate', 'potential', $climateAverages),
                    'development'   => CombinedReportTextUtility::getClimateCriteriaText('climate', 'development', $climateAverages),

                    'should'            => CombinedReportTextUtility::getOverallShouldText($climateSecondAverages, 40, 70, 'employees'),
                    'highestShould'     => CombinedReportTextUtility::getHighestShouldText($climateSecondAverages, 'employees'),
                    'highestDifference' => CombinedReportTextUtility::getHighestShouldDifferenceText($climateAverages, $climateSecondAverages, 'employees'),
                    'lowestDifference'  => CombinedReportTextUtility::getLowestShouldDifferenceText($climateAverages, $climateSecondAverages, 'employees'),
                ),
                10 => array(
                    'homogenous'                => CombinedReportTextUtility::getHomogenityText(TRUE, 'climate', $climateDistribution),
                    'homogenous.signature'      => CombinedReportTextUtility::getHomogenityText(TRUE, 'climate', $climateDistribution, 'signature'),
                    'homogenous.development'    => CombinedReportTextUtility::getHomogenityText(TRUE, 'climate', $climateDistribution, 'development'),
                    'heterogenous'              => CombinedReportTextUtility::getHomogenityText(FALSE, 'climate', $climateDistribution),
                ),
                11 => array(
                    'autonomy'          => CombinedReportTextUtility::getClimateValue($climateAverages, "autonomy"),
                    'credibility'       => CombinedReportTextUtility::getClimateValue($climateAverages, "credibility"),
                    'safety'            => CombinedReportTextUtility::getClimateValue($climateAverages, "safety"),
                    'development'       => CombinedReportTextUtility::getClimateValue($climateAverages, "development"),
                    'clearness'         => CombinedReportTextUtility::getClimateValue($climateAverages, "clearness"),
                    'flexibility'       => CombinedReportTextUtility::getClimateValue($climateAverages, "flexibility"),
                    'recognition'       => CombinedReportTextUtility::getClimateValue($climateAverages, "recognition"),
                    'identification'    => CombinedReportTextUtility::getClimateValue($climateAverages, "identification"),
                    'teamspirit'        => CombinedReportTextUtility::getClimateValue($climateAverages, "teamspirit"),
                )
            )
		);

        $view->setDynamicTexts($dynamicTexts);

        foreach($view->getCriteriaUidsForTool($view->getManagementStylesTool()) as $criteriaUid) {
            $view->setPercentage("managementAverages", $criteriaUid, $managementAverages[$criteriaUid][ToolReportCalculator::PERCENTAGE]);
            $view->setPercentage("managementDistribution", $criteriaUid, $managementDistribution[$criteriaUid]);
        }

        foreach($view->getCriteriaUidsForTool($view->getTeamClimateTool()) as $criteriaUid) {
            $view->setPercentage("employees", $criteriaUid, $climateAverages[$criteriaUid][ToolReportCalculator::PERCENTAGE]);
            $view->setPercentage("employeesSecond", $criteriaUid, $climateSecondAverages[$criteriaUid][ToolReportCalculator::PERCENTAGE]);
            $view->setPercentage("climateDistribution", $criteriaUid, $climateDistribution[$criteriaUid]);
        }

		return $view->render();

	}

	private function sanitizeFilepath($filepath) {
		return trim( preg_replace( '/<!--(.|\s)*?-->/' , '' , htmlspecialchars_decode( strip_tags( $filepath ) ) ) );
	}
}
