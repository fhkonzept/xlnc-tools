<?php
namespace Xlnc\XlncTools\Report;

use \Xlnc\XlncTools\Domain\Model\LeaderSheet;
use \Xlnc\XlncTools\Domain\Model\Sheet;
use \Xlnc\XlncTools\Domain\Model\Surveyresult;

use \Xlnc\XlncTools\Utility\StatisticsUtility as Stats;


/**
 * calculator class for generating the report for a given tool and team
 * @package Xlnc\XlncTools
 * @author Malte Muth <muth@fh-konzept.de>
 */
class ToolReportCalculator {

    const SHEETTYPE_LEADER  = "leader";
    const SHEETTYPE_MEMBER  = "member";

    const FIRST_ANSWER      = "answer";
    const SECOND_ANSWER     = "secondAnswer";

    const RANKING                   = "ranking";
    const RANKING_FINE              = "fineRanking";
    const PERCENTAGE                = "percentage";

    const RANKING_LOW       = "low";
    const RANKING_MEDIUM    = "medium";
    const RANKING_HIGH      = "high";

    const RANKING_FINE_LOW                   = "low";
    const RANKING_FINE_LOW_TO_MEDIUM         = "low_to_medium";
    const RANKING_FINE_MEDIUM                = "medium";
    const RANKING_FINE_MEDIUM_TO_HIGH        = "medium_to_high";
    const RANKING_FINE_HIGH                  = "high";

    const RANKING_DEVELOPMENT   = "development";
    const RANKING_POTENTIAL     = "potential";
    const RANKING_SIGNATURE     = "signature";

    const INVITED           = "invited";
    const FINISHED          = "finished";

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Team
     */
    protected $team;

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Tool
     */
    protected $tool;

    /**
     * raw data from the database
     * memo cache for getAnswerValues
     * @see loadAnswerValues
     * @var array
     */
    protected $_answerValues;

    /**
     * sorted, filtered and transformed data from the database
     * memo cache for getFilteredAnswerValues
     * @see loadAnswerValues
     * @var array
     */
    protected $_filteredValues;

    /**
     * general memoCache for use in function calls
     * @var array<mixed>
     */
    protected $_memoCache = [];

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     * @inject
     */
    protected $objectManager;

    /**
     * @var \Xlnc\XlncTools\Report\ResultContainer
     */
    protected $result;

    /**
     * @var \Xlnc\XlncTools\Report\ResultContainer
     */
    protected $selfResult;

    /**
     * @var \Xlnc\XlncTools\Report\ResultContainer
     */
    protected $secondResult;

    /**
     * @var \Xlnc\XlncTools\Report\ResultContainer
     */
    protected $secondSelfResult;

    /**
     * @var \Xlnc\XlncTools\Domain\Repository\ItemRepository
     * @inject
     */
    protected $itemRepository;

    /**
     * lazy loading accessor for $result
     * @return \Xlnc\XlncTools\Report\ResultContainer
     */
    public function getResult() {
        if(NULL == $this->result)
            $this->result = $this->objectManager->get(ResultContainer::class,
                $this->getFilteredAnswerValues()[self::SHEETTYPE_MEMBER][self::FIRST_ANSWER],
                $this
            );

        return $this->result;
    }

    /**
     * lazy loading accessor for $secondResult
     * @return \Xlnc\XlncTools\Report\ResultContainer
     */
    public function getSecondResult() {
        if(NULL == $this->secondResult)
            $this->secondResult = $this->objectManager->get(ResultContainer::class,
                $this->getFilteredAnswerValues()[self::SHEETTYPE_MEMBER][self::SECOND_ANSWER],
                $this
            );

        return $this->secondResult;
    }

    /**
     * lazy loading accessor for $selfResult
     * @return \Xlnc\XlncTools\Report\ResultContainer
     */
    public function getSelfResult() {
        if(NULL == $this->selfResult)
            $this->selfResult = $this->objectManager->get(ResultContainer::class,
                $this->getFilteredAnswerValues()[self::SHEETTYPE_LEADER][self::FIRST_ANSWER],
                $this
            );

        return $this->selfResult;
    }

    /**
     * lazy loading accessor for $secondSelfResult
     * @return \Xlnc\XlncTools\Report\ResultContainer
     */
    public function getSecondSelfResult() {
        if(NULL == $this->secondSelfResult)
            $this->secondSelfResult = $this->objectManager->get(ResultContainer::class,
                $this->getFilteredAnswerValues()[self::SHEETTYPE_LEADER][self::SECOND_ANSWER],
                $this
            );

        return $this->secondSelfResult;
    }

    /**
     * @param \Xlnc\XlncTools\Domain\Model\Team $team
     * @param \Xlnc\XlncTools\Domain\Model\Team $tool
     */
    public function __construct($team, $tool) {
        $this->team = $team;
        $this->tool = $tool;
    }

    /**
     * returns data for the sample size table as array
     * with first-level keys "leader" or "member"
     * and second-level keys "invited" or "finished"
     * e.g. ["member"]["finished"] for the number of finished team member sheets
     * @return array a 2-dimensional array
     */
    public function getSampleSize() {
        return [
            self::SHEETTYPE_LEADER => [
                self::INVITED => $this->getTotalSheetCountByType()[self::SHEETTYPE_LEADER],
                self::FINISHED => $this->getFinishedSheetCountByType()[self::SHEETTYPE_LEADER]
            ],
            self::SHEETTYPE_MEMBER => [
                self::INVITED => $this->getTotalSheetCountByType()[self::SHEETTYPE_MEMBER],
                self::FINISHED => $this->getFinishedSheetCountByType()[self::SHEETTYPE_MEMBER]
            ]
        ];
    }

    /**
     * returns the criteriaUids in this result, converted to strings
     * @return array<string>
     */
    public function getCriteriaUids() {
        if(!isset($this->_memoCache["getCriteriaUids"])) {
            $returnValue = [];
            foreach($this->tool->getItems()->toArray() as $item) {
                $stringUid = is_object($item->getCriteria()) ? (string)$item->getCriteria()->getUid() : '0';
                if(!in_array($stringUid, $returnValue)) $returnValue[] = $stringUid;
            }
            $this->_memoCache["getCriteriaUids"] = $returnValue;
        }

        return $this->_memoCache["getCriteriaUids"];
    }

    /**
     * returns the itemUids in this result, converted to strings
     * @return array<string>
     */
    public function getItemUids() {
        return (
            array_map(function ($item) {
                return (string)$item->getUid();
            }, $this->tool->getScorableItems())
        );
    }

    /**
     * returns the items relevant to this report
     * @return array<\Xlnc\XlncTools\Domain\Model\Item>
     */
    public function getItems() {
        return $this->tool->getScorableItems();
    }

    /**
     * returns the resulting text for text answers
     * @return array
     */
    public function getTextAnswers() {

        $result = [];


        foreach($this->tool->getItems() as $item) {
            if($item->getTxExtbaseType() == 'Tx_XlncTools_TextItem') {
                $result[$item->getUid()] = array(
                    'item'      => $item,
                    'values'    => array()
                    );

                // we can't use filterAnswerValues here, since they don't contain
                // text answers anymore, but we still initialize them;
                foreach($this->_answerValues as $value) {
                    if($value['itemUid'] == $item->getUid() && $value['answerTextValue'] != '') {
                        $result[$item->getUid()]['values'][] = $value;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * gets the total number of member sheets (finished or not)
     * @return int
     */
    protected function getTotalSheetCountByType() {
        return [
            self::SHEETTYPE_LEADER => $this->getLeaderSheetCount(),
            self::SHEETTYPE_MEMBER => count($this->team->getSheets()) - $this->getLeaderSheetCount()
        ];
    }

    /**
     * returns the number of sheets (finished or not) for the leader
     * @return int {0,1}
     */
    protected function getLeaderSheetCount() {
        return $this->team->hasLeaderSheet() ? 1 : 0;
    }

    /**
     * returns the absolute difference in score percentage indexed by criteriaUid
     * @return array<int>
     */
    public function getScoreDifferenceByCriteriaUid() {
        return $this->getScoreDifferenceByCriteriaUidBetween(
            $this->getSelfResult(),
            $this->getResult()
        );
    }

    /**
     * returns the absolute difference in score percentage indexed by criteriaUid
     * @return array<int>
     */
    public function getSelfScoreDifferenceByCriteriaUid() {
        return $this->getScoreDifferenceByCriteriaUidBetween(
            $this->getSelfResult(),
            $this->getSecondSelfResult()
        );
    }

    /**
     * returns the absolute difference in score percentage indexed by criteriaUid
     * @return array<int>
     */
    public function getSecondScoreDifferenceByCriteriaUid() {
        return $this->getScoreDifferenceByCriteriaUidBetween(
            $this->getSecondResult(),
            $this->getSecondSelfResult()
        );
    }

    /**
     * returns the absolute difference in score percentage indexed by criteriaUid
     * @return array<int>
     */
    public function getOtherScoreDifferenceByCriteriaUid() {
        return $this->getScoreDifferenceByCriteriaUidBetween(
            $this->getSecondResult(),
            $this->getResult()
        );
    }

    /**
     * returns the maximum score difference
     * @return int
     * @todo check if this is actually needed
     */
    public function getMaxScoreDifference() {
        return max($this->getScoreDifferenceByCriteriaUid());
    }

    /**
     * returns the score difference indexed by item, sorted in descending order
     * @return array<float>
     * @todo check if this is needed
     */
    public function getItemScoreDifferenceSorted() {
        $selfScore = $this->getSelfResult()->getMeanScoreByItemUid();
        $score = $this->getResult()->getMeanScoreByItemUid();

        $differences = [];
        foreach($this->getItemUids() as $uid) {
            $differences[$uid] = abs($selfScore[$uid] - $score[$uid]);
        }
        arsort($differences, SORT_NUMERIC);

        return $differences;
    }


    /**
     * returns the first 5 items with the highest discrepancy between score
     * and selfScore
     * @return array<array>
     */
    public function getHighestFiveScoreDifferences() {
        $differences = array_slice( $this->getItemScoreDifferenceSortedBetween($this->getResult(), $this->getSelfResult()), 0, 5, TRUE);
        $selfScore = $this->getSelfResult()->getMeanScoreByItemUid();
        $score = $this->getResult()->getMeanScoreByItemUid();
        $returnValue = [];

        foreach ($differences as $itemUid => $difference) {
            $returnValue[] = [
                "difference" => $difference,
                "item" => $this->itemRepository->findByUid($itemUid),
                "selfScore" => $selfScore[$itemUid],
                "score" => $score[$itemUid]
            ];
        }

        return $returnValue;
    }

    /**
     * returns the first 5 items with the highest discrepancy between score
     * and secondScore
     * @return array<array>
     */
    public function getHighestFiveSecondScoreDifferences() {
        $differences = array_slice( $this->getItemScoreDifferenceSortedBetween($this->getResult(), $this->getSecondResult()), 0, 5, TRUE);
        $secondScore = $this->getSecondResult()->getMeanScoreByItemUid();
        $score = $this->getResult()->getMeanScoreByItemUid();
        $returnValue = [];

        foreach ($differences as $itemUid => $difference) {
            $returnValue[] = [
                "difference" => $difference,
                "item" => $this->itemRepository->findByUid($itemUid),
                "secondScore" => $secondScore[$itemUid],
                "score" => $score[$itemUid]
            ];
        }

        return $returnValue;
    }

  
    /**
     * returns the first 5 items with the Highest Score
     * @return array<array>
     */
    public function getHighestFiveScores() {
        $score = $this->getResult()->getMeanScoreByItemUid();
        $selfScore = $this->getSelfResult()->getMeanScoreByItemUid();


        arsort($score, SORT_NUMERIC);


        $highestScores = array_slice($score, 0, 5, TRUE);

        $returnValue = [];

        foreach ($highestScores as $itemUid => $score) {
            $returnValue[] = [
                "item" => $this->itemRepository->findByUid($itemUid),
                "selfScore" => $selfScore[$itemUid],
                "score" => $score

            ];

        }

        return $returnValue;
    }
    /**
     * returns the first 5 items with the lowest Score
     * @return array<array>
     */
    public function getLowestFiveScores() {
        $score = $this->getResult()->getMeanScoreByItemUid();
        $selfScore = $this->getSelfResult()->getMeanScoreByItemUid();


        arsort($score, SORT_NUMERIC);

        $lowestScores = array_reverse($score, TRUE);

        $lowestScores = array_slice($lowestScores, 0, 5, TRUE);

        $returnValue = [];

        foreach ($lowestScores as $itemUid => $score) {
            $returnValue[] = [
                "item" => $this->itemRepository->findByUid($itemUid),
                "selfScore" => $selfScore[$itemUid],
                "score" => $score

            ];

        }

        return $returnValue;
    }

    /**
     * returns the absolute difference in score percentage for the given result
     * containers, indexed by criteriaUid
     * @param \Xlnc\XlncTools\Result\ResultContainer $resultA
     * @param \Xlnc\XlncTools\Result\ResultContainer $resultB
     * @return array<int>
     */
    protected function getScoreDifferenceByCriteriaUidBetween($resultA, $resultB) {
        $scoreA = $resultA->getScoreByCriteriaUid();
        $scoreB = $resultB->getScoreByCriteriaUid();
        $returnValue = [];

        foreach($this->getCriteriaUids() as $uid) {
            $returnValue[$uid] =
                abs($scoreA[$uid][self::PERCENTAGE]
                - $scoreB[$uid][self::PERCENTAGE]);
        }

        return $returnValue;
    }

    /**
     * returns the absolute difference in score percentage for the given result
     * containers, indexed by itemUid
     * @param \Xlnc\XlncTools\Result\ResultContainer $resultA
     * @param \Xlnc\XlncTools\Result\ResultContainer $resultB
     * @return array<float>
     */
    protected function getScoreDifferenceByItemUidBetween($resultA, $resultB) {
        $scoreA = $resultA->getMeanScoreByItemUid();
        $scoreB = $resultB->getMeanScoreByItemUid();

        $returnValue = [];

        foreach($this->getItemUids() as $uid) {
            $returnValue[$uid] =
                abs($scoreA[$uid]
                - $scoreB[$uid]);
        }

        return $returnValue;
    }

    /**
     * returns the score difference between the resultContainers given
     * as parameter, indexed by item, sorted in descending order
     * @param \Xlnc\XlncTools\Result\ResultContainer $resultA
     * @param \Xlnc\XlncTools\Result\ResultContainer $resultB
     * @return array<float>
     * @todo check if this is needed
     */
    public function getItemScoreDifferenceSortedBetween($resultA, $resultB) {
        $differences = $this->getScoreDifferenceByItemUidBetween($resultA, $resultB);
        arsort($differences, SORT_NUMERIC);

        return $differences;
    }

    /**
     * returns the amount of finished sheets by type;
     * @return array an array with keys "leader" and "member"
     */
    protected function getFinishedSheetCountByType() {

        $filteredValues = $this->getFilteredAnswerValues();
        $finishedSheetCount = $this->team->getFinishedSheetCount($this->tool);
        $finishedLeaderSheetCount = $this->team->LeaderSheetFinished($this->tool) ? 1 : 0;
        $sheetCountByType = [
            self::SHEETTYPE_LEADER => $finishedLeaderSheetCount,
            self::SHEETTYPE_MEMBER => $finishedSheetCount - $finishedLeaderSheetCount
        ];

        return $sheetCountByType;
    }

    /**
     * memoized version of filterAnswerValues
     * @see filterAnswerValues
     * @return array
     */
    protected function getFilteredAnswerValues() {
        if(NULL === $this->_filteredValues) {
            $this->filterAnswerValues();
        }

        return $this->_filteredValues;
    }

    /**
     * filters the values into a 2-dimensional array by sheet type and answer index
     * @todo optimize and/or replace with SQL
     */
    private function filterAnswerValues() {
        $answerValues = $this->getAnswerValues();

        $this->_filteredValues = [
            self::SHEETTYPE_LEADER => [
                self::FIRST_ANSWER => [],
                self::SECOND_ANSWER => [],
            ],
            self::SHEETTYPE_MEMBER => [
                self::FIRST_ANSWER => [],
                self::SECOND_ANSWER => []
            ]
        ];

        foreach($answerValues as $value) {
            // only count answers to items which we actually want to score
            if(in_array($value["itemUid"], $this->getItemUids())) {
                // split up into two arrays for leader and member answers each
                $sheetTypeKey = ($value["isLeaderSheet"] == 1)
                    ? self::SHEETTYPE_LEADER
                    : self::SHEETTYPE_MEMBER;
                // copy into two arrays, one for the first answer, one for the second
                $answerValue = $value;
                $answerValue["value"] = (int)$answerValue["answerValue"];
                // remove unneeded data from copy
                unset($answerValue["answerValue"]);
                unset($answerValue["secondAnswerValue"]);
                unset($answerValue["isLeaderSheet"]);
                unset($answerValue["sheetUid"]);
                $this->_filteredValues[$sheetTypeKey][self::FIRST_ANSWER][] = $answerValue;

                // repeat for second answer value
                $secondAnswerValue = $value;
                $secondAnswerValue["value"] = (int)$secondAnswerValue["secondAnswerValue"];
                unset($secondAnswerValue["answerValue"]);
                unset($secondAnswerValue["secondAnswerValue"]);
                unset($secondAnswerValue["isLeaderSheet"]);
                unset($secondAnswerValue["sheetUid"]);
                $this->_filteredValues[$sheetTypeKey][self::SECOND_ANSWER][] = $secondAnswerValue;
            }

        }
    }

    /**
     * memoized version of loadAnswerValues
     * @see loadAnswerValues
     * @return [type] [description]
     */
    protected function getAnswerValues() {
        if(NULL === $this->_answerValues) {
            $this->loadAnswerValues();
        }

        return $this->_answerValues;
    }

    /**
     * loads the needed values from the database using SQL for speed
     * @todo add more where conditions and / or sanity checks
     */
    private function loadAnswerValues() {
        $this->_answerValues = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
			self::__SQL_GET_ANSWER_VALUES_FIELDS,
			self::__SQL_GET_ANSWER_VALUES_FROM,
			sprintf(self::__SQL_GET_ANSWER_VALUES_WHERE,
				Surveyresult::STATUS_FINISHED,
				$this->team->getUid(),
				$this->tool->getUid()
			)
		);
    }

    /**
     * SQL field data to be selected
     * @var string
     */
    const __SQL_GET_ANSWER_VALUES_FIELDS = <<<SQL
	CASE sheet.tx_extbase_type
		WHEN 'Tx_XlncTools_LeaderSheet'
		THEN 1
		ELSE 0 END 			as isLeaderSheet,
	sheet.uid 				as sheetUid,
	item.uid 				as itemUid,
	criteria.uid 			as criteriaUid,
	answer.value - 1 		as answerValue,
    answer.text_value       as answerTextValue,
	secondAnswer.value - 1 	as secondAnswerValue
SQL;

    /**
     * SQL table data for fields to be selected from
     * @var string
     */
	const __SQL_GET_ANSWER_VALUES_FROM = <<<SQL
	`tx_xlnctools_domain_model_surveyresult` as result
LEFT JOIN
	`tx_xlnctools_domain_model_tool` as tool ON result.tool = tool.uid
LEFT JOIN
	`tx_xlnctools_domain_model_team` as team ON team.uid = result.team
LEFT JOIN
	`tx_xlnctools_domain_model_sheet` as sheet ON sheet.team = team.uid AND sheet.uid = result.sheet
LEFT JOIN
	`tx_xlnctools_domain_model_item` as item ON item.parentid = tool.uid
LEFT JOIN
	`tx_xlnctools_domain_model_dictionary_item_criteria` as criteria ON item.criteria = criteria.uid
LEFT JOIN
	`tx_xlnctools_domain_model_answer` as answer ON answer.item = item.uid AND answer.sheet = sheet.uid AND answer.answer_index = 1
LEFT JOIN
	`tx_xlnctools_domain_model_answer` as secondAnswer ON secondAnswer.item = item.uid AND secondAnswer.sheet = sheet.uid AND secondAnswer.answer_index = 2
SQL;

    /**
     * SQL where conditions
     * @var string
     */
	const __SQL_GET_ANSWER_VALUES_WHERE = <<<SQL
		result.status = %s
	AND
		team.uid = %s
	AND
		tool.uid = %s
SQL;

}
