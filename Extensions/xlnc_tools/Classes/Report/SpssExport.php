<?php
namespace Xlnc\XlncTools\Report;

use Xlnc\XlncTools\Domain\Model\Surveyresult;

class SpssExport {

    /**
     * @var array<\Xlnc\XlncTools\Domain\Model\Team>
     */
    protected $teams;

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Tool>
     */
    protected $tools;

    protected $_header;
    protected $_rows;

    protected $_headIndex;
    protected $_rowIndex;

    protected $_answerValues;
    protected $_itemUids;

    protected $__crunched = FALSE;

    public function __construct($tool, $teams) {
        $this->teams = $teams;
        $this->tool = $tool;
    }

    public function getCsvString() {
        $this->crunch();

        $header = join($this->_header, ",");
        $rows = array_map(function ($r) {
            return join($r, ",");
        }, $this->_rows);
        return $header . "\n" . join($rows, "\n");
    }

    private function crunch() {
        if(!$this->__crunched) {
            $this->loadValues();
            $this->makeHeader();
            $this->makeRows();
            $this->__crunched = TRUE;
        }
    }

    private function makeHeader() {
        $firstHeaders = [
            'sheet',
            'team',
            'isLeaderSheet',
            'age',
            'gender',
            'country',
            'graduation',
            'education',
            'experience',
            'managementexperience',
            'managementlevel',
            'businessunit',
            'language',

            'reference',

            'industry',
            'internationality',
            'employees',
            'turnover'
        ];
        $itemIndexOffset = count($firstHeaders);
        $toolName = strtolower(end(explode('_', $this->tool->getTitle())));
        $itemHeaders = [];
        for($i = 0; $i < count($this->_itemUids); $i++) {
            $uid = $this->_itemUids[$i];
            $itemHeaders[] = $toolName . $uid . "_1";
            $itemHeaders[] = $toolName . $uid . "_2";
            $this->_headIndex[$uid] = (2 * $i) + $itemIndexOffset;
        }

        $this->_header = array_merge($firstHeaders, $itemHeaders);
    }


    private function makeRows() {
        foreach($this->_answerValues as $row) {
            $this->insertRow($row);
        }

        if(!is_array($this->_rows)) { $this->_rows = []; }
    }

    private function insertRow($row) {
        $sheetUid = $row["sheetUid"];
        if(isset($this->_rowIndex[$sheetUid])) {
            $index = $this->_rowIndex[$sheetUid];
        } else {
            $index = count($this->_rows);
            $this->_rowIndex[$sheetUid] = $index;
            $this->_rows[] = [
                $sheetUid,
                $row["teamUid"],
                $row["isLeaderSheet"],
                $row['age'],
                $row['gender'],
                $row['country'],
                $row['graduation'],
                $row['education'],
                $row['experience'],
                $row['managementexperience'],
                $row['managementlevel'],
                $row['businessunit'],
                $row['language'],

                $row['reference'],

                $row['industry'],
                $row['internationality'],
                $row['employees'],
                $row['turnover']
            ];

        }

        $itemUid = $row["itemUid"];
        $this->_rows[$index][$this->_headIndex[$itemUid]] = $row["answerValue"];
        $this->_rows[$index][$this->_headIndex[$itemUid] + 1] = $row["secondAnswerValue"];
    }

    private function loadValues() {
        $teamUidList = implode(array_map( function ($team) {
            return $team->getUid();
        }, $this->teams), ',' );

        $this->_answerValues = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
			self::__SQL_GET_ANSWER_VALUES_FIELDS,
			self::__SQL_GET_ANSWER_VALUES_FROM,
			sprintf(self::__SQL_GET_ANSWER_VALUES_WHERE,
				Surveyresult::STATUS_FINISHED,
				$teamUidList,
				$this->tool->getUid()
			)
		);

        $this->_itemUids = array_map( function ($item) {
            return $item->getUid();
        }, $this->tool->getItems()->toArray());
    }


    /**
     * SQL field data to be selected
     * @var string
     */
    const __SQL_GET_ANSWER_VALUES_FIELDS = <<<SQL
    CASE sheet.tx_extbase_type
        WHEN 'Tx_XlncTools_LeaderSheet'
        THEN 1
        ELSE 0 END 			as isLeaderSheet,
    sheet.uid 				as sheetUid,
    sheet.access_key        as code,
    item.uid 				as itemUid,
    team.uid 				as teamUid,
    result.language	        as language,
    criteria.uid 			as criteriaUid,
    answer.value - 1 		as answerValue,
    answer.text_value       as answerTextValue,
    secondAnswer.value - 1 	as secondAnswerValue,


    sheet.age                         as age,
    sheet.gender                      as gender,
    sheet.country                     as country,
    sheet.graduation                  as graduation,
    sheet.education                   as education,
    sheet.experience                  as experience,
    sheet.managementexperience        as managementexperience,
    sheet.managementlevel             as managementlevel,
    sheet.businessunit                as businessunit,

    project.reference                 as reference,

    customer.industry                 as industry,
    customer.internationality         as internationality,
    customer.num_employees            as employees,
    customer.turnover                 as turnover
SQL;

    /**
     * SQL table data for fields to be selected from
     * @var string
     */
    const __SQL_GET_ANSWER_VALUES_FROM = <<<SQL
    `tx_xlnctools_domain_model_surveyresult` as result
LEFT JOIN
    `tx_xlnctools_domain_model_tool` as tool ON result.tool = tool.uid
LEFT JOIN
    `tx_xlnctools_domain_model_team` as team ON team.uid = result.team
LEFT JOIN
    `tx_xlnctools_domain_model_project` as project ON project.uid = team.project
LEFT JOIN
    `tx_xlnctools_domain_model_customer` as customer ON customer.uid = project.customer
LEFT JOIN
    `tx_xlnctools_domain_model_sheet` as sheet ON sheet.team = team.uid AND sheet.uid = result.sheet
LEFT JOIN
    `tx_xlnctools_domain_model_item` as item ON item.parentid = tool.uid
LEFT JOIN
    `tx_xlnctools_domain_model_dictionary_item_criteria` as criteria ON item.criteria = criteria.uid
LEFT JOIN
    `tx_xlnctools_domain_model_answer` as answer ON answer.item = item.uid AND answer.sheet = sheet.uid AND answer.answer_index = 1
LEFT JOIN
    `tx_xlnctools_domain_model_answer` as secondAnswer ON secondAnswer.item = item.uid AND secondAnswer.sheet = sheet.uid AND secondAnswer.answer_index = 2
SQL;

    /**
     * SQL where conditions
     * @var string
     */
    const __SQL_GET_ANSWER_VALUES_WHERE = <<<SQL
        result.status = %s
    AND
        team.uid IN (%s)
    AND
        tool.uid = %s
    AND item.deleted = 0 AND item.hidden = 0
    AND sheet.deleted = 0 AND sheet.hidden = 0
    AND answer.deleted = 0
SQL;


}
