<?php
namespace Xlnc\XlncTools\Report;

use \Xlnc\XlncTools\Utility\StatisticsUtility as Stats;
use \Xlnc\XlncTools\Report\ToolReportCalculator;

use \Xlnc\XlncTools\Report\Normalization\NormalizationFactory;

/**
 * sub-result container for use in ToolReportCalculator
 * @package Xlnc\XlncTools
 * @author Malte Muth <muth@fh-konzept.de>
 */
class ResultContainer {

    /**
     * filtered data from the database
     * @var array
     */
    protected $values;

    /**
     * memoization cache for getValuesByCriteriaUidAndItemUid
     * @var array
     */
    protected $valuesByCriteriaAndItemUid = NULL;

    /**
     * memoization cache for getValuesByCriteriaUid
     * @var array
     */
    protected $valuesByCriteriaUid = NULL;

    /**
     * backreference to the report using this container
     * @var \Xlnc\XlncTools\Report\ToolReportCalculator
     */
    protected $report;

    /**
     * array containing the needed normalizers
     * @var array<\Xlnc\XlncTools\Report\Normalization\Normalization>
     */
    protected $normalizers;

    /**
     * @param array $values
     * @param \Xlnc\XlncTools\Report $report
     */
    public function __construct($values, $report) {
        $this->values = $values;
        $this->report = $report;

        $this->_buildNormalizers();
    }

    /**
     * returns the agreement data by criteriaUid
     * [criteriaUid]["percentage"] is a float
     * [criteriaUid]["ranking"] is a string
     * @return array
     */
    public function getAgreementByCriteriaUid() {
        $criteriaUids = $this->report->getCriteriaUids();
        $percentages = $this->getAgreementPercentageByCriteriaUid();
        $rankings = $this->getAgreementRankingByCriteriaUid();

        $agreementData = [];
        foreach($criteriaUids as $uid) {
            $agreementData[$uid] = [
                ToolReportCalculator::PERCENTAGE => $percentages[$uid],
                ToolReportCalculator::RANKING => $rankings[$uid]
            ];
        }

        return $agreementData;
    }

    /**
     * returns the total agreement percentage as average of agreements
     * across criteriaUids
     * @return float
     */
    public function getTotalAgreement() {
        $agreementData = $this->getAgreementByCriteriaUid();

        $percentages = [];

        foreach($agreementData as $data) {
            $percentages[] = $data[ToolReportCalculator::PERCENTAGE];
        }
        $meanPercentage = (int)Stats::mean($percentages);
        return [
            ToolReportCalculator::PERCENTAGE => $meanPercentage,
            ToolReportCalculator::RANKING => $this->transformAgreementPercentageToRanking($meanPercentage)
        ];
    }

    /**
     * returns the score indexed by criteriaUid
     * [criteriaUid]["percentage"] is an integer
     * [criteriaUid]["ranking"] is a string
     * [criteriaUid]["fineRanking"] is a string
     * @return [type] [description]
     */
    public function getScoreByCriteriaUid() {
        $rankings = $this->getScoreRankingByCriteriaUid();
        $fineRankings = $this->getScoreFineRankingByCriteriaUid();
        $percentages = $this->getScorePercentageByCriteriaUid();
        $returnValue = [];

        foreach($this->report->getCriteriaUids() as $uid) {
            $returnValue[$uid] = [
                ToolReportCalculator::PERCENTAGE => $percentages[$uid],
                ToolReportCalculator::RANKING => $rankings[$uid],
                ToolReportCalculator::RANKING_FINE => $fineRankings[$uid],
            ];
        }

        return $returnValue;
    }

    /**
     * returns the mean score indexed by itemUid
     * @return array<float>
     */
    public function getMeanScoreByItemUid() {
        $values = $this->getValuesByItemUid();
        $returnValue = [];

        foreach($values as $uid => $value) {
            $returnValue[$uid] = Stats::mean($value);
        }

        return $returnValue;
    }

    /**
     * return the maximum achievable score by criteriaUid
     * @return array<int>
     */
    protected function getMaxScoreByCriteriaUid() {
        $returnValue = [];

        foreach($this->getValuesByCriteriaUid() as $uid => $values) {
            // @todo remove magic number
            $returnValue[$uid] = 4 * count($values);
        }

        return $returnValue;
    }

    /**
     * returns the total scare by criteriaUid
     * @return array<int>
     */
    protected function getScoreValuesByCriteriaUid() {
        $returnValue = [];

        foreach($this->getValuesByCriteriaUid() as $uid => $values) {
            $returnValue[$uid] = Stats::sum($values);
        }

        return $returnValue;
    }

    protected function getScorePercentageByCriteriaUid() {
        $criteriaUids = $this->report->getCriteriaUids();
        $scores = $this->getScoreValuesByCriteriaUid();
        $max = $this->getMaxScoreByCriteriaUid();

        $returnValue = [];

        foreach($this->getValuesByCriteriaUid() as $uid => $values) {
            //$percentage = (int) (100 * ( (float) $scores[$uid] / $max[$uid] ) );
            $returnValue[$uid] = $this->normalizers[$uid]->normalizeValue($scores[$uid], $max[$uid]);
            //$returnValue[$uid] = $percentage;
        }

        return $returnValue;
    }

    protected function getScoreRankingByCriteriaUid() {
        $percentages = $this->getScorePercentageByCriteriaUid();
        $returnValue = [];

        foreach($percentages as $uid => $percentage) {
            $returnValue[$uid] = $this->transformScorePercentageToRanking($percentage);
        }

        return $returnValue;
    }

    protected function getScoreFineRankingByCriteriaUid() {
        $percentages = $this->getScorePercentageByCriteriaUid();
        $returnValue = [];

        foreach($percentages as $uid => $percentage) {
            $returnValue[$uid] = $this->transformScorePercentageToFineRanking($percentage);
        }

        return $returnValue;
    }

    /**
     * returns a 3-dimensional array of answer values, indexed by criteriaUid and itemUid;
     * i.e. [item.criteriaUid][itemUid] is an array of all answer values of with this itemUid,
     * @note values are converted to int here, uids stay as strings. This is by
     * design since we don't want to have "sparse" arrays; $a[17] = $val gives an
     * array with at least 18 values, $a["17"] = $val with at least one.
     * @return [type] [description]
     * @todo make sure that each criteriaUid is in the return value
     */
    protected function getValuesByCriteriaUidAndItemUid() {
        if(NULL === $this->valuesByCriteriaAndItemUid) {
            $returnValue = [];
            foreach($this->values as $value) {
                // only regard leader answers

                if(!isset($returnValue[$value["criteriaUid"]]))
                    $returnValue[$value["criteriaUid"]] = [];

                if(!isset($returnValue[$value["criteriaUid"]][$value["itemUid"]]))
                    $returnValue[$value["criteriaUid"]][$value["itemUid"]] = [];

                $returnValue[$value["criteriaUid"]][$value["itemUid"]][] = (int)$value["value"];
            }
            $this->valuesByCriteriaAndItemUid = $returnValue;
        }

        return $this->valuesByCriteriaAndItemUid;
    }

    /**
     * returns the answer values, indexed by criteriaUid, as 2-dimensional array:
     * dimension 1 is the criteriaUid as string,
     * dimension 2 is keyless
     * e.g. ["17"][5]
     * @return array a 2-dimensional array;
     *
     */
    protected function getValuesByCriteriaUid() {
        if(NULL === $this->valuesByCriteriaUid) {
            $returnValue = [];

            foreach($this->getValuesByCriteriaUidAndItemUid() as $criteriaUid => $items) {
                $returnValue[$criteriaUid] = [];
                foreach($items as $uid => $values) {
                    $returnValue[$criteriaUid] = array_merge(
                        $returnValue[$criteriaUid],
                        $values
                    );
                }

            }

            $this->valuesByCriteriaUid = $returnValue;
        }


        return $this->valuesByCriteriaUid;
    }

    /**
     * returns a 2-dimensional array of answer values, indexed by itemUid
     * i.e. [itemUid] is an array of answer values for this item
     * @return array
     * @todo make sure all itemUids re in here
     */
    protected function getValuesByItemUid() {
        $returnValue = [];
        foreach($this->getValuesByCriteriaUidAndItemUid() as $criteria => $items) {
            foreach($items as $uid => $values) {
                $returnValue[$uid] = $values;
            }
        }
        return $returnValue;
    }

    /**
     * returns the standard deviation of values by itemUid
     * @return array
     */
    protected function getStandardDeviationByItemUid() {
        $returnValue = [];

        foreach($this->getValuesByItemUid() as $uid => $values) {
            $returnValue[$uid] = Stats::standardDeviation($values);
        }

        return $returnValue;
    }

    /**
     * returns the average of standard deviations of items, indexed by criteriaUid
     * @return
     */
    protected function getAverageStandardDeviationByCriteriaUid() {
        $returnValue = [];
        $standardDeviation = $this->getStandardDeviationByItemUid();

        foreach($this->getValuesByCriteriaUidAndItemUid() as $uid => $items) {
            $deviations = array_map( function($itemUid) use ($standardDeviation) {
                return $standardDeviation[$itemUid];
            }, array_keys($items));

            $returnValue[$uid] = Stats::mean($deviations);
        }

        return $returnValue;
    }

    /**
     * return the agreement, derived from standard deviation averages, indexed by criteriaUid
     * @param  [type] $uid [description]
     * @return [type]      [description]
     */
    protected function getAgreementPercentageByCriteriaUid() {
        $averages = $this->getAverageStandardDeviationByCriteriaUid();
        $returnValue = [];

        foreach($averages as $uid => $average) {
            // this is derived from Niklas' Excel sheets;
            // 2.25 is max deviation (0% agreement),
            // 0 is min deviation (100% agreement)
            // and linear in between
            $returnValue[$uid] = max(0, 100 - (int)((float)$average / 0.0225));
        }

        return $returnValue;
    }

    protected function getAgreementRankingByCriteriaUid() {
        $percentages = $this->getAgreementPercentageByCriteriaUid();
        $returnValue = [];

        foreach($percentages as $uid => $percentage) {
            $returnValue[$uid] = $this->transformAgreementPercentageToRanking($percentage);
        }

        return $returnValue;
    }

    /**
     * transforms a percentage to a ranking identifier
     * @param  float $p
     * @return string
     */
    private function transformAgreementPercentageToRanking($p) {
        if($p < 33) {
            return ToolReportCalculator::RANKING_LOW;
        } else if($p < 66) {
            return ToolReportCalculator::RANKING_MEDIUM;
        } else {
            return ToolReportCalculator::RANKING_HIGH;
        }
    }

    /**
     * transforms a percentage to a ranking identifier
     * @param  float $p
     * @return string
     */
    public function transformScorePercentageToRanking($p) {
        if($p < 41) {
            return ToolReportCalculator::RANKING_DEVELOPMENT;
        } else if($p < 71) {
            return ToolReportCalculator::RANKING_POTENTIAL;
        } else {
            return ToolReportCalculator::RANKING_SIGNATURE;
        }
    }

    /**
     * transforms a percentage to a finer ranking identifier
     * @param  float $p
     * @return string
     */
    private function transformScorePercentageToFineRanking($p) {
        if($p < 20) {
            return ToolReportCalculator::RANKING_FINE_LOW;
        } else if($p < 40) {
            return ToolReportCalculator::RANKING_FINE_LOW_TO_MEDIUM;
        } else if($p < 60) {
            return ToolReportCalculator::RANKING_FINE_MEDIUM;
        } else if($p < 80) {
            return ToolReportCalculator::RANKING_FINE_MEDIUM_TO_HIGH;
        } else {
            return ToolReportCalculator::RANKING_FINE_HIGH;
        }
    }

    /**
     * initializes the normalizers needed later
     */
    protected function _buildNormalizers() {
        foreach($this->report->getItems() as $item) {
            $criteria = $item->getCriteria();
            $criteriaUid = "" . $criteria->getUid();
            if(!isset($this->normalizers[$criteriaUid])) {
                $this->normalizers[$criteriaUid] = NormalizationFactory::buildForItemCriteria($criteria);
            }
        }
    }

}
