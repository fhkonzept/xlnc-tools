<?php

namespace Xlnc\XlncTools\Report;

use \TYPO3\CMS\Extbase\Object\ObjectManager;
use \Xlnc\XlncTools\Domain\Model\Team,
    \Xlnc\XlncTools\Domain\Model\Tool,
    \Xlnc\XlncTools\Domain\Model\ClimateTool,
    \Xlnc\XlncTools\Domain\Model\LeadershipTool,
    \Xlnc\XlncTools\View\Report\StyleReportPdfView,
    \Xlnc\XlncTools\View\Report\SingleStyleReportPdfView,
    \Xlnc\XlncTools\View\Report\ClimateReportPdfView,
    \Xlnc\XlncTools\View\Report\ClimateSelfReportPdfView,
    \Xlnc\XlncTools\Utility\ManagementStylesTextUtility,
    \Xlnc\XlncTools\Utility\ManagementSingleStylesTextUtility,
    \Xlnc\XlncTools\Utility\TeamClimateTextUtility;

class ToolReportGenerator {

    const REPORT_VARIANT_SHORT = "single";

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
    protected $objectManager;

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Team
     */
    protected $team;

    /**
     * @var \Xlnc\XlncTools\Domain\Model\Tool
     */
    protected $tool;

    /**
     * @param \Xlnc\XlncTools\Domain\Model\Team $team
     * @param \Xlnc\XlncTools\Domain\Model\Tool $tool
     * @param string $variant
     */
    public function __construct(Team $team, Tool $tool, $variant) {
        $this->team = $team;
        $this->tool = $tool;
        $this->variant = $variant;
    }

    /**
     * [getFilePath description]
     * @return [type] [description]
     */
    public function getFilePath() {
        $methodName = $this->getReportMethod();
        if (NULL === $methodName) {
            throw new \Exception("Could not determine report method name.");
        }
        $filePath = $this->$methodName($this->team, $this->tool);
        return $this->sanitizeFilepath($filePath);
    }

    private function getReportMethod() {
        $toolClass = get_class($this->tool);

        // @todo turn this into a proper mapping
        switch ($toolClass) {
            case ClimateTool::class :
                if ($this->variant == self::REPORT_VARIANT_SHORT){
                    return "getClimateSelfReport";
                } 
                return "getClimateReport";
                break;
            case LeadershipTool::class :
                if ($this->variant === self::REPORT_VARIANT_SHORT) {
                    return "getSingleStyleReport";
                } else {
                    return "getStyleReport";
                }
                break;
        }

        /* default case */
        return NULL;
    }

    private function getStyleReport($team, $tool) {

        // Get new report
        $report = $this->objectManager->get(\Xlnc\XlncTools\Report\ToolReportCalculator::class, $team, $tool);

        // Set report PDF template
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
        $pdfTemplatePath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('xlnc_tools') . 'Resources/Private/Templates/Pdf/Reports/Styles.html';
        // Create standalone view and assign template
        $pdfView = $objectManager->get(\TYPO3\CMS\Fluid\View\StandaloneView::class);
        $pdfView->setTemplatePathAndFilename($pdfTemplatePath);

        $self = $report->getSelfResult()->getScoreByCriteriaUid();
        $secondSelf = $report->getSecondSelfResult()->getScoreByCriteriaUid();
        // Get employees result
        $employees = $report->getResult()->getScoreByCriteriaUid();
        $secondEmployees = $report->getSecondResult()->getScoreByCriteriaUid();
        $textAnswerResults = $report->getTextAnswers();
        // Get agreement, first question
        $agreement = $report->getResult()->getAgreementByCriteriaUid();
        // Get agreement, second question
        $agreementSecond = $report->getSecondResult()->getAgreementByCriteriaUid();
        // Differences
        $highestFiveScoreDifferences = $report->getHighestFiveScoreDifferences();
        $highestFiveScores = $report->getHighestFiveScores();
        $lowestFiveScores = $report->getLowestFiveScores();

        // Dynamic Texts
        $dynamicTexts = array(
            'chapters' => array(
                5 => array(
                    'is_agreement' => ManagementStylesTextUtility::getOverallAgreementText($agreement),
                    'is_style' => ManagementStylesTextUtility::getMaxAgreementStyleText($agreement),
                    'situation_adequacy' => ManagementStylesTextUtility::getOverallAgreementText($agreementSecond),
                    'situation_adequacy_style' => ManagementStylesTextUtility::getMaxAgreementStyleText($agreementSecond)
                ),
                6 => array(
                    'signature' => ManagementStylesTextUtility::getCriteriaText('self', 'signature', $self),
                    'potential' => ManagementStylesTextUtility::getCriteriaText('self', 'potential', $self),
                    'development' => ManagementStylesTextUtility::getCriteriaText('self', 'development', $self),
                    'situation_adequacy' => ManagementStylesTextUtility::getOverallAgreementText($secondSelf, 40, 60, 'self'),
                    'situation_adequacy_style' => ManagementStylesTextUtility::getMaxAgreementStyleText($secondSelf)
                ),
                7 => array(
                    'signature' => ManagementStylesTextUtility::getCriteriaText('employees', 'signature', $employees),
                    'potential' => ManagementStylesTextUtility::getCriteriaText('employees', 'potential', $employees),
                    'development' => ManagementStylesTextUtility::getCriteriaText('employees', 'development', $employees),
                    'situation_adequacy' => ManagementStylesTextUtility::getOverallAgreementText($secondEmployees, 40, 60, 'self'),
                    'situation_adequacy_style' => ManagementStylesTextUtility::getMaxAgreementStyleText($secondEmployees)
                ),
                8 => array(
                    'comparisonHigher' => ManagementStylesTextUtility::getHigherComparison($self, $employees, 'selfToEmployeesHigher'),
                    'comparisonLower' => ManagementStylesTextUtility::getLowerComparison($self, $employees, 'selfToEmployeesLower'),
                    'comparisonOverall' => ManagementStylesTextUtility::getOverallComparison($self, $employees, 'selfToEmployeesOverall'),
                    'comparisonDifference' => ManagementStylesTextUtility::getMaxComparisonDifference($self, $employees, 'selfToEmployeesDifference'),
                    'bandwidth' => ManagementStylesTextUtility::getBandwidth($employees, 'employeeBandwidth'),
                ),
                12 => array(
                    'normative' => ManagementStylesTextUtility::getStyleValue($employees, "normative"),
                    'directive' => ManagementStylesTextUtility::getStyleValue($employees, "directive"),
                    'participative' => ManagementStylesTextUtility::getStyleValue($employees, "participative"),
                    'integrative' => ManagementStylesTextUtility::getStyleValue($employees, "integrative"),
                    'coachive' => ManagementStylesTextUtility::getStyleValue($employees, "coachive"),
                    'inspirative' => ManagementStylesTextUtility::getStyleValue($employees, "inspirative"),
                )
            )
        );

        $styleReport = $this->objectManager->get(StyleReportPdfView::class);

        $styleReport->setTeam($team);
        $styleReport->setTool($tool);
        $styleReport->setReport($report);

        $styleReport->setDynamicTexts($dynamicTexts);
        $styleReport->setTextAnswerResults($textAnswerResults);

        foreach ($styleReport->getCriteriaUids() as $criteriaUid) {
            $styleReport->setPercentage("agreement", $criteriaUid, $agreement[$criteriaUid]['percentage']);
            $styleReport->setPercentage("agreementSecond", $criteriaUid, $agreementSecond[$criteriaUid]['percentage']);

            $styleReport->setPercentage("self", $criteriaUid, $self[$criteriaUid]['percentage']);
            $styleReport->setRanking("self", $criteriaUid, $secondSelf[$criteriaUid]['fineRanking']);
            $styleReport->setPercentage("employees", $criteriaUid, $employees[$criteriaUid]['percentage']);
            $styleReport->setRanking("employees", $criteriaUid, $secondEmployees[$criteriaUid]['fineRanking']);
        }

        foreach ($highestFiveScoreDifferences as $difference) {
            $styleReport->addScoreDifference("scoreDifference", $difference["item"], $difference["score"], $difference["selfScore"]);
        }


        foreach ($highestFiveScores as $difference) {
            $styleReport->addScoreDifference("highestFiveScores", $difference["item"], $difference["score"], $difference["selfScore"]);
        }
        foreach ($lowestFiveScores as $difference) {
            $styleReport->addScoreDifference("lowestFiveScores", $difference["item"], $difference["score"], $difference["selfScore"]);
        }

        return $styleReport->render();
    }

    private function getSingleStyleReport($team, $tool) {
        // Get new report
        $report = $this->objectManager->get(\Xlnc\XlncTools\Report\ToolReportCalculator::class, $team, $tool);

        // Set report PDF template
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
        $pdfTemplatePath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('xlnc_tools') . 'Resources/Private/Templates/Pdf/Reports/SingleStyles.html';
        // Create standalone view and assign template
        $pdfView = $objectManager->get(\TYPO3\CMS\Fluid\View\StandaloneView::class);
        $pdfView->setTemplatePathAndFilename($pdfTemplatePath);

        $self = $report->getSelfResult()->getScoreByCriteriaUid();
        $secondSelf = $report->getSecondSelfResult()->getScoreByCriteriaUid();
        // Get employees result
        $employees = $report->getResult()->getScoreByCriteriaUid();
        $secondEmployees = $report->getSecondResult()->getScoreByCriteriaUid();
        $textAnswerResults = $report->getTextAnswers();
        // Get agreement, first question
        $agreement = $report->getResult()->getAgreementByCriteriaUid();
        // Get agreement, second question
        $agreementSecond = $report->getSecondResult()->getAgreementByCriteriaUid();
        // Differences
        $highestFiveScoreDifferences = $report->getHighestFiveScoreDifferences();



        // Dynamic Texts
        $dynamicTexts = array(
            'chapters' => array(
                5 => array(
                    'is_agreement' => ManagementStylesTextUtility::getOverallAgreementText($agreement),
                    'is_style' => ManagementStylesTextUtility::getMaxAgreementStyleText($agreement),
                    'situation_adequacy' => ManagementStylesTextUtility::getOverallAgreementText($agreementSecond),
                    'situation_adequacy_style' => ManagementStylesTextUtility::getMaxAgreementStyleText($agreementSecond)
                ),
                6 => array(
                    'signature' => ManagementStylesTextUtility::getCriteriaText('self', 'signature', $self),
                    'potential' => ManagementStylesTextUtility::getCriteriaText('self', 'potential', $self),
                    'development' => ManagementStylesTextUtility::getCriteriaText('self', 'development', $self),
                    'situation_adequacy' => ManagementStylesTextUtility::getOverallAgreementText($secondSelf, 40, 60, 'self'),
                    'situation_adequacy_style' => ManagementStylesTextUtility::getMaxAgreementStyleText($secondSelf)
                ),
                7 => array(
                    'signature' => ManagementStylesTextUtility::getCriteriaText('employees', 'signature', $employees),
                    'potential' => ManagementStylesTextUtility::getCriteriaText('employees', 'potential', $employees),
                    'development' => ManagementStylesTextUtility::getCriteriaText('employees', 'development', $employees),
                    'situation_adequacy' => ManagementStylesTextUtility::getOverallAgreementText($secondEmployees, 40, 60, 'self'),
                    'situation_adequacy_style' => ManagementStylesTextUtility::getMaxAgreementStyleText($secondEmployees)
                ),
                8 => array(
                    'comparisonHigher' => ManagementStylesTextUtility::getHigherComparison($self, $employees, 'selfToEmployeesHigher'),
                    'comparisonLower' => ManagementStylesTextUtility::getLowerComparison($self, $employees, 'selfToEmployeesLower'),
                    'comparisonOverall' => ManagementStylesTextUtility::getOverallComparison($self, $employees, 'selfToEmployeesOverall'),
                    'comparisonDifference' => ManagementStylesTextUtility::getMaxComparisonDifference($self, $employees, 'selfToEmployeesDifference'),
                    'bandwidth' => ManagementStylesTextUtility::getBandwidth($employees, 'employeeBandwidth'),
                ),
                12 => array(
                    'normative' => ManagementStylesTextUtility::getStyleValue($employees, "normative"),
                    'directive' => ManagementStylesTextUtility::getStyleValue($employees, "directive"),
                    'participative' => ManagementStylesTextUtility::getStyleValue($employees, "participative"),
                    'integrative' => ManagementStylesTextUtility::getStyleValue($employees, "integrative"),
                    'coachive' => ManagementStylesTextUtility::getStyleValue($employees, "coachive"),
                    'inspirative' => ManagementStylesTextUtility::getStyleValue($employees, "inspirative"),
                )
            )
        );

        $styleReport = $this->objectManager->get(SingleStyleReportPdfView::class);

        $styleReport->setTeam($team);
        $styleReport->setTool($tool);
        $styleReport->setReport($report);

        $styleReport->setDynamicTexts($dynamicTexts);
        $styleReport->setTextAnswerResults($textAnswerResults);

        foreach ($styleReport->getCriteriaUids() as $criteriaUid) {
            $styleReport->setPercentage("agreement", $criteriaUid, $agreement[$criteriaUid]['percentage']);
            $styleReport->setPercentage("agreementSecond", $criteriaUid, $agreementSecond[$criteriaUid]['percentage']);

            $styleReport->setPercentage("self", $criteriaUid, $self[$criteriaUid]['percentage']);
            $styleReport->setRanking("self", $criteriaUid, $secondSelf[$criteriaUid]['fineRanking']);
            $styleReport->setPercentage("employees", $criteriaUid, $employees[$criteriaUid]['percentage']);
            $styleReport->setRanking("employees", $criteriaUid, $secondEmployees[$criteriaUid]['fineRanking']);
        }

        foreach ($highestFiveScoreDifferences as $difference) {
            $styleReport->addScoreDifference("scoreDifference", $difference["item"], $difference["score"], $difference["selfScore"]);
        }



        return $styleReport->render();
    }
    
    private function getClimateSelfReport($team, $tool) {
        
        // Get new report
        $report = $this->objectManager->get(\Xlnc\XlncTools\Report\ToolReportCalculator::class, $team, $tool);

        // Get data
        // Get self results
        $self = $report->getSelfResult()->getScoreByCriteriaUid();
        $secondSelf = $report->getSecondSelfResult()->getScoreByCriteriaUid();
        // Get employees result
        //$employees = $report->getResult()->getScoreByCriteriaUid();
        // Get agreement, first question
        // Get agreement, second question
        //$secondEmployees = $report->getSecondResult()->getScoreByCriteriaUid();

        //$agreement = $report->getResult()->getAgreementByCriteriaUid();
        //$agreementSecond = $report->getSecondResult()->getAgreementByCriteriaUid();


        $textAnswerResults = $report->getTextAnswers();

        // Differences
        $highestFiveSecondScoreDifferences = $report->getHighestFiveSecondScoreDifferences();
        $highestFiveScoreDifferences = $report->getHighestFiveScoreDifferences();

        $dynamicTexts = array(
            'chapters' => array(
                /*
                5 => array(
                    'is_agreement' => TeamClimateTextUtility::getOverallAgreementText($agreement),
                    'is_climate' => TeamClimateTextUtility::getMaxAgreementClimateText($agreement),
                    'should_agreement' => TeamClimateTextUtility::getOverallAgreementText($agreementSecond),
                    'should_climate' => TeamClimateTextUtility::getMaxAgreementClimateText($agreementSecond)
                ),*/
                5 => array(
                    'strong' => TeamClimateTextUtility::getCriteriaText('self', 'signature', $self),
                    'middle' => TeamClimateTextUtility::getCriteriaText('self', 'potential', $self),
                    'weak' => TeamClimateTextUtility::getCriteriaText('self', 'development', $self),
                    'should' => TeamClimateTextUtility::getOverallShouldText($secondSelf, 40, 70, 'self'),
                    'highestShould' => TeamClimateTextUtility::getHighestShouldText($secondSelf, 'self'),
                    'highestDifference' => TeamClimateTextUtility::getHighestShouldDifferenceText($self, $secondSelf, 'self'),
                ),
                /*
                7 => array(
                    'strong' => TeamClimateTextUtility::getCriteriaText('employees', 'signature', $employees),
                    'middle' => TeamClimateTextUtility::getCriteriaText('employees', 'potential', $employees),
                    'weak' => TeamClimateTextUtility::getCriteriaText('employees', 'development', $employees),
                    'should' => TeamClimateTextUtility::getOverallShouldText($secondEmployees, 40, 70, 'employees'),
                    'highestShould' => TeamClimateTextUtility::getHighestShouldText($secondEmployees, 'employees'),
                    'highestDifference' => TeamClimateTextUtility::getHighestShouldDifferenceText($employees, $secondEmployees, 'employees'),
                ),
                8 => array(
                    'high' => TeamClimateTextUtility::getComparisonText($self, $employees, 15, 101, 'high'),
                    'middle' => TeamClimateTextUtility::getComparisonText($self, $employees, 8, 15, 'middle'),
                    'low' => TeamClimateTextUtility::getComparisonText($self, $employees, -1, 8, 'low'),
                ),
                6 => array(
                    'autonomy' => TeamClimateTextUtility::getClimateValue($employees, "autonomy"),
                    'credibility' => TeamClimateTextUtility::getClimateValue($employees, "credibility"),
                    'safety' => TeamClimateTextUtility::getClimateValue($employees, "safety"),
                    'development' => TeamClimateTextUtility::getClimateValue($employees, "development"),
                    'clearness' => TeamClimateTextUtility::getClimateValue($employees, "clearness"),
                    'flexibility' => TeamClimateTextUtility::getClimateValue($employees, "flexibility"),
                    'recognition' => TeamClimateTextUtility::getClimateValue($employees, "recognition"),
                    'identification' => TeamClimateTextUtility::getClimateValue($employees, "identification"),
                    'teamspirit' => TeamClimateTextUtility::getClimateValue($employees, "teamspirit"),
                )*/
            )
        );


        $climateReport = $this->objectManager->get(ClimateSelfReportPdfView::class);

        $climateReport->setDynamicTexts($dynamicTexts);

        $climateReport->setTeam($team);
        $climateReport->setTool($tool);
        $climateReport->setReport($report);

        foreach ($climateReport->getCriteriaUids() as $criteriaUid) {
            // $climateReport->setPercentage("agreement", $criteriaUid, $agreement[$criteriaUid]['percentage']);
            // $climateReport->setPercentage("agreementSecond", $criteriaUid, $agreementSecond[$criteriaUid]['percentage']);

            $climateReport->setPercentage("self", $criteriaUid, $self[$criteriaUid]['percentage']);
            $climateReport->setPercentage("secondSelf", $criteriaUid, $secondSelf[$criteriaUid]['percentage']);

            $climateReport->setRanking("self", $criteriaUid, $self[$criteriaUid]['fineRanking']);
            // $climateReport->setPercentage("employees", $criteriaUid, $employees[$criteriaUid]['percentage']);
            // $climateReport->setPercentage("secondEmployees", $criteriaUid, $secondEmployees[$criteriaUid]['percentage']);

            // $climateReport->setRanking("employees", $criteriaUid, $employees[$criteriaUid]['fineRanking']);
        }

        foreach ($highestFiveSecondScoreDifferences as $difference) {
            $climateReport->addScoreDifference("secondScoreDifference", $difference["item"], $difference["score"], $difference["secondScore"]);
        }

        foreach ($highestFiveScoreDifferences as $difference) {
            $climateReport->addScoreDifference("scoreDifference", $difference["item"], $difference["selfScore"], $difference["score"]);
        }

        return $climateReport->render();
    }

    private function getClimateReport($team, $tool) {

        // Get new report
        $report = $this->objectManager->get(\Xlnc\XlncTools\Report\ToolReportCalculator::class, $team, $tool);

        // Get data
        // Get self results
        $self = $report->getSelfResult()->getScoreByCriteriaUid();
        $secondSelf = $report->getSecondSelfResult()->getScoreByCriteriaUid();
        // Get employees result
        $employees = $report->getResult()->getScoreByCriteriaUid();
        // Get agreement, first question
        // Get agreement, second question
        $secondEmployees = $report->getSecondResult()->getScoreByCriteriaUid();

        $agreement = $report->getResult()->getAgreementByCriteriaUid();
        $agreementSecond = $report->getSecondResult()->getAgreementByCriteriaUid();


        $textAnswerResults = $report->getTextAnswers();

        // Differences
        $highestFiveSecondScoreDifferences = $report->getHighestFiveSecondScoreDifferences();
        $highestFiveScoreDifferences = $report->getHighestFiveScoreDifferences();

        $dynamicTexts = array(
            'chapters' => array(
                5 => array(
                    'is_agreement' => TeamClimateTextUtility::getOverallAgreementText($agreement),
                    'is_climate' => TeamClimateTextUtility::getMaxAgreementClimateText($agreement),
                    'should_agreement' => TeamClimateTextUtility::getOverallAgreementText($agreementSecond),
                    'should_climate' => TeamClimateTextUtility::getMaxAgreementClimateText($agreementSecond)
                ),
                6 => array(
                    'strong' => TeamClimateTextUtility::getCriteriaText('self', 'signature', $self),
                    'middle' => TeamClimateTextUtility::getCriteriaText('self', 'potential', $self),
                    'weak' => TeamClimateTextUtility::getCriteriaText('self', 'development', $self),
                    'should' => TeamClimateTextUtility::getOverallShouldText($secondSelf, 40, 70, 'self'),
                    'highestShould' => TeamClimateTextUtility::getHighestShouldText($secondSelf, 'self'),
                    'highestDifference' => TeamClimateTextUtility::getHighestShouldDifferenceText($self, $secondSelf, 'self'),
                ),
                7 => array(
                    'strong' => TeamClimateTextUtility::getCriteriaText('employees', 'signature', $employees),
                    'middle' => TeamClimateTextUtility::getCriteriaText('employees', 'potential', $employees),
                    'weak' => TeamClimateTextUtility::getCriteriaText('employees', 'development', $employees),
                    'should' => TeamClimateTextUtility::getOverallShouldText($secondEmployees, 40, 70, 'employees'),
                    'highestShould' => TeamClimateTextUtility::getHighestShouldText($secondEmployees, 'employees'),
                    'highestDifference' => TeamClimateTextUtility::getHighestShouldDifferenceText($employees, $secondEmployees, 'employees'),
                ),
                8 => array(
                    'high' => TeamClimateTextUtility::getComparisonText($self, $employees, 15, 101, 'high'),
                    'middle' => TeamClimateTextUtility::getComparisonText($self, $employees, 8, 15, 'middle'),
                    'low' => TeamClimateTextUtility::getComparisonText($self, $employees, -1, 8, 'low'),
                ),
                11 => array(
                    'autonomy' => TeamClimateTextUtility::getClimateValue($employees, "autonomy"),
                    'credibility' => TeamClimateTextUtility::getClimateValue($employees, "credibility"),
                    'safety' => TeamClimateTextUtility::getClimateValue($employees, "safety"),
                    'development' => TeamClimateTextUtility::getClimateValue($employees, "development"),
                    'clearness' => TeamClimateTextUtility::getClimateValue($employees, "clearness"),
                    'flexibility' => TeamClimateTextUtility::getClimateValue($employees, "flexibility"),
                    'recognition' => TeamClimateTextUtility::getClimateValue($employees, "recognition"),
                    'identification' => TeamClimateTextUtility::getClimateValue($employees, "identification"),
                    'teamspirit' => TeamClimateTextUtility::getClimateValue($employees, "teamspirit"),
                )
            )
        );


        $climateReport = $this->objectManager->get(ClimateReportPdfView::class);

        $climateReport->setDynamicTexts($dynamicTexts);

        $climateReport->setTeam($team);
        $climateReport->setTool($tool);
        $climateReport->setReport($report);

        foreach ($climateReport->getCriteriaUids() as $criteriaUid) {
            $climateReport->setPercentage("agreement", $criteriaUid, $agreement[$criteriaUid]['percentage']);
            $climateReport->setPercentage("agreementSecond", $criteriaUid, $agreementSecond[$criteriaUid]['percentage']);

            $climateReport->setPercentage("self", $criteriaUid, $self[$criteriaUid]['percentage']);
            $climateReport->setPercentage("secondSelf", $criteriaUid, $secondSelf[$criteriaUid]['percentage']);

            // $climateReport->setRanking("self", $criteriaUid, $self[$criteriaUid]['fineRanking']);
            $climateReport->setPercentage("employees", $criteriaUid, $employees[$criteriaUid]['percentage']);
            $climateReport->setPercentage("secondEmployees", $criteriaUid, $secondEmployees[$criteriaUid]['percentage']);

            // $climateReport->setRanking("employees", $criteriaUid, $employees[$criteriaUid]['fineRanking']);
        }

        foreach ($highestFiveSecondScoreDifferences as $difference) {
            $climateReport->addScoreDifference("secondScoreDifference", $difference["item"], $difference["score"], $difference["secondScore"]);
        }

        foreach ($highestFiveScoreDifferences as $difference) {
            $climateReport->addScoreDifference("scoreDifference", $difference["item"], $difference["selfScore"], $difference["score"]);
        }

        return $climateReport->render();
    }

    private function sanitizeFilepath($filepath) {
        return trim(preg_replace('/<!--(.|\s)*?-->/', '', htmlspecialchars_decode(strip_tags($filepath))));
    }

}
