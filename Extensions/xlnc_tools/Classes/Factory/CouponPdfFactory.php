<?php
namespace Xlnc\XlncTools\Factory;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use Xlnc\XlncTools\Utility\SheetUtility;
use	Xlnc\XlncTools\Uri\UriBuilder;

use Xlnc\XlncTools\Domain\Model\LeaderSheet;
use Xlnc\XlncTools\Domain\Model\Sheet;

use Xlnc\XlncTools\Utility\LanguageUtility;

/**
 * This is not a factory. This class provides static methods for building coupon
 * invitation PDFs.
 *
 * @author Malte Muth	<muth@fh-konzept.de>
 * @package Xlnc\XlncTools
 */
class CouponPdfFactory {

	/**
	 * creates a coupon PDF from the given invitation
	 * @param  \Xlnc\XlncTools\Domain\Model\Invitation $invitation
	 * @return string file path to the temporary PDF, relative to PATH_site
	 */
	public static function createFromInvitation($invitation) {
		$filename = self::buildFilename($invitation);

		$relativeFilename = "typo3temp/" . $filename . ".pdf";

		$objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
		$pdfTemplatePath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('xlnc_tools') . 'Resources/Private/Templates/Pdf/Coupon.html';

		$invitationTextMap = [
	        LeaderSheet::class => 'Tx_XlncTools_LeaderInvitationText',
	        Sheet::class => 'Tx_XlncTools_InvitationText'
	    ];
		$text = $invitation->getTeam()->getTextInLanguage($invitationTextMap[$invitation->getSheetType()], $invitation->getLanguage());

		$pdfView = $objectManager->get(\TYPO3\CMS\Fluid\View\StandaloneView::class);
		$pdfView->setTemplatePathAndFilename($pdfTemplatePath);

		$uri = $objectManager->get(UriBuilder::class)->buildLoginUrlWithCode($invitation);

		$pdfView->setTemplatePathAndFilename($pdfTemplatePath);
		$pdfView->assignMultiple(
			array(
				'accessKey'	=> $invitation->getAddress(),
				'filename'	=> $filename,
				'authenticationUri' => $uri,
				'text' => $text,
				'invitation' => $invitation
			)
		);
		LanguageUtility::setLanguage($invitation->getLanguage());

		$relativeFilename = substr(trim($pdfView->render()), strlen(PATH_site));
		LanguageUtility::resetLanguage();


		return $relativeFilename;
	}

	/**
	 * creates a zip containing coupon PDFs for the given invitations
	 * @param  array<\Xlnc\XlncTools\Domain\Model\Invitation> $invitation
	 * @return string file path to the temporary ZIP, relative to PATH_site
	 */
	public static function createZipFromInvitations($invitations) {
		$filename = self::buildZipFilename($invitations);

		$relativeFilename = "typo3temp/" . $filename . ".zip";


		$objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
		$zipTemplatePath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('xlnc_tools') . 'Resources/Private/Templates/Zip/Coupons.html';

		$zipView = $objectManager->get(\TYPO3\CMS\Fluid\View\StandaloneView::class);
		$zipView->setTemplatePathAndFilename($zipTemplatePath);

		$zipView->setTemplatePathAndFilename($zipTemplatePath);
		$zipView->assignMultiple(
			array(
				"pdfs" => array_map( function ($invitation) {
					return self::createFromInvitation($invitation);
				}, $invitations),
				"filename" => $filename
			)
		);

		$relativeFilename = substr(trim($zipView->render()), strlen(PATH_site));


		return $relativeFilename;
	}

	/**
	 * build a file identifier for coupon files
	 * @param  \Xlnc\XlncTools\Domain\Model\Invitation $invitation
	 * @return string
	 */
	public static function buildFilename($invitation) {
		return md5($invitation->getAddress());
	}

	/**
	 * build a file identifier for coupon zip files
	 * @param  array<\Xlnc\XlncTools\Domain\Model\Invitation> $invitations
	 * @return string
	 */
	public static function buildZipFilename($invitations) {
		$codes = array_map( function($invitation) {
			return $invitation->getAddress();
		}, $invitations);
		return md5(implode("-", $codes));
	}

}
