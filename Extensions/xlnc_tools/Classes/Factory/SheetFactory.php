<?php
namespace Xlnc\XlncTools\Factory;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use Xlnc\XlncTools\Utility\AccessKeyUtility;

class SheetFactory {

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     * @inject
     */
	protected $objectManager = NULL;

	/**
	 * creates a sheet for the given invitation
	 * @param  \Xlnc\XlncTools\Domain\Model\Invitation $invitation
	 * @return \Xlnc\XlncTools\Domain\Model\Sheet
	 */
	public function createFromInvitation($invitation) {
		$sheet = $this->objectManager->get($invitation->getSheetType());
		$accessKey = AccessKeyUtility::getNewAccessKey();
		$sheet->setAccessKey($accessKey);

		return $sheet;
	}

}
?>
