<?php
namespace Xlnc\XlncTools\Utility;

/**
 * @author  Malte Muth <muth@fh-konzept.de>
 */
class RestServiceUtility {

	const GLOBAL_REST_ROUTE_KEY = "REST_ROUTES";

	/**
	 * registers a route to be handled by the central REST dispatcher
	 * @todo  implement additional actions
	 * @param  string $routeName                   the URL part of this route
	 * @param  string $resourceName                the parameter name of the resource as expected by the default member actions in the controller
	 * @param  string $controllerName              the controller name
	 * @param  string $extensionName               the extension name
	 * @param  array  $additionalCollectionActions additional actions on the collection
	 * @param  array  $additionalMemberActions     additional actions on the member
	 */
	public static function registerResource(	$routeName,
												$resourceName,
												$controllerName,
												$extensionName,
												$additionalCollectionActions = array(),
												$additionalMemberActions = array()) {
		if(!is_array($GLOBALS['T3_VAR']['ext']['xlnc_tools'][self::GLOBAL_REST_ROUTE_KEY]))
			$GLOBALS['T3_VAR']['ext']['xlnc_tools'][self::GLOBAL_REST_ROUTE_KEY] = [];

		$GLOBALS['T3_VAR']['ext']['xlnc_tools'][self::GLOBAL_REST_ROUTE_KEY][$routeName] = [
			"controller" => $controllerName,
			"extension" => $extensionName,
			"resource" => $resourceName
		];

	}

	public static function getMap() {
		return $GLOBALS['T3_VAR']['ext']['xlnc_tools'][self::GLOBAL_REST_ROUTE_KEY];
	}
}
