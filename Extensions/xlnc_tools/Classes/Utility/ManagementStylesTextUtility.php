<?php
namespace Xlnc\XlncTools\Utility;

use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class ManagementStylesTextUtility {


    const XLF_FILE = 'reports.styles.';

    static public function styleLabels() {
        return [
            12 	=> 'normative',
			4 	=> 'directive',
			13 	=> 'participative',
			10 	=> 'integrative',
			3 	=> 'coachive',
			9 	=> 'inspirative'
        ];
    }

    static public function styleUids() {
        return array_keys(self::styleLabels());
    }


    public static function t($label) {
        $translation = LocalizationUtility::translate(self::XLF_FILE . $label, 'xlnc_tools');
        if("" == $translation) {
            return $label;
        } else {
            return $translation;
        }
    }

    static public function getMaxAgreementStyleText($agreements) {

		$labelId = '';
		$currentMaxValue = 0;

		$stylesLookup = self::styleLabels();

		foreach($agreements as $id => $agreement) {
			if((int) $agreement['percentage'] > $currentMaxValue) {
				$labelId = $stylesLookup[$id];
				$currentMaxValue = (int) $agreement['percentage'];
			}
		}

		return self::t('general.styles.' . $labelId . '.alt');
	}

    static public function getOverallAgreementText($agreements, $lowVal = 33, $middleVal = 66, $scope = 'agreement') {
        // initialize count
        $levelCount = [
            "low" => 0,
            "middle" => 0,
            "high" => 0
        ];

        // map agreements to levels and count how many agreements are in each level
        $levelMap = [];
        foreach(self::styleUids() as $uid) {
            $value = $agreements[$uid]["percentage"];
            if($value < $lowVal) {
                $levelMap[$uid] = "low";
                $levelCount["low"]++;
            } elseif ($value < $middleVal) {
                $levelMap[$uid] = "middle";
                $levelCount["middle"]++;
            } else {
                $levelMap[$uid] = "high";
                $levelCount["high"]++;
            }
        }

        // evaluate
        if ( 6 === $levelCount["low"] ) {
            $labelId = 'low';
        } elseif ( 0 === $levelCount["high"] && 0 < $levelCount["low"] ) {
            $labelId = 'low_to_middle';
        } elseif ( 6 === $levelCount["middle"] ) {
            $labelId = 'middle';
        } elseif ( 0 === $levelCount["low"] && 0 < $levelCount["middle"] ) {
            $labelId = 'middle_to_high';
        } elseif ( 6 === $levelCount["high"] ) {
            $labelId = 'high';
        } else {
            $labelId = 'high_variance';
        }

		return self::t($scope . '.' . $labelId);
    }

    static public function getCriteriaText($type, $scope, $criteriaResults) {

		$txt = self::t($type .'.' . $scope);

		$and = self::t('general.and');
		$the = self::t('general.the');
		$asWellAs = self::t('general.as_well_as');

		$stylesLookup = self::styleLabels();

		$criteriaMatches = array();

		foreach(self::styleUids() as $uid) {
            $criteriaResult = $criteriaResults[$uid];
			if($criteriaResult['ranking'] == $scope ) {
				$criteriaMatches[] = $stylesLookup[$uid];
			}
		}

		if(count($criteriaMatches) == 0) {

			return self::t('general.none');

		} else {

			$styleTxts = array();
			$descriptionTxts = array();
			foreach($criteriaMatches as $labelId) {
				$styleTxts[] = self::t('general.styles.' . $labelId);
				$descriptionTxts[] = self::t('general.styles.descriptions.' . $labelId);
			}

			if(count($criteriaMatches) == 1) {

				$styleTxt = $styleTxts[0];
				$descriptionTxt = $descriptionTxts[0];

				$txt = sprintf($txt, $styleTxt, $descriptionTxt);

			} else if(count($criteriaMatches) == 2 || count($criteriaMatches) == 3) {

				$styleTxt = implode(', ' . $the . ' ', array_slice($styleTxts, 0, count($criteriaMatches)-1));
				$styleTxt .= ' ' . $and . ' ' . $the . ' ' . $styleTxts[count($criteriaMatches)-1];

				$descriptionTxt = implode(', ', array_slice($descriptionTxts, 0, count($criteriaMatches)-1));
				$descriptionTxt .= ' ' . $asWellAs . ' ' . $descriptionTxts[count($criteriaMatches)-1];

				$txt = sprintf($txt, $styleTxt, $descriptionTxt);

			} else if(count($criteriaMatches) == 4 || count($criteriaMatches) == 5) {

				$styleTxt = implode(', ' . $the . ' ', array_slice($styleTxts, 0, count($criteriaMatches)-1));
				$styleTxt .= ' ' . $and . ' ' . $the . ' ' . $styleTxts[count($criteriaMatches)-1];

				$descriptionTxt = implode(', ', array_slice($descriptionTxts, 0, 2));
				$descriptionTxt .= ' ' . $asWellAs . ' ' . $descriptionTxts[2];

				$txt2 = self::t( $type .'.' . $scope . '.also');

				$descriptionTxt2 = implode(', ', array_slice($descriptionTxts, 3, 1));
				if(count($criteriaMatches) == 5)
					$descriptionTxt2 .= ' ' . $asWellAs . ' ' . $descriptionTxts[4];

				$descriptionTxt2 = sprintf($txt2, $descriptionTxt2);

				$txt = sprintf($txt, $styleTxt, $descriptionTxt) . ' ' . $descriptionTxt2;

			} else if(count($criteriaMatches) == 6) {

				$txt = self::t( $type . '.' . $scope . '.all');

				$descriptionTxt = implode(', ', array_slice($descriptionTxts, 0, 2));
				$descriptionTxt .= ' ' . $asWellAs . ' ' . $descriptionTxts[2];

				$txt2 = self::t($type .'.' . $scope . '.also');

				$descriptionTxt2 = implode(', ', array_slice($descriptionTxts, 3, 2));
				$descriptionTxt2 .= ' ' . $asWellAs . ' ' . $descriptionTxts[5];

				$descriptionTxt2 = sprintf($txt2, $descriptionTxt2);

				$txt = sprintf($txt, $descriptionTxt) . ' ' . $descriptionTxt2;
			}

		}

		return $txt;

	}

    public static function getHigherComparison(
        $firstScore,
        $secondScore,
        $scope,
        $limit = 10) {

        $higherScores = [];

        // collect style labels with 10% or more discrepancy "up"
        foreach(self::styleLabels() as $uid => $label) {
            if($firstScore[$uid]["percentage"] > $secondScore[$uid]["percentage"] + $limit - 1) {
                $higherScores[] = $label;
            }
        }

        if( 0 === count($higherScores) ) {

			$txt =  self::t('general.none');

		} else {

			$styleTxts = array();
			foreach($higherScores as $labelId) {
				$styleTxts[] = self::t('general.styles.' . $labelId . ".alt");
			}

            $txt = self::t('comparison.' . $scope);
    		$and = self::t('general.and');
    		$the = self::t('general.the.alt');

			if(count($higherScores) == 1) {

				$styleTxt = $styleTxts[0];

				$txt = sprintf($txt, $styleTxt);

			} else  {

				$styleTxt = implode(', ' . $the . ' ', array_slice($styleTxts, 0, count($higherScores)-1));
				$styleTxt .= ' ' . $and . ' ' . $the . ' ' . $styleTxts[count($higherScores)-1];


				$txt = sprintf($txt, $styleTxt);

			}
		}
		return $txt;


    }

    public static function getLowerComparison(
        $firstScore,
        $secondScore,
        $scope,
        $limit = 10) {

        return self::getHigherComparison($secondScore, $firstScore, $scope, $limit);

    }

    public static function getOverallComparison($firstScore, $secondScore, $scope) {

        $higherFirst = 0;
        $higherSecond = 0;
        foreach(self::styleUids() as $uid) {
            if($firstScore[$uid]["percentage"] > $secondScore[$uid]["percentage"])
                $higherFirst++;
            if($firstScore[$uid]["percentage"] < $secondScore[$uid]["percentage"])
                $higherSecond++;
        }

        if ( 0 === $higherFirst && 0 === $higherSecond) {
            $txt = self::t("comparison.same." . $scope);
        } else {
            $txt = self::t("comparison." . $scope);

            if ( 6 === $higherFirst || 6 == $higherSecond ) {
                $snippetOne = self::t("comparison.consistently");
            } elseif( abs($higherFirst - $higherSecond) > 3 ) {
                $snippetOne = self::t("comparison.mostly");
            } elseif ( abs($higherFirst - $higherSecond) > 0 ) {
                $snippetOne = self::t("comparison.slightly");
            } else {
                $snippetOne = self::t("comparison.inconsistently");
            }

            if ( $higherFirst > $higherSecond) {
                $snippetTwo = self::t("comparison.higher." . $scope);
            } elseif ( $higherSecond > $higherFirst ) {
                $snippetTwo = self::t("comparison.lower." . $scope);
            } else {
                // ===
                $snippetTwo = self::t("comparison.neither." . $scope);
            }

            $txt = sprintf($txt, $snippetOne, $snippetTwo);
        }

        return $txt;
    }

    public static function getMaxComparisonDifference($firstScore, $secondScore, $scope) {

        $differences = [];
        foreach(self::styleUids() as $uid) {
            $differences[$uid] = abs($firstScore[$uid]["percentage"] - $secondScore[$uid]["percentage"]);
        }

        asort($differences);
        $differences = array_slice($differences, count($differences) - 2, count($differences) - 1, TRUE);

        $snippets = [];
        $labels = self::styleLabels();
        foreach($differences as $key => $_) {
            $snippets[] = self::t("general.styles." . $labels[$key] . ".alt");
        }
        $txt = self::t("comparison.difference." . $scope);

        $txt = sprintf($txt,
            $snippets[1],
            $snippets[0]
        );

        return $txt;
    }

    public static function getBandwidth($score, $scope) {
        $styleCriteriaCount = [
            "development" => 0,
            "potential" => 0,
            "signature" => 0
        ];

        foreach(self::styleUids() as $uid) {
            $value = $score[$uid]["percentage"];
            if($value <= 40) {
                $styleCriteriaCount["development"]++;
            } elseif($value <= 70) {
                $styleCriteriaCount["potential"]++;
            } else {
                $styleCriteriaCount["signature"]++;
            }
        }

        $txt = self::t("comparison.bandwidth." . $scope);

        if ($styleCriteriaCount["signature"] >= 4) {
            $snippet = self::t("comparison.bandwidth.veryHigh." . $scope);
        } elseif ( $styleCriteriaCount["signature"] >= 2 && $styleCriteriaCount["potential"] >= 2 ) {
            $snippet = self::t("comparison.bandwidth.high." . $scope);
        } elseif( $styleCriteriaCount["signature"] == 0 && $styleCriteriaCount["potential"] <= 2) {
            $snippet = self::t("comparison.bandwidth.low." . $scope);
        } else {
            $snippet = self::t("comparison.bandwidth.medium." . $scope);
        }

        $txt = sprintf($txt, $snippet);

        return $txt;
    }

    public static function getStyleValue($score, $style) {
        $txt = self::t("styleValue");
        $labels = self::styleLabels();
        $key = array_search($style, self::styleLabels());
        $percentage = $score[$key]["percentage"];

        if ($percentage > 70) {
            $txt = self::t("styleValue.signature");
        } elseif($percentage > 40) {
            $txt = self::t("styleValue.potential");
        } else {
            $txt = self::t("styleValue.development");
        }

        return trim($txt);
    }

}
