<?php
namespace Xlnc\XlncTools\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class SessionUtility {

	static public function getAccessKey() {
		return $GLOBALS['TSFE']->fe_user->getKey('ses', 'accesskey');
	}

	static public function storeAccessKey($accessKey) {
		$GLOBALS['TSFE']->fe_user->setKey('ses', 'accesskey', $accessKey);
		$GLOBALS['TSFE']->storeSessionData();
	}

	static public function getToolId() {
		return $GLOBALS['TSFE']->fe_user->getKey('ses', 'toolid');
	}

	static public function storeToolId($toolId) {
		$GLOBALS['TSFE']->fe_user->setKey('ses', 'toolid', $toolId);
		$GLOBALS['TSFE']->storeSessionData();
	}

	static public function getCurrentSheet() {
		$objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
		$sheetRepository = $objectManager->get('Xlnc\XlncTools\Domain\Repository\SheetRepository');
		return $sheetRepository->setIgnoreStoragePage()->findByAccessKey(self::getAccessKey())->current();
	}

	static public function getCurrentTool() {
		$objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
		$toolRepository = $objectManager->get('Xlnc\XlncTools\Domain\Repository\ToolRepository');
		return $toolRepository->setIgnoreStoragePage()->findByUid((int) self::getToolId());
	}

	static public function isLanguageIdSet($toolId) {
		return $GLOBALS['TSFE']->fe_user->getKey('ses', 'languageId' . $toolId . '_' . self::getAccessKey()) != NULL;
	}

	static public function getLanguageId($toolId) {
		return $GLOBALS['TSFE']->fe_user->getKey('ses', 'languageId' . $toolId . '_' . self::getAccessKey());
	}

	static public function storeLanguageId($toolId, $languageId) {
		$GLOBALS['TSFE']->fe_user->setKey('ses', 'languageId' . $toolId . '_' . self::getAccessKey(), $languageId);
		$GLOBALS['TSFE']->storeSessionData();
	}

	/**
	 * @return TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Xlnc\XlncTools\Domain\Model\Answer>
	 *
	 * @param Xlnc\XlncTools\Domain\Model\Tool $selectedTool
	 * @param Xlnc\XlncTools\Domain\Model\Sheet $sheet optional - of not given, current session sheet is used
	 */
	static public function getToolAnswers($selectedTool, $sheet = NULL)
	{
		$answers = array();

		if(NULL === $sheet) $sheet = self::getCurrentSheet();
		if(NULL === $sheet || FALSE === $sheet) return FALSE;

		$i = 0;
	    if($sheet->getAnswers()) {
	    	foreach($sheet->getAnswers() as $answer) {
	    		if($answer->getItem() && $answer->getItem()->getParentid() == $selectedTool->getUid()) {
	    			$answers[] = $answer;
	    		}
	    		$i++;
	    	}
	    }
	    return $answers;
	}

	static public function getCurrentToolAnswers() {
		return self::getToolAnswers(self::getCurrentTool());
	}
}
?>
