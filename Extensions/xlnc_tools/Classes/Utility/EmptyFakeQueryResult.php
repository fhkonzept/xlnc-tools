<?php
namespace Xlnc\XlncTools\Utility;

/**
 * used to "empty" a relation in extbase domain models
 */
class EmptyFakeQueryResult {
    public function toArray() {
        return [];
    }
}
