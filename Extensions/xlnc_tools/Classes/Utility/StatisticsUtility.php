<?php
namespace Xlnc\XlncTools\Utility;

class StatisticsUtility {

	static public function mean($values) {
		if(!is_array($values)) {
			throw new \InvalidArgumentException( sprintf("mean expects parameter to be an array, got %s.", gettype($values)));
		}
		$count = count($values);
		$sum = 0.0;
		foreach($values as $value) {
			$sum += ((float)$value) / $count;
		}
		return $sum;
	}

	/**
	 * lifted from http://php.net/manual/es/function.stats-standard-deviation.php#114473
	 * @param  array $values
	 * @return float
	 */
	static public function standardDeviation($values) {
		if(!is_array($values)) {
			throw new \InvalidArgumentException( sprintf("mean expects parameter to be an array, got %s.", gettype($values)));
		}
		$count = count($values);
        if ($count === 0) return 0;

        $mean = self::mean($values);

        $sum = 0.0;
        foreach ($values as $value) {
            $d = ((float) $value) - $mean;
            $sum += $d * $d;
        };

        return sqrt($sum / $count);
	}

	static public function sum($values) {
		return array_sum($values);
	}

}
?>
