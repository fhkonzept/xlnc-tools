<?php
namespace Xlnc\XlncTools\Utility;

use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Utility for handling language changes within the same TSFE request
 *
 * Usage:
 * LanguageUtility::setLanguage($english);
 *
 * // ... TSFE now runs in english
 *
 * LanguageUtility::resetLanguage();
 *
 * // ... TSFE now has the language it had before
 *
 */
class LanguageUtility {
    protected static $originalLanguage = NULL;

    /**
     * set TSFE language to a specific given SystemLanguage, if given
     * @param \Xlnc\XlncTools\Domain\Model\SystemLanguage|NULL $language
     */
	public static function setLanguage($language) {
        if(NULL === $language) return;
        if(NULL === self::$originalLanguage) {
            self::$originalLanguage = [
                "config.language" => $GLOBALS["TSFE"]->config['config']['language'],
                "lang" => $GLOBALS["TSFE"]->lang,
                "sys_language_content" => $GLOBALS["TSFE"]->sys_language_content,
            ];
        }
        $GLOBALS["TSFE"]->config['config']['language'] = $language->getIsoCode();;
        $GLOBALS["TSFE"]->lang = $language->getIsoCode();
        $GLOBALS["TSFE"]->sys_language_content = $language->getUid();
        self::resetLocalizationUtility();
	}

    /**
     * resets the TSFE language to what it was before setLanguage was called
     * the first time (consecutive calls without resetting will not keep track
     * of TSFE language changes)
     */
	public static function resetLanguage() {
        if(NULL !== self::$originalLanguage) {

            $GLOBALS["TSFE"]->config['config']['language'] = self::$originalLanguage["config.language"];
            $GLOBALS["TSFE"]->lang = self::$originalLanguage["lang"];
            $GLOBALS["TSFE"]->sys_language_content = self::$originalLanguage["sys_language_content"];

            self::$originalLanguage = NULL;
        }
        self::resetLocalizationUtility();
	}

    /**
     * resets LocalizationUtility to respect the change in TSFE
     */
    protected static function resetLocalizationUtility() {
        $localization = new \ReflectionClass(LocalizationUtility::class);

        $llCache = $localization->getProperty('LOCAL_LANG');
        $llCache->setAccessible(TRUE);
        $llCache->setValue([]);
    }
}
