<?php
namespace Xlnc\XlncTools\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use Xlnc\XlncTools\Utility\AccessKeyUtility;

class SheetUtility {

	/**
	* @param string $emails
	* @param $team \Xlnc\XlncTools\Domain\Model\Team
	*/
	static public function getNewSheets($emails, $team) {

		$sheets = array();

		foreach($emails as $email) {
			$sheets[] = self::createSheet($email, $team);
		}

		return $sheets;
	}

	/**
	* @param string $email
	* @param $team \Xlnc\XlncTools\Domain\Model\Team
	*/
	static public function createSheet($email, $team) {

		$sheet = NULL;
		$accessKey = AccessKeyUtility::getNewAccessKey();
		
		if($accessKey != '') {

			$objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
			
			if($team->getEmail() && $email == $team->getEmail()) {
				$sheet = $objectManager->get('Xlnc\XlncTools\Domain\Model\LeaderSheet');
			} else {
				$sheet = $objectManager->get('Xlnc\XlncTools\Domain\Model\Sheet');
			}
			$sheet->setAccessKey($accessKey);
		}

		return $sheet;
	}

	/**
	* @param array<Xlnc\XlncTools\Domain\Model\Sheet> $sheets
	* @param Xlnc\XlncTools\Domain\Model\Team $team
	*/
	static public function addSheetsToTeam($sheets, $team) {

		$objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
		$teamRepository = $objectManager->get('Xlnc\XlncTools\Domain\Repository\TeamRepository');
		foreach($sheets as $sheet) {
			$team->addSheet($sheet);
		}

		$teamRepository->update($team);
		$teamRepository->persistAll();

		return $team;
	}

}
?>