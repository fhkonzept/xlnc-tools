<?php
namespace Xlnc\XlncTools\Utility;

use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class CombinedReportTextUtility {

    const XLF_FILE = 'reports.combined.';

    static public function managementStyleLabels() {
        return [
            12 	=> 'normative',
			4 	=> 'directive',
			13 	=> 'participative',
			10 	=> 'integrative',
			3 	=> 'coachive',
			9 	=> 'inspirative'
        ];
    }

    static public function climateLabels() {
        return [
            2 	=> 'autonomy',
			7 	=> 'credibility',
			14 	=> 'safety',
			5 	=> 'development',
			11 	=> 'clearness',
			6 	=> 'flexibility',
			1 	=> 'recognition',
			8 	=> 'identification',
			15 	=> 'teamspirit'
        ];
    }

    static public function managementStyleUids() {
        return array_keys(self::managementStyleLabels());
    }

    static public function climateUids() {
        return array_keys(self::climateLabels());
    }

    public static function t($label) {
        $translation = LocalizationUtility::translate(self::XLF_FILE . $label, 'xlnc_tools');
        if("" == $translation) {
            return $label;
        } else {
            return $translation;
        }
    }

    public static function listify($strings) {
        if(count($strings) == 1) {
            return $strings[0];
        } else {
            $and = self::t('general.and');
            $txt = implode(', ', array_slice($strings, 0, count($strings)-1));
            $txt .= ' ' . $and . ' ' . $strings[count($strings)-1];
        }

        return $txt;
    }

    static public function getClimateCriteriaText($type, $scope, $criteriaResults) {

		$lookup = self::climateLabels();

		$criteriaMatches = array();

		foreach(self::climateUids() as $uid) {
            $criteriaResult = $criteriaResults[$uid];
			if($criteriaResult['ranking'] == $scope ) {
				$criteriaMatches[] = $lookup[$uid];
			}
		}

		if(count($criteriaMatches) == 0) {
			return self::t('general.none');

		} else {

			$styleTxts = array();
			$descriptionTxts = array();
			foreach($criteriaMatches as $labelId) {
				$styleTxts[] = self::t('general.climates.' . $labelId);
			}

            $txt = self::t($type .'.' . $scope);
            if(count($criteriaMatches) === 1) {
                $singularTxt = self::t($type .'.' . $scope . ".singular");
                if(trim($singularTxt) != "") {
                    $txt = $singularTxt;
                }
            }
            $txt = sprintf($txt, self::listify($styleTxts));

            // get the three highest scores
            $percentages = [];
            $labelLookup = array_flip(self::climateLabels());
            foreach($criteriaMatches as $label) {
                $uid = $labelLookup[$label];
                $percentages[$uid] = $criteriaResults[$uid]["percentage"];
            }

            asort($percentages);
            $percentages = array_reverse($percentages, TRUE);
            #$percentages = array_reverse($percentages, TRUE);

            // $needAdditionalText = FALSE;
            // if(count($percentages) > 3) {
            //     $percentages = array_slice($percentages, 0, 3, TRUE);
            //     $needAdditionalText = TRUE;
            // }

            // reset $criteriaMatches
            $criteriaMatches = [];
            foreach($percentages as $uid => $_) {
                $criteriaMatches[$uid] = $lookup[$uid];
            }
            $styleTxts = [];
            foreach($criteriaMatches as $labelId) {
				$styleTxts[] = self::t('general.climates.' . $labelId);
			}

            // if($needAdditionalText) {
            //     $additionalText = self::t($type . "." . $scope . ".additional");
            //     $additionalText = sprintf($additionalText, self::listify($styleTxts));
            //
            //     $txt = $txt . " " . $additionalText;
            // }

            // foreach($criteriaMatches as $labelId) {
            //     $descriptionTxts[] = self::t('general.climates.descriptions.' . $labelId . "." . $scope);
            // }
            //
            // $descriptionText = self::t($type . "." . $scope . ".description");
            // $descriptionText = sprintf($descriptionText, self::listify($descriptionTxts));

            $txt = $txt . " " . $descriptionText;
		}

		return $txt;

	}

    static public function getManagementCriteriaText($type, $scope, $criteriaResults) {

		$txt = self::t($type .'.' . $scope);

		$and = self::t('general.and');
		$the = self::t('general.the');
		$asWellAs = self::t('general.as_well_as');

		$stylesLookup = self::managementStyleLabels();

		$criteriaMatches = array();

		foreach(self::managementStyleUids() as $uid) {
            $criteriaResult = $criteriaResults[$uid];
			if($criteriaResult['ranking'] == $scope ) {
				$criteriaMatches[] = $stylesLookup[$uid];
			}
		}

		if(count($criteriaMatches) == 0) {

			return self::t('general.none');

		} else {

			$styleTxts = array();
			$descriptionTxts = array();
			foreach($criteriaMatches as $labelId) {
				$styleTxts[] = self::t('general.styles.' . $labelId);
				$descriptionTxts[] = self::t('general.styles.descriptions.' . $labelId);
			}

            if(count($criteriaMatches) > 1) {
                $txtPlural = self::t($type .'.' . $scope . '.plural');
                if($txtPlural) {
                    $txt = $txtPlural;
                }
            }
			if(count($criteriaMatches) == 1) {

				$styleTxt = $styleTxts[0];
				$descriptionTxt = $descriptionTxts[0];

				$txt = sprintf($txt, $styleTxt, $descriptionTxt);

			} else if(count($criteriaMatches) == 2 || count($criteriaMatches) == 3) {

				$styleTxt = implode(', ' . $the . ' ', array_slice($styleTxts, 0, count($criteriaMatches)-1));
				$styleTxt .= ' ' . $and . ' ' . $the . ' ' . $styleTxts[count($criteriaMatches)-1];

				$descriptionTxt = implode(', ', array_slice($descriptionTxts, 0, count($criteriaMatches)-1));
				$descriptionTxt .= ' ' . $asWellAs . ' ' . $descriptionTxts[count($criteriaMatches)-1];

				$txt = sprintf($txt, $styleTxt, $descriptionTxt);

			} else if(count($criteriaMatches) == 4 || count($criteriaMatches) == 5) {

				$styleTxt = implode(', ' . $the . ' ', array_slice($styleTxts, 0, count($criteriaMatches)-1));
				$styleTxt .= ' ' . $and . ' ' . $the . ' ' . $styleTxts[count($criteriaMatches)-1];

				$descriptionTxt = implode(', ', array_slice($descriptionTxts, 0, 2));
				$descriptionTxt .= ' ' . $asWellAs . ' ' . $descriptionTxts[2];

				$txt2 = self::t( $type .'.' . $scope . '.also');

				$descriptionTxt2 = implode(', ', array_slice($descriptionTxts, 3, 1));
				if(count($criteriaMatches) == 5)
					$descriptionTxt2 .= ' ' . $asWellAs . ' ' . $descriptionTxts[4];

				$descriptionTxt2 = sprintf($txt2, $descriptionTxt2);

				$txt = sprintf($txt, $styleTxt, $descriptionTxt) . ' ' . $descriptionTxt2;

			} else if(count($criteriaMatches) == 6) {

				$txt = self::t( $type . '.' . $scope . '.all');

				$descriptionTxt = implode(', ', array_slice($descriptionTxts, 0, 2));
				$descriptionTxt .= ' ' . $asWellAs . ' ' . $descriptionTxts[2];

				$txt2 = self::t($type .'.' . $scope . '.also');

				$descriptionTxt2 = implode(', ', array_slice($descriptionTxts, 3, 2));
				$descriptionTxt2 .= ' ' . $asWellAs . ' ' . $descriptionTxts[5];

				$descriptionTxt2 = sprintf($txt2, $descriptionTxt2);

				$txt = sprintf($txt, $descriptionTxt) . ' ' . $descriptionTxt2;
			}

		}

		return $txt;

	}

    public static function getHomogenityText($homogenous, $type, $values, $scope = NULL) {
        $homogenousStyleUids = static::getStylesGreater60($homogenous, $values, $scope);

        $labelTexts = [];
        $labelsFunc = $type === 'management' ? "managementStyleLabels" : "climateLabels";
        $labelType = $type === 'management' ? "styles" : "climates";
        foreach(static::$labelsFunc() as $uid => $label) {
            if( in_array($uid, $homogenousStyleUids) ) {
                $labelTexts[] = static::t('general.' . $labelType . '.' . $label . '.alt');
            }
        }

        $and = self::t('general.and');
		$the = self::t('general.the');

        if(is_null($scope)) {
            $scope = 'all';
        }

        $base = $homogenous ? 'homogenity.' : 'heterogenity.';

        if(count($labelTexts) > 1) {
            $lastLabelText = array_pop($labelTexts);

            $labelList = implode(", ", $labelTexts) . " " . $and . " " . $lastLabelText;
            $txt = static::t($base . $type . '.' . $scope . '.plural' );
            $txt = sprintf($txt, $labelList);
        } elseif (count($labelTexts) == 0) {
            $txt = static::t($base . $type . '.' . $scope . '.none' );
        } else {
            $labelList = end($labelTexts);
            $txt = static::t($base . $type . '.' . $scope . '.singular');
            $txt = sprintf($txt, $labelList);
        }

        return $txt;
    }

    public static function getStylesGreater60($positive, $values, $scope = NULL) {
        $stylesGreater60 = [];

        // if a scope is given, only count styles in the given scope
        if(!is_null($scope)) {
            $values = array_map( function ($value) use ($scope) {
                return [$value[$scope]];
            }, $values);
        }

        // also, unset the 0 style that still somehow ends up here
        unset($values['0']);

        foreach( $values as $styleUid => $value ) {
            $valueOver60 = FALSE;

            foreach($value as $result) {
                if ( $result >= 60 ) {
                    $valueOver60 = TRUE;
                }
            }
            if ( ($valueOver60 === $positive) ) {
                $stylesGreater60[] = $styleUid;
            }
        }

        return $stylesGreater60;
    }

    static public function getOverallShouldText($shoulds, $lowVal = 40, $middleVal = 70, $scope = 'self') {
        $levelCount = [
            "low" => 0,
            "middle" => 0,
            "high" => 0
        ];



        // map agreements to levels and count how many agreements are in each level
        $levelMap = [];
        foreach(self::climateUids() as $uid) {
            if($shoulds[$uid]["percentage"] < $lowVal) {
                $levelMap[$uid] = "low";
                $levelCount["low"]++;
            } elseif ($shoulds[$uid]["percentage"] < $middleVal) {
                $levelMap[$uid] = "middle";
                $levelCount["middle"]++;
            } else {
                $levelMap[$uid] = "high";
                $levelCount["high"]++;
            }
        }

        // evaluate
        if (9 === $levelCount["high"]) {
            $labelId = 'allHigh';
        } elseif (9 === $levelCount["middle"]) {
            $labelId = 'allMiddle';
        } elseif (9 === $levelCount["low"]) {
            $labelId = 'allLow';
        } elseif ( 6 <= $levelCount["high"] && 0 === $levelCount["low"] ) {
            $labelId = 'high';
        } elseif ( 6 > $levelCount["middle"] && 0 === $levelCount["high"] ) {
            $labelId = 'low';
        } else {
            $labelId = 'medium';
        }

        $txt = self::t("should." . $labelId . '.' . $scope);

        return $txt;
    }

    static public function getHighestShouldText($scores, $scope = "self") {
        $percentages = [];

        foreach(self::climateUids() as $uid) {
            $percentages[$uid] = $scores[$uid]["percentage"];
        }

        asort($percentages);
        $percentages = array_reverse($percentages, TRUE);
        $percentages = array_slice($percentages, 0, 3, TRUE);

        $labels = [];
        $lookup = self::climateLabels();
        foreach($percentages as $uid => $_) {
            $labels[] = self::t("general.climates." . $lookup[$uid]);
        }

        $txt = self::t("highestShould." . $scope);
        $txt = sprintf($txt, self::listify($labels));

        return $txt;
    }

    static public function getHighestShouldDifferenceText($firstScore, $secondScore, $scope = "self") {
        $differences = [];
        foreach(self::climateUids() as $uid) {
            $differences[$uid] = abs($firstScore[$uid]["percentage"] - $secondScore[$uid]["percentage"]);
        }

        asort($differences);
        $differences = array_reverse($differences, TRUE);
        $differences = array_slice($differences, 0, 3, TRUE);

        $climateTxts = [];
        $lookup = self::climateLabels();
        foreach($differences as $uid => $_) {
            $climateTxts[] = self::t("general.climates." . $lookup[$uid]);
        }

        $txt = self::t("highestShouldDifference." . $scope);
        $txt = sprintf($txt, self::listify($climateTxts));

        return $txt;
    }

    static public function getLowestShouldDifferenceText($firstScore, $secondScore, $scope = "self") {

        $differences = [];
        foreach(self::climateUids() as $uid) {
            $differences[$uid] = abs($firstScore[$uid]["percentage"] - $secondScore[$uid]["percentage"]);
        }

        asort($differences);
        $differences = array_slice($differences, 0, 3, TRUE);

        $climateTxts = [];
        $lookup = self::climateLabels();
        foreach($differences as $uid => $_) {
            $climateTxts[] = self::t("general.climates." . $lookup[$uid]);
        }

        $txt = self::t("lowestShouldDifference." . $scope);
        $txt = sprintf($txt, self::listify($climateTxts));

        return $txt;
    }

    public static function getClimateValue($score, $climate) {
        $txt = self::t("styleValue");
        $labels = self::climateLabels();
        $key = array_search($climate, self::climateLabels());
        $percentage = $score[$key]["percentage"];

        if ($percentage > 70) {
            $txt = self::t("climateValue.signature");
        } elseif($percentage > 40) {
            $txt = self::t("climateValue.potential");
        } else {
            $txt = self::t("climateValue.development");
        }

        return trim($txt);
    }

}
