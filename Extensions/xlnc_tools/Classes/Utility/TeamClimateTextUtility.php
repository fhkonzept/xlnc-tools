<?php
namespace Xlnc\XlncTools\Utility;

use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class TeamClimateTextUtility {

    const XLF_FILE = 'reports.climate.';

    static public function climateLabels() {
        return [
            2 	=> 'autonomy',
			7 	=> 'credibility',
			14 	=> 'safety',
			5 	=> 'development',
			11 	=> 'clearness',
			6 	=> 'flexibility',
			1 	=> 'recognition',
			8 	=> 'identification',
			15 	=> 'teamspirit'
        ];
    }

    static public function climateUids() {
        return array_keys(self::climateLabels());
    }

    public static function t($label) {
        $translation = LocalizationUtility::translate(self::XLF_FILE . $label, 'xlnc_tools');
        return $translation;
    }

    public static function listify($strings) {
        if(count($strings) == 1) {
            return $strings[0];
        } else {
            $and = self::t('general.and');
            $txt = implode(', ', array_slice($strings, 0, count($strings)-1));
            $txt .= ' ' . $and . ' ' . $strings[count($strings)-1];
        }

        return $txt;
    }

    static public function getMaxAgreementClimateText($agreements) {

		$labelId = '';
		$currentMaxValue = 0;

		$stylesLookup = self::climateLabels();

		foreach($agreements as $id => $agreement) {
			if((int) $agreement['percentage'] > $currentMaxValue) {
				$labelId = $stylesLookup[$id];
				$currentMaxValue = (int) $agreement['percentage'];
			}
		}

		return self::t('general.climates.' . $labelId);
	}

    static public function getOverallAgreementText($agreements, $lowVal = 33, $middleVal = 66, $scope = 'agreement') {
        // initialize count
        $levelCount = [
            "low" => 0,
            "middle" => 0,
            "high" => 0
        ];

        // map agreements to levels and count how many agreements are in each level
        $levelMap = [];
        foreach(self::climateUids() as $uid) {
            if($agreements[$uid]['percentage'] < $lowVal) {
                $levelMap[$uid] = "low";
                $levelCount["low"]++;
            } elseif ($agreements[$uid]['percentage'] < $middleVal) {
                $levelMap[$uid] = "middle";
                $levelCount["middle"]++;
            } else {
                $levelMap[$uid] = "high";
                $levelCount["high"]++;
            }
        }

        // evaluate
        if ( 9 === $levelCount["low"] ) {
            $labelId = 'low';
        } elseif ( 0 === $levelCount["high"] && 0 < $levelCount["low"] ) {
            $labelId = 'low_to_middle';
        } elseif ( 9 === $levelCount["middle"] ) {
            $labelId = 'middle';
        } elseif ( 0 === $levelCount["low"] && 0 < $levelCount["middle"] ) {
            $labelId = 'middle_to_high';
        } elseif ( 9 === $levelCount["high"] ) {
            $labelId = 'high';
        } else {
            $labelId = 'high_variance';
        }
        $txt = self::t($scope . '.' . $labelId);
		return $txt;
    }

    static public function getOverallShouldText($shoulds, $lowVal = 40, $middleVal = 70, $scope = 'self') {
        $levelCount = [
            "low" => 0,
            "middle" => 0,
            "high" => 0
        ];



        // map agreements to levels and count how many agreements are in each level
        $levelMap = [];
        foreach(self::climateUids() as $uid) {
            if($shoulds[$uid]["percentage"] < $lowVal) {
                $levelMap[$uid] = "low";
                $levelCount["low"]++;
            } elseif ($shoulds[$uid]["percentage"] < $middleVal) {
                $levelMap[$uid] = "middle";
                $levelCount["middle"]++;
            } else {
                $levelMap[$uid] = "high";
                $levelCount["high"]++;
            }
        }

        // evaluate
        if (9 === $levelCount["high"]) {
            $labelId = 'allHigh';
        } elseif (9 === $levelCount["middle"]) {
            $labelId = 'allMiddle';
        } elseif (9 === $levelCount["low"]) {
            $labelId = 'allLow';
        } elseif ( 6 <= $levelCount["high"] && 0 === $levelCount["low"] ) {
            $labelId = 'high';
        } elseif ( 6 > $levelCount["middle"] && 0 === $levelCount["high"] ) {
            $labelId = 'low';
        } else {
            $labelId = 'medium';
        }

        $txt = self::t("should." . $labelId . '.' . $scope);

        return $txt;
    }

    static public function getHighestShouldText($scores, $scope = "self") {
        $percentages = [];

        foreach(self::climateUids() as $uid) {
            $percentages[$uid] = $scores[$uid]["percentage"];
        }

        asort($percentages);
        $percentages = array_reverse($percentages, TRUE);
        $percentages = array_slice($percentages, 0, 3, TRUE);

        $labels = [];
        $lookup = self::climateLabels();
        foreach($percentages as $uid => $_) {
            $labels[] = self::t("general.climates." . $lookup[$uid]);
        }

        $txt = self::t("highestShould." . $scope);
        $txt = sprintf($txt, self::listify($labels));

        return $txt;
    }

    static public function getHighestShouldDifferenceText($firstScore, $secondScore, $scope = "self") {

        $differences = [];
        foreach(self::climateUids() as $uid) {
            $differences[$uid] = abs($firstScore[$uid]["percentage"] - $secondScore[$uid]["percentage"]);
        }



        asort($differences);
        $differences = array_reverse($differences, TRUE);
        $differences = array_slice($differences, 0, 3, TRUE);

        $climateTxts = [];
        $lookup = self::climateLabels();
        foreach($differences as $uid => $_) {
            $climateTxts[] = self::t("general.climates." . $lookup[$uid]);
        }

        $txt = self::t("highestShouldDifference." . $scope);
        $txt = sprintf($txt, self::listify($climateTxts));

        return $txt;
    }

    static public function getCriteriaText($type, $scope, $criteriaResults) {

		$lookup = self::climateLabels();

		$criteriaMatches = array();

		foreach(self::climateUids() as $uid) {
            $criteriaResult = $criteriaResults[$uid];
			if($criteriaResult['ranking'] == $scope ) {
				$criteriaMatches[] = $lookup[$uid];
			}
		}

		if(count($criteriaMatches) == 0) {
			return self::t('general.none');

		} else {

			$styleTxts = array();
			$descriptionTxts = array();
			foreach($criteriaMatches as $labelId) {
				$styleTxts[] = self::t('general.climates.' . $labelId);
			}

            $txt = self::t($type .'.' . $scope);
            if(count($criteriaMatches) === 1) {
                $singularTxt = self::t($type .'.' . $scope . ".singular");
                if(trim($singularTxt) != "") {
                    $txt = $singularTxt;
                }
            }
            $txt = sprintf($txt, self::listify($styleTxts));

            // get the three highest scores
            $percentages = [];
            $labelLookup = array_flip(self::climateLabels());
            foreach($criteriaMatches as $label) {
                $uid = $labelLookup[$label];
                $percentages[$uid] = $criteriaResults[$uid]["percentage"];
            }

            asort($percentages);
            $percentages = array_reverse($percentages, TRUE);
            #$percentages = array_reverse($percentages, TRUE);

            $needAdditionalText = FALSE;
            if(count($percentages) > 3) {
                $percentages = array_slice($percentages, 0, 3, TRUE);
                $needAdditionalText = TRUE;
            }

            // reset $criteriaMatches
            $criteriaMatches = [];
            foreach($percentages as $uid => $_) {
                $criteriaMatches[$uid] = $lookup[$uid];
            }
            $styleTxts = [];
            foreach($criteriaMatches as $labelId) {
				$styleTxts[] = self::t('general.climates.' . $labelId);
			}

            if($needAdditionalText) {
                $additionalText = self::t($type . "." . $scope . ".additional");
                $additionalText = sprintf($additionalText, self::listify($styleTxts));

                $txt = $txt . " " . $additionalText;
            }

            foreach($criteriaMatches as $labelId) {
                $descriptionTxts[] = self::t('general.climates.descriptions.' . $labelId . "." . $scope);
            }

            $descriptionText = self::t($type . "." . $scope . ".description");
            $descriptionText = sprintf($descriptionText, self::listify($descriptionTxts));

            $txt = $txt . " " . $descriptionText;
		}

		return $txt;

	}

    public static function getComparisonText($firstScore, $secondScore, $from, $to, $scope) {
        $differences = [];
        foreach(self::climateUids() as $uid) {
            $d = abs($firstScore[$uid]["percentage"] - $secondScore[$uid]["percentage"]);
            if($from <= $d && $d < $to) {
                $differences[$uid] = $d;
            }

        }

        $climateTxts = [];
        $lookup = self::climateLabels();
        foreach($differences as $uid => $_) {
            $climateTxts[] = self::t("general.climates." . $lookup[$uid]);
        }

        $txt = self::t("comparison." . $scope);
        if(count($differences) === 0) {
            return self::t("general.none");
        } elseif (count($differences) === 1) {
            $txt = self::t("comparison.singular." . $scope);
        }
        $txt = sprintf($txt, self::listify($climateTxts));

        return $txt;
    }

    public static function getOverallComparison($firstScore, $secondScore, $scope) {

        $higherFirst = 0;
        $higherSecond = 0;
        foreach(self::climateUids() as $uid) {
            if($firstScore[$uid]["percentage"] > $secondScore[$uid]["percentage"])
                $higherFirst++;
            if($firstScore[$uid]["percentage"] < $secondScore[$uid]["percentage"])
                $higherSecond++;
        }

        if ( 0 === $higherFirst && 0 === $higherSecond) {
            $txt = self::t("comparsion.same." . $scope);
        } else {
            $txt = self::t("comparison." . $scope);

            if ( 6 === $higherFirst || 6 == $higherSecond ) {
                $snippetOne = self::t("comparison.consistently");
            } elseif( abs($higherFirst - $higherSecond) > 3 ) {
                $snippetOne = self::t("comparison.mostly");
            } elseif ( abs($higherFirst - $higherSecond) > 0 ) {
                $snippetOne = self::t("comparison.slightly");
            } else {
                $snippetOne = self::t("comparison.inconsistently");
            }

            if ( $higherFirst > $higherSecond) {
                $snippetTwo = self::t("comparison.higher." . $scope);
            } elseif ( $higherSecond > $higherFirst ) {
                $snippetTwo = self::t("comparison.lower." . $scope);
            } else {
                // ===
                $snippetTwo = self::t("comparison.neither." . $scope);
            }

            $txt = sprintf($txt, $snippetOne, $snippetTwo);
        }

        return $txt;
    }

    public static function getMaxComparisonDifference($firstScore, $secondScore, $scope) {

        $differences = [];
        foreach(self::climateUids() as $uid) {
            $differences[$uid] = abs($firstScore[$uid]["percentage"] - $secondScore[$uid]["percentage"]);
        }

        asort($differences);
        $differences = array_slice($differences, count($differences) - 2, count($differences) - 1, TRUE);

        $snippets = [];
        $labels = self::climateLabels();
        foreach($differences as $key => $_) {
            $snippets[] = self::t("general.styles." . $labels[$key] . ".alt");
        }
        $txt = self::t("comparison.difference." . $scope);

        $txt = sprintf($txt,
            $snippets[1],
            $snippets[0]
        );

        return $txt;
    }

    public static function getBandwidth($score, $scope) {
        $styleCriteriaCount = [
            "development" => 0,
            "potential" => 0,
            "signature" => 0
        ];

        foreach(self::climateUids() as $uid) {
            $value = $score[$uid]["percentage"];
            if($value <= 40) {
                $styleCriteriaCount["development"]++;
            } elseif($value <= 70) {
                $styleCriteriaCount["potential"]++;
            } else {
                $styleCriteriaCount["signature"]++;
            }
        }

        $txt = self::t("comparison.bandwidth." . $scope);

        if ( $styleCriteriaCount["signature"] >= 2 && $styleCriteriaCount["potential"] >= 2 ) {
            $snippet = self::t("comparison.bandwidth.high." . $scope);
        } elseif( $styleCriteriaCount["signature"] == 0 && $styleCriteriaCount["potential"] <= 2) {
            $snippet = self::t("comparison.bandwidth.low." . $scope);
        } else {
            $snippet = self::t("comparison.bandwidth.medium." . $scope);
        }

        $txt = sprintf($txt, $snippet);

        return $txt;
    }

    public static function getClimateValue($score, $climate) {
        $txt = self::t("styleValue");
        $labels = self::climateLabels();
        $key = array_search($climate, self::climateLabels());
        $percentage = $score[$key]["percentage"];

        if ($percentage > 70) {
            $txt = self::t("climateValue.signature");
        } elseif($percentage > 40) {
            $txt = self::t("climateValue.potential");
        } else {
            $txt = self::t("climateValue.development");
        }

        return trim($txt);
    }

}
