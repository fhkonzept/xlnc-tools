<?php
namespace Xlnc\XlncTools\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility,
	TYPO3\CMS\Core\Utility\ExtensionManagementUtility,
	TYPO3\CMS\Fluid\View\StandaloneView,
	TYPO3\CMS\Extbase\Object\ObjectManager;

use Xlnc\XlncTools\Domain\Repository\MailLogRepository,
	Xlnc\XlncTools\Domain\Model\MailLog;

use Xlnc\XlncTools\Domain\Model\LeaderSheet;
use Xlnc\XlncTools\Domain\Model\Sheet;

use	Xlnc\XlncTools\Uri\UriBuilder;

class MailUtility {

	public static function sendInvitation($invitation, $sheet) {
		$objectManager = GeneralUtility::makeInstance(ObjectManager::class);

		$mailView = $objectManager->get(StandaloneView::class);
		$mailView->setTemplatePathAndFilename(
			ExtensionManagementUtility::extPath('xlnc_tools') . 'Resources/Private/Templates/Mail/Invite.html'
		);

		$extConf = @unserialize ( $GLOBALS["TYPO3_CONF_VARS"]['EXT']['extConf']['xlnc_tools'] );

		$uri = $objectManager->get(UriBuilder::class)->buildLoginUrl($invitation);

		if(trim($extConf["httpUsername"]) != "") {
			// if given, we also add any "guard" authentication directly in the mail message.
			$parts = explode("://", $uri);
			$uri = $parts[0] . "://" . trim($extConf["httpUsername"]) . ":" . trim($extConf["httpPassword"]) . "@" . $parts[1];
		}


		$invitationTextMap = [
	        LeaderSheet::class => 'Tx_XlncTools_LeaderInvitationText',
	        Sheet::class => 'Tx_XlncTools_InvitationText'
	    ];

		$mailView->assignMultiple(
			array(
				'invitation' => $invitation,
				'text'		=> $invitation->getTeam()->getTextInLanguage($invitationTextMap[$invitation->getSheetType()], $invitation->getLanguage()),
				'email' => $invitation->getAddress(),
				'accessKey'	=> $sheet->getAccessKey(),
				'accessUri' => $uri,
			)
		);

		return $mailView->render();
	}

	static public function sendReport($team, $tool, $filePath) {
		$objectManager = GeneralUtility::makeInstance(ObjectManager::class);

		$mailView = $objectManager->get(StandaloneView::class);
		$mailView->setTemplatePathAndFilename(
			ExtensionManagementUtility::extPath('xlnc_tools') . 'Resources/Private/Templates/Mail/SendReport.html'
		);

		$mailView->assignMultiple(
			array(
				'team' => $team,
				'tool' => $tool,
				'filePath' => $filePath
			)
		);

		return $mailView->render();
	}

	/**
	* @param \Xlnc\XlncTools\Domain\Model\Team $team
	*/
	static public function sendReminderMails($team) {

		$extConf = @unserialize ( $TYPO3_CONF_VARS ['EXT'] ['extConf'] ['xlnc_tools'] );

		$objectManager = GeneralUtility::makeInstance(ObjectManager::class);
		$mailLogRepository = $objectManager->get(MailLogRepository::class);

		$mailInvitations = $team->getMailInvitations();
		$mailLogRepository = $objectManager->get(MailLogRepository::class);

		$neededInvitations = array_filter($mailInvitations, function ($invitation) use ($team, $mailLogRepository) {
			$email = $invitation->getAddress();
			$mailLog = $mailLogRepository->setIgnoreStoragePage()->findByTeamAndEmailAndScope($team, $email, MailLog::SCOPE_REMINDER);
			return FALSE === $mailLog;
		});

		$sentEmails = [];

		if(!empty($neededInvitations)) {
			// teams might not have any email addresses attached, but if they do ...

			// add generic notification addresses
			$notificationAddress = filter_var($extConf['notificationAddress'], FILTER_SANITIZE_EMAIL);
			if($notificationAddress !== FALSE) {
				$emails[] = $notificationAddress;
			}

			$project = $team->getProject();
			// add project owner's address, if given
			//
			$owner = $project->getOwner();
			if(NULL !== $owner) {

				$ownerAddress = filter_var($owner->getEmail(), FILTER_SANITIZE_EMAIL);
				if($ownerAddress !== FALSE) {
					$emails[] = $ownerAddress;
				}
			}

			$mailTemplatePath = $extConf['reminderMailTemplate'] ? $extConf['reminderMailTemplate'] : ExtensionManagementUtility::extPath('xlnc_tools') . 'Resources/Private/Templates/Mail/Reminder.html';

			$invitationTextMap = [
				LeaderSheet::class => 'Tx_XlncTools_LeaderInvitationText',
				Sheet::class => 'Tx_XlncTools_InvitationText'
			];

			foreach($neededInvitations as $invitation) {

				$mailView = $objectManager->get(StandaloneView::class);
				$mailView->setTemplatePathAndFilename($mailTemplatePath);
				$mailView->assign('team', $team);
				$email = $invitation->getAddress();
				$mailView->assign('email', $email);

				$reminderTextObject = $invitation->getTeam()->getTextInLanguage('Tx_XlncTools_ReminderText', $invitation->getLanguage());
				$reminderText = str_replace('###INVITATION_DATE###', $invitation->getCrdate()->format('d.m.Y'), $reminderTextObject->getBodyText());

				$mailView->assign('reminderText', $reminderText);
				LanguageUtility::setLanguage($invitation->getLanguage());

				if($mailView->render()) {

					$sentEmails[] = $email;

					$mailLog = $objectManager->get(MailLog::class);
					$mailLog->setTeam($team->getUid());
					$mailLog->setEmail($email);
					$mailLog->setScope(MailLog::SCOPE_REMINDER);

					$mailLogRepository->add($mailLog);
					$mailLogRepository->persistAll();
				}

				LanguageUtility::resetLanguage();
			}
		}

		return $sentEmails;
	}

}
?>
