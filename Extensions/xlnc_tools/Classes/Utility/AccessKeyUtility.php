<?php
namespace Xlnc\XlncTools\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class AccessKeyUtility {

	static protected $keyCharTable = array('A','B','C','E','F','G','H','K','M','N','P','R','S','T','W','X','Y','Z','2','3','4','5','6','7','8','9');
	static protected $keyLength = 6;

	/**
	*
	*/
	static public function getNewAccessKey() {
		$accessKey = self::generateAccessKey();
		while(TRUE === self::accessKeyExists($accessKey)) {
			$accessKey = self::generateAccessKey();
		}
		return $accessKey;
	}

	/**
	* @param string $accessKey
	*/
	static public function accessKeyExists($accessKey) {
		$objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
		$sheetRepository = $objectManager->get('Xlnc\XlncTools\Domain\Repository\SheetRepository');
		return $sheetRepository->setIgnoreStoragePage()->findByAccessKey($accessKey)->count() > 0;
	}

	/**
	*
	*/
	static protected function generateAccessKey() {
		$accessKey = '';
		shuffle(self::$keyCharTable);
		$randIndices = array_rand(self::$keyCharTable, self::$keyLength);
		foreach($randIndices as $randIndex) {
			$accessKey .= self::$keyCharTable[$randIndex];
		}
		return $accessKey;
	}	
}
?>