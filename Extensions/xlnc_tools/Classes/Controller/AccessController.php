<?php
namespace Xlnc\XlncTools\Controller;

use Xlnc\XlncTools\Utility\SheetUtility;
use Xlnc\XlncTools\Utility\MailUtility;
use Xlnc\XlncTools\Utility\PdfUtility;
use Xlnc\XlncTools\Utility\SessionUtility;

class AccessController extends \Xlnc\XlncTools\Controller\BaseController {

	protected $validMessageIds = [
		"logoutSuccessful",
		"teamLocked"
	];

	/**
	 * @param  string $messageId message key to be displayed.
	 */
	public function formAction($messageId = NULL) {
		// check that the message is allowed and this is indeed a redirect here
		if(in_array($messageId, $this->validMessageIds) && !$this->request->getOriginalRequest()) {
			$this->view->assign("messageId", $messageId);
		}
	}

	/**
	 * @param string $code
	 * @validate $code Xlnc\XlncTools\Validation\Validator\AccesscodeValidator
	 */
	public function authenticateAction($code) {
	
		SessionUtility::storeAccessKey($code);
		
		if ( $this->extConf['landingPage'] == 1 ) {
			$this->redirect('landingPage');
		} else {
			$this->redirect('dashboard', 'Tool', 'xlncTools', array(), (int) $this->extConf['toolPageId']);
		};		
	}
	
	
	public function landingPageAction() {
		if ( $this->extConf['landingPage'] == 1 ) {
			$sheet = SessionUtility::getCurrentSheet();
		
			$this->view->assign("toolPageId", (int) $this->extConf['toolPageId']);
			$this->view->assign("sheet",  $sheet);
		} else {
			$this->redirect('dashboard', 'Tool', 'xlncTools', array(), (int) $this->extConf['toolPageId']);
		};	
		
		
	}

}