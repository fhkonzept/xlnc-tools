<?php
namespace Xlnc\XlncTools\Controller;

use Xlnc\XlncTools\Utility\SessionUtility;

use TYPO3\CMS\Core\Imaging\Icon,
	TYPO3\CMS\Core\Imaging\IconFactory;

class ToolController extends \Xlnc\XlncTools\Controller\BaseController {

	/**
     * @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher
     */
    protected $sigSlotDispatcher;

    /**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Sheet\AgeRepository
	 * @inject
	 */
	protected $ageRepository = NULL;

    /**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Sheet\CountryRepository
	 * @inject
	 */
	protected $countryRepository = NULL;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Sheet\BusinessunitRepository
	 * @inject
	 */
	protected $businessunitRepository = NULL;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Sheet\EducationRepository
	 * @inject
	 */
	protected $educationRepository = NULL;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Sheet\ExperienceRepository
	 * @inject
	 */
	protected $experienceRepository = NULL;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Sheet\GenderRepository
	 * @inject
	 */
	protected $genderRepository = NULL;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Sheet\GraduationRepository
	 * @inject
	 */
	protected $graduationRepository = NULL;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Sheet\ManagementexperienceRepository
	 * @inject
	 */
	protected $managementexperienceRepository = NULL;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Sheet\ManagementlevelRepository
	 * @inject
	 */
	protected $managementlevelRepository = NULL;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\SystemLanguageRepository
	 * @inject
	 */
	protected $systemLanguageRepository;

	protected $totalPages = 0;
	protected $currentPage = 0;
	protected $isTextAnswersPage = FALSE;


	public function dashboardAction()
	{
		$sheet = SessionUtility::getCurrentSheet();

		if(!$sheet) {
			$this->redirect('form', 'Access', 'XlncTools', array(), (int) $this->extConf['authenticatePageId']);
		}

        $this->checkTeamLock($sheet);

		$toolsData = array();
		$tools = $sheet->getTeam()->getProject()->getTools();

		foreach($tools as $tool) {
			$countFinishedItems = $sheet->getSheetAnswersFinishedNumber($tool);
			$countRequiredItems =  $sheet->getTxExtbaseType() == 'Tx_XlncTools_LeaderSheet' ? $tool->getRequiredItemsNumber() :  count($tool->getItems());
			$toolsData[] = array(
				'tool'					=> $tool,
				'countFinishedItems'	=> $countFinishedItems,
				'countRequiredItems'	=> $countRequiredItems,
				'percentFinished'		=> floor(($countFinishedItems/$countRequiredItems)*100)
				);
		}

		$totalTools = count($toolsData);
		$finishedTools = count(array_filter( $toolsData, function ($data) {
			return $data["countFinishedItems"] >= $data["countRequiredItems"];
		}));
		$startedTools = count(array_filter( $toolsData, function ($data) {
			return $data["countFinishedItems"] > 0;
		}));

		if ( $totalTools <= $finishedTools) {
			$dashboardStatus = "all_tools_finished";
		} else if ( 0 < $finishedTools)	{
			$dashboardStatus = "one_tool_finished";
		} else if ( 0 < $startedTools) {
			$dashboardStatus = "one_tool_started";
		} else {
			$dashboardStatus = "no_tool_started";
		}

		$this->view->assignMultiple(
			array(
				'sheet' 	=> $sheet,
				'toolsData'	=> $toolsData,
				'dashboardStatus' => $dashboardStatus
			)
		);
	}
	
	
	
	public function formAction()
	{
		if(!$this->request->hasArgument('tool') || (int) $this->request->getArgument('tool') <= 0) {
			$this->forward('dashboard');
		}

		SessionUtility::storeToolId((int) $this->request->getArgument('tool'));

		$sheet = SessionUtility::getCurrentSheet();
		$tool = SessionUtility::getCurrentTool();

		if(!$sheet || !$tool) {
			$this->redirect('form', 'Access', 'xlncTools', array(), (int) $this->extConf['authenticatePageId']);
		}

        $this->checkTeamLock($sheet);

		if($sheet->getSheetPersonalDataFinished()) {
			if($tool->getDisplayIntro())
				$this->forward('intro');
			else
				$this->forward('survey');
		}

		$countries = $this->countryRepository->setIgnoreStoragePage()->findAll()->toArray();

		// find the country with minimal "sorting" and put it at the top
		$sortings = array_map(function ($country) {
			return $country->getSorting();
		}, $countries);

		$minSorting = min($sortings);
		$minIndex = array_search($minSorting, $sortings);

		$firstOption = $countries[$minIndex];
		array_splice($countries, $minIndex, 1);

		$countries = array_merge([$firstOption], $countries);

		$this->view->assignMultiple(
			array(
				'sheet' 				=> $sheet,
				'tool'					=> $tool,
				'ages'					=> $this->ageRepository->setIgnoreStoragePage()->findAll(),
				'genders'				=> $this->genderRepository->setIgnoreStoragePage()->findAll(),
				'graduations'			=> $this->graduationRepository->setIgnoreStoragePage()->findAll(),
				'experiences'			=> $this->experienceRepository->setIgnoreStoragePage()->findAll(),
				'educations'			=> $this->educationRepository->setIgnoreStoragePage()->findAll(),
				'managementExperiences'	=> $this->managementexperienceRepository->setIgnoreStoragePage()->findAll(),
				'managementLevels'		=> $this->managementlevelRepository->setIgnoreStoragePage()->findAll(),
				'businessUnits'			=> $this->businessunitRepository->setIgnoreStoragePage()->findAll(),
				'countries'				=> $countries
				)
			);
	}

	/**
	* @param \Xlnc\XlncTools\Domain\Model\Sheet $sheet
	* @validate $sheet Xlnc\XlncTools\Validation\Validator\SheetValidator
	*/
	public function updateSheetAction(\Xlnc\XlncTools\Domain\Model\Sheet $sheet) {

		$this->sheetRepository->update($sheet);
		$tool = SessionUtility::getCurrentTool();

		if(!$sheet || !$tool) {
			$this->redirect('form', 'Access', 'xlncTools', array(), (int) $this->extConf['authenticatePageId']);
		}

        $this->checkTeamLock($sheet);

		if($tool->getDisplayIntro())
			$this->forward('intro');
		else
			$this->forward('survey');
	}

	/**
	*/
	public function introAction()
	{
		
		$sheet = SessionUtility::getCurrentSheet();
		$tool = SessionUtility::getCurrentTool();

		$languages = $this->systemLanguageRepository->findAllAndDefault();

		$currentLanguage = $this->systemLanguageRepository->getCurrentLanguage();

		$iconFactory = $this->objectManager->get(IconFactory::class);
		$uriBuilder = $this->uriBuilder;

		$languages = array_map(function ($language) use ($iconFactory, $uriBuilder, $tool, $sheet) {
			$uri = $uriBuilder
				->reset()
				->uriFor('survey', ["language" => $language, "tool" => $tool, "sheet" => $sheet]);
			$flagIcon = $iconFactory->getIcon('flags-' . $language->getFlag(), Icon::SIZE_SMALL)->render();
			return [
				"uid" => $language->getUid(),
				"title" => $language->getTitle(),
				"flagIcon" => $flagIcon,
				"uri" => $uri
			];
		}, $languages);

		if(!$sheet || !$tool) {
			$this->redirect('form', 'Access', 'xlncTools', array(), (int) $this->extConf['authenticatePageId']);
		}

		if($sheet->getSheetAnswersStarted($tool)) {
			$this->forward('survey');
		}

		$this->view->assignMultiple(
			array(
				'altIntro' => $this->settings['altIntro'],
				'sheet' => $sheet,
				'tool' => $tool,
				'languages' => $languages,
				'currentLanguage' => $currentLanguage,
				'sheet' => $sheet,
				)
			);
	}

	/**
	*/
	public function surveyAction()
	{
		$sheet = SessionUtility::getCurrentSheet();
		$tool = SessionUtility::getCurrentTool();

		if(!$sheet || !$tool) {
			$this->redirect('form', 'Access', 'xlncTools', array(), (int) $this->extConf['authenticatePageId']);
		}

        $this->checkTeamLock($sheet);

		if($sheet->getSheetAnswersFinished($tool)) {
			$this->forward('dashboard');
		}

		$data = array(
        	'tool' 		=> $tool,
        	'sheet'		=> $sheet,
        	'request'	=> $this->request
        	);
        $this->emitBeforeSurveyActionSignal($data);

		if(empty($this->request->getOriginalRequestMappingResults()->forProperty('sheet')->getErrors()))
			$this->prepareSheetForSurvey($sheet);

		$this->view->assignMultiple(
			array(
				'sheet' 			=> $sheet,
				'answers' 			=> SessionUtility::getCurrentToolAnswers(),
				'tool' 				=> $tool,
				'totalPages'		=> $this->totalPages,
				'currentPage'		=> $this->currentPage,
				'isTextAnswersPage'	=> $this->isTextAnswersPage
				)
			);
	}

	public function initializeUpdateSheetAnswersAction() {

        if($this->request->hasArgument('sheet')) {
            $sheetArg = $this->request->getArgument('sheet');
        } else {
            $this->redirect('survey');
        }


		if(!empty($sheetArg['answers'])) {

			$sheet = SessionUtility::getCurrentSheet();

			if(is_object($sheet)) {

				$propertyMappingConfiguration = $this->arguments->getArgument('sheet')->getPropertyMappingConfiguration()->getConfigurationFor('answers');

				foreach($sheet->getAnswers() as $answer) {
					if(!array_key_exists($answer->getUid(), $sheetArg['answers'])) {
						$propertyMappingConfiguration->allowProperties($answer->getUid());
						$sheetArg['answers'][$answer->getUid()]['__identity'] = $answer->getUid();
					}
				}

				$this->request->setArgument('sheet', $sheetArg);
			}
		}

	}

	/**
	* @param \Xlnc\XlncTools\Domain\Model\Sheet $sheet
	* @validate $sheet Xlnc\XlncTools\Validation\Validator\SheetAnswersValidator
	*/
	public function updateSheetAnswersAction(\Xlnc\XlncTools\Domain\Model\Sheet $sheet) {

		$tool = SessionUtility::getCurrentTool();

		if(!$sheet || !$tool) {
			$this->redirect('form', 'Access', 'xlncTools', array(), (int) $this->extConf['authenticatePageId']);
		}

        $this->checkTeamLock($sheet);

		$answers = SessionUtility::getCurrentToolAnswers();

		$answersAlreadyGiven = FALSE;
		foreach($answers as $answer) {
			if($answer->getStatus() == 1) {
				$answer->setStatus(2);
			}
		}

		$this->sheetRepository->update($sheet);
		$this->sheetRepository->persistAll();

		if($sheet->getSheetAnswersFinished($tool) === TRUE) {

			// SignalSlot for toolStarted
			$data = array(
				'sheet'	=> $sheet,
				'tool'	=> $tool
				);
			$this->emitToolFinishedActionSignal($data);
			//
			// if($tool->getDisplayOutro())
			// 	$this->forward('outro');
			// else
			// 	$this->forward('dashboard');
			//
			$this->redirect('dashboard');

		} else {
			$this->forward('survey');
		}
	}

	public function interruptAction() {
		SessionUtility::storeToolId(0);
		$this->forward('dashboard');
	}

    /**
     * @param  string $messageId key of message to show on login form
     */
	public function logoutAction() {
        SessionUtility::storeToolId(0);
		SessionUtility::storeAccessKey($code);
        $this->redirectToLogin("logoutSuccessful");
	}

	/**
	*/
	public function outroAction() {

		$sheet = SessionUtility::getCurrentSheet();
		$tool = SessionUtility::getCurrentTool();

		if(!$sheet || !$tool) {
			$this->redirect('form', 'Access', 'xlncTools', array(), (int) $this->extConf['authenticatePageId']);
		}

		if(!$sheet->getSheetAnswersFinished($tool)) {
			$this->forward('dashboard');
		}

		SessionUtility::storeToolId(0);

		$this->view->assignMultiple(
			array(
				'sheet' => $sheet,
				'tool' => $tool
				)
			);
	}

	protected function prepareSheetForSurvey($sheet) {

		$tool = SessionUtility::getCurrentTool();
		$toolHasTextAnswers = FALSE;

		if(count(SessionUtility::getCurrentToolAnswers()) == 0) {

			foreach($tool->getItems() as $item) {

				// Main question
				if( $item->getTxExtbaseType() != 'Tx_XlncTools_TextItem' || ( $item->getTxExtbaseType() == 'Tx_XlncTools_TextItem' && $sheet->getTxExtbaseType() == 'Tx_XlncTools_Sheet')) {
					$blankMainAnswer = $this->objectManager->get('Xlnc\XlncTools\Domain\Model\Answer');
					$blankMainAnswer->setItem($item);
					$blankMainAnswer->setAnswerIndex(1);
					$sheet->addAnswer($blankMainAnswer);
				}

				if($item->getTxExtbaseType() != 'Tx_XlncTools_TextItem') {

					// Compare question
					$blankCompareAnswer = $this->objectManager->get('Xlnc\XlncTools\Domain\Model\Answer');
					$blankCompareAnswer->setItem($item);
					$blankCompareAnswer->setAnswerIndex(2);
					$sheet->addAnswer($blankCompareAnswer);

				}

			}

			// SignalSlot for toolStarted
			$data = array(
				'sheet'	=> $sheet,
				'tool'	=> $tool
				);
			$this->emitToolStartedActionSignal($data);

		}

		$itemsPerPage = $tool->getItemsPerPage() > 0 ? $tool->getItemsPerPage() : 5;
		$answersPerPage = $itemsPerPage * 2;

		$i = 0;
		$j = 0;
		$k = 0;

		$scaleAnswersFinished = TRUE;

		foreach(SessionUtility::getCurrentToolAnswers() as $answer) {

			if($answer->getItem()->getTxExtbaseType() != 'Tx_XlncTools_TextItem') {

				if(($i < $answersPerPage && $answer->getStatus() == 0) || $answer->getStatus() == 1)  {
					$answer->setStatus(1);
					$scaleAnswersFinished = FALSE;
					$i++;
				} else if($answer->getStatus() == 2) {
					$k++;
				}
				$j++;

			} else {

				$toolHasTextAnswers = TRUE;

			}

		}

		$this->currentPage = ceil($k/($answersPerPage)) + 1;
		$this->totalPages = ceil($j/($answersPerPage));

		if($toolHasTextAnswers && $sheet->getTxExtbaseType() == 'Tx_XlncTools_Sheet') {

			$this->totalPages += 1;

			if($scaleAnswersFinished) {

				$this->currentPage = $this->totalPages;
				$this->isTextAnswersPage = TRUE;

				foreach(SessionUtility::getCurrentToolAnswers() as $answer) {

					if($answer->getItem()->getTxExtbaseType() == 'Tx_XlncTools_TextItem') {

						$answer->setStatus(1);

					}

				}

			}
		}

		$this->sheetRepository->update($sheet);
		$this->sheetRepository->persistAll();

	}

	/************************************************************************************************************
	* Signal/Slot
	*/

	/**
     * Emits the beforeSurvey action signal
     *
     * @param array $data Array with data
     * @return int ID of item
     */
    protected function emitBeforeSurveyActionSignal($data) {
        $res = $this->getSignalSlotDispatcher()->dispatch(\Xlnc\XlncTools\Controller\ToolController::class, 'beforeSurvey', array($this, $data));
        return $res[0];
    }

    /**
     * Emits the toolStarted action signal
     *
     * @param array $data Array with data
     * @return int ID of item
     */
    protected function emitToolStartedActionSignal($data) {
        $res = $this->getSignalSlotDispatcher()->dispatch(\Xlnc\XlncTools\Controller\ToolController::class, 'toolStarted', array($this, $data));
        return $res[0];
    }

    /**
     * Emits the toolFinished action signal
     *
     * @param array $data Array with data
     * @return int ID of item
     */
    protected function emitToolFinishedActionSignal($data) {
        $res = $this->getSignalSlotDispatcher()->dispatch(\Xlnc\XlncTools\Controller\ToolController::class, 'toolFinished', array($this, $data));
        return $res[0];
    }

    /**
     * Get the SignalSlot dispatcher
     *
     * @return \TYPO3\CMS\Extbase\SignalSlot\Dispatcher
     */
    protected function getSignalSlotDispatcher() {
        if (!isset($this->sigSlotDispatcher)) {
            $this->sigSlotDispatcher = $this->objectManager->get('TYPO3\\CMS\\Extbase\\SignalSlot\\Dispatcher');
        }
        return $this->sigSlotDispatcher;
    }

    protected function checkTeamLock($sheet) {
        $team = $sheet->getTeam();
        if($team->getIsFinished()) {
            $this->redirectToLogin('teamLocked');
        }
    }

    protected function redirectToLogin($messageId) {
        $this->redirect('form', 'Access', 'XlncTools', array("messageId" => $messageId), (int) $this->extConf['authenticatePageId']);
    }

    public function redirectToSurveyWithLanguage($languageId, $toolId, $sheetId) {

    	$uriBuilder = $this->controllerContext->getUriBuilder();
		$uriBuilder->reset();
		$uriBuilder->setArguments(array(
			'L'	=> (int) $languageId,
			'tx_xlnctools_tool' => array(
				'controller'	=> 'Tool',
				'action'		=> 'survey',
				'tool' 			=> (int) $toolId,
				'sheet'			=> (int) $sheetId,
				'language'		=> (int) $languageId
			)
		));
		$uri = $uriBuilder->build();

    	$this->redirectToUri($uri);
    }

}
