<?php
namespace Xlnc\XlncTools\Controller\Backend;

use \Xlnc\XlncTools\Domain\Model\Customer;

use TYPO3\CMS\Backend\Utility\BackendUtility,
    TYPO3\CMS\Core\Utility\GeneralUtility,
    TYPO3\CMS\Extbase\Object\ObjectManager;

use TYPO3\CMS\Core\Authentication\BackendUserAuthentication,
    TYPO3\CMS\Core\FormProtection\BackendFormProtection,
    TYPO3\CMS\Core\FormProtection\FormProtectionFactory,
    TYPO3\CMS\Core\Registry;

class ManagementController extends \Xlnc\XlncTools\Controller\BaseController {

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\ProjectRepository
	 * @inject
	 */
	protected $projectRepository;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\CustomerRepository
	 * @inject
	 */
	protected $customerRepository;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\SheetRepository
	 * @inject
	 */
	protected $sheetRepository;

	public function indexAction()
	{
		#$this->view->assign("projects", $this->projectRepository->setIgnoreStoragePage()->findAll());
		#$this->view->assign("customers", $this->customerRepository->setIgnoreStoragePage()->findAll());
		$this->view->assign("newCustomerUrl", self::newCustomerUrl());
	}

	private static function newCustomerUrl() {
		// this only works if $GLOBALS['BE_USER'] is set (I'm not kidding)
		// this is true for be_users that are admins, but not necessarily for "normal" users
		// we need to initialize $GLOBALS['BE_USER'] with something that will make the UriBuilder
		// convince that we're actually in the backend
		if(!isset($GLOBALS['BE_USER'])) {
			self::buildBackendUserAndFormProtection();
		}
		return BackendUtility::getModuleUrl('record_edit') . '&' . self::newCustomerUrlParams();
	}

	private static function newCustomerUrlParams() {
		// @warning hardcoded PID
        return 'edit[tx_xlnctools_domain_model_customer][17]=new';
    }

	private static function buildBackendUserAndFormProtection() {
        $GLOBALS['BE_USER'] = GeneralUtility::makeInstance(ObjectManager::class)->get(BackendUserAuthentication::class);
        $GLOBALS['BE_USER']->start();

        FormProtectionFactory::set(BackendFormProtection::class,
            GeneralUtility::makeInstance(BackendFormProtection::class,
                $GLOBALS['BE_USER'],
                GeneralUtility::makeInstance(Registry::class),
                function() {}
            )
        );
        FormProtectionFactory::set('default', FormProtectionFactory::get(BackendFormProtection::class));
    }

}
