<?php
namespace Xlnc\XlncTools\Controller\Backend;

class ArchiveController extends \Xlnc\XlncTools\Controller\BaseController {

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Customer\EmployeesRepository
	 * @inject
	 */
	protected $employeesRepository;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Customer\TurnoverRepository
	 * @inject
	 */
	protected $turnoverRepository;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Customer\InternationalityRepository
	 * @inject
	 */
	protected $internationalityRepository;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Customer\IndustryRepository
	 * @inject
	 */
	protected $industryRepository;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\Dictionary\Project\ReferenceRepository
	 * @inject
	 */
	protected $referenceRepository;

	public function indexAction()
	{
		$this->view->assign("industries", $this->industryRepository->setIgnoreStoragePage()->findAll());
		$this->view->assign("turnovers", $this->turnoverRepository->setIgnoreStoragePage()->findAll());
		$this->view->assign("employees", $this->employeesRepository->setIgnoreStoragePage()->findAll());
		$this->view->assign("internationalities", $this->internationalityRepository->setIgnoreStoragePage()->findAll());

		$this->view->assign("references", $this->referenceRepository->setIgnoreStoragePage()->findAll());
	}

}
