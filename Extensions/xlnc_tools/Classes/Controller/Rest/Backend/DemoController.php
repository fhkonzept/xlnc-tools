<?php
namespace Xlnc\XlncTools\Controller\Rest\Backend;

use \Xlnc\XlncTools\View\Report;
use \Xlnc\XlncTools\Report\Normalization\NormalizationFactory;

use \Xlnc\XlncTools\Domain\Model\Dictionary\Item\Criteria;

use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use Xlnc\XlncTools\Utility\LanguageUtility;

use Xlnc\XlncTools\Domain\Model\SystemLanguage as Language;

class DemoController extends BaseController {

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\SystemLanguageRepository
	 * @inject
	 */
	protected $languageRepository;

	public function styleAction()
	{
        $styleReport = $this->objectManager->get(Report\StyleReportPdfView::class);

        $styleReport->setPercentage("self", Report\StyleReportPdfView::NORMATIVE, 11);
        $styleReport->setPercentage("self", Report\StyleReportPdfView::DIRECTIVE, 22);
        $styleReport->setPercentage("self", Report\StyleReportPdfView::PARTICIPATIVE, 33);
        $styleReport->setPercentage("self", Report\StyleReportPdfView::INTEGRATIVE, 44);
        $styleReport->setPercentage("self", Report\StyleReportPdfView::COACHIVE, 55);
        $styleReport->setPercentage("self", Report\StyleReportPdfView::INSPIRATIVE, 66);

        $styleReport->setRanking("self", Report\StyleReportPdfView::NORMATIVE, 'high');
        $styleReport->setRanking("self",Report\StyleReportPdfView::DIRECTIVE, 'medium');
        $styleReport->setRanking("self",Report\StyleReportPdfView::PARTICIPATIVE, 'low');
        $styleReport->setRanking("self",Report\StyleReportPdfView::INTEGRATIVE, 'low_to_medium');
        $styleReport->setRanking("self",Report\StyleReportPdfView::COACHIVE, 'medium_to_high');
        $styleReport->setRanking("self",Report\StyleReportPdfView::INSPIRATIVE, 'medium');

        $styleReport->setPercentage("employees", Report\StyleReportPdfView::NORMATIVE,77);
        $styleReport->setPercentage("employees", Report\StyleReportPdfView::DIRECTIVE, 33);
        $styleReport->setPercentage("employees", Report\StyleReportPdfView::PARTICIPATIVE, 22);
        $styleReport->setPercentage("employees", Report\StyleReportPdfView::INTEGRATIVE, 55);
        $styleReport->setPercentage("employees", Report\StyleReportPdfView::COACHIVE, 99);
        $styleReport->setPercentage("employees", Report\StyleReportPdfView::INSPIRATIVE, 11);

        $styleReport->setRanking("employees", Report\StyleReportPdfView::NORMATIVE, 'low_to_medium');
        $styleReport->setRanking("employees", Report\StyleReportPdfView::DIRECTIVE, 'medium_to_high');
        $styleReport->setRanking("employees", Report\StyleReportPdfView::PARTICIPATIVE, 'medium');
        $styleReport->setRanking("employees", Report\StyleReportPdfView::INTEGRATIVE, 'low');
        $styleReport->setRanking("employees", Report\StyleReportPdfView::COACHIVE, 'medium_to_high');
        $styleReport->setRanking("employees", Report\StyleReportPdfView::INSPIRATIVE, 'high');

        $styleReport->setPercentage("agreement", Report\StyleReportPdfView::NORMATIVE,10);
        $styleReport->setPercentage("agreement", Report\StyleReportPdfView::DIRECTIVE, 20);
        $styleReport->setPercentage("agreement", Report\StyleReportPdfView::PARTICIPATIVE, 30);
        $styleReport->setPercentage("agreement", Report\StyleReportPdfView::INTEGRATIVE, 40);
        $styleReport->setPercentage("agreement", Report\StyleReportPdfView::COACHIVE, 50);
        $styleReport->setPercentage("agreement", Report\StyleReportPdfView::INSPIRATIVE, 60);


        $styleReport->setPercentage("agreementSecond", Report\StyleReportPdfView::NORMATIVE,70);
        $styleReport->setPercentage("agreementSecond", Report\StyleReportPdfView::DIRECTIVE, 60);
        $styleReport->setPercentage("agreementSecond", Report\StyleReportPdfView::PARTICIPATIVE, 50);
        $styleReport->setPercentage("agreementSecond", Report\StyleReportPdfView::INTEGRATIVE, 40);
        $styleReport->setPercentage("agreementSecond", Report\StyleReportPdfView::COACHIVE, 30);
        $styleReport->setPercentage("agreementSecond", Report\StyleReportPdfView::INSPIRATIVE, 20);

		$styleReport->addScoreDifference("scoreDifference", "Frage 1", 4, 0);
		$styleReport->addScoreDifference("scoreDifference", "Frage 1", 3, 1);
		$styleReport->addScoreDifference("scoreDifference", "Frage 1", 2, 2);
		$styleReport->addScoreDifference("scoreDifference", "Frage 1", 1, 3);
		$styleReport->addScoreDifference("scoreDifference", "Frage 1", 0, 4);

		$styleReport->addScoreDifference("secondScoreDifference", "Frage 2", 2, 3);
		$styleReport->addScoreDifference("secondScoreDifference", "Frage 2", 2, 1);
		$styleReport->addScoreDifference("secondScoreDifference", "Frage 2", 2, 1);
		$styleReport->addScoreDifference("secondScoreDifference", "Frage 2", 2, 1);
		$styleReport->addScoreDifference("secondScoreDifference", "Frage 2", 0, 1);

		$this->response->setHeader("Content-type", "application/pdf");
		$this->response->sendHeaders();
        readfile(trim(strip_tags($styleReport->render())));
		exit();
	}

	public function climateAction() {
        $climateReport = $this->objectManager->get(Report\ClimateReportPdfView::class);
        $climateReport->render();
	}

    /**
     * @param \Xlnc\XlncTools\Domain\Model\SystemLanguage $language
     */
	public function barchartAction($language = NULL) {
        LanguageUtility::setLanguage($language);
		$view = $this->objectManager->get(StandaloneView::class);
        $templateBasePath = "typo3conf/ext/xlnc_tools/Resources/Private/Templates/Pdf/Reports/Partials/Charts/";
        // $view->setTemplatePathAndFilename($templateBasePath . "TeamClimate/BarchartAgreement.html");
		// $view->setTemplatePathAndFilename($templateBasePath . "TeamClimate/BarchartComparison.html");
        // $view->setTemplatePathAndFilename($templateBasePath . "TeamClimate/BarchartEmployees.html");
		// $view->setTemplatePathAndFilename($templateBasePath . "TeamClimate/BarchartSelf.html");
		//$view->setTemplatePathAndFilename($templateBasePath . "Combined/BarchartClimate.html");
		// $view->setTemplatePathAndFilename($templateBasePath . "Combined/BarchartManagementStyles.html");
		// $view->setTemplatePathAndFilename($templateBasePath . "Combined/ClimateDistribution.html");
		// $view->setTemplatePathAndFilename($templateBasePath . "Combined/ManagementStylesDistribution.html");
		//$view->setTemplatePathAndFilename($templateBasePath . "ManagementStyles/BarchartAgreement.html");
		//$view->setTemplatePathAndFilename($templateBasePath . "ManagementStyles/BarchartComparison.html");
		//$view->setTemplatePathAndFilename($templateBasePath . "ManagementStyles/BarchartEmployees.html");
		$view->setTemplatePathAndFilename($templateBasePath . "ManagementStyles/BarchartSelf.html");


		#$view->render();

		$this->response->setContent(file_get_contents(trim($view->render())));
		$this->response->setHeader("Content-type", "image/png");
		$this->response->sendHeaders();
		$this->response->send();
		exit();
	}

	/**
	 * @param  \Xlnc\XlncTools\Domain\Model\Dictionary\Item\Criteria $criteria
	 * @param float $value
	 */
	public function normalizationAction(Criteria $criteria, $value) {
		$normalization = NormalizationFactory::buildForItemCriteria($criteria);
		\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($normalization->normalizeValue($value));
		die();
	}

	public function languageSwitchAction() {
		LanguageUtility::setLanguage($this->languageRepository->findByUid(1));
		\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump(LocalizationUtility::translate('validator.accesscode.empty', 'xlnc_tools'));
		LanguageUtility::resetLanguage();
		LanguageUtility::setLanguage($this->languageRepository->getGerman());
		\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump(LocalizationUtility::translate('validator.accesscode.empty', 'xlnc_tools'));
		die("language");
	}

    /**
     * @param \Xlnc\XlncTools\Domain\Model\SystemLanguage $language
     */
    public function textAction($language = NULL) {
        LanguageUtility::setLanguage($language);
        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump(
            \Xlnc\XlncTools\Utility\TeamClimateTextUtility::getOverallShouldText($secondSelf, 'employees')
        );

        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump(
            LocalizationUtility::translate('tx_xlnctools_domain_model_invitation.salutation.Männlich', 'xlnc_tools')
        );


        die();
    }

}
