<?php
namespace Xlnc\XlncTools\Controller\Rest\Backend;



class LanguageController extends BaseController {

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\SystemLanguageRepository
	 * @inject
	 */
	protected $languageRepository;

	public function indexAction()
	{
		$this->view->assign("languages", $this->languageRepository->findAllAndDefault());
	}

}
