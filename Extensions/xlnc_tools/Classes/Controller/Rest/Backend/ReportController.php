<?php

namespace Xlnc\XlncTools\Controller\Rest\Backend;

use \Xlnc\XlncTools\TypeConverter\CsvArrayConverter;
use \Xlnc\XlncTools\Report\CombinedReportGenerator;
use \Xlnc\XlncTools\Utility\LanguageUtility;

class ReportController extends BaseController {

    public function initializeCombineAction() {
        $csvArrayConverter = $this->objectManager->get(CsvArrayConverter::class);
        $this->arguments
                ->getArgument('teams')
                ->getPropertyMappingConfiguration()
                ->setTypeConverter($csvArrayConverter)
                ->setTypeConverterOption(
                        CsvArrayConverter::class,
                        'targetClass',
                        \Xlnc\XlncTools\Domain\Model\Team::class
        );
    }

    /**
     * @param array<\Xlnc\XlncTools\Domain\Model\Team> $teams
     * 
     * @param \Xlnc\XlncTools\Domain\Model\SystemLanguage $language
     * 
     */
    public function combineAction($teams, $language = null) {
        LanguageUtility::setLanguage($language);
        $reportGenerator = $this->objectManager->get(CombinedReportGenerator::class, $teams);

        $reportFilepath = $reportGenerator->getFilePath();
        $this->response->setHeader("Content-type", "application/pdf");
        $this->response->sendHeaders();
        readfile($reportFilepath);
        exit;
    }

}
