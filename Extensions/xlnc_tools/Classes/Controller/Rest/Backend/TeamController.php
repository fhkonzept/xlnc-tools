<?php

namespace Xlnc\XlncTools\Controller\Rest\Backend;

use TYPO3\CMS\Extbase\Utility\LocalizationUtility,
    TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter;
use \Xlnc\XlncTools\Domain\Model\CouponInvitation,
    \Xlnc\XlncTools\Domain\Model\Sheet,
    \Xlnc\XlncTools\Domain\Model\MailText,
    \Xlnc\XlncTools\Domain\Model\LeaderInvitationText,
    \Xlnc\XlncTools\Domain\Model\ReminderText,
    \Xlnc\XlncTools\Utility\MailUtility,
    \Xlnc\XlncTools\Report\ToolReportGenerator,
    \Xlnc\XlncTools\Utility\LanguageUtility;

#ini_set('max_execution_time', 60);

class TeamController extends BaseController {

    /**
     * @var \Xlnc\XlncTools\Domain\Repository\TeamRepository
     * @inject
     */
    protected $teamRepository;

    /**
     * @var \Xlnc\XlncTools\Domain\Repository\ArchivedTeamRepository
     * @inject
     */
    protected $archivedTeamRepository;

    public function indexAction() {
        $teams = $this->teamRepository->setIgnoreStoragePage()->findByIsArchived(FALSE);
        $this->view->assign("teams", $teams);
    }

    public function archivedAction() {
        $teams = $this->archivedTeamRepository->setIgnoreStoragePage()->findByIsArchived(TRUE);
        $this->view->assign("teams", $teams);
    }

    /**
     * @param  XlncXlncToolsDomainModelTeam $team
     */
    public function showAction(\Xlnc\XlncTools\Domain\Model\Team $team) {
        $this->view->assign("team", $team);
    }

    public function initializeUpdateAction() {
        $pmc = $this->arguments->getArgument("team")
                ->getPropertyMappingConfiguration();
        $pmc->skipUnknownProperties();
        $pmc->setTypeConverterOption(
                PersistentObjectConverter::class, PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED, TRUE
        );
    }

    /**
     * @param  XlncXlncToolsDomainModelTeam $team
     */
    public function updateAction(\Xlnc\XlncTools\Domain\Model\Team $team) {
        $this->teamRepository->update($team);
        $this->view->assign("team", $team);
    }

    /**
     * @param  XlncXlncToolsDomainModelTeam $team
     */
    public function couponsAction(\Xlnc\XlncTools\Domain\Model\Team $team) {
        $zipFileName = \Xlnc\XlncTools\Factory\CouponPdfFactory::createZipFromInvitations(
                        $team->getInvitationsByType()[CouponInvitation::class][Sheet::class]
        );
        $this->redirectToUri($zipFileName);
    }

    /**
     * @param \Xlnc\XlncTools\Domain\Model\Team $team
     * @param \Xlnc\XlncTools\Domain\Model\Tool $tool
     * @param \Xlnc\XlncTools\Domain\Model\SystemLanguage $language
     * @param string $variant
     */
    public function reportAction($team, $tool, $language = NULL, $variant = NULL) {
        LanguageUtility::setLanguage($language);

        $reportGenerator = $this->objectManager->get(ToolReportGenerator::class, $team, $tool, $variant);

        $reportFilepath = $reportGenerator->getFilePath();
        $this->response->setHeader("Content-type", "application/pdf");
        $this->response->sendHeaders();
        readfile($reportFilepath);
        exit;
    }

    /**
     * @param  \Xlnc\XlncTools\Domain\Model\Team $team
     * @param  \Xlnc\XlncTools\Domain\Model\Tool $tool
     */
    public function sendReportAction($team, $tool) {

        $reportGenerator = $this->objectManager->get(ToolReportGenerator::class, $team, $tool);

        $reportFilepath = $reportGenerator->getFilePath();

        MailUtility::sendReport($team, $tool, $reportFilepath);

        $teamAndTool = new \Xlnc\XlncTools\Dto\TeamAndTool($team, $tool);

        $this->view->assign("teamAndTool", $teamAndTool);
    }

    public function initializeRemindAction() {
        $teamArgument = $this->request->getArgument("team");
        $this->request->setArgument("team", ["__identity" => $teamArgument["__identity"]]);
    }

    /**
     * @param \Xlnc\XlncTools\Domain\Model\Team $team
     */
    public function remindAction($team) {
        $this->view->assign("emails", MailUtility::sendReminderMails($team));
    }

    /**
     * @param  \Xlnc\XlncTools\Domain\Model\Team $team
     */
    public function lockAction($team) {
        $team->lock();
        $this->teamRepository->update($team);
        $this->view->assign("team", $team);
    }

    /**
     * @param  \Xlnc\XlncTools\Domain\Model\Team $team
     */
    public function unlockAction($team) {
        $team->unlock();
        $this->teamRepository->update($team);
        $this->view->assign("team", $team);
    }

    /**
     * @param  \Xlnc\XlncTools\Domain\Model\Team $team
     */
    public function archiveAction($team) {
        $team->setIsArchived(TRUE);
        $this->teamRepository->update($team);
        $this->view->assign("team", $team);
    }

    public function initializeRequestTextAction() {
        // since the arguments type and language get attached to both as arguments
        // and properties, TYPO3 throws up unless we get rid of them first
        // this is a routing / mapping / dispatching bug that needs to be fixed elsewhere
        $teamArgument = $this->request->getArgument("team");
        foreach ($teamArgument as $propertyName => $_) {
            if ($propertyName !== "__identity") {
                unset($teamArgument[$propertyName]);
            }
        }
        $this->request->setArgument("team", $teamArgument);
        $this->arguments->getArgument("team")->getPropertyMappingConfiguration()->skipUnknownProperties();
    }

    /**
     * @param  \Xlnc\XlncTools\Domain\Model\Team $team
     * @param  \Xlnc\XlncTools\Domain\Model\SystemLanguage $language
     * @param  string $type
     */
    public function requestTextAction($team, $language, $type) {
        if (!$team->hasTextInLanguage($type, $language)) {
            $this->buildTextTemplate($team, $language, $type);
            if ($type !== 'Tx_XlncTools_ReminderText' && !$team->hasTextInLanguage('Tx_XlncTools_ReminderText', $language)) {
                $this->buildTextTemplate($team, $language, 'Tx_XlncTools_ReminderText');
            }
        }

        #\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($team->getTextInLanguage($type, $language));
        #die();
        $this->view->assign("text", $team->getTextInLanguage($type, $language));
    }

    private function buildTextTemplate($team, $language, $type) {
        $typeToClassMap = [
            "Tx_XlncTools_LeaderInvitationText" => LeaderInvitationText::class,
            "Tx_XlncTools_InvitationText" => Mailtext::class,
            "Tx_XlncTools_ReminderText" => ReminderText::class,
        ];

        $typeToArgumentsMap = [
            "Tx_XlncTools_InvitationText" => [
                "leaderSalutationGenitive", "leaderFullname",
                "leaderSalutationGenitive", "leaderLastname",
                "leaderPronounGenitive",
                "leaderSalutationGenitive", "leaderLastname",
                "endDate",
            ],
            "Tx_XlncTools_LeaderInvitationText" => [
                "endDate",
            ],
            "Tx_XlncTools_ReminderText" => [
                "endDate",
                "leaderSalutationGenitive", "leaderFullname",
            ]
        ];

        $newText = $this->objectManager->get($typeToClassMap[$type]);
        $newText->setLanguage($language);
        $newText->setTxExtbaseType($type);

        \Xlnc\XlncTools\Utility\LanguageUtility::setLanguage($language);
        $defaultText = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_xlnctools_domain_model_invitation.defaultText.' . $type, 'xlnc_tools');

        $text = call_user_func_array("sprintf", array_merge(
                        [$defaultText], $this->buildTextArguments($team, $typeToArgumentsMap[$type])
        ));
        $newText->setBodytext($text);
        \Xlnc\XlncTools\Utility\LanguageUtility::resetLanguage($language);

        $team->getMailTexts()->attach($newText);
        $this->teamRepository->update($team);
    }

    private function buildTextArguments($team, $args) {
        $values = [];
        foreach ($args as $argName) {
            $values[] = $this->getTextArgumentValue($team, $argName);
        }
        return $values;
    }

    private function getTextArgumentValue($team, $argName) {
        switch ($argName) {
            case 'leaderSalutation':
                return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_xlnctools_domain_model_invitation.gender.' . $team->getGender()->getTitle(), 'xlnc_tools');
            case 'leaderSalutationGenitive':
                return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_xlnctools_domain_model_invitation.gender.' . $team->getGender()->getTitle() . '.genitive', 'xlnc_tools');
            case 'leaderPronounGenitive':
                return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_xlnctools_domain_model_invitation.pronoun.' . $team->getGender()->getTitle() . '.genitive', 'xlnc_tools');
            case 'leaderFullname':
                return $team->getFirstName() . ' ' . $team->getLastName();
            case 'leaderLastname':
                return $team->getLastName();
            case 'endDate':
                return $team->getEndsAt() ? $team->getEndsAt()->format('d.m.Y') : 'MISSING_DATE';
            default:
                return $argName;
        }
    }

}
