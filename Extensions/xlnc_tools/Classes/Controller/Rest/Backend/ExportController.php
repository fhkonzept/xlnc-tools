<?php
namespace Xlnc\XlncTools\Controller\Rest\Backend;

use \Xlnc\XlncTools\Report\SpssExport;
use \Xlnc\XlncTools\TypeConverter\CsvArrayConverter;

class ExportController extends BaseController {

    public function initializeSpssAction() {
        $csvArrayConverter = $this->objectManager->get(CsvArrayConverter::class);
        $this->arguments
            ->getArgument('teams')
            ->getPropertyMappingConfiguration()
            ->setTypeConverter($csvArrayConverter)
            ->setTypeConverterOption(
                CsvArrayConverter::class,
                'targetClass',
                \Xlnc\XlncTools\Domain\Model\Team::class
            );
    }

    /**
     * @param  \Xlnc\XlncTools\Domain\Model\Tool $tool
     * @param  array<\Xlnc\XlncTools\Domain\Model\Team> $teams
     */
    public function spssAction($tool, $teams) {
        $export = new SpssExport(
            $tool,
            $teams
        );

        $csvString = $export->getCsvString();
        header("Content-Type: text/csv");
        echo $csvString;
        exit;
        #$this->view->assign("export", $export);
    }

}
