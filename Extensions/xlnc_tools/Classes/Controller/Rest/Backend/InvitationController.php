<?php
namespace Xlnc\XlncTools\Controller\Rest\Backend;

use TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter;

use \Xlnc\XlncTools\Domain\Model\MailInvitation;
use \Xlnc\XlncTools\Domain\Model\CouponInvitation;
use \Xlnc\XlncTools\Domain\Model\Sheet;
use \Xlnc\XlncTools\Domain\Model\LeaderSheet;

class InvitationController extends BaseController {

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\TeamRepository
	 * @inject
	 */
	protected $teamRepository;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\LeaderSheetRepository
	 * @inject
	 */
	protected $leaderSheetRepository;

	/**
	 * @var \Xlnc\XlncTools\Factory\SheetFactory
	 * @inject
	 */
	protected $sheetFactory;

	/**
	 * a map for service call values, mapping to Invitation type class names
	 * @var array
	 */
	private $invitationTypeMap = [
		"Mail" => MailInvitation::class,
		"Coupon" => CouponInvitation::class
	];

	/**
	 * a map for service call values, mapping to Sheet type class names
	 * @var [type]
	 */
	private $sheetTypeMap = [
		"Sheet" => Sheet::class,
		"Leader" => LeaderSheet::class
	];

	private $invitationCreatorFunctionMap = [
		LeaderSheet::class => [
			MailInvitation::class => "createLeaderMailInvitation",
			CouponInvitation::class => "createLeaderCouponInvitation",
		],
		Sheet::class => [
			MailInvitation::class => "createMailInvitation",
			CouponInvitation::class => "createCouponInvitation",
		]
	];

	/**
	 * interpret the request to match the invitation model
	 */
	public function initializeCreateAction() {
		$invitationArgument = $this->request->getArgument("invitation");
		/*
			load the team from persistence instead of service call value
		 */
		$invitationArgument["team"] = [ "__identity" => $invitationArgument["team"]["uid"]];



		/*
			force the invitation argument to be of the requested subtype
			@warning using reflection like this is quite possibly a stupid idea
			@todo handle invalid values
		 */
		$reflectionClass = new \ReflectionClass('TYPO3\CMS\Extbase\Mvc\Controller\Argument');
		$reflectionProperty = $reflectionClass->getProperty('dataType');
		$reflectionProperty->setAccessible(true);
		$reflectionProperty->setValue(
			$this->arguments->getArgument("invitation"),
		 	$this->invitationTypeMap[$invitationArgument["invitationType"]]
		);
		unset($invitationArgument["invitationType"]);

		/*
			map the requested Sheet subtype to a FQCN
			@todo handle invalid values
		 */
		$invitationArgument["sheetType"] = $this->sheetTypeMap[$invitationArgument["sheetType"]];

		/*
			wrapping up: set the updated argument, allow the expected values and
			allow creation of a new Invitation object
		 */
		$this->request->setArgument("invitation", $invitationArgument);

		$pmc = $this->arguments->getArgument('invitation')->getPropertyMappingConfiguration();
		$pmc->allowProperties("team", "txExtbaseType", "sheetType", "address", "language");

		$pmc->setTypeConverterOption(
			PersistentObjectConverter::class,
			PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
			TRUE
		);


	}

	/**
	 * @param \Xlnc\XlncTools\Domain\Model\Invitation $invitation
	 * @validate $invitation \Xlnc\XlncTools\Validation\Rest\NewInvitationValidator
	 */
	public function createAction($invitation)
	{
		$creatorFunction = $this->invitationCreatorFunctionMap[$invitation->getSheetType()][get_class($invitation)];

		$this->$creatorFunction($invitation);

		$invitation->getTeam()->addInvitation($invitation);

		$this->teamRepository->update($invitation->getTeam());

		$invitation->send();

		$this->view->assign("invitation", $invitation);
	}

	/**
	 * @param \Xlnc\XlncTools\Domain\Model\CouponInvitation $invitation
	 */
	public function downloadAction($invitation)
	{
		$this->redirectToUri($invitation->send());
	}

	/**
	 * @todo document and refactor anything below here
	 */
	private function createLeaderMailInvitation($invitation) {

		if(!$invitation->getTeam()->hasLeaderInvitation()) {
			$sheet = $this->sheetFactory->createFromInvitation($invitation);
			$invitation->getTeam()->addSheet($sheet);
			$invitation->setSheet($sheet);
		} else {
			// leader has an invitation, but it's a coupon
			// so there's already a sheet.
			$sheet = $this->leaderSheetRepository->setIgnoreStoragePage()->findByTeam($invitation->getTeam())->getFirst();
			$invitation->setSheet($sheet);
		}

	}

	private function createLeaderCouponInvitation($invitation) {
		if(!$invitation->getTeam()->hasLeaderInvitation()) {
			$sheet = $this->sheetFactory->createFromInvitation($invitation);
			$invitation->getTeam()->addSheet($sheet);
			$invitation->setSheet($sheet);
			$invitation->setAddress($sheet->getAccessKey());
		} else {
			// leader has an invitation, but it's a mail
			// so there's already a sheet.
			$sheet = $this->leaderSheetRepository->setIgnoreStoragePage()->findByTeam($invitation->getTeam())->getFirst();
			$invitation->setSheet($sheet);
			$invitation->setAddress($sheet->getAccessKey());
		}
	}

	private function createMailInvitation($invitation) {
		$sheet = $this->sheetFactory->createFromInvitation($invitation);
		$invitation->getTeam()->addSheet($sheet);
		$invitation->setSheet($sheet);
	}

	private function createCouponInvitation($invitation) {
		$sheet = $this->sheetFactory->createFromInvitation($invitation);
		$invitation->getTeam()->addSheet($sheet);
		$invitation->setSheet($sheet);
		$invitation->setAddress($sheet->getAccessKey());
	}

}
