<?php
namespace Xlnc\XlncTools\Controller\Rest\Backend;

class BaseController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

    protected $namespacesViewObjectNamePattern = 'Xlnc\XlncTools\View\@controller\@action';

    /**
     * @var \TYPO3\CMS\Core\Authentication\BackendUserAuthentication
     * @inject
     */
    protected $backendUser;

    public function initializeAction() {
        $this->initializeBackendUser();
        if($this->request->getControllerActionName() != "unauthorized" && (
            NULL === $this->backendUser || !$this->backendUser->user["uid"] > 0
            )) {
            $this->forward("unauthorized");
        };
    }

    public function errorAction() {
        $flattenedErrors = $this->arguments->getValidationResults()->getFlattenedErrors();
        $errorMessages = array_map(function($errors) {
            return array_map(function($error) {
                return [
                    "message" => $error->render(),
                    "code" => $error->getCode(),
                    "title" => $error->getTitle()
                ];
            }, $errors);
        }, $flattenedErrors);
        $this->response->setStatus(400);
        $this->response->setHeader("Content-Type", "text/json");
        $this->response->setContent(json_encode($errorMessages));
        $this->response->send();
        exit();
    }

    public function unauthorizedAction() {
        $flattenedErrors = $this->arguments->getValidationResults()->getFlattenedErrors();
        $errorMessages = array_map(function($errors) {
            return array_map(function($error) {
                return [
                    "message" => "This service needs authorization which you do not have.",
                    "code" => "403",
                    "title" => "Authorization needed"
                ];
            }, $errors);
        }, $flattenedErrors);
        $this->response->setStatus(403);
        $this->response->setHeader("Content-Type", "text/json");
        $this->response->setContent(json_encode($errorMessages));
        $this->response->send();
        exit();
    }

    private function initializeBackendUser() {
        if(NULL === $this->backendUser->user) {
            $this->backendUser->start();
        }
    }

}
