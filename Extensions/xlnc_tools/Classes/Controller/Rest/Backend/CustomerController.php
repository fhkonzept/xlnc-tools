<?php
namespace Xlnc\XlncTools\Controller\Rest\Backend;

class CustomerController extends BaseController {

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\CustomerRepository
	 * @inject
	 */
	protected $customerRepository;

	public function indexAction()
	{
        $customers = $this->customerRepository->setIgnoreStoragePage()->findAll();
        $this->view->assign("customers", $customers);
	}

	/**
	 * @param  XlncXlncToolsDomainModelCustomer $customer
	 */
	public function showAction(\Xlnc\XlncTools\Domain\Model\Customer $customer) {
		$this->view->assign("customer", $customer);
	}

}
