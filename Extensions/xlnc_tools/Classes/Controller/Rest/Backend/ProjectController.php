<?php
namespace Xlnc\XlncTools\Controller\Rest\Backend;

use Xlnc\XlncTools\Domain\Model\Project;

class ProjectController extends BaseController {

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\ProjectRepository
	 * @inject
	 */
	protected $projectRepository;

	public function indexAction()
	{
        $projects = $this->projectRepository->setIgnoreStoragePage()->findAll();
        $this->view->assign("projects", $projects);
	}

	/**
	 * @param \Xlnc\XlncTools\Domain\Model\Project $project
	 */
	public function showAction(Project $project) {
		$this->view->assign("project", $project);
	}

}
