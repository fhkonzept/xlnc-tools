<?php
namespace Xlnc\XlncTools\Controller\Rest;

use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * @author  Malte Muth <muth@fh-konzept.de>
 *
 * a lightweight RESTful route dispatcher, implementing the following pattern:
 *
 * basic REST pattern for a resource:
 * collection:
 * POST 	resource 		=> create
 * GET 		resource 		=> index
 * any other collection requests
 * need to have at least two parameters (resource/@action)
 *
 * member:
 * PUT 		resource/@uid 	=> update
 * GET 		resource/@uid 	=> show
 * DELETE 	resource/@uid 	=> delete
 * any other member requests
 * need to have at least three parameters (resource/@uid/@action)
 *
 * @todo implement protection for unsafe actions
 * @todo implement authorization
 */
class RestDispatchController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	protected $defaultViewObjectName = 'TYPO3\CMS\Extbase\Mvc\View\JsonView';

	/**
	 * map for registered Controller/Extension pairs, along with the expected parameter name for a single resource
	 * @var array
	 */
	protected $controllerMap = [];

	public function __construct() {
		$this->controllerMap = \Xlnc\XlncTools\Utility\RestServiceUtility::getMap();
	}

    public function indexAction() {
		$status = 404;
		$message = "Resource not found.";
        $this->response->setStatus($status, $message);
		$this->view->assign("value", ["status" => $status, "message" => $message]);
    }

	/**
	 * default action map for every controller
	 * @var array
	 */
	protected $defaultActionMap = [
		"collection" 	=> [
			"POST" 		=> "create",
			"GET"		=> "index"
		],
		"member"		=> [
			"GET"		=> "show",
			"PUT"		=> "update",
			"DELETE"	=> "delete"
		]
	];


	/**
	 * parts of the URL path that can be ignored
	 * @var array
	 */
	protected $discardablePathParts = [
		"", "api", "debug"
	];

	protected $pathParts;

	public function dispatchAction() {
		$this->forward(
			$this->getAction(),
			$this->getControllerName(),
			$this->getExtensionName(),
			$this->getArguments()
		);
	}

	private function getControllerAndExtension() {
		$resourceKey = $this->pathParameters()[0];
		return $this->controllerMap[$resourceKey];
	}

	private function getExtensionName() {
		return $this->getControllerAndExtension()["extension"];
	}

	private function getControllerName() {
		$controllerName = $this->getControllerAndExtension()["controller"];
		if(is_null($controllerName)) {
			return NULL;
		} else {
			return "Rest\\" . $controllerName;
		}
	}

	/**
	 * get the action name to call on the target controller
	 * @todo  implement whitelist for non-default actions
	 * @return string
	 */
	private function getAction() {

		$pathParts 	= $this->pathParameters();
		$pathCount  = count($pathParts);
		$method 	= $this->request->getMethod();
		switch ($pathCount) {
			case 1:
				// can only be a collection method
				$defaultCollectionMethod = $this->defaultActionMap["collection"][$method];
				if($defaultCollectionMethod) {
					return $defaultCollectionMethod;
				} else {
					return "default";
				}

			case 2:
				// can be a custom collection method or a member method
				if(!$this->isCollectionRequest()) {
					$defaultMemberMethod = $this->defaultActionMap["member"][$method];
					if($defaultMemberMethod) {
						return $defaultMemberMethod;
					} else {
						$this->throwStatus(404);
					}
				} else {
					return $pathParts[1];
				}

			default:
				if($this->isCollectionRequest()) {
					return $pathParts[1];
				} else {
					return $pathParts[2];
				}
		}
	}

	/**
	 * get the arguments to pass to the action
	 * respects both arguments via URL and in the request body (as JSON)
	 * @return array
	 */
	private function getArguments() {
		$postArguments = $this->needsBodyArguments() ?
			$this->getBodyArguments() : [];
		$urlArguments = $this->getUrlArguments();

		if(!is_array($urlArguments)) $urlArguments = [];
		if(!is_array($postArguments)) $postArguments = [];

		ArrayUtility::mergeRecursiveWithOverrule($urlArguments, $postArguments);

		return $urlArguments;
	}

	/**
	 * returns the arguments contained in the URL
	 *
	 * recognizes an object ID for member-type requests
	 * any other type is treated as boolean (indicating presence)
	 * @return array
	 */
	private function getUrlArguments() {
		$pathParts = $this->pathParameters();

		$urlArguments = [];
		$memberArgument = [];
		$remainingParts = [];
		if(!$this->isCollectionRequest()) {
			$memberKey = $this->controllerMap[$pathParts[0]]["resource"];
			$memberId = (int)$pathParts[1];
			$memberArgument = [ $memberKey => [ "__identity" => $memberId ] ];

			$remainingParts = array_slice($pathParts, 3);

		} else {
			$remainingParts = array_slice($pathParts, 2);
		}

		$additionalArguments = [];
		// treat the rest of the arguments as key => value pairs
		for($i=0;$i<count($remainingParts);$i++) {
			if($i % 2 == 0) {
				$additionalArguments[$remainingParts[$i]] = NULL;
			} else {
				$additionalArguments[$remainingParts[$i-1]] = $remainingParts[$i];
			}
		}

		return array_merge($memberArgument, $additionalArguments);
	}

	/**
	 * read the source body, interpret as JSON and return as nested associative array
	 * @return array
	 */
	private function getBodyArguments() {
		$body = json_decode(file_get_contents("php://input"), TRUE);


		// requests do not "mention" the resources name,
		// so we're injecting them here.
		$pathParts = $this->pathParameters();
		$memberKey = $this->controllerMap[$pathParts[0]]["resource"];

		// also, __identity is an awkward key. We're accepting 'uid' as well.
		if(isset($body["uid"])) {
			$body["__identity"] = $body["uid"];
			unset($body["uid"]);
		}

		return [ $memberKey => $body ];
	}

	/**
	 * returns TRUE if the request actually needs a body to work
	 * @todo fix this
	 * @warning function stub
	 * @return boolean
	 */
	private function needsBodyArguments() {
		return $this->request->getMethod() != "GET";
	}

	/**
	 * returns true when this request is to be applied to the collection
	 * @return  boolean
	 */
	private function isCollectionRequest() {
		$pathParts 	= $this->pathParameters();
		$pathCount  = count($pathParts);

		switch ($pathCount) {
			case 1:
				return TRUE;
			default:
				return !is_numeric($pathParts[1]);
		}
	}

	/**
	 * returns TRUE if this is one of the five default actions
	 * @return boolean
	 */
	private function isDefaultAction() {
		$pathParts 	= $this->pathParameters();
		$pathCount  = count($pathParts);
		$method 	= $this->request->getMethod();

		switch ($pathCount) {
			case 1:
				return in_array($method, ["GET", "POST"]);
			case 2:
				if ( is_numeric($pathParts[1]) ) {
					# looks like a member request
					return in_array($method, ["GET", "PUT", "DELETE"]);
				} else {
					return FALSE;
				}
			case 3:
				return FALSE;
			default:
				return FALSE;
		}
	}

	/**
	 * returns the parts of the request URI that do not belong to the server setup
	 * @return array
	 */
	private function pathParameters() {
		$pathParts = explode("/", parse_url($this->request->getRequestUri(), PHP_URL_PATH));
		$discardableParts = $this->discardablePathParts;
		$recognizableParts = array_filter($pathParts, function ($item) use ($discardableParts) {
			return !in_array($item, $discardableParts);
		});

		return array_values($recognizableParts);
	}

	/**
	 * default action for debugging purposes
	 * @return string
	 */
	public function defaultAction() {
		return "DEFAULT_ACTION";
	}

}
