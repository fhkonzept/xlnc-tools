<?php
namespace Xlnc\XlncTools\Controller;

class BaseController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	* extConf: Extension Manager Configuration
	* @var array
	*/
	protected $extConf;

	/**
	 * sheetRepository: Storage of tool sheets
	 *
	 * @var \Xlnc\XlncTools\Domain\Repository\SheetRepository
	 * @inject
	 */
	protected $sheetRepository = NULL;


	protected $availableArguments = array('code','sheet');

	public function initializeAction() {
		$this->extConf = @unserialize ( $GLOBALS['TYPO3_CONF_VARS'] ['EXT'] ['extConf'] ['xlnc_tools'] );
	}

	protected function getErrorFlashMessage() {
		$msg = '';
		foreach($this->availableArguments as $argumentName) {
			if($this->arguments->hasArgument($argumentName) && $this->arguments->getArgument($argumentName)->getValidationResults()->getErrors()) {
				foreach($this->arguments->getArgument($argumentName)->getValidationResults()->getErrors() as $error) {
					$msg .= $error->getMessage() . chr(10);
				}
			}	
		}
		return $msg;
	}
}
?>