<?php
namespace Xlnc\XlncTools\SignalSlot;

use TYPO3\CMS\Core\Utility\GeneralUtility;

use Xlnc\XlncTools\Utility\SessionUtility;
use Xlnc\XlncTools\Domain\Model\Surveyresult;

use \TYPO3\CMS\Core\Error\Http\PageNotFoundException;

class Tool {

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 * @inject
	 */
	protected $objectManager;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\SurveyresultRepository
	 * @inject
	 */
	protected $surveyresultRepository;

	/**
	 * @var \Xlnc\XlncTools\Domain\Repository\SystemLanguageRepository
	 * @inject
	 */
	protected $systemLanguageRepository;

	/**
	 * ensures that a language has been chosen for the tool given by
	 * $data["tool"], and implements the "language lock" for the given tool
	 *
	 * @param Xlnc\XlncTools\Controller\ToolController $pObj
	 * @param array $data The data, containing: Xlnc\XlncTools\Domain\Model\Sheet sheet, Xlnc\XlncTools\Domain\Model\Tool tool
	 * @return array
	 */
	public function beforeSurvey($pObj, $data) {

		$result = [];
		/**
		 * @todo replace this with proper passing of arguments
		 * @var [type]
		 */
		$args = $data['request']->getArguments();

		/**
		 * @var \Xlnc\XlncTools\Domain\Model\Tool
		 */
		$tool = $data["tool"];

		/**
		 * @var \Xlnc\XlncTools\Domain\Model\Sheet
		 */
		$sheet = $data["sheet"];

		/**
		 * the language chosen by the user, if given
		 * @var int|NULL
		 */
		$chosenLanguageUid = NULL;

		/**
		 * the language that this sheet/tool combination is locked to, if given
		 * @var int|NULL
		 */
		$lockedLanguageUid = NULL;

		// language choice for this sheet and tool stored in the surveyresult model
		$surveyresult = $this->surveyresultRepository->setIgnoreStoragePage()->findByToolAndSheet($tool, $sheet);

		/**
		 * there are X cases
		 * 1) the locked language is already stored in a surveyresult. Then this
		 *    language is authoritative for the lock.
		 * 2) there is no locked language or no survey result, but a locked
		 *    language in the Session. In this case, the surveyresult must be
		 *    updated or created elsewhere, but we lock to the language either
		 *    way here.
		 * 3) there is no surveyresult, or no locked language, and no language
		 *    in the Session. Now either we have a submitted language; then lock
		 *    this language - otherwise:
		 * X) we do have a problem, since it's not
		 *    clear how a user would end up in this state.
		 */

		if($surveyresult === false || $surveyresult->getLanguage() === NULL) {

			// if there is no surveyresult yet, or the surveyresult does not
			// specify a language, we need to consult the session instead
			if(!SessionUtility::isLanguageIdSet($tool->getUid())) {
				// Case 3) if the session does not hold language data, we need
				// to determine the language from the request.

				/**
				 * @todo replace this with a generic solution
				 */
				if(!isset($args["language"])) {
					// X): no language in Surveyresult, Session or Request
					throw new PageNotFoundException(
						"XlncTools: no language lock requested for sheet #" . $sheet->getUid() . " and tool #" . $tool->getUid()
					);
				}

				$chosenLanguageUid = (int)$args["language"];
				$availableSysLanguages = $this->systemLanguageRepository->findAllAndDefault();

				foreach($availableSysLanguages as $availableSysLanguage) {
					if($chosenLanguageUid === $availableSysLanguage->getUid()) {
						$lockedLanguageUid = $availableSysLanguage->getUid();
					}
				}

				if( NULL === $lockedLanguageUid ) {
					// invalid language choice
					throw new PageNotFoundException(
						"XlncTools: invalid language choice " . $chosenLanguageUid  . " or sheet #" . $sheet->getUid() . " and tool #" . $tool->getUid()
					);
				}

				SessionUtility::storeLanguageId($tool->getUid(), $lockedLanguageUid);

			} else {
				// Case 2) the surveyresult does not have a lock, but the session does.
				$lockedLanguageUid = SessionUtility::getLanguageId($data['tool']->getUid());
			}

		} else {
			// Case 1)
			$lockedLanguageUid = $surveyresult->getLanguage();
		}

		// this is the "otherwise" case again - no lock could be established after
		// inspecting Surveyresult, Session and Request (in that order)
		// at this particular place, we're in case 1) but the surveyresult has no language (???)
		if(NULL === $lockedLanguageUid) {
			throw new PageNotFoundException(
				"XlncTools: could not determine language lock for sheet #" . $sheet->getUid() . " and tool #" . $tool->getUid()
			);
		}

		// this establishes the lock by forcefully redirecting
		if($lockedLanguageUid !== (int)GeneralUtility::_GET('L')) {
			$pObj->redirectToSurveyWithLanguage($lockedLanguageUid, $data['tool']->getUid(), $data['sheet']->getUid());
		}

		return $result;
	}

	/**
	 * Calling the toolStarted signal
	 *
	 * @param Xlnc\XlncTools\Controller\ToolController $pObj
	 * @param array $data The data, containing: Xlnc\XlncTools\Domain\Model\Sheet sheet, Xlnc\XlncTools\Domain\Model\Tool tool
	 * @return array
	 */
	public function toolStarted($pObj, $data) {

		$result = array();

		// Create Surveyresult item
		$objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
		$surveyresultRepository = $objectManager->get('Xlnc\XlncTools\Domain\Repository\SurveyresultRepository');
		$surveyresult = $surveyresultRepository->setIgnoreStoragePage()->findByToolAndSheet($data['tool']->getUid(), $data['sheet']->getUid());
		if($surveyresult === false) {
			$surveyresult = $objectManager->get('Xlnc\XlncTools\Domain\Model\Surveyresult');
			$surveyresult->setCustomer($data['sheet']->getTeam()->getProject()->getCustomer()->getUid());
			$surveyresult->setProject($data['sheet']->getTeam()->getProject()->getUid());
			$surveyresult->setTeam($data['sheet']->getTeam()->getUid());
			$surveyresult->setTool($data['tool']->getUid());
			$surveyresult->setSheet($data['sheet']->getUid());
			$surveyresult->setLanguage((int) SessionUtility::getLanguageId($data['tool']->getUid()));
			$surveyresultRepository->add($surveyresult);
		} else {
			$surveyresult->setLanguage((int) SessionUtility::getLanguageId($data['tool']->getUid()));
			$surveyresult->setStatus(Surveyresult::STATUS_STARTED);
			$surveyresultRepository->update($surveyresult);
		}
		$surveyresultRepository->persistAll();

		$result[] = $surveyresult->getUid();
		$result[] = $data;

		return $result;
	}

	/**
	 * Calling the toolFinished signal
	 *
	 * @param Xlnc\XlncTools\Controller\ToolController $pObj
	 * @param array $data The data, containing: Xlnc\XlncTools\Domain\Model\Sheet sheet, Xlnc\XlncTools\Domain\Model\Tool tool
	 * @return array
	 */
	public function toolFinished($pObj, $data) {

		$result = array();

		// Finish Surveyresult item
		$objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
		$surveyresultRepository = $objectManager->get('Xlnc\XlncTools\Domain\Repository\SurveyresultRepository');
		$surveyresult = $surveyresultRepository->setIgnoreStoragePage()->findByToolAndSheet($data['tool']->getUid(), $data['sheet']->getUid());
		$surveyresult->setStatus(Surveyresult::STATUS_FINISHED);
		$surveyresultRepository->update($surveyresult);
		$surveyresultRepository->persistAll();

		$result[] = $surveyresult->getUid();
		$result[] = $data;

		return $result;
	}

}

?>
