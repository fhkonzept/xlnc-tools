<?php
namespace Xlnc\XlncTools\Dto;

class TeamAndTool {

    public function __construct($team, $tool) {
        $this->team = $team;
        $this->tool = $tool;
    }

    /**
	 * @var \Xlnc\XlncTools\Domain\Model\Team
	 */
	protected $team;

	/**
	 * @return \Xlnc\XlncTools\Domain\Model\Team
	 */
	public function getTeam()
	{
	    return $this->team;
	}

	/**
	 * @param \Xlnc\XlncTools\Domain\Model\Team $team Value to set
	 */
	public function setTeam($team)
	{
	    $this->team = $team;
	}

	/**
	 * @var \Xlnc\XlncTools\Domain\Model\Tool
	 */
	protected $tool;

	/**
	 * @return \Xlnc\XlncTools\Domain\Model\Tool
	 */
	public function getTool()
	{
	    return $this->tool;
	}

	/**
	 * @param \Xlnc\XlncTools\Domain\Model\Tool $tool Value to set
	 */
	public function setTool($tool)
	{
	    $this->tool = $tool;
	}
}
