<?php
namespace Xlnc\XlncTools\ViewHelpers;

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * renders the "start survey in {language}" label
 */
class StartSurveyLabelViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param string $key the locallang key prefix to use
	 * @param \Xlnc\XlncTools\Domain\Model\SystemLanguage the language that the label should point to
	 * @return string
	 */
	public function render($language, $key = "static.start_survey.") {
        return LocalizationUtility::translate($key . $language->getIsoCode(), "xlnc_tools");
	}

}
