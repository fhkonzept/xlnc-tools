<?php
namespace Xlnc\XlncTools\ViewHelpers;

use \TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class ProgressViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param integer $currentPage
	 * @param integer $totalPages
	 * @return string
	 * Fluid example:
	 * <code>
	 *	<xlnc:progress totalPages="{totalPages}" currentPage="{currentPage}" />
	 * </code>
	 */
	public function render($currentPage = 0, $totalPages = 0) {

		$content = '';

		$itemWidth = 100/($totalPages+2);

		$content .= '<div class="clearfix"></div>';

		$content .= '<div class="xlnc-Tools-survey-progress-item xlnc-Tools-survey-progress-indicator-item" style="width: ' . $itemWidth . '%">';
		$content .= '</div>';

		for($i=1;$i<=$totalPages;$i++) {
			$content .= '<div class="xlnc-Tools-survey-progress-item xlnc-Tools-survey-progress-indicator-item" style="width: ' . $itemWidth . '%">';
			if($i == $currentPage) {
				$content .= '<div class="xlnc-Tools-survey-progress-currentPage-indicator">
					<span class="glyphicon glyphicon-triangle-bottom"></span>
					</div>';
			} else {
				$content .= '<span></span>';
			}
			$content .= '</div>';
		}

		$content .= '<div class="xlnc-Tools-survey-progress-item xlnc-Tools-survey-progress-indicator-item" style="width: ' . $itemWidth . '%">';
		$content .= '</div>';

		$content .= '<div class="clearfix"></div>';

		$content .= '<div class="xlnc-Tools-survey-progress-item" style="width: ' . $itemWidth . '%">';
		$content .= '<span class="hidden-xs">';
		$content .= LocalizationUtility::translate('static.survey.start', 'xlnc_tools');
		$content .= '</span>';
		$content .= '</div>';

		for($i=1;$i<=$totalPages;$i++) {
			$content .= '<div class="xlnc-Tools-survey-progress-item" style="width: ' . $itemWidth . '%">';
			if($i == 1 || $i == $totalPages || $i == $currentPage) {
				$content .= $i;
			} else {
				$content .= '<span class="pipe"></span>';
			}
			$content .= '</div>';
		}

		$content .= '<div class="xlnc-Tools-survey-progress-item" style="width: ' . $itemWidth . '%">';
		$content .= '<span class="hidden-xs">';
		$content .= LocalizationUtility::translate('static.survey.finish', 'xlnc_tools');
		$content .= '</span>';
		$content .= '</div>';

		$content .= '<div class="clearfix"></div>';

		return $content;

	}

}
?>
