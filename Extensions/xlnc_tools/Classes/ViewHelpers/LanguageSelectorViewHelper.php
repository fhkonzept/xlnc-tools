<?php
namespace Xlnc\XlncTools\ViewHelpers;

use TYPO3\CMS\Core\Imaging\Icon;

class LanguageSelectorViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

    const TEMPLATE = <<<'TEMPLATE'
    <div class="language-selector text-center">
        <div class="dropdown">
            <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                %s %s
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                    %s
                </li>
            </ul>
        </div>
    </div>
TEMPLATE;

    const ITEM_TEMPLATE = <<<'ITEM_TEMPLATE'
        <a class="" href="%s">%s %s</a>
ITEM_TEMPLATE;

    /**
     * @var \Xlnc\XlncTools\Domain\Repository\SystemLanguageRepository
     * @inject
     */
    protected $languageRepository;

    /**
     * @var TYPO3\CMS\Core\Imaging\IconFactory
     * @inject
     */
    protected $iconFactory;

	/**
	 * @return string
	 * Fluid example:
	 */
	public function render() {

        $languages = $this->languageRepository->findAllAndDefault();

        #$uriBuilder = $this->getControllerContext();

        $uriBuilder = $this->controllerContext->getUriBuilder();
        $request = $this->controllerContext->getRequest();
        $currentLanguage = $this->languageRepository->getCurrentLanguage();

        $iconFactory = $this->iconFactory;

        $items = array_map(function ($language) use ($uriBuilder, $currentLanguage, $iconFactory, $request) {
            return sprintf(self::ITEM_TEMPLATE,
                #$language->getUid() === $currentLanguage->getUid() ? 'btn-primary' : 'btn-default',
                $uriBuilder
                    ->reset()
                    ->setArguments(array_merge(['L' => $language->getUid()]))
                    ->uriFor($request->getControllerActionName(), $request->getArguments()),
                $iconFactory->getIcon('flags-' . $language->getFlag(), Icon::SIZE_SMALL)->render(),
                $language->getTitle()
            );
        }, $languages);

		return sprintf(
            self::TEMPLATE,
            $this->iconFactory->getIcon('flags-' . $currentLanguage->getFlag(), Icon::SIZE_SMALL)->render(),
            $currentLanguage->getTitle(),
            implode("", $items)
        );

	}

}
?>
