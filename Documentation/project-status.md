# Todo-Liste XLNC Tools

## Reihenfolge

### Step 1
#### erledigt

- [ ] Fragebögen: done.
- [ ] 4-stufiges Rating ist jetzt 5-stufig @Malte
- [ ] Einladung der Teilnehmer jetzt per Mail und Coupon @Malte
- [ ] Unterbrechung und Wiederaufnahme: done.
- [ ] Einleitungstext: done.
- [ ] Zuordnung Selbst- und Fremdeinschätzungen: done.
- [ ] spätere Einladung: done.
- [ ] 5 Fragen pro Seite: jetzt pro Tool (einmal 4, einmal 5) @Frank

#### Frontend
- [ ] responsive Layout für's Frontend @Frank
- [ ] Reihenfolge: Erst demografische Daten, dann Willkommenstext, dann Dashboard: klären @Malte @Conny
- [ ] Seiten Impressum, Kontakt, Nutzungsbedingungen, "etc." @Malte, @Conny
#### Frontend & Backend
- [ ] Freitextantworten: Texte fehlen? @Malte
- [ ] Reports: offen. @Frank @Malte
- [ ] Freitextantworten in den Report übernehmen @Frank
- [ ] Einleitungstext vor offenen Fragen: fehlt @Malte
#### Backend-Verwaltung
- [ ] Ergebnisberechnung: 50% @Malte
- [ ] Backend-Verwaltung: 50% @Malte
- [ ] Freitextantworten nicht als Antworten für den Report zählen @Malte @Frank
- [ ] Report über Backend senden: fehlt @Malte
- [ ] Einleitungstexte für E-Mails, für FK und MA getrennet, vorausgefülllt: fehlt. @Malte
#### Sonstiges
- [ ] generische XLNC-Mail-Adresse: fehlt @Malte
- [ ] SSL-Zertifikat fehlt, auch Step2 möglich (technisch) @Malte
- [ ] Layouts Mail und Coupon

### Step 2
- [ ] Doku zur Pflege von Tools und Auswertungen: fehlt @Malte
- [ ] Report-Emfängerliste: fehlt @Malte
- [ ] Report-Versand (Datum/Uhrzeit): fehlt @Malte
- [ ] automatisches Ende von "Teams": fehlt @Malte
- [ ] Reminder: fehlt @Malte
- [ ] Backend auf Deutsch: @Kathrin?
- [ ] Gesamtreport: offen, Step 2 @Malte
- [ ] Filterung für Gesamtreport: offen, Step 2 @Malte
- [ ] Möglichkeit zur Anpassung/Aktualisierung der hinterlegten Datenquellen zur
Ergebniserstellung <- Filterung? @Malte
- [ ]  Einrichtung der Verwaltungsbenutzerkonten
### Step 3
- [ ] Datenexport? @Frank
