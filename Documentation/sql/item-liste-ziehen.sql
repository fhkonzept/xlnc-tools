# Item-Liste für Kommunikation / Abgleich / Synchronisierung mit Niklas

SELECT
	i.uid AS ID,
	i.question AS Fragetext,
	i.alternative_question AS 'Fragetext (Führungskraft)',
	CASE i.sys_language_uid
	WHEN 1 THEN 'EN'
	WHEN 0 THEN 'DE'
	END as Sprache,
	CASE i.l10n_parent
	WHEN 0 THEN '-'
	ELSE i.l10n_parent
	END as 'Ursprungs-ID',
	CASE
	WHEN c.uid IS NULL THEN oc.title
	ELSE c.title
	END as Dimension,
	CASE
	WHEN t.uid IS NULL
	THEN ot.title
	ELSE t.title
	END AS Tool,
	i.hidden as ausgeblendet,
	i.deleted as 'gelöscht'


FROM
	tx_xlnctools_domain_model_item AS i
LEFT JOIN
	tx_xlnctools_domain_model_tool AS t ON i.parentid = t.uid
LEFT JOIN
	tx_xlnctools_domain_model_dictionary_item_criteria AS c ON c.uid = i.criteria
LEFT JOIN
	tx_xlnctools_domain_model_item AS o ON i.l10n_parent = o.uid
LEFT JOIN
	tx_xlnctools_domain_model_tool AS ot ON o.parentid = ot.uid
LEFT JOIN
	tx_xlnctools_domain_model_dictionary_item_criteria AS oc ON o.criteria = oc.uid
