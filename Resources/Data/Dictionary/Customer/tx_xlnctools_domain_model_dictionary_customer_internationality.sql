# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: xlnc-tools-dev
# Generation Time: 2015-12-02 12:34:07 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tx_xlnctools_domain_model_dictionary_customer_internationality
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tx_xlnctools_domain_model_dictionary_customer_internationality`;

CREATE TABLE `tx_xlnctools_domain_model_dictionary_customer_internationality` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `sys_language_uid` int(11) NOT NULL DEFAULT '0',
  `l10n_parent` int(11) NOT NULL DEFAULT '0',
  `l10n_diffsource` mediumblob,
  `sorting` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `parent` (`pid`),
  KEY `language` (`l10n_parent`,`sys_language_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `tx_xlnctools_domain_model_dictionary_customer_internationality` WRITE;
/*!40000 ALTER TABLE `tx_xlnctools_domain_model_dictionary_customer_internationality` DISABLE KEYS */;

INSERT INTO `tx_xlnctools_domain_model_dictionary_customer_internationality` (`uid`, `pid`, `title`, `tstamp`, `crdate`, `cruser_id`, `deleted`, `hidden`, `sys_language_uid`, `l10n_parent`, `l10n_diffsource`, `sorting`)
VALUES
	(1,25,'National',1446415648,1446415648,1,0,0,-1,0,X'613A343A7B733A353A227469746C65223B4E3B733A363A2268696464656E223B4E3B733A31363A227379735F6C616E67756167655F756964223B4E3B733A31313A226C31306E5F706172656E74223B4E3B7D',0),
	(2,25,'Europaweit',1446415658,1446415658,1,0,0,-1,0,X'613A343A7B733A353A227469746C65223B4E3B733A363A2268696464656E223B4E3B733A31363A227379735F6C616E67756167655F756964223B4E3B733A31313A226C31306E5F706172656E74223B4E3B7D',0),
	(3,25,'Weltweit',1446415668,1446415668,1,0,0,-1,0,X'613A343A7B733A353A227469746C65223B4E3B733A363A2268696464656E223B4E3B733A31363A227379735F6C616E67756167655F756964223B4E3B733A31313A226C31306E5F706172656E74223B4E3B7D',0);

/*!40000 ALTER TABLE `tx_xlnctools_domain_model_dictionary_customer_internationality` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
