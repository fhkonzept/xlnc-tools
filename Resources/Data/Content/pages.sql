# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: xlnc-tools-dev
# Generation Time: 2015-12-07 12:36:33 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `t3ver_oid` int(11) NOT NULL DEFAULT '0',
  `t3ver_id` int(11) NOT NULL DEFAULT '0',
  `t3ver_wsid` int(11) NOT NULL DEFAULT '0',
  `t3ver_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `t3ver_state` tinyint(4) NOT NULL DEFAULT '0',
  `t3ver_stage` int(11) NOT NULL DEFAULT '0',
  `t3ver_count` int(11) NOT NULL DEFAULT '0',
  `t3ver_tstamp` int(11) NOT NULL DEFAULT '0',
  `t3ver_move_id` int(11) NOT NULL DEFAULT '0',
  `t3_origuid` int(11) NOT NULL DEFAULT '0',
  `tstamp` int(11) unsigned NOT NULL DEFAULT '0',
  `sorting` int(11) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `perms_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `perms_groupid` int(11) unsigned NOT NULL DEFAULT '0',
  `perms_user` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `perms_group` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `perms_everybody` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `editlock` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `crdate` int(11) unsigned NOT NULL DEFAULT '0',
  `cruser_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hidden` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `doktype` int(11) unsigned NOT NULL DEFAULT '0',
  `TSconfig` text COLLATE utf8_unicode_ci,
  `is_siteroot` tinyint(4) NOT NULL DEFAULT '0',
  `php_tree_stop` tinyint(4) NOT NULL DEFAULT '0',
  `tx_impexp_origuid` int(11) NOT NULL DEFAULT '0',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `starttime` int(11) unsigned NOT NULL DEFAULT '0',
  `endtime` int(11) unsigned NOT NULL DEFAULT '0',
  `urltype` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `shortcut` int(10) unsigned NOT NULL DEFAULT '0',
  `shortcut_mode` int(10) unsigned NOT NULL DEFAULT '0',
  `no_cache` int(10) unsigned NOT NULL DEFAULT '0',
  `fe_group` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `subtitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `layout` int(11) unsigned NOT NULL DEFAULT '0',
  `url_scheme` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `target` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `media` int(11) unsigned NOT NULL DEFAULT '0',
  `lastUpdated` int(10) unsigned NOT NULL DEFAULT '0',
  `keywords` text COLLATE utf8_unicode_ci,
  `cache_timeout` int(10) unsigned NOT NULL DEFAULT '0',
  `cache_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `newUntil` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `no_search` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `SYS_LASTCHANGED` int(10) unsigned NOT NULL DEFAULT '0',
  `abstract` text COLLATE utf8_unicode_ci,
  `module` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `extendToSubpages` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `author_email` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nav_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nav_hide` tinyint(4) NOT NULL DEFAULT '0',
  `content_from_pid` int(10) unsigned NOT NULL DEFAULT '0',
  `mount_pid` int(10) unsigned NOT NULL DEFAULT '0',
  `mount_pid_ol` tinyint(4) NOT NULL DEFAULT '0',
  `alias` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `l18n_cfg` tinyint(4) NOT NULL DEFAULT '0',
  `fe_login_mode` tinyint(4) NOT NULL DEFAULT '0',
  `backend_layout` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `backend_layout_next_level` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tsconfig_includes` text COLLATE utf8_unicode_ci,
  `categories` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  KEY `t3ver_oid` (`t3ver_oid`,`t3ver_wsid`),
  KEY `parent` (`pid`,`deleted`,`sorting`),
  KEY `alias` (`alias`),
  KEY `determineSiteRoot` (`is_siteroot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`uid`, `pid`, `t3ver_oid`, `t3ver_id`, `t3ver_wsid`, `t3ver_label`, `t3ver_state`, `t3ver_stage`, `t3ver_count`, `t3ver_tstamp`, `t3ver_move_id`, `t3_origuid`, `tstamp`, `sorting`, `deleted`, `perms_userid`, `perms_groupid`, `perms_user`, `perms_group`, `perms_everybody`, `editlock`, `crdate`, `cruser_id`, `hidden`, `title`, `doktype`, `TSconfig`, `is_siteroot`, `php_tree_stop`, `tx_impexp_origuid`, `url`, `starttime`, `endtime`, `urltype`, `shortcut`, `shortcut_mode`, `no_cache`, `fe_group`, `subtitle`, `layout`, `url_scheme`, `target`, `media`, `lastUpdated`, `keywords`, `cache_timeout`, `cache_tags`, `newUntil`, `description`, `no_search`, `SYS_LASTCHANGED`, `abstract`, `module`, `extendToSubpages`, `author`, `author_email`, `nav_title`, `nav_hide`, `content_from_pid`, `mount_pid`, `mount_pid_ol`, `alias`, `l18n_cfg`, `fe_login_mode`, `backend_layout`, `backend_layout_next_level`, `tsconfig_includes`, `categories`)
VALUES
	(1,0,0,0,0,'',0,0,0,0,0,0,1449063533,256,0,1,0,31,27,0,0,1444051487,1,0,'xlnc-leadership.com',4,'TCEFORM.tx_xlnctools_domain_model_team.sheets.PAGE_TSCONFIG_ID = 18',1,0,0,'',0,0,1,41,0,1,'','',0,0,'',0,0,'',0,'',0,'',0,1448568110,'','',0,'','','',0,0,0,0,'',0,0,'1','1','EXT:xlnc_de/Configuration/PageTSconfig/PageLayouts.txt',0),
	(2,1,0,0,0,'',0,0,0,0,0,0,1449143736,1000000000,1,1,0,31,27,0,0,1444059014,1,0,'Hauptnavigation',254,'',0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,'',0,'',0,'',0,0,'','',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(3,1,0,0,0,'',0,0,0,0,0,0,1449143748,1000000000,1,1,0,31,27,0,0,1444059044,1,0,'Footer',254,'',0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,'',0,'',0,'',0,0,'','',0,'','','',0,0,0,0,'',0,0,'','1','',0),
	(6,1,0,0,0,'',0,0,0,0,0,0,1449143725,232,0,1,0,31,27,0,0,1444059126,1,0,'XLNC_c',1,NULL,0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,NULL,0,'',0,NULL,0,1449074359,NULL,'',0,'','','XLNC_<em>c</em>',0,0,0,0,'',0,0,'','',NULL,0),
	(7,1,0,0,0,'',0,0,0,0,0,0,1449143728,250,0,1,0,31,27,0,0,1444059126,1,0,'Über XLNC',1,NULL,0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,1449143728,NULL,'',0,'','','',0,0,0,0,'',0,0,'','',NULL,0),
	(13,1,0,0,0,'',0,0,0,0,0,0,1445458613,768,0,1,0,31,27,0,0,1445458613,1,0,'XLNC Data',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(14,1,0,0,0,'',0,0,0,0,0,0,1449143722,160,0,1,0,31,27,0,0,1444059126,1,0,'Impressum',1,NULL,0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,1449143722,NULL,'',0,'','','',0,0,0,0,'',0,0,'','',NULL,0),
	(15,1,0,0,0,'',0,0,0,0,0,0,1449143724,208,0,1,0,31,27,0,0,1444059126,1,0,'Kontakt',1,NULL,0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,NULL,0,'',0,NULL,0,1449143724,NULL,'',0,'','','',0,0,0,0,'',0,0,'','',NULL,0),
	(16,13,0,0,0,'',0,0,0,0,0,0,1445458684,256,0,1,0,31,27,0,0,1445458678,1,0,'Tools',254,'',0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,'',0,'',0,'',0,0,'','',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(17,13,0,0,0,'',0,0,0,0,0,0,1445500612,128,0,1,0,31,27,0,0,1445459901,1,0,'Kunden/Projekte',254,'',0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,'',0,'',0,'',0,0,'','',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(18,13,0,0,0,'',0,0,0,0,0,0,1445500492,512,0,1,0,31,27,0,0,1445491965,1,0,'Sheets',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(19,1,0,0,0,'',0,0,0,0,0,0,1449143838,64,0,1,0,31,27,0,0,1445577258,1,0,'Tool',1,'',0,0,0,'',0,0,1,0,0,0,'','',1,0,'',0,0,'',0,'',0,'',0,1449143838,'','',0,'','','',1,0,0,0,'',0,0,'','','',0),
	(20,13,0,0,0,'',0,0,0,0,0,0,1446414095,768,0,1,0,31,27,0,0,1446414062,1,0,'Dictionaries',254,'',0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,'',0,'',0,'',0,0,'','',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(21,20,0,0,0,'',0,0,0,0,0,0,1446414122,256,0,1,0,31,27,0,0,1446414106,1,0,'Customer',254,'',0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,'',0,'',0,'',0,0,'','',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(22,21,0,0,0,'',0,0,0,0,0,0,1446414147,256,0,1,0,31,27,0,0,1446414142,1,0,'Industries',254,'',0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,'',0,'',0,'',0,0,'','',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(23,21,0,0,0,'',0,0,0,0,0,0,1446415343,512,0,1,0,31,27,0,0,1446415343,1,0,'Number of employees',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(24,21,0,0,0,'',0,0,0,0,0,0,1446415501,768,0,1,0,31,27,0,0,1446415501,1,0,'Turnovers',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(25,21,0,0,0,'',0,0,0,0,0,0,1446415627,1024,0,1,0,31,27,0,0,1446415627,1,0,'Internationality',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(26,20,0,0,0,'',0,0,0,0,0,0,1446416217,512,0,1,0,31,27,0,0,1446416217,1,0,'Project',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(27,26,0,0,0,'',0,0,0,0,0,0,1446416250,256,0,1,0,31,27,0,0,1446416238,1,0,'References',254,'',0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,'',0,'',0,'',0,0,'','',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(28,26,0,0,0,'',0,0,0,0,0,0,1446416437,512,0,1,0,31,27,0,0,1446416437,1,0,'Profiles',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(29,20,0,0,0,'',0,0,0,0,0,0,1446560638,768,0,1,0,31,27,0,0,1446500803,1,0,'Sheet',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(30,29,0,0,0,'',0,0,0,0,0,0,1446500837,256,0,1,0,31,27,0,0,1446500826,1,0,'Ages',254,'',0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,'',0,'',0,'',0,0,'','',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(31,29,0,0,0,'',0,0,0,0,0,0,1446500944,512,0,1,0,31,27,0,0,1446500944,1,0,'Genders',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(32,29,0,0,0,'',0,0,0,0,0,0,1446501081,768,0,1,0,31,27,0,0,1446501001,1,0,'Educations',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(33,29,0,0,0,'',0,0,0,0,0,0,1446501076,1024,0,1,0,31,27,0,0,1446501076,1,0,'Graduations',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(34,29,0,0,0,'',0,0,0,0,0,0,1446501155,1280,0,1,0,31,27,0,0,1446501155,1,0,'Experiences',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(35,29,0,0,0,'',0,0,0,0,0,0,1446501252,1536,0,1,0,31,27,0,0,1446501252,1,0,'Management experiences',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(36,29,0,0,0,'',0,0,0,0,0,0,1446501326,1792,0,1,0,31,27,0,0,1446501326,1,0,'Management levels',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(37,29,0,0,0,'',0,0,0,0,0,0,1446501395,2048,0,1,0,31,27,0,0,1446501395,1,0,'Business units',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(38,20,0,0,0,'',0,0,0,0,0,0,1446501514,1024,0,1,0,31,27,0,0,1446501514,1,0,'Item',254,'',0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(39,38,0,0,0,'',0,0,0,0,0,0,1446501540,256,0,1,0,31,27,0,0,1446501526,1,0,'Criteria',254,'',0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,'',0,'',0,'',0,0,'','',0,'','','',0,0,0,0,'',0,0,'','','',0),
	(40,29,0,0,0,'',0,0,0,0,0,0,1448988816,2304,0,1,0,31,27,0,0,1448988811,1,0,'Countries',254,NULL,0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','',NULL,0),
	(41,1,0,0,0,'',0,0,0,0,0,0,1449063343,32,0,1,0,31,27,0,0,1449063336,1,0,'Login',1,NULL,0,0,0,'',0,0,1,0,0,0,'0','',0,0,'',0,0,NULL,0,'',0,NULL,0,1449063448,NULL,'',0,'','','',0,0,0,0,'',0,0,'','',NULL,0),
	(42,1,0,0,0,'',0,0,0,0,0,0,1449143914,16,0,1,0,31,27,0,0,1449063499,1,0,'Home',4,NULL,0,0,0,'',0,0,1,41,0,0,'','',0,0,'',0,0,NULL,0,'',0,NULL,0,0,NULL,'',0,'','','',0,0,0,0,'',0,0,'','',NULL,0),
	(43,13,0,0,0,'',0,0,0,0,0,0,1449131066,64,0,1,0,31,27,0,0,1449069791,1,0,'API Endpoint Placeholder',1,NULL,0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,NULL,0,'',0,NULL,0,1449131066,NULL,'',0,'','','',1,0,0,0,'',0,0,'','',NULL,0),
	(44,1,0,0,0,'',0,0,0,0,0,0,1449143727,244,0,1,0,31,27,0,0,1449072409,1,0,'XLNC_s',1,NULL,0,0,0,'',0,0,1,0,0,0,'','',0,0,'',0,0,NULL,0,'',0,NULL,0,1449073683,NULL,'',0,'','','XLNC_<em>s</em>',0,0,0,0,'',0,0,'','',NULL,0);

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
